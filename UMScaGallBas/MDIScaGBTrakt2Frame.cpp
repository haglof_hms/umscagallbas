// MDIScaGBTrakt2Frame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
//#include "UMScaGallBas.h"
#include "MDIScaGBTrakt2Frame.h"
#include "MDIScaGBTrakt2FormView.h"
#include "ResLangFileReader.h"
#include "ScaGBTraktSelectList.h"

// CMDIScaGBTrakt2Frame dialog

IMPLEMENT_DYNCREATE(CMDIScaGBTrakt2Frame, CMDIChildWnd)

CMDIScaGBTrakt2Frame::CMDIScaGBTrakt2Frame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CMDIScaGBTrakt2Frame::~CMDIScaGBTrakt2Frame()
{
}

BEGIN_MESSAGE_MAP(CMDIScaGBTrakt2Frame, CMDIChildWnd)
		ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


void CMDIScaGBTrakt2Frame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_TRAKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}


void CMDIScaGBTrakt2Frame::OnClose(void)
{

	CMDIScaGBTrakt2FormView *pView = (CMDIScaGBTrakt2FormView *)getFormViewByID(IDD_FORMVIEW6);
	if (pView)
	{
		if (pView->getIsDirty())
		{
			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
			{
				pView->saveTrakt();
			}
		}
	}
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	CMDIChildWnd::OnClose();
}

int CMDIScaGBTrakt2Frame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
//	if (!isDBConnection(m_sLangFN))
//		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}


	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sMsgCap = xml->str(IDS_STRING7624);
			m_sMsg1 = xml->str(IDS_STRING7629);
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDIScaGBTrakt2Frame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);
	nForenklad_global=1;

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_TRAKT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIScaGBTrakt2Frame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	/*AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,TRUE);*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}

BOOL CMDIScaGBTrakt2Frame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIScaGBTrakt2Frame diagnostics

#ifdef _DEBUG
void CMDIScaGBTrakt2Frame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIScaGBTrakt2Frame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIScaGBTrakt2Frame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCAGB_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCAGB_TRAKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIScaGBTrakt2Frame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIScaGBTrakt2Frame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIScaGBTrakt2Frame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		nForenklad_global=1;
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		showFormView(IDD_FORMVIEW3,m_sLangFN);
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
		//CScaGBTraktSelectList *pView = (CScaGBTraktSelectList *)getFormViewByID(IDD_FORMVIEW3);
		//pView->forenklad(1);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}