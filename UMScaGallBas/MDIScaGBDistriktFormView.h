#pragma once
#include "SQLAPI.h" // main SQLAPI++ header

#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"


// CMDIScaGBDistriktFormView form view

class CMDIScaGBDistriktFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIScaGBDistriktFormView)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CMDIScaGBDistriktFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIScaGBDistriktFormView();

	CMyExtEdit2 m_wndEdit1;
	CMyExtEdit2 m_wndEdit2;

	CMyExtStatic2 m_wndLbl1;
	CMyExtStatic2 m_wndLbl2;

	CXTResizeGroupBox m_wndGroup;

	vecDistriktData m_vecDistriktData;
	DISTRIKT_DATA m_structDistriktIdentifer;
	UINT m_nDBIndex;

	void setLanguage(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);
	void addDistrikt();
	void saveDistrikt();
	void deleteDistrikt();

	void getDistriktsFromDB();

	CString m_sSaveData;
	CString m_sErrCap;
	CString m_sNotOkToSave;
	CString m_sUpdateDistriktMsg;
	BOOL isDataChanged(void);
	BOOL isOKToSave(void);
	void getEnteredData(DISTRIKT_DATA&);
	void resetDistrikt(enumRESET reset);

	BOOL m_bIsDirty;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW4 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void doPopulate(UINT);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIScaGBDistriktFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
