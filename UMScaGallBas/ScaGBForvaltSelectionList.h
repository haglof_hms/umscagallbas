#pragma once

//#include "SQLAPI.h" // main SQLAPI++ header
#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"

// CScaGBForvaltSelectionList dialog

class CScaGBForvaltSelectionList : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CScaGBForvaltSelectionList)
	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CScaGBForvaltSelectionList();           // protected constructor used by dynamic creation
	virtual ~CScaGBForvaltSelectionList();

	CMyReportCtrl2 m_wndForvaltReport;
	BOOL setupReport(void);
	void populateReport(void);

	vecForvaltData m_vecForvaltData;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
public:
	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};
