// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500                 // Target Windows 2000

#define _WIN32_WINNT 0x0500


#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#define _CRT_SECURE_NO_WARNING
#define _CRT_SECURE_NO_DEPRECATE

#define _CRT_NON_CONFORMING_SWPRINTFS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include "IoStream"

using namespace std;

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

//#ifndef _AFX_NO_DAO_SUPPORT
//#include <afxdao.h>			// MFC DAO database classes
//#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <vector>
#include <XTToolkitPro.h>	// Xtreme Toolkit MFC extensions

#include "SQLAPI.h"
#include "DBBaseClass_ADODirect.h"
#include "pad_hms_miscfunc.h"

#include "UMScaGallBasDB.h"
#include <afxdhtml.h>

static BOOL bShowLicenseDialog=TRUE;


const LPCTSTR LICENSE_FILE_NAME						= _T("License.dll");
const LPCTSTR LICENSE_NAME								= _T("License");
const LPCTSTR UMSCAGALLBAS_LIC_ID					= _T("H9001");
const LPCTSTR LICENSE_CHECK							= _T("CheckLicense");

const LPCTSTR PROGRAM_NAME								= _T("UMScaGallBas");	


const LPCTSTR REG_WP_SCAGB_FORVALT_KEY				= _T("UMScaGallBas\\Forvalt\\Placement");
const LPCTSTR REG_WP_SCAGB_LIST_FORVALT_KEY		= _T("UMScaGallBas\\ListForvalt\\Placement");

const LPCTSTR REG_WP_SCAGB_DISTRIKT_KEY			= _T("UMScaGallBas\\Distrikt\\Placement");
const LPCTSTR REG_WP_SCAGB_LIST_DISTRIKT_KEY		= _T("UMScaGallBas\\ListDistrikt\\Placement");

const LPCTSTR REG_WP_SCAGB_TRAKT_KEY				= _T("UMScaGallBas\\Trakt\\Placement");
const LPCTSTR REG_WP_SCAGB_LIST_TRAKT_KEY			= _T("UMScaGallBas\\ListTrakt\\Placement");

const LPCTSTR REG_WP_SCAGB_TEST_KEY					= _T("UMScaGallBas\\Test\\Placement");



//const LPCTSTR REG_LANG_ITEM							   = _T("LANGSET");					// Item in registry, for abbrevated language; 051114 p�d
//const LPCTSTR REG_ROOT									= _T("Software\\HaglofManagmentSystem");
//const LPCTSTR REG_LANG_KEY								= _T("Language");
//const LPCTSTR SUBDIR_LANGUAGE							= _T("Language");
//const LPCTSTR SUBDIR_REPORTS							= _T("Reports");					// Reports directory

const LPCTSTR TBL_FORVALT								= _T("dbo.sca_forvalt_table");
const LPCTSTR TBL_DISTRIKT								= _T("dbo.sca_district_table");
const LPCTSTR TBL_TRAKT	    							= _T("dbo.sca_trakt_table");
const LPCTSTR TBL_PLOT	    							= _T("dbo.sca_plot_table");
const LPCTSTR TBL_TREE	    							= _T("dbo.sca_tree_table");
const LPCTSTR TBL_VIEW_ORIGINAL						= _T("dbo.sca_original");
const LPCTSTR TBL_VIEW_FORENKLAD						= _T("dbo.sca_forenklad");


const LPCTSTR SQL_CREATE_VIEW_ORIGINAL				= _T("create view dbo.sca_original as select * from sca_trakt_table where trakt_forenklad = 0");
const LPCTSTR SQL_CREATE_VIEW_FORENKLAD			= _T("create view dbo.sca_forenklad as select * from sca_trakt_table where trakt_forenklad = 1");
const LPCTSTR HXL_DLL_NAME								= _T("XMLScaReader.dll");

		

#define ID_UPDATE_ITEM									19999

#define WM_USER_MSG_SUITE								(WM_USER + 2)
#define MSG_IN_SUITE										(WM_USER + 10)

#define ID_NEW_ITEM										32786
#define ID_OPEN_ITEM										32787
#define ID_SAVE_ITEM										32788
#define ID_DELETE_ITEM									32789
#define ID_PREVIEW_ITEM									32790

// Defines for Database navigation button, in HMSShell toolbar; 060412 p�d
#define ID_DBNAVIG_START								32778
#define ID_DBNAVIG_NEXT									32779
#define ID_DBNAVIG_PREV									32780
#define ID_DBNAVIG_END									32781
#define ID_DBNAVIG_LIST									32791

//strings in xml-file
#define IDS_STRING7501									7501
#define IDS_STRING7502									7502
#define IDS_STRING7503									7503
#define IDS_STRING7504									7504

#define IDS_STRING752									752
#define IDS_STRING753									753
#define IDS_STRING756									756
#define IDS_STRING757									757

#define IDS_STRING7519									7519
#define IDS_STRING7520									7520
#define IDS_STRING7521									7521
#define IDS_STRING7522									7522
#define IDS_STRING7523									7523
#define IDS_STRING7524									7524
#define IDS_STRING7525									7525
#define IDS_STRING7526									7526
#define IDS_STRING7527									7527
#define IDS_STRING7528									7528
#define IDS_STRING7529									7529
#define IDS_STRING7530									7530
#define IDS_STRING7531									7531
#define IDS_STRING7532									7532
#define IDS_STRING7533									7533
#define IDS_STRING7534									7534
#define IDS_STRING7535									7535
#define IDS_STRING7536									7536
#define IDS_STRING7537									7537
#define IDS_STRING7538									7538
#define IDS_STRING7539									7539
#define IDS_STRING7540									7540
#define IDS_STRING7541									7541
#define IDS_STRING7542									7542

#define IDS_STRING7550									7550
#define IDS_STRING7551									7551
#define IDS_STRING7552									7552
#define IDS_STRING7553									7553
#define IDS_STRING7554									7554
#define IDS_STRING7555									7555
#define IDS_STRING7556									7556
#define IDS_STRING7557									7557
#define IDS_STRING7558									7558
#define IDS_STRING7559									7559
#define IDS_STRING7560									7560
#define IDS_STRING7561									7561
#define IDS_STRING7562									7562
#define IDS_STRING7563									7563
#define IDS_STRING7564									7564
#define IDS_STRING7565									7565
#define IDS_STRING7566									7566
#define IDS_STRING7567									7567
#define IDS_STRING7568									7568

#define IDS_STRING7570									7570
#define IDS_STRING7571									7571
#define IDS_STRING7572									7572
#define IDS_STRING7573									7573
#define IDS_STRING7574									7574
#define IDS_STRING7575									7575
#define IDS_STRING7576									7576

#define IDS_STRING7580									7580
#define IDS_STRING7581									7581
#define IDS_STRING7582									7582
#define IDS_STRING7583									7583
#define IDS_STRING7584									7584
#define IDS_STRING7585									7585
#define IDS_STRING7586									7586
#define IDS_STRING7587									7587

#define IDS_STRING7590									7590
#define IDS_STRING7591									7591
#define IDS_STRING7592									7592
#define IDS_STRING7593									7593
#define IDS_STRING7594									7594
#define IDS_STRING7595									7595
#define IDS_STRING7596									7596
#define IDS_STRING7597									7597
#define IDS_STRING7598									7598
#define IDS_STRING7599									7599

#define IDS_STRING7600									7600
#define IDS_STRING7601									7601
#define IDS_STRING7602									7602
#define IDS_STRING7603									7603
#define IDS_STRING7604									7604
#define IDS_STRING7605									7605
#define IDS_STRING7606									7606
#define IDS_STRING7607									7607
#define IDS_STRING7608									7608
#define IDS_STRING7609									7609

#define IDS_STRING7610									7610
#define IDS_STRING7611									7611
#define IDS_STRING7612									7612
#define IDS_STRING7613									7613
#define IDS_STRING7614									7614
#define IDS_STRING7615									7615

#define IDS_STRING7620									7620
#define IDS_STRING7621									7621
#define IDS_STRING7622									7622
#define IDS_STRING7623									7623
#define IDS_STRING7624									7624

#define IDS_STRING7626									7626
#define IDS_STRING7627									7627
#define IDS_STRING7628									7628
#define IDS_STRING7629									7629
#define IDS_STRING7630									7630
#define IDS_STRING7631									7631
#define IDS_STRING7632									7632
#define IDS_STRING7633									7633
#define IDS_STRING7634									7634
#define IDS_STRING7635									7635
#define IDS_STRING7636									7636
#define IDS_STRING7637									7637
#define IDS_STRING7638									7638
#define IDS_STRING7639									7639
#define IDS_STRING7640									7640
#define IDS_STRING7641									7641
#define IDS_STRING7642									7642
#define IDS_STRING7643									7643
#define IDS_STRING7644									7644
#define IDS_STRING7645									7645
#define IDS_STRING7646									7646
#define IDS_STRING7647									7647
#define IDS_STRING7648									7648
#define IDS_STRING7649									7649
#define IDS_STRING7650									7650
#define IDS_STRING7651									7651
#define IDS_STRING7652									7652

#define IDS_STRING7655									7655
#define IDS_STRING7656									7656
#define IDS_STRING7657									7657
#define IDS_STRING7658									7658
#define IDS_STRING7659									7659
#define IDS_STRING7660									7660
#define IDS_STRING7661									7661

#define IDS_STRING7662									7662
#define IDS_STRING7663									7663
#define IDS_STRING7664									7664

#define IDS_STRING7665	7665
#define IDS_STRING7666	7666
#define IDS_STRING7667	7667
#define IDS_STRING7668	7668
#define IDS_STRING7669	7669
#define IDS_STRING7670	7670
#define IDS_STRING7671	7671
#define IDS_STRING7672	7672
#define IDS_STRING7673	7673
#define IDS_STRING7674	7674
#define IDS_STRING7675	7675
#define IDS_STRING7676	7676
#define IDS_STRING7677	7677
#define IDS_STRING7678	7678
#define IDS_STRING7679	7679

#define IDS_STRING7680	7680
#define IDS_STRING7681	7681
#define IDS_STRING7682	7682
#define IDS_STRING7683	7683
#define IDS_STRING7684	7684
#define IDS_STRING7685	7685
#define IDS_STRING7686	7686

#define IDS_STRING7687	7687
#define IDS_STRING7688	7688
#define IDS_STRING7689	7689

#define IDS_STRING7690	7690
#define IDS_STRING7691	7691
#define IDS_STRING7692	7692

#define IDC_FORVALT_REPORT								0x8000
#define IDC_TABCONTROL_TRAKT							0x8001
#define IDC_TRAKT_REPORT								0x8002
#define IDC_TABCONTROL_1								0x8002
#define IDC_TRAKT_DATA_REPORT							0x8003
#define IDC_TRAKT_DATA_REPORT2						0x8004
#define IDC_TRAKT_DATA_REPORT3						0x8005
#define IDC_TRAKT_DATA_REPORT4						0x8005
#define IDC_DISTRIKT_REPORT							0x8006

const int MIN_X_SIZE_SCAGB_FORVALT					= 280;
const int MIN_Y_SIZE_SCAGB_FORVALT					= 190;
const int SIZE_SCAGB_FORVALT_COL						= 80;

const int MIN_X_SIZE_SCAGB_DISTRIKT					= 280;
const int MIN_Y_SIZE_SCAGB_DISTRIKT					= 190;
const int SIZE_SCAGB_DISTRIKT_COL					= 80;

const int MIN_X_SIZE_GALLBAS_LIST_FORVALT			= 400;
const int MIN_Y_SIZE_GALLBAS_LIST_FORVALT			= 280;

const int MIN_X_SIZE_GALLBAS_LIST_DISTRIKT		= 400;
const int MIN_Y_SIZE_GALLBAS_LIST_DISTRIKT		= 280;

const int MIN_X_SIZE_SCAGB_TRAKT						= 680;
const int MIN_Y_SIZE_SCAGB_TRAKT						= 560;

const int MIN_X_SIZE_SCAGB_LIST_TRAKT				= 400;
const int MIN_Y_SIZE_SCAGB_LIST_TRAKT				= 280;

const int COL_SIZE_TAB									= 100;

static BOOL ALLOW_EDIT_FORE =FALSE;
static BOOL ALLOW_EDIT_EFTER =FALSE;
static BOOL ALLOW_EDIT_KVARV =FALSE;
static BOOL ALLOW_EDIT_UTTIST =FALSE;
static BOOL ALLOW_EDIT_UTTMST =FALSE;
static BOOL ALLOW_EDIT_UTTTOT =FALSE;
static BOOL ALLOW_EDIT_FORENKLAD =FALSE;
static BOOL ALLOW_EDIT_FORENKLAD2 =FALSE;
static BOOL ALLOW_EDIT_SKAD =FALSE;
static BOOL ALLOW_EDIT_YTOR =FALSE;
static BOOL ALLOW_EDIT_TREE =FALSE;

typedef enum _action { MANUALLY,FROM_FILE,NOTHING,CHANGED } enumACTION;

typedef enum _traktpages { PAGE_KVARV,PAGE_UTTAGISK,PAGE_UTTAGISTV } enumTRAKTPAGES;

typedef enum _reset {	RESET_TO_FIRST_SET_NB,					// Set to first item and set Navigationbar
								RESET_TO_FIRST_NO_NB,					// Set to first item and no Navigationbar (disable all)
								RESET_TO_LAST_SET_NB,					// Set to last item and set Navigationbar
								RESET_TO_LAST_NO_NB,						// Set to last item and no Navigationbar (disable all)
								RESET_TO_JUST_ENTERED_SET_NB,			// Set to just entered data (from file or manually) and set Navigationbar
								RESET_TO_JUST_ENTERED_NO_NB			// Set to just entered data (from file or manually) and no Navigationbar
								} enumRESET;

// Question 1 selects ALL "trakts"
const LPCTSTR SQL_TRAKT_QUESTION1			= _T("select * from %s order by trakt_date,trakt_forvalt,trakt_driv_enh,trakt_avv_id,trakt_inv_typ,trakt_forenklad");

const LPCTSTR SQL_TRAKT_QUESTION_INDEX_ONLY	= _T("select trakt_forvalt,trakt_driv_enh,trakt_avv_id,trakt_inv_typ,trakt_forenklad from %s order by trakt_date,trakt_forvalt,trakt_driv_enh,trakt_avv_id,trakt_inv_typ,trakt_forenklad");

const LPCTSTR SQL_TRAKT_QUESTION_INDEX_AND_FORENKLAD = _T("select trakt_forvalt,trakt_driv_enh,trakt_avv_id,trakt_inv_typ,trakt_forenklad from %s order by trakt_date,trakt_forvalt,trakt_driv_enh,trakt_avv_id,trakt_inv_typ,trakt_forenklad");

const LPCTSTR SQL_PLOT_QUESTION1			= _T("select * from %s order by plot_trakt_forvalt,plot_trakt_avv_id,plot_trakt_inv_typ,plot_id");

const LPCTSTR SQL_TREE_QUESTION1			= _T("select * from %s order by Tree_Trakt_forvalt,Tree_Trakt_avv_id,Tree_Trakt_inv_typ,Tree_PlotId,Tree_TreeId");

#define KVARV_VARINDEX_M3SK_MAX        4000
#define KVARV_VARINDEX_M3FUB_MAX       4000
#define KVARV_VARINDEX_M3STAMSK_MAX    4000
#define KVARV_VARINDEX_M3STAMFUB_MAX	4000
#define KVARV_VARINDEX_GY_MAX          4000
#define KVARV_VARINDEX_ANTAL_MAX       4000
#define KVARV_VARINDEX_DG_MAX          200
#define KVARV_VARINDEX_HGV_MAX         100
#define KVARV_VARINDEX_H25_MAX         100
#define KVARV_VARINDEX_DGV_MAX         200

#define FORE_VARINDEX_M3SK_MAX         4000
#define FORE_VARINDEX_M3FUB_MAX        4000
#define FORE_VARINDEX_M3STAMSK_MAX     4000
#define FORE_VARINDEX_M3STAMFUB_MAX    4000
#define FORE_VARINDEX_GY_MAX           4000
#define FORE_VARINDEX_ANTAL_MAX        4000
#define FORE_VARINDEX_DG_MAX           200
#define FORE_VARINDEX_HGV_MAX          100
#define FORE_VARINDEX_H25_MAX          100
#define FORE_VARINDEX_DGV_MAX          200

#define UTTAGVARINDEX_M3SK_MAX         4000
#define UTTAGVARINDEX_M3FUB_MAX        4000
#define UTTAGVARINDEX_M3STAMSK_MAX     4000
#define UTTAGVARINDEX_M3STAMFUB_MAX    4000
#define UTTAGVARINDEX_GY_MAX           4000
#define UTTAGVARINDEX_GYANDEL_MAX      100
#define UTTAGVARINDEX_ANTAL_MAX        4000
#define UTTAGVARINDEX_ANTALANDEL_MAX   100
#define UTTAGVARINDEX_DG_MAX           100
#define UTTAGVARINDEX_DGV_MAX          200

#define DISTRIKT_MAX_LEN					2
#define FORVALT_MAX_LEN						3
#define DRIV_ENH_MAX_LEN					5
//#define DELID_MAX_LEN						1
#define AVLAGG_MAX_LEN						5
#define MASKINLAG_MAX_LEN					2
#define URSPRUNG_MAX_LEN					1
#define PRODLED_MAX_LEN						2
#define DATUM_MAX_LEN						8
#define HUGGNFORM_MAX_LEN					2
#define TERRF_MAX_LEN						1



/* Create table for F�rvaltning */
const LPCTSTR SQL_CREATE_FORVALT_TABLE	= _T("CREATE TABLE dbo.sca_forvalt_table (")
													  _T("forvalt_num int NOT NULL PRIMARY KEY,")
													  _T("forvalt_name nchar(50) NOT NULL default '',")
													  _T("create_date datetime NOT NULL default CURRENT_TIMESTAMP);");

/* Create table for Distrikt */
const LPCTSTR SQL_CREATE_DISTRIKT_TABLE	= _T("CREATE TABLE dbo.sca_district_table (")
													  _T("distrikt_num int NOT NULL PRIMARY KEY,")
													  _T("distrikt_name nchar(50) NOT NULL default '',")
													  _T("create_date datetime NOT NULL default CURRENT_TIMESTAMP);");

/* Create table for trakt */
const LPCTSTR SQL_CREATE_TRAKT_TABLE	= _T("CREATE TABLE dbo.sca_trakt_table (")
												  _T("trakt_forvalt 		int NOT NULL,")
												  _T("trakt_avv_id 			varchar(12) NOT NULL,")
												  _T("trakt_inv_typ 		int NOT NULL,")
												  _T("trakt_forenklad		int NOT NULL,")
												  _T("trakt_driv_enh 		int NOT NULL,")
												  _T("trakt_avlagg			int,")
												  _T("trakt_distrikt		int,")
												  _T("trakt_maskinlag		int,")
												  _T("trakt_ursprung		int,")
												  _T("trakt_prod_led		int,")
												  _T("trakt_namn			nchar(50),")
												  _T("trakt_date			nchar(50),")
												  _T("trakt_areal			float NOT NULL,")
												  _T("trakt_ant_ytor		float,")
												  _T("trakt_medel_ovre_hojd float,")
												  _T("trakt_si_sp			int,")
												  _T("trakt_si_tot			float,")
												  _T("trakt_alder			float,")
												  _T("trakt_progver		nchar(50),")
												  _T("trakt_gy_dir_fore	float,")
												  _T("trakt_gy_dir_efter	float,")
												  _T("trakt_enter_type	int,")
												  _T("trakt_notes			ntext,")
  _T("trakt_fore_tall_m3sk float,  trakt_fore_tall_m3fub float,  trakt_fore_tall_mstamsk float,  trakt_fore_tall_mstamfub float,  trakt_fore_tall_gy float,  trakt_fore_tall_antal float,  trakt_fore_tall_dg float,  trakt_fore_tall_hgv float,  trakt_fore_tall_h25 float,  trakt_fore_tall_dgv float,")
  _T("trakt_fore_gran_m3sk float,  trakt_fore_gran_m3fub float,  trakt_fore_gran_mstamsk float,  trakt_fore_gran_mstamfub float,  trakt_fore_gran_gy float,  trakt_fore_gran_antal float,  trakt_fore_gran_dg float,  trakt_fore_gran_hgv float,  trakt_fore_gran_h25 float,  trakt_fore_gran_dgv float,")
  _T("trakt_fore_bjork_m3sk float,  trakt_fore_bjork_m3fub float,  trakt_fore_bjork_mstamsk float,  trakt_fore_bjork_mstamfub float,  trakt_fore_bjork_gy float,  trakt_fore_bjork_antal float,  trakt_fore_bjork_dg float,  trakt_fore_bjork_hgv float,  trakt_fore_bjork_h25 float,  trakt_fore_bjork_dgv float,")
  _T("trakt_fore_OLov_m3sk float,  trakt_fore_OLov_m3fub float,  trakt_fore_OLov_mstamsk float,  trakt_fore_OLov_mstamfub float,  trakt_fore_OLov_gy float,  trakt_fore_OLov_antal float,  trakt_fore_OLov_dg float,  trakt_fore_OLov_hgv float,  trakt_fore_OLov_h25 float,  trakt_fore_OLov_dgv float,")
  _T("trakt_fore_OBarr_m3sk float,  trakt_fore_OBarr_m3fub float,  trakt_fore_OBarr_mstamsk float,  trakt_fore_OBarr_mstamfub float,  trakt_fore_OBarr_gy float,  trakt_fore_OBarr_antal float,  trakt_fore_OBarr_dg float,  trakt_fore_OBarr_hgv float,  trakt_fore_OBarr_h25 float,  trakt_fore_OBarr_dgv float,")
  _T("trakt_fore_Conto_m3sk float,  trakt_fore_Conto_m3fub float,  trakt_fore_Conto_mstamsk float,  trakt_fore_Conto_mstamfub float,  trakt_fore_Conto_gy float,  trakt_fore_Conto_antal float,  trakt_fore_Conto_dg float,  trakt_fore_Conto_hgv float,  trakt_fore_Conto_h25 float,  trakt_fore_Conto_dgv float,")
  _T("trakt_fore_Torra_m3sk float,  trakt_fore_Torra_m3fub float,  trakt_fore_Torra_mstamsk float,  trakt_fore_Torra_mstamfub float,  trakt_fore_Torra_gy float,  trakt_fore_Torra_antal float,  trakt_fore_Torra_dg float,  trakt_fore_Torra_hgv float,  trakt_fore_Torra_h25 float,  trakt_fore_Torra_dgv float,")
  _T("trakt_fore_Totalt_m3sk float,  trakt_fore_Totalt_m3fub float,  trakt_fore_Totalt_mstamsk float,  trakt_fore_Totalt_mstamfub float,  trakt_fore_Totalt_gy float,  trakt_fore_Totalt_antal float,  trakt_fore_Totalt_dg float,  trakt_fore_Totalt_hgv float,  trakt_fore_Totalt_h25 float,  trakt_fore_Totalt_dgv float,")
  _T("trakt_kvarv_tall_m3sk float,  trakt_kvarv_tall_m3fub float,  trakt_kvarv_tall_mstamsk float,  trakt_kvarv_tall_mstamfub float,  trakt_kvarv_tall_gy float,  trakt_kvarv_tall_antal float,  trakt_kvarv_tall_dg float,  trakt_kvarv_tall_hgv float,  trakt_kvarv_tall_h25 float,  trakt_kvarv_tall_dgv float,")
  _T("trakt_kvarv_gran_m3sk float,  trakt_kvarv_gran_m3fub float,  trakt_kvarv_gran_mstamsk float,  trakt_kvarv_gran_mstamfub float,  trakt_kvarv_gran_gy float,  trakt_kvarv_gran_antal float,  trakt_kvarv_gran_dg float,  trakt_kvarv_gran_hgv float,  trakt_kvarv_gran_h25 float,  trakt_kvarv_gran_dgv float,")
  _T("trakt_kvarv_bjork_m3sk float,  trakt_kvarv_bjork_m3fub float,  trakt_kvarv_bjork_mstamsk float,  trakt_kvarv_bjork_mstamfub float,  trakt_kvarv_bjork_gy float,  trakt_kvarv_bjork_antal float,  trakt_kvarv_bjork_dg float,  trakt_kvarv_bjork_hgv float,  trakt_kvarv_bjork_h25 float,  trakt_kvarv_bjork_dgv float,")
  _T("trakt_kvarv_OLov_m3sk float,  trakt_kvarv_OLov_m3fub float,  trakt_kvarv_OLov_mstamsk float,  trakt_kvarv_OLov_mstamfub float,  trakt_kvarv_OLov_gy float,  trakt_kvarv_OLov_antal float,  trakt_kvarv_OLov_dg float,  trakt_kvarv_OLov_hgv float,  trakt_kvarv_OLov_h25 float,  trakt_kvarv_OLov_dgv float,")
  _T("trakt_kvarv_OBarr_m3sk float,  trakt_kvarv_OBarr_m3fub float,  trakt_kvarv_OBarr_mstamsk float,  trakt_kvarv_OBarr_mstamfub float,  trakt_kvarv_OBarr_gy float,  trakt_kvarv_OBarr_antal float,  trakt_kvarv_OBarr_dg float,  trakt_kvarv_OBarr_hgv float,  trakt_kvarv_OBarr_h25 float,  trakt_kvarv_OBarr_dgv float,")
  _T("trakt_kvarv_Conto_m3sk float,  trakt_kvarv_Conto_m3fub float,  trakt_kvarv_Conto_mstamsk float,  trakt_kvarv_Conto_mstamfub float,  trakt_kvarv_Conto_gy float,  trakt_kvarv_Conto_antal float,  trakt_kvarv_Conto_dg float,  trakt_kvarv_Conto_hgv float,  trakt_kvarv_Conto_h25 float,  trakt_kvarv_Conto_dgv float,")
  _T("trakt_kvarv_Torra_m3sk float,  trakt_kvarv_Torra_m3fub float,  trakt_kvarv_Torra_mstamsk float,  trakt_kvarv_Torra_mstamfub float,  trakt_kvarv_Torra_gy float,  trakt_kvarv_Torra_antal float,  trakt_kvarv_Torra_dg float,  trakt_kvarv_Torra_hgv float,  trakt_kvarv_Torra_h25 float,  trakt_kvarv_Torra_dgv float,")
  _T("trakt_kvarv_Totalt_m3sk float,  trakt_kvarv_Totalt_m3fub float,  trakt_kvarv_Totalt_mstamsk float,  trakt_kvarv_Totalt_mstamfub float,  trakt_kvarv_Totalt_gy float,  trakt_kvarv_Totalt_antal float,  trakt_kvarv_Totalt_dg float,  trakt_kvarv_Totalt_hgv float,  trakt_kvarv_Totalt_h25 float,  trakt_kvarv_Totalt_dgv float,")
  _T("trakt_uttot_tall_m3sk float,  trakt_uttot_tall_m3fub float,  trakt_uttot_tall_mstamsk float,  trakt_uttot_tall_mstamfub float,  trakt_uttot_tall_gy float,  trakt_uttot_tall_gy_andel float,  trakt_uttot_tall_antal  float,  trakt_uttot_tall_antal_andel float,  trakt_uttot_tall_dg float,  trakt_uttot_tall_dgv float,")
  _T("trakt_uttot_gran_m3sk float,  trakt_uttot_gran_m3fub float,  trakt_uttot_gran_mstamsk float,  trakt_uttot_gran_mstamfub float,  trakt_uttot_gran_gy float,  trakt_uttot_gran_gy_andel float,  trakt_uttot_gran_antal  float,  trakt_uttot_gran_antal_andel float,  trakt_uttot_gran_dg float,  trakt_uttot_gran_dgv float,")
  _T("trakt_uttot_bjork_m3sk float,  trakt_uttot_bjork_m3fub float,  trakt_uttot_bjork_mstamsk float,  trakt_uttot_bjork_mstamfub float,  trakt_uttot_bjork_gy float,  trakt_uttot_bjork_gy_andel float,  trakt_uttot_bjork_antal  float,  trakt_uttot_bjork_antal_andel float,  trakt_uttot_bjork_dg float,  trakt_uttot_bjork_dgv float,")
  _T("trakt_uttot_OLov_m3sk float,  trakt_uttot_OLov_m3fub float,  trakt_uttot_OLov_mstamsk float,  trakt_uttot_OLov_mstamfub float,  trakt_uttot_OLov_gy float,  trakt_uttot_OLov_gy_andel float,  trakt_uttot_OLov_antal  float,  trakt_uttot_OLov_antal_andel float,  trakt_uttot_OLov_dg float,  trakt_uttot_OLov_dgv float,")
  _T("trakt_uttot_OBarr_m3sk float,  trakt_uttot_OBarr_m3fub float,  trakt_uttot_OBarr_mstamsk float,  trakt_uttot_OBarr_mstamfub float,  trakt_uttot_OBarr_gy float,  trakt_uttot_OBarr_gy_andel float,  trakt_uttot_OBarr_antal  float,  trakt_uttot_OBarr_antal_andel float,  trakt_uttot_OBarr_dg float,  trakt_uttot_OBarr_dgv float,")
  _T("trakt_uttot_Conto_m3sk float,  trakt_uttot_Conto_m3fub float,  trakt_uttot_Conto_mstamsk float,  trakt_uttot_Conto_mstamfub float,  trakt_uttot_Conto_gy float,  trakt_uttot_Conto_gy_andel float,  trakt_uttot_Conto_antal  float,  trakt_uttot_Conto_antal_andel float,  trakt_uttot_Conto_dg float,  trakt_uttot_Conto_dgv float,")
  _T("trakt_uttot_Torra_m3sk float,  trakt_uttot_Torra_m3fub float,  trakt_uttot_Torra_mstamsk float,  trakt_uttot_Torra_mstamfub float,  trakt_uttot_Torra_gy float,  trakt_uttot_Torra_gy_andel float,  trakt_uttot_Torra_antal  float,  trakt_uttot_Torra_antal_andel float,  trakt_uttot_Torra_dg float,  trakt_uttot_Torra_dgv float,")
  _T("trakt_uttot_Totalt_m3sk float,  trakt_uttot_Totalt_m3fub float,  trakt_uttot_Totalt_mstamsk float,  trakt_uttot_Totalt_mstamfub float,  trakt_uttot_Totalt_gy float,  trakt_uttot_Totalt_gy_andel float,  trakt_uttot_Totalt_antal  float,  trakt_uttot_Totalt_antal_andel float,  trakt_uttot_Totalt_dg float,  trakt_uttot_Totalt_dgv float,")
  _T("trakt_utmstv_tall_m3sk float,  trakt_utmstv_tall_m3fub float,  trakt_utmstv_tall_mstamsk float,  trakt_utmstv_tall_mstamfub float,  trakt_utmstv_tall_gy float,  trakt_utmstv_tall_gy_andel float,  trakt_utmstv_tall_antal  float,  trakt_utmstv_tall_antal_andel float,  trakt_utmstv_tall_dg float,  trakt_utmstv_tall_dgv float,")
  _T("trakt_utmstv_gran_m3sk float,  trakt_utmstv_gran_m3fub float,  trakt_utmstv_gran_mstamsk float,  trakt_utmstv_gran_mstamfub float,  trakt_utmstv_gran_gy float,  trakt_utmstv_gran_gy_andel float,  trakt_utmstv_gran_antal  float,  trakt_utmstv_gran_antal_andel float,  trakt_utmstv_gran_dg float,  trakt_utmstv_gran_dgv float,")
  _T("trakt_utmstv_bjork_m3sk float,  trakt_utmstv_bjork_m3fub float,  trakt_utmstv_bjork_mstamsk float,  trakt_utmstv_bjork_mstamfub float,  trakt_utmstv_bjork_gy float,  trakt_utmstv_bjork_gy_andel float,  trakt_utmstv_bjork_antal  float,  trakt_utmstv_bjork_antal_andel float,  trakt_utmstv_bjork_dg float,  trakt_utmstv_bjork_dgv float,")
  _T("trakt_utmstv_OLov_m3sk float,  trakt_utmstv_OLov_m3fub float,  trakt_utmstv_OLov_mstamsk float,  trakt_utmstv_OLov_mstamfub float,  trakt_utmstv_OLov_gy float,  trakt_utmstv_OLov_gy_andel float,  trakt_utmstv_OLov_antal  float,  trakt_utmstv_OLov_antal_andel float,  trakt_utmstv_OLov_dg float,  trakt_utmstv_OLov_dgv float,")
  _T("trakt_utmstv_OBarr_m3sk float,  trakt_utmstv_OBarr_m3fub float,  trakt_utmstv_OBarr_mstamsk float,  trakt_utmstv_OBarr_mstamfub float,  trakt_utmstv_OBarr_gy float,  trakt_utmstv_OBarr_gy_andel float,  trakt_utmstv_OBarr_antal  float,  trakt_utmstv_OBarr_antal_andel float,  trakt_utmstv_OBarr_dg float,  trakt_utmstv_OBarr_dgv float,")
  _T("trakt_utmstv_Conto_m3sk float,  trakt_utmstv_Conto_m3fub float,  trakt_utmstv_Conto_mstamsk float,  trakt_utmstv_Conto_mstamfub float,  trakt_utmstv_Conto_gy float,  trakt_utmstv_Conto_gy_andel float,  trakt_utmstv_Conto_antal  float,  trakt_utmstv_Conto_antal_andel float,  trakt_utmstv_Conto_dg float,  trakt_utmstv_Conto_dgv float,")
  _T("trakt_utmstv_Torra_m3sk float,  trakt_utmstv_Torra_m3fub float,  trakt_utmstv_Torra_mstamsk float,  trakt_utmstv_Torra_mstamfub float,  trakt_utmstv_Torra_gy float,  trakt_utmstv_Torra_gy_andel float,  trakt_utmstv_Torra_antal  float,  trakt_utmstv_Torra_antal_andel float,  trakt_utmstv_Torra_dg float,  trakt_utmstv_Torra_dgv float,")
  _T("trakt_utmstv_Totalt_m3sk float,  trakt_utmstv_Totalt_m3fub float,  trakt_utmstv_Totalt_mstamsk float,  trakt_utmstv_Totalt_mstamfub float,  trakt_utmstv_Totalt_gy float,  trakt_utmstv_Totalt_gy_andel float,  trakt_utmstv_Totalt_antal  float,  trakt_utmstv_Totalt_antal_andel float,  trakt_utmstv_Totalt_dg float,  trakt_utmstv_Totalt_dgv float,")
  _T("trakt_utistv_tall_m3sk float,  trakt_utistv_tall_m3fub float,  trakt_utistv_tall_mstamsk float,  trakt_utistv_tall_mstamfub float,  trakt_utistv_tall_gy float,  trakt_utistv_tall_gy_andel float,  trakt_utistv_tall_antal  float,  trakt_utistv_tall_antal_andel float,  trakt_utistv_tall_dg float,trakt_utistv_tall_dgv float,")
  _T("trakt_utistv_gran_m3sk float,  trakt_utistv_gran_m3fub float,  trakt_utistv_gran_mstamsk float,  trakt_utistv_gran_mstamfub float,  trakt_utistv_gran_gy float,  trakt_utistv_gran_gy_andel float,  trakt_utistv_gran_antal  float,  trakt_utistv_gran_antal_andel float,  trakt_utistv_gran_dg float,trakt_utistv2_dgv float,")
  _T("trakt_utistv_bjork_m3sk float,  trakt_utistv_bjork_m3fub float,  trakt_utistv_bjork_mstamsk float,  trakt_utistv_bjork_mstamfub float,  trakt_utistv_bjork_gy float,  trakt_utistv_bjork_gy_andel float,  trakt_utistv_bjork_antal  float,  trakt_utistv_bjork_antal_andel float,  trakt_utistv_bjork_dg float,trakt_utistv_bjork_dgv float,")
  _T("trakt_utistv_OLov_m3sk float,  trakt_utistv_OLov_m3fub float,  trakt_utistv_OLov_mstamsk float,  trakt_utistv_OLov_mstamfub float,  trakt_utistv_OLov_gy float,  trakt_utistv_OLov_gy_andel float,  trakt_utistv_OLov_antal  float,  trakt_utistv_OLov_antal_andel float,  trakt_utistv_OLov_dg float,trakt_utistv_OLov_dgv float,")
  _T("trakt_utistv_OBarr_m3sk float,  trakt_utistv_OBarr_m3fub float,  trakt_utistv_OBarr_mstamsk float,  trakt_utistv_OBarr_mstamfub float,  trakt_utistv_OBarr_gy float,  trakt_utistv_OBarr_gy_andel float,  trakt_utistv_OBarr_antal  float,  trakt_utistv_OBarr_antal_andel float,  trakt_utistv_OBarr_dg float,trakt_utistv_OBarr_dgv float,")
  _T("trakt_utistv_Conto_m3sk float,  trakt_utistv_Conto_m3fub float,  trakt_utistv_Conto_mstamsk float,  trakt_utistv_Conto_mstamfub float,  trakt_utistv_Conto_gy float,  trakt_utistv_Conto_gy_andel float,  trakt_utistv_Conto_antal  float,  trakt_utistv_Conto_antal_andel float,  trakt_utistv_Conto_dg float,trakt_utistv_Conto_dgv float,")
  _T("trakt_utistv_Torra_m3sk float,  trakt_utistv_Torra_m3fub float,  trakt_utistv_Torra_mstamsk float,  trakt_utistv_Torra_mstamfub float,  trakt_utistv_Torra_gy float,  trakt_utistv_Torra_gy_andel float,  trakt_utistv_Torra_antal  float,  trakt_utistv_Torra_antal_andel float,  trakt_utistv_Torra_dg float,trakt_utistv_Torra_dgv float,")
  _T("trakt_utistv_Totalt_m3sk float,  trakt_utistv_Totalt_m3fub float,  trakt_utistv_Totalt_mstamsk float,  trakt_utistv_Totalt_mstamfub float,  trakt_utistv_Totalt_gy float,  trakt_utistv_Totalt_gy_andel float,  trakt_utistv_Totalt_antal  float,  trakt_utistv_Totalt_antal_andel float,  trakt_utistv_Totalt_dg float,trakt_utistv_Totalt_dgv float,")
  _T("trakt_gakvot_tall  float,  trakt_gakvot_gran float,  trakt_gakvot_bjork float,  trakt_gakvot_Olov float,  trakt_gakvot_OBarr float,  trakt_gakvot_Conto float,  trakt_gakvot_Torra float,  trakt_gakvot_Totalt float,")
  _T("trakt_skadade1_1 float,  trakt_skadade1_2 float,  trakt_skadade1_3 float,  trakt_skadade1_4 float,")
  _T("trakt_skadade2_1 float,  trakt_skadade2_2 float,  trakt_skadade2_3 float,  trakt_skadade2_4 float,")
  _T("trakt_skadade2_5 float,  trakt_skadade2_6 float,")
  _T("trakt_spar1 float,  trakt_spar2 float,  trakt_spar3 float,")
  _T("trakt_stv1 float,   trakt_stv2 float,  trakt_stv3 float,")
  _T("trakt_spar_over_20mha float,")
  _T("create_date datetime NOT NULL default CURRENT_TIMESTAMP,")
  _T("PRIMARY KEY (trakt_forvalt,trakt_avv_id,trakt_inv_typ,trakt_forenklad));");



const LPCTSTR SQL_ADD_TRAKT_TABLE_AVVDATUM	= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_trakt_table' and column_name = 'trakt_date2')  ")
_T("alter table sca_trakt_table add trakt_date2 nchar(50)");


const LPCTSTR SQL_ADD_TRAKT_TABLE_HUGGNFORM	= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_trakt_table' and column_name = 'trakt_huggnform')  ")
_T("alter table sca_trakt_table add trakt_huggnform int");


const LPCTSTR SQL_ADD_TRAKT_TABLE_TERRF		= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_trakt_table' and column_name = 'trakt_terrf')  ")
_T("alter table sca_trakt_table add trakt_terrf int");


const LPCTSTR SQL_ADD_TRAKT_TABLE_PARTOF		= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_trakt_table' and column_name = 'trakt_partof')  ")
_T("alter table sca_trakt_table add trakt_partof int");

const LPCTSTR SQL_ADD_TRAKT_TABLE_DEFH25		= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_trakt_table' and column_name = 'trakt_defaultH25_1')  ")
_T("alter table sca_trakt_table add ")
_T("trakt_defaultH25_1 int,")
_T("trakt_defaultH25_2 int,")
_T("trakt_defaultH25_3 int,")
_T("trakt_defaultH25_4 int,")
_T("trakt_defaultH25_5 int,")
_T("trakt_defaultH25_6 int,")
_T("trakt_defaultH25_7 int;");

const LPCTSTR SQL_ADD_TRAKT_TABLE_COORD		= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_trakt_table' and column_name = 'trakt_coord')  ")
_T("alter table sca_trakt_table add trakt_coord ntext");


const LPCTSTR SQL_ADD_PLOT_COLUMNS = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
_T("where table_name = 'sca_plot_table' and column_name = 'plot_antal')  ")
_T("alter table sca_plot_table add ")
_T("plot_antal int,")
_T("plot_areal int,")
_T("plot_brosthojdsalder int,")
_T("plot_si int,")
_T("plot_si_tradslag int,")
_T("plot_skad_rota int,")
_T("plot_spar_langd1 int,")
_T("plot_spar_langd2 int,")
_T("plot_spar_matstracka int,")
_T("plot_stv_avst1 int,")
_T("plot_stv_avst2 int,")
_T("plot_stv_bredd1 int,")
_T("plot_stv_bredd2 int,")
_T("plot_ohjd int,")
_T("plot_skad_stam1 int,")
_T("plot_skad_stam2 int,")
_T("plot_skad_rot1 int,")
_T("plot_skad_rot2 int,")
_T("plot_skad_stam_rot1 int,")
_T("plot_skad_stam_rot2 int;");


const LPCTSTR SQL_CREATE_PLOT_TABLE	=  _T("CREATE TABLE dbo.sca_plot_table(")
 	_T("plot_id					int NOT NULL,")
	_T("plot_stam_gy			float NOT NULL,")
	_T("plot_stam_antal		float NOT NULL,")
	_T("plot_stubb_gy			float NOT NULL,")
	_T("plot_stubb_antal		float NOT NULL,")
	_T("plot_skad_stam			float NOT NULL,")
	_T("plot_skad_rot			float NOT NULL,")
	_T("plot_skad_stam_rot	float NOT NULL,")
	_T("plot_skadandel_tot	float NOT NULL,")
	_T("plot_skadandel_stam	float NOT NULL,")
	_T("plot_skadandel_rot	float NOT NULL,")
	_T("plot_skadandel_rota	float NOT NULL,")
	_T("plot_x   				int NOT NULL,")
	_T("plot_y					int NOT NULL,")
	_T("plot_trakt_forvalt	int NOT NULL,")
	_T("plot_trakt_avv_id		varchar(12) NOT NULL,")
	_T("plot_trakt_inv_typ	int NOT NULL,")
	_T("plot_trakt_forenklad	int NOT NULL,")
	_T("create_date datetime NOT NULL default CURRENT_TIMESTAMP,")
  _T("PRIMARY KEY (")
	_T("plot_id,")
	_T("plot_trakt_forvalt,")
	_T("plot_trakt_avv_id,")
	_T("plot_trakt_inv_typ,")
	_T("plot_trakt_forenklad")
	_T("),")
  _T("CONSTRAINT FK_Sca_Plot ")
  _T("FOREIGN KEY (")
	_T("plot_trakt_forvalt,")
	_T("plot_trakt_avv_id,")
	_T("plot_trakt_inv_typ,")
	_T("plot_trakt_forenklad")
	_T(")")
  _T("REFERENCES sca_trakt_table (trakt_forvalt,trakt_avv_id,trakt_inv_typ,trakt_forenklad)")
  _T("ON DELETE CASCADE);");



const LPCTSTR SQL_CREATE_TREE_TABLE	=  _T("CREATE TABLE dbo.sca_tree_table(")
 	_T("Tree_Trakt_forvalt		int NOT NULL,")
	_T("Tree_Trakt_avv_id		varchar(12) NOT NULL,")
	_T("Tree_Trakt_inv_typ		int NOT NULL,")
	_T("Tree_Trakt_forenklad	int NOT NULL,")
	_T("Tree_PlotId				int NOT NULL,")
	_T("Tree_TreeId				int NOT NULL,")
	_T("Tree_SpecId				int NOT NULL,")
	_T("Tree_Dia					int NOT NULL,")
	_T("Tree_Anth					int NOT NULL,")
	_T("Tree_Hgt					int NOT NULL,")
	_T("Tree_Hgt2					int NOT NULL,")
	_T("Tree_Typ					int NOT NULL,")
	_T("create_date datetime NOT NULL default CURRENT_TIMESTAMP,")
  _T("PRIMARY KEY (")
  	_T("Tree_Trakt_forvalt,")
	_T("Tree_Trakt_avv_id,")
	_T("Tree_Trakt_inv_typ,")
	_T("Tree_Trakt_forenklad,")
	_T("Tree_TreeId")
	_T("),")
  _T("CONSTRAINT FK_Sca_Tree ")
  _T("FOREIGN KEY (")
  	_T("Tree_Trakt_forvalt,")
	_T("Tree_Trakt_avv_id,")
	_T("Tree_Trakt_inv_typ,")
	_T("Tree_Trakt_forenklad")
	_T(")")
  _T("REFERENCES sca_trakt_table (trakt_forvalt,trakt_avv_id,trakt_inv_typ,trakt_forenklad)")
  _T("ON DELETE CASCADE);");

/*
INSERT INTO sca_forvalt_table (forvalt_num,forvalt_name) values(120,'Medelpad');
INSERT INTO sca_forvalt_table (forvalt_num,forvalt_name) values(123,'J�mtland');
INSERT INTO sca_forvalt_table (forvalt_num,forvalt_name) values(140,'�ngermanland');
INSERT INTO sca_forvalt_table (forvalt_num,forvalt_name) values(148,'V�sterbotten');
INSERT INTO sca_forvalt_table (forvalt_num,forvalt_name) values(163,'Norbotten');*/
