
#include "stdafx.h"
#include "Resource.h"
#include "MDIScaGBDistriktFrame.h"
#include "UMScaGallBasDB.h"
#include "ResLangFileReader.h"
#include "ScaGBDistriktSelectionList.h"
#include "UMHmsMisc.h"
#include "ScaGBDistriktSelectionList.h"


/////////////////////////////////////////////////////////////////////////////
// CMDIScaGBDistriktFrame

IMPLEMENT_DYNCREATE(CMDIScaGBDistriktFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIScaGBDistriktFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIScaGBDistriktFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIScaGBDistriktFrame::CMDIScaGBDistriktFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CMDIScaGBDistriktFrame::~CMDIScaGBDistriktFrame()
{
}

void CMDIScaGBDistriktFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_DISTRIKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

int CMDIScaGBDistriktFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}


	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIScaGBDistriktFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}


// load the placement in OnShowWindow()
void CMDIScaGBDistriktFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_DISTRIKT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIScaGBDistriktFrame::OnSetFocus(CWnd*)
{
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

BOOL CMDIScaGBDistriktFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIScaGBDistriktFrame diagnostics

#ifdef _DEBUG
void CMDIScaGBDistriktFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIScaGBDistriktFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIScaGBDistriktFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCAGB_DISTRIKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCAGB_DISTRIKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIScaGBDistriktFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIScaGBDistriktFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIScaGBDistriktFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	// Check the wparam to see what the user wants to do; 060922 p�d
	if (wParam == ID_OPEN_ITEM)
	{
//		setupFiles();
	}
	else if (wParam == ID_DBNAVIG_LIST)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		showFormView(IDD_FORMVIEW5,m_sLangFN);
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	} 
	return 0L;
}

// MY METHODS

/*
void CMDIScaGBDistriktFrame::setupFiles(void)
{
	CString sTitle;
	CString sFileName;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sTitle = xml->str(IDS_STRING200);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	CMyFileDialog fdlg(sTitle,TRUE,DEFAULT_EXTENSION,"",OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT,FILEOPEN_FILTER);
	if (fdlg.DoModal() == IDOK)
	{
		CJonasDLL_handling *hnd = new CJonasDLL_handling();
		if (hnd)
		{
			POSITION pos = fdlg.GetStartPosition();
			while (pos)
			{
				sFileName = fdlg.GetNextPathName(pos);
				TRAKT_IDENTIFER m_structTraktIdentifer;
				hnd->enterDataToDB(m_dbConnectionData,
													 sFileName.GetBuffer(),
													 extractFileName(sFileName).GetBuffer(),
													 &m_structTraktIdentifer);
			}
			
			delete hnd;
		}


	}	// if (fdlg.DoModal() == IDOK)
}*/


// CMDIScaGBDistriktFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CScaGBDistriktelectionListFrame

IMPLEMENT_DYNCREATE(CScaGBDistriktSelectionListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CScaGBDistriktSelectionListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CScaGBDistriktSelectionListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CScaGBDistriktSelectionListFrame::CScaGBDistriktSelectionListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CScaGBDistriktSelectionListFrame::~CScaGBDistriktSelectionListFrame()
{
}

void CScaGBDistriktSelectionListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_LIST_DISTRIKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CScaGBDistriktSelectionListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CScaGBDistriktSelectionListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_LIST_DISTRIKT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CScaGBDistriktSelectionListFrame::OnSetFocus(CWnd*)
{
}

BOOL CScaGBDistriktSelectionListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CScaGBDistriktSelectionListFrame diagnostics

#ifdef _DEBUG
void CScaGBDistriktSelectionListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CScaGBDistriktSelectionListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CScaGBDistriktSelectionListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_GALLBAS_LIST_DISTRIKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_GALLBAS_LIST_DISTRIKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CScaGBDistriktSelectionListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CScaGBDistriktSelectionListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CScaGBDistriktSelectionListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

// MY METHODS

// CScaGBDistriktSelectionListFrame message handlers
