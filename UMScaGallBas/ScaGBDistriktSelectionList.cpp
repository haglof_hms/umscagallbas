// ScaGBDistriktSelectionList.cpp : implementation file
//

#include "stdafx.h"
#include "ScaGBDistriktSelectionList.h"
#include "ResLangFileReader.h"
#include "MDIScaGBDistriktFormView.h"

// CScaGBDistriktSelectionList dialog

IMPLEMENT_DYNCREATE(CScaGBDistriktSelectionList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CScaGBDistriktSelectionList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_DISTRIKT_REPORT, OnReportItemDblClick)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CScaGBDistriktSelectionList::CScaGBDistriktSelectionList()
	: CXTResizeFormView(CScaGBDistriktSelectionList::IDD)
{
}

CScaGBDistriktSelectionList::~CScaGBDistriktSelectionList()
{
}

void CScaGBDistriktSelectionList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CScaGBDistriktSelectionList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CScaGBDistriktSelectionList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	if (m_bConnected)
	{
		// Get Distrikt information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistrikts(m_vecDistriktData);
			delete pDB;

			setupReport();
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

BOOL CScaGBDistriktSelectionList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CScaGBDistriktSelectionList diagnostics

#ifdef _DEBUG
void CScaGBDistriktSelectionList::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CScaGBDistriktSelectionList::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CScaGBDistriktSelectionList message handlers

void CScaGBDistriktSelectionList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndDistriktReport.GetSafeHwnd() != NULL)
	{
	setResize(&m_wndDistriktReport,1,1,rect.right - 2,rect.bottom - 2);
	}
}

void CScaGBDistriktSelectionList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CScaGBDistriktSelectionList::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndDistriktReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndDistriktReport.Create(this, IDC_DISTRIKT_REPORT ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndDistriktReport.GetSafeHwnd() != NULL)
				{

					m_wndDistriktReport.ShowWindow( SW_NORMAL );
		         pCol = m_wndDistriktReport.AddColumn(new CXTPReportColumn(0,xml->str(IDS_STRING7503),SIZE_SCAGB_DISTRIKT_COL));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndDistriktReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING7504),SIZE_SCAGB_DISTRIKT_COL));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndDistriktReport,1,1,rect.right - 2,rect.bottom - 2);

					populateReport();
				}	// if (m_wndDistriktReport.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CScaGBDistriktSelectionList::populateReport(void)
{
	for (UINT i = 0;i < m_vecDistriktData.size();i++)
	{
		DISTRIKT_DATA data = m_vecDistriktData[i];
		m_wndDistriktReport.AddRecord(new CForvaltReportDataRec(i,data.m_nDistriktID,data.m_sDistriktName));
	}
	m_wndDistriktReport.Populate();
	m_wndDistriktReport.UpdateWindow();
}

void CScaGBDistriktSelectionList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT *)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CForvaltReportDataRec *pRec = (CForvaltReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDIScaGBDistriktFormView *pView = (CMDIScaGBDistriktFormView *)getFormViewByID(IDD_FORMVIEW4);
		if (pView)
		{
			pView->doPopulate(pRec->getIndex());
		}
	}
}