// MDIScaGBForvaltFormView.cpp : implementation file
//
#include "stdafx.h"
#include "MDIScaGBForvaltFormView.h"
#include "MDIScaGBForvaltFrame.h"
#include "ResLangFileReader.h"
#include "UMHmsMisc.h"
// CMDIScaGBForvaltFormView

IMPLEMENT_DYNCREATE(CMDIScaGBForvaltFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIScaGBForvaltFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIScaGBForvaltFormView::CMDIScaGBForvaltFormView()
	: CXTResizeFormView(CMDIScaGBForvaltFormView::IDD)
{
	m_bConnected = FALSE;
}

CMDIScaGBForvaltFormView::~CMDIScaGBForvaltFormView()
{
}

void CMDIScaGBForvaltFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL1_2, m_wndLbl2);

	DDX_Control(pDX, IDC_EDIT1_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT1_2, m_wndEdit2);

	DDX_Control(pDX, IDC_GROUP1_1, m_wndGroup);
	//}}AFX_DATA_MAP
}


BOOL CMDIScaGBForvaltFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIScaGBForvaltFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly( TRUE );
	m_wndEdit1.SetLimitText(FORVALT_MAX_LEN);

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
   m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly( TRUE );
	

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	getForvaltsFromDB();
	m_nDBIndex = 0;
	populateData(m_nDBIndex);
	setLanguage();
	m_bIsDirty=FALSE;
	
}

void CMDIScaGBForvaltFormView::getForvaltsFromDB()
{
	if (m_bConnected)
	{
		// Get Forvalt information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getForvalts(m_vecForvaltData);
			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

void CMDIScaGBForvaltFormView::OnSetFocus(CWnd*)
{	
	if(m_vecForvaltData.size()==0)
		setNavigationButtons(FALSE,FALSE);
	else
	setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecForvaltData.size()-1));
}

BOOL CMDIScaGBForvaltFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CMDIScaGBForvaltFormView diagnostics

#ifdef _DEBUG
void CMDIScaGBForvaltFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIScaGBForvaltFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIScaGBForvaltFormView message handlers

void CMDIScaGBForvaltFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING7503)+_T(" :"));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING7504)+_T(" :"));
			
			m_sErrCap = xml->str(IDS_STRING7574);
			m_sSaveData.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7644),
									xml->str(IDS_STRING7645));
			m_sNotOkToSave.Format(_T("%s\n\n%s\n\n%s"),
									xml->str(IDS_STRING7646),
									xml->str(IDS_STRING7647),
									xml->str(IDS_STRING7648));
			m_sUpdateForvaltMsg.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7649),
									xml->str(IDS_STRING7650));

		}
		delete xml;
	}

}

void CMDIScaGBForvaltFormView::populateData(UINT idx)
{
	FORVALT_DATA data;
	if (!m_vecForvaltData.empty() && 
		  idx >= 0 && 
			idx < m_vecForvaltData.size())
	{
		data = m_vecForvaltData[idx];
		m_structForvaltIdentifer.m_nForvaltID=data.m_nForvaltID;
		m_structForvaltIdentifer.m_sForvaltName=data.m_sForvaltName;
		m_wndEdit1.setInt(data.m_nForvaltID);
		m_wndEdit2.SetWindowText(data.m_sForvaltName);

		CString tst=_T(""),tst2=_T(""),tst3=_T("");
		RLFReader *xml = new RLFReader;
		CMDIScaGBForvaltFrame *pView = (CMDIScaGBForvaltFrame *)getFormViewByID(IDD_FORMVIEW)->GetParent();
		if (xml->Load(m_sLangFN))
			tst3=xml->str(IDS_STRING7501);

		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),idx+1,m_vecForvaltData.size());
				tst.Format(_T("%s%s"),tst3,tst2);
				pDoc->SetTitle(tst);		
			}
		}
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDIScaGBForvaltFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			addForvalt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveForvalt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteForvalt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (!isDataChanged())
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}
				break;
		}
		case ID_DBNAVIG_PREV :
		{
			if (!isDataChanged())
			{

				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;

				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
			}
				break;
		}
		case ID_DBNAVIG_NEXT :
		{
			if (!isDataChanged())
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecForvaltData.size() - 1))
					m_nDBIndex = (UINT)m_vecForvaltData.size() - 1;

				if (m_nDBIndex == (UINT)m_vecForvaltData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			if (!isDataChanged())
			{

				m_nDBIndex = (UINT)m_vecForvaltData.size()-1;
				setNavigationButtons(TRUE,FALSE);
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

BOOL CMDIScaGBForvaltFormView::isDataChanged(void)
{
	if (m_bIsDirty)
	{
		if (::MessageBox(0,m_sSaveData,m_sErrCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		{
			saveForvalt();
			m_bIsDirty=FALSE;
		   return TRUE;
		}
		else
			m_bIsDirty=FALSE;
	}
	return FALSE;
}

void CMDIScaGBForvaltFormView::addForvalt()
{
	
	m_wndEdit1.SetReadOnly( FALSE );
	m_wndEdit2.SetReadOnly( FALSE );
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_bIsDirty=TRUE;

}

void CMDIScaGBForvaltFormView::getEnteredData(FORVALT_DATA &fData)
{
	fData.m_sForvaltName=m_wndEdit2.getText();
	fData.m_nForvaltID=m_wndEdit1.getInt();
	/*CString tst;
	tst.Format("Num: %d Namn: %s",m_wndEdit1.getInt(),m_wndEdit2.getText());
	AfxMessageBox(tst);*/
}
void CMDIScaGBForvaltFormView::saveForvalt()
{
	FORVALT_DATA fData=FORVALT_DATA();
	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			getEnteredData(fData);
			// Setup index information; 061207 p�d
			

			if (!pDB->addForvalt( fData ))
			{
				if ((::MessageBox(0,m_sUpdateForvaltMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
				{
						pDB->updForvalt( fData );
						m_structForvaltIdentifer.m_nForvaltID=fData.m_nForvaltID;
						m_structForvaltIdentifer.m_sForvaltName=fData.m_sForvaltName;
				}
			}
			else
			{
			   m_structForvaltIdentifer.m_nForvaltID=fData.m_nForvaltID;
				m_structForvaltIdentifer.m_sForvaltName=fData.m_sForvaltName;
			}
			delete pDB;
		}	// if (pDB != NULL)		
		m_bIsDirty=FALSE;		

	}	// if (isOKToSave())
	resetForvalt(RESET_TO_JUST_ENTERED_SET_NB);

}

void CMDIScaGBForvaltFormView::resetForvalt(enumRESET reset)
{
	// Get updated data from Database
	getForvaltsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecForvaltData.size() > 0)
	{
		/*
		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}*/
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecForvaltData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecForvaltData.size()-1));
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{
			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecForvaltData.size();i++)
			{
				FORVALT_DATA data = m_vecForvaltData[i];
				if (data.m_nForvaltID == m_structForvaltIdentifer.m_nForvaltID &&
					data.m_sForvaltName == m_structForvaltIdentifer.m_sForvaltName)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons(m_nDBIndex > 0,
					   								m_nDBIndex < (m_vecForvaltData.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
	m_wndEdit1.SetReadOnly( TRUE );
	m_wndEdit2.SetReadOnly( TRUE );
}

BOOL CMDIScaGBForvaltFormView::isOKToSave(void)
{
	CString tst=m_wndEdit2.getText();
	if (	tst==_T("") || m_wndEdit1.getInt() <= 0 )
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}
	return TRUE;
}

void CMDIScaGBForvaltFormView::deleteForvalt()
{
	FORVALT_DATA dataForvalt;
	CString sMsg=_T("");
	CString sText1;	//	Ta bort trakt
	CString sText2;	// Skall denna trakt tas bort ... ?
	CString sNr,sName,sT1,sT2;

	CString sCaption;	// Meddelande
	CString sOKBtn;	// Ta bort
	CString sCancelBtn;// Avbryt
	BOOL bDidDel=FALSE;

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecForvaltData.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
				if (fileExists(m_sLangFN))
				{
					RLFReader *xml = new RLFReader;
					if (xml->Load(m_sLangFN))
					{
						sText1 = xml->str(IDS_STRING7651);
						sText2 = xml->str(IDS_STRING7652);
						sCaption = xml->str(IDS_STRING7574);
						sOKBtn = xml->str(IDS_STRING7575);
						sCancelBtn = xml->str(IDS_STRING7576);
						sT1 = xml->str(IDS_STRING7503);
						sT2 = xml->str(IDS_STRING7504);
					}	
					delete xml;
				}
					dataForvalt = m_vecForvaltData[m_nDBIndex];
					// Setup a message for user upon deleting 
					sName=dataForvalt.m_sForvaltName;
					sNr.Format(_T("%d"),dataForvalt.m_nForvaltID);
															
					sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%s</b><br>%s : <b>%s</b><br><hr><font size=\"+4\" color=\"#ff0000\"><br><br><center>%s</center></font>"),
						sText1,
						sT1,sNr,
						sT2,sName,
						sText2);


					/*
					sMsg.Format("%s","<font size=\"+4\"><center><b>");
					tmp.Format("%s</b></center></font><br><hr><br>",sText1);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT1,dataTrakt.m_nTrakt_forvalt_num);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT2,dataTrakt.m_nTrakt_id_typ);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT3,dataTrakt.m_nTrakt_id);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT4,dataTrakt.m_nTrakt_del_id);sMsg+=tmp;
					tmp.Format("%s : <b>%s</b><br>",sT5,dataTrakt.m_sTrakt_name);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT6,dataTrakt.m_nTrakt_ursprung);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT7,dataTrakt.m_nTrakt_inv_typ);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT8,dataTrakt.m_nTrakt_distrikt_num);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT9,dataTrakt.m_nTrakt_prod_ledare);sMsg+=tmp;
					tmp.Format("%s : <b>%s</b><br>",sT10,dataTrakt.m_sTrakt_alt_id);sMsg+=tmp;
					tmp.Format("%s : <b>%s</b><br>",sT11,dataTrakt.m_sTrakt_date);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT12,dataTrakt.m_nTrakt_maskinlag);sMsg+=tmp;
					tmp.Format("%s : <b>%10.3f</b><br>",sT13,dataTrakt.m_fTrakt_areal);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br><hr><br>",sT14,dataTrakt.m_nTrakt_ant_ytor);sMsg+=tmp;
					tmp.Format("%s<br>",sText2);sMsg+=tmp;
					tmp.Format("%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>",sText3);sMsg+=tmp;
					tmp.Format("%s</center></font>",sText4);sMsg+=tmp;*/
					
					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delForvalt(dataForvalt);
						bDidDel=TRUE;
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	if(bDidDel==TRUE)
		resetForvalt(RESET_TO_LAST_SET_NB);
	else
		resetForvalt(RESET_TO_JUST_ENTERED_SET_NB);
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIScaGBForvaltFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC
void CMDIScaGBForvaltFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
										   m_nDBIndex < (m_vecForvaltData.size()-1));
}
