#include "StdAfx.h"

#include "UMScaGallBasDB.h"


void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
   if (hWndReciv != NULL)
   {
      DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;
		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;
		data.conn = NULL;
		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
   }
}


//////////////////////////////////////////////////////////////////////////////////
// CScaGallBasDBDB; Handle ALL transactions for GallBas

CScaGallBasDB::CScaGallBasDB() 
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CScaGallBasDB::CScaGallBasDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CScaGallBasDB::CScaGallBasDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}
// PUBLIC
BOOL CScaGallBasDB::doTableExists(LPCTSTR table_name)
{
	CString sSQL;
	CString err;
   BOOL bReturn = FALSE;
	try
	{
		// Set SQL question to send to database db_name; 061101 p�d
		sSQL.Format(_T("select object_id('%s','U') as 'objectid'"),table_name);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	   while(m_saCommand.FetchNext())
		//if(m_saCommand.FetchNext())
			bReturn=TRUE;
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_FORVALT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CScaGallBasDB::createTable(LPCTSTR SQL_SCRPT)
{
	CString sSQL;
	CString err;
   
	try
	{
		sSQL.Format(_T("%s"),SQL_SCRPT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_FORVALT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


// F�RVALTNINGAR

// Handle data in sca_forvalt_table; 
BOOL CScaGallBasDB::getForvalts(vecForvaltData &vec)
{
	
	CString sSQL;
	CString err;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s"),TBL_FORVALT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_forvalt_data(m_saCommand.Field(1).asShort(),(LPCTSTR)m_saCommand.Field(2).asString().GetBinaryBuffer(0)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_FORVALT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CScaGallBasDB::addForvalt(FORVALT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
	sSQL.Format(_T("select * from %s where forvalt_num = %d "),TBL_FORVALT,rec.m_nForvaltID);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (forvalt_num,forvalt_name) values(:1,:2)"),TBL_FORVALT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()	= rec.m_nForvaltID;
			m_saCommand.Param(2).setAsString()	= rec.m_sForvaltName;
			m_saCommand.Prepare();
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &)
	{
		 // print error message
//		AfxMessageBox((const char*)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::updForvalt(FORVALT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		sSQL.Format(_T("select * from %s where forvalt_num = %d"),TBL_FORVALT,rec.m_nForvaltID);
		if (exists(sSQL))
		{
		   sSQL.Format(_T("update %s set forvalt_name=:1 where forvalt_num=:2"),TBL_FORVALT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.m_sForvaltName; 
			m_saCommand.Param(2).setAsShort()	= rec.m_nForvaltID;
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::delForvalt(FORVALT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where forvalt_num = %d"),TBL_FORVALT,rec.m_nForvaltID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where forvalt_num =:1"),TBL_FORVALT,rec.m_nForvaltID);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nForvaltID;
			m_saCommand.Execute();			
			m_saConnection.Commit();
			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CScaGallBasDB::getDistrikts(vecDistriktData &vec)
{
	CString sSQL;
	CString err;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s"),TBL_DISTRIKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_district_data(m_saCommand.Field(1).asShort(),(LPCTSTR)m_saCommand.Field(2).asString().GetBinaryBuffer(0)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_DISTRIKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CScaGallBasDB::addDistrikt(DISTRIKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
	sSQL.Format(_T("select * from %s where distrikt_num = %d "),TBL_DISTRIKT,rec.m_nDistriktID);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (distrikt_num,distrikt_name) values(:1,:2)"),TBL_DISTRIKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()	= rec.m_nDistriktID;
			m_saCommand.Param(2).setAsString()	= rec.m_sDistriktName; 
			m_saCommand.Prepare();
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &)
	{
		 // print error message
//		AfxMessageBox((const char*)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::updDistrikt(DISTRIKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		sSQL.Format(_T("select * from %s where distrikt_num = %d"),TBL_DISTRIKT,rec.m_nDistriktID);
		if (exists(sSQL))
		{
		   sSQL.Format(_T("update %s set distrikt_name=:1 where distrikt_num=:2"),TBL_DISTRIKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.m_sDistriktName; 
			m_saCommand.Param(2).setAsShort()	= rec.m_nDistriktID;
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::delDistrikt(DISTRIKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where distrikt_num = %d"),TBL_DISTRIKT,rec.m_nDistriktID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where distrikt_num =:1"),TBL_DISTRIKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nDistriktID;
			m_saCommand.Execute();			
			m_saConnection.Commit();
			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}




void CScaGallBasDB::Load_Trakt_Variables(TRAKT_DATA &trkt)
{
	int i=1,spec=0,var=0;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_forvalt=MY_NULL;  
	else
		trkt.m_nTrakt_forvalt=m_saCommand.Field(i).asLong(); 	i++;

	trkt.m_sTrakt_avv_id=m_saCommand.Field(i).asString(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_inv_typ=MY_NULL;  
	else
		trkt.m_nTrakt_inv_typ=m_saCommand.Field(i).asShort(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_forenklad=MY_NULL;  
	else
		trkt.m_nTrakt_forenklad=m_saCommand.Field(i).asShort();	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_driv_enh = MY_NULL;  
	else
		trkt.m_nTrakt_driv_enh = m_saCommand.Field(i).asLong(); i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_avlagg=MY_NULL;  
	else
		trkt.m_nTrakt_avlagg=m_saCommand.Field(i).asLong(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_distrikt=MY_NULL;  
	else
		trkt.m_nTrakt_distrikt=m_saCommand.Field(i).asLong(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_maskinlag=MY_NULL;  
	else
		trkt.m_nTrakt_maskinlag=m_saCommand.Field(i).asShort(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_ursprung=MY_NULL;  
	else
		trkt.m_nTrakt_ursprung=m_saCommand.Field(i).asShort(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_prod_led=MY_NULL;  
	else
		trkt.m_nTrakt_prod_led=m_saCommand.Field(i).asShort(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_sTrakt_namn=_T("");  
	else
		trkt.m_sTrakt_namn=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
	i++;	

	if(m_saCommand.Field(i).isNull())
		trkt.m_sTrakt_date=_T("");  
	else
		trkt.m_sTrakt_date.Format(_T("%8.8s"),(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0));  	
		i++; 

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_areal=MY_NULL;  
	else
		trkt.m_fTrakt_areal=m_saCommand.Field(i).asDouble();  	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_ant_ytor=MY_NULL;  
	else
		trkt.m_fTrakt_ant_ytor=m_saCommand.Field(i).asDouble(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_medel_ovre_hojd=MY_NULL;  
	else
		trkt.m_fTrakt_medel_ovre_hojd=m_saCommand.Field(i).asDouble();  	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_si_sp=MY_NULL;  
	else
		trkt.m_nTrakt_si_sp=m_saCommand.Field(i).asShort();	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_si_tot=MY_NULL;  
	else
		trkt.m_fTrakt_si_tot=m_saCommand.Field(i).asDouble();  	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_alder=MY_NULL;  
	else
		trkt.m_fTrakt_alder=m_saCommand.Field(i).asDouble();  	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_sTrakt_progver=_T("");  
	else
		trkt.m_sTrakt_progver=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_gy_dir_fore=MY_NULL;
	else
		trkt.m_fTrakt_gy_dir_fore=m_saCommand.Field(i).asDouble();	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_fTrakt_gy_dir_mal=MY_NULL;
	else
		trkt.m_fTrakt_gy_dir_mal=m_saCommand.Field(i).asDouble();	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_enter_type=MY_NULL;
	else
		trkt.m_nTrakt_enter_type=m_saCommand.Field(i).asShort();	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_sTrakt_notes=_T("");  
	else
		trkt.m_sTrakt_notes=m_saCommand.Field(i).asCLob();  	i++;


	for(spec=0;spec<NUM_OF_SP+1;spec++)
		for(var=0;var<NUM_OF_FORE;var++)
		{			
			if(m_saCommand.Field(i).isNull())
			{
				trkt.m_recData.m_recVars.m_recFore[var][spec]=MY_NULL;
				//CString tst=_T("");
				//tst.Format("Null var nr= %d \n Value = %10.3f",i,trkt.m_recData.m_recVars.m_recKvarvarande[var][spec]);
				//AfxMessageBox(tst);
			}
			else
				trkt.m_recData.m_recVars.m_recFore[var][spec]=m_saCommand.Field(i).asDouble();		
			i++;
		}

	for(spec=0;spec<NUM_OF_SP+1;spec++)
		for(var=0;var<NUM_OF_KVARV;var++)
		{			
			if(m_saCommand.Field(i).isNull())
			{
				trkt.m_recData.m_recVars.m_recKvarvarande[var][spec]=MY_NULL;
				//CString tst=_T("");
				//tst.Format("Null var nr= %d \n Value = %10.3f",i,trkt.m_recData.m_recVars.m_recKvarvarande[var][spec]);
				//AfxMessageBox(tst);
			}
			else
				trkt.m_recData.m_recVars.m_recKvarvarande[var][spec]=m_saCommand.Field(i).asDouble();		
			i++;
		}

	for(spec=0;spec<NUM_OF_SP+1;spec++)
		for(var=0;var<NUM_OF_UTTAG;var++)
		{
			if(m_saCommand.Field(i).isNull())
				trkt.m_recData.m_recVars.m_recUttagTot[var][spec]=MY_NULL;
			else
				trkt.m_recData.m_recVars.m_recUttagTot[var][spec]=m_saCommand.Field(i).asDouble();		
			i++;
		}
	for(spec=0;spec<NUM_OF_SP+1;spec++)
		for(var=0;var<NUM_OF_UTTAG;var++)
		{
			if(m_saCommand.Field(i).isNull())
				trkt.m_recData.m_recVars.m_recUttagMstv[var][spec]=MY_NULL;
			else
				trkt.m_recData.m_recVars.m_recUttagMstv[var][spec]=m_saCommand.Field(i).asDouble();		
			i++;
		}
	for(spec=0;spec<NUM_OF_SP+1;spec++)
		for(var=0;var<NUM_OF_UTTAG;var++)
		{
			if(m_saCommand.Field(i).isNull())
				trkt.m_recData.m_recVars.m_recUttagIstv[var][spec]=MY_NULL;
			else
				trkt.m_recData.m_recVars.m_recUttagIstv[var][spec]=m_saCommand.Field(i).asDouble();		
			i++;
		}			

	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		if(m_saCommand.Field(i).isNull())
			trkt.m_recData.m_recVars.m_recGaKvot[spec]=MY_NULL;
		else
			trkt.m_recData.m_recVars.m_recGaKvot[spec]=m_saCommand.Field(i).asDouble();		
		i++;
	}


	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Skadade_Trad=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Skadade_Trad=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Skadade_Stam=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Skadade_Stam=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Skadade_Rot=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Skadade_Rot=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Rota=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Rota=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Zon1_Stam=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Zon1_Stam=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Zon2_Stam=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Zon2_Stam=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Zon1_Rot=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Zon1_Rot=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Zon2_Rot=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Zon2_Rot=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Zon1_Rot_Stam=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Zon1_Rot_Stam=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Zon2_Rot_Stam=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Zon2_Rot_Stam=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Sparstracka=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Sparstracka=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Spardjup=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Spardjup=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Spardjup_Andel=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Spardjup_Andel=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Stv_Avst=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Stv_Avst=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Stv_Bredd=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Stv_Bredd=m_saCommand.Field(i).asDouble();i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Stv_Areal=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Stv_Areal=m_saCommand.Field(i).asDouble();	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_recData.m_recVars.m_Spar_over_20mha=MY_NULL;
	else
		trkt.m_recData.m_recVars.m_Spar_over_20mha=m_saCommand.Field(i).asDouble();	i++;
	//En extre i++ h�r f�r att l�sa f�rbi created date kolumnen i databasen
	i++;
	if(m_saCommand.Field(i).isNull())
		trkt.m_sTrakt_date2=_T("");  
	else
		trkt.m_sTrakt_date2.Format(_T("%8.8s"),(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0));  	
		i++; 

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_huggnform=MY_NULL;  
	else
		trkt.m_nTrakt_huggnform=m_saCommand.Field(i).asShort(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_terrf=MY_NULL;  
	else
		trkt.m_nTrakt_terrf=m_saCommand.Field(i).asShort(); 	i++;

	if(m_saCommand.Field(i).isNull())
		trkt.m_nTrakt_partof=MY_NULL;  
	else
		trkt.m_nTrakt_partof=m_saCommand.Field(i).asShort(); 	i++;

	for(spec=0;spec<NUM_OF_SP;spec++)
	{
		if(m_saCommand.Field(i).isNull())
			trkt.m_nDefaultH25[spec]=MY_NULL;  
		else
			trkt.m_nDefaultH25[spec]=m_saCommand.Field(i).asShort(); 	
		i++;
	}

	if(m_saCommand.Field(i).isNull())
		trkt.m_sTrakt_coord=_T("");  
	else
		trkt.m_sTrakt_coord=m_saCommand.Field(i).asCLob();  	i++;
}


void CScaGallBasDB::Load_Trakt_Variables(vecTraktData &vec)
{
int i=1,spec=0,var=0;
TRAKT_DATA trkt;
trkt=TRAKT_DATA();

 Load_Trakt_Variables(trkt);

 vec.push_back(_trakt_data(
	 trkt.m_nTrakt_forvalt,
	 trkt.m_nTrakt_driv_enh,
	 trkt.m_sTrakt_avv_id,
	 trkt.m_nTrakt_inv_typ,
	 trkt.m_nTrakt_avlagg,
	 trkt.m_nTrakt_distrikt,
	 trkt.m_nTrakt_maskinlag,
	 trkt.m_nTrakt_ursprung,
	 trkt.m_nTrakt_prod_led,
	 trkt.m_sTrakt_namn,
	 trkt.m_sTrakt_date,
	 trkt.m_fTrakt_areal,
	 trkt.m_fTrakt_ant_ytor,
	 trkt.m_fTrakt_medel_ovre_hojd,
	 trkt.m_nTrakt_si_sp,
	 trkt.m_fTrakt_si_tot,
	 trkt.m_fTrakt_alder,
	 trkt.m_sTrakt_progver,
	 trkt.m_fTrakt_gy_dir_fore,
	 trkt.m_fTrakt_gy_dir_mal,
	 trkt.m_nTrakt_enter_type,
	 trkt.m_nTrakt_forenklad,
	 trkt.m_sTrakt_notes,
	 trkt.m_sTrakt_date2,
	 trkt.m_nTrakt_huggnform,
	 trkt.m_nTrakt_terrf,
	 trkt.m_nTrakt_partof,
	 trkt.m_nDefaultH25[0],
	 trkt.m_nDefaultH25[1],
	 trkt.m_nDefaultH25[2],
	 trkt.m_nDefaultH25[3],
	 trkt.m_nDefaultH25[4],
	 trkt.m_nDefaultH25[5],
	 trkt.m_nDefaultH25[6],
	 trkt.m_sTrakt_coord,
	 trkt.m_recData
	 ));	
}

BOOL CScaGallBasDB::getTraktIndex(vecTraktIndex &vec,int forenklad)
{
	TCHAR szBuffer[1024];

	CString err;
	int check=0;
	double check2=0.0;
	try
	{
		vec.clear();
		getSQLTraktQuestionIndex(szBuffer);
		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
		check=m_saCommand.Field("trakt_forenklad").asShort();
			if(forenklad==check)
			{
				vec.push_back(_trakt_index(
					m_saCommand.Field(1).asLong(),
					m_saCommand.Field(2).asLong(),
					m_saCommand.Field(3).asString().GetBuffer(0),
					m_saCommand.Field(4).asShort(),
					m_saCommand.Field(5).asShort()
					));
			}
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakt(TRAKT_INDEX &trkt_idx,TRAKT_DATA &trkt)
{
	CString str=_T(""),err=_T("");
	CTraktVariables rec;
	try
	{
		str.Format(_T("select * from %s where trakt_forvalt = %d and trakt_avv_id = '%s' and trakt_inv_typ= %d and trakt_forenklad = %d"),
			TBL_TRAKT,
			trkt_idx.m_nTrakt_forvalt,
			trkt_idx.m_sTrakt_avv_id,
			trkt_idx.m_nTrakt_inv_typ,
			trkt_idx.m_nTrakt_forenklad
			);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(trkt);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


BOOL CScaGallBasDB::getTrakts(vecTraktData &vec,int forenklad)
{
	TCHAR szBuffer[1024];
	CTraktVariables rec;
	int check=-1;
	try
	{
		vec.clear();
		getSQLTraktQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			check=m_saCommand.Field("trakt_forenklad").asShort();
			if(forenklad==check)
			{
			Load_Trakt_Variables(vec);
			}
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}




BOOL CScaGallBasDB::getCorrectPlotNumbers(TRAKT_DATA trkt,vector<PLOT_CORRECTION> &vct)
{

	CString str=_T(""),err=_T("");
	int i=0;
	PLOT_CORRECTION v;
	try
	{
		str.Format(_T("select distinct Tree_PlotId, sum(((Tree_Dia/2000.0)*(Tree_Dia/2000.0))*3.14159265/(200.0/10000.0)) from %s where Tree_Trakt_forvalt = %d and Tree_Trakt_avv_id  = '%s' and Tree_Trakt_inv_typ  = %d and Tree_Trakt_forenklad = %d group by Tree_PlotId"),
			TBL_TREE,
			trkt.m_nTrakt_forvalt,
			trkt.m_sTrakt_avv_id,
			trkt.m_nTrakt_inv_typ,
			trkt.m_nTrakt_forenklad
			);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			v=PLOT_CORRECTION(m_saCommand.Field(1).asLong(),m_saCommand.Field(2).asDouble());
			//v.resize(v.size()+1,m_saCommand.Field(1).asLong());
			vct.push_back(v);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		// print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


BOOL CScaGallBasDB::getPlots(TRAKT_DATA &trkt)
{
	CString str=_T(""),err=_T("");
	int i=0;
	PLOT_DATA tPlot;
	tPlot=PLOT_DATA();
	try
	{
		trkt.m_vPlotData.clear();
		str.Format(_T("select * from %s where plot_trakt_forvalt = %d and plot_trakt_avv_id = '%s' and plot_trakt_inv_typ= %d and plot_trakt_forenklad= %d  order by plot_id"),
				TBL_PLOT,
				trkt.m_nTrakt_forvalt,
				trkt.m_sTrakt_avv_id,
				trkt.m_nTrakt_inv_typ,
				trkt.m_nTrakt_forenklad
				);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			i=1;
			// 1
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_id=MY_NULL;  
			else
				tPlot.m_nPlot_id=m_saCommand.Field(i).asShort(); 
			i++;
			// 2
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_stam_gy=MY_NULL;  
			else
				tPlot.m_fPlot_stam_gy=m_saCommand.Field(i).asDouble(); 
			i++;
			// 3
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_stam_antal=MY_NULL;  
			else
				tPlot.m_fPlot_stam_antal=m_saCommand.Field(i).asDouble(); 
			i++;
			// 4
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_stubb_gy=MY_NULL;  
			else
				tPlot.m_fPlot_stubb_gy=m_saCommand.Field(i).asDouble(); 
			i++;
			// 5
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_stubb_antal=MY_NULL;  
			else
				tPlot.m_fPlot_stubb_antal=m_saCommand.Field(i).asDouble(); 
			i++;
			// 6
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skad_stam=MY_NULL;  
			else
				tPlot.m_fPlot_skad_stam=m_saCommand.Field(i).asDouble(); 
			i++;
			// 7
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skad_rot=MY_NULL;  
			else
				tPlot.m_fPlot_skad_rot=m_saCommand.Field(i).asDouble(); 
			i++;
			// 8
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skad_stam_rot=MY_NULL;  
			else
				tPlot.m_fPlot_skad_stam_rot=m_saCommand.Field(i).asDouble(); 
			i++;
			// 9
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skadandel_tot=MY_NULL;  
			else
				tPlot.m_fPlot_skadandel_tot=m_saCommand.Field(i).asDouble(); 
			i++;
			// 10
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skadandel_stam=MY_NULL;  
			else
				tPlot.m_fPlot_skadandel_stam=m_saCommand.Field(i).asDouble(); 
			i++;
			// 11
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skadandel_rot=MY_NULL;  
			else
				tPlot.m_fPlot_skadandel_rot=m_saCommand.Field(i).asDouble(); 
			i++;
			// 12
			if(m_saCommand.Field(i).isNull())
				tPlot.m_fPlot_skadandel_rota=MY_NULL;  
			else		
				tPlot.m_fPlot_skadandel_rota=m_saCommand.Field(i).asDouble(); 
			i++;
			// 13
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_X=MY_NULL;  
			else
				tPlot.m_nPlot_X=m_saCommand.Field(i).asLong();  
			i++;
			// 14
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_Y=MY_NULL;  
			else
				tPlot.m_nPlot_Y=m_saCommand.Field(i).asLong();  
			i++;
			// 15
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_trakt_forvalt=MY_NULL;  
			else
				tPlot.m_nPlot_trakt_forvalt=m_saCommand.Field(i).asLong();  
			i++;
			// 17
			if(m_saCommand.Field(i).isNull())
				tPlot.m_sPlot_trakt_avv_id=_T("");
			else
				tPlot.m_sPlot_trakt_avv_id=m_saCommand.Field(i).asString();
			i++;
			// 18
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_trakt_inv_typ=MY_NULL;  
			else
				tPlot.m_nPlot_trakt_inv_typ=m_saCommand.Field(i).asShort();
			i++;
			// 19
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_trakt_forenklad=MY_NULL;  
			else
				tPlot.m_nPlot_trakt_forenklad=m_saCommand.Field(i).asShort();
			//Hoppa f�rbi create date d�rav i++ tv� g�nger
			i++;
			i++;
			// 20
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_antal=MY_NULL;  
			else
				tPlot.m_nPlot_antal=m_saCommand.Field(i).asShort();
			i++;
			// 21
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_areal=MY_NULL;  	
			else
				tPlot.m_nPlot_areal=m_saCommand.Field(i).asShort();
			i++;
			// 22
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_brosthojdsalder=MY_NULL;  
			else
				tPlot.m_nPlot_brosthojdsalder=m_saCommand.Field(i).asShort();
			i++;
			// 23
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_si=MY_NULL;  
			else
				tPlot.m_nPlot_si=m_saCommand.Field(i).asShort();
			i++;
			// 24
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_si_tradslag=MY_NULL;  
			else
				tPlot.m_nPlot_si_tradslag=m_saCommand.Field(i).asShort();
			i++;
			// 25
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_rota=MY_NULL;  
			else
				tPlot.m_nPlot_skad_rota=m_saCommand.Field(i).asShort();
			i++;
			// 26
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_spar_langd1=MY_NULL;  
			else
				tPlot.m_nPlot_spar_langd1=m_saCommand.Field(i).asShort();
			i++;
			// 27
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_spar_langd2=MY_NULL;  
			else
				tPlot.m_nPlot_spar_langd2=m_saCommand.Field(i).asShort();
			i++;
			// 28
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_spar_matstracka=MY_NULL;  
			else
				tPlot.m_nPlot_spar_matstracka=m_saCommand.Field(i).asShort();
			i++;
			// 29
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_stv_avst1=MY_NULL;  
			else
				tPlot.m_nPlot_stv_avst1=m_saCommand.Field(i).asShort();
			i++;
			// 30
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_stv_avst2=MY_NULL;  
			else
				tPlot.m_nPlot_stv_avst2=m_saCommand.Field(i).asShort();
			i++;
			// 31
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_stv_bredd1=MY_NULL;  
			else
				tPlot.m_nPlot_stv_bredd1=m_saCommand.Field(i).asShort();
			i++;
			// 32
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_stv_bredd2=MY_NULL;  
			else
				tPlot.m_nPlot_stv_bredd2=m_saCommand.Field(i).asShort();
			i++;
			// 33
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_ohjd=MY_NULL;  
			else
				tPlot.m_nPlot_ohjd=m_saCommand.Field(i).asShort();
			i++;
			// 34
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_stam1=MY_NULL;  
			else
				tPlot.m_nPlot_skad_stam1=m_saCommand.Field(i).asShort();
			i++;
			// 35
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_stam2=MY_NULL;  
			else
				tPlot.m_nPlot_skad_stam2=m_saCommand.Field(i).asShort();
			i++;
			// 36
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_rot1=MY_NULL;  
			else
				tPlot.m_nPlot_skad_rot1=m_saCommand.Field(i).asShort();
			i++;
			// 37
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_rot2=MY_NULL;  
			else
				tPlot.m_nPlot_skad_rot2=m_saCommand.Field(i).asShort();
			i++;
			// 38
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_stam_rot1=MY_NULL;  
			else
				tPlot.m_nPlot_skad_stam_rot1=m_saCommand.Field(i).asShort();
			i++;
			// 39
			if(m_saCommand.Field(i).isNull())
				tPlot.m_nPlot_skad_stam_rot2=MY_NULL;  
			else
				tPlot.m_nPlot_skad_stam_rot2=m_saCommand.Field(i).asShort();
			i++;

			trkt.m_vPlotData.push_back(tPlot);
			/*
			trkt.m_vPlotData.push_back(_plot_data(
			m_saCommand.Field(1).asShort(),
			m_saCommand.Field(2).asDouble(),
			m_saCommand.Field(3).asDouble(),
			m_saCommand.Field(4).asDouble(),
			m_saCommand.Field(5).asDouble(),
			m_saCommand.Field(6).asDouble(),
			m_saCommand.Field(7).asDouble(),
			m_saCommand.Field(8).asDouble(),
			m_saCommand.Field(9).asDouble(),
			m_saCommand.Field(10).asDouble(),
			m_saCommand.Field(11).asDouble(),
			m_saCommand.Field(12).asDouble(),
			m_saCommand.Field(13).asLong(),
			m_saCommand.Field(14).asLong(),
			m_saCommand.Field(15).asLong(),
			m_saCommand.Field(16).asLong(),
			m_saCommand.Field(17).asShort(),
			m_saCommand.Field(18).asShort(),
			m_saCommand.Field(19).asShort(),

			m_saCommand.Field(20).asShort(),
			m_saCommand.Field(21).asShort(),
			m_saCommand.Field(22).asShort(),
			m_saCommand.Field(23).asShort(),
			m_saCommand.Field(24).asShort(),
			m_saCommand.Field(25).asShort(),
			m_saCommand.Field(26).asShort(),
			m_saCommand.Field(27).asShort(),
			m_saCommand.Field(28).asShort(),
			m_saCommand.Field(29).asShort(),
			m_saCommand.Field(30).asShort(),
			m_saCommand.Field(31).asShort(),
			m_saCommand.Field(32).asShort(),
			m_saCommand.Field(33).asShort(),

			m_saCommand.Field(34).asDouble(),
			m_saCommand.Field(35).asDouble(),
			m_saCommand.Field(36).asDouble(),
			m_saCommand.Field(37).asDouble(),
			m_saCommand.Field(38).asDouble(),
			m_saCommand.Field(39).asDouble()
			));*/
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

/*BOOL CScaGallBasDB::getPlots(vecPlotData &vec)
{
	TCHAR szBuffer[1024];
	CString err=_T("");
	try
	{
		vec.clear();
		getSQLPlotQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_plot_data(
			m_saCommand.Field(1).asShort(),
			m_saCommand.Field(2).asDouble(),
			m_saCommand.Field(3).asDouble(),
			m_saCommand.Field(4).asDouble(),
			m_saCommand.Field(5).asDouble(),
			m_saCommand.Field(6).asDouble(),
			m_saCommand.Field(7).asDouble(),
			m_saCommand.Field(8).asDouble(),
			m_saCommand.Field(9).asDouble(),
			m_saCommand.Field(10).asDouble(),
			m_saCommand.Field(11).asDouble(),
			m_saCommand.Field(12).asDouble(),
			m_saCommand.Field(13).asLong(),
			m_saCommand.Field(14).asLong(),
			m_saCommand.Field(15).asLong(),
			m_saCommand.Field(16).asLong(),
			m_saCommand.Field(17).asShort(),
			m_saCommand.Field(18).asShort(),
			m_saCommand.Field(19).asShort(),

			m_saCommand.Field(20).asShort(),
			m_saCommand.Field(21).asShort(),
			m_saCommand.Field(22).asShort(),
			m_saCommand.Field(23).asShort(),
			m_saCommand.Field(24).asShort(),
			m_saCommand.Field(25).asShort(),
			m_saCommand.Field(26).asShort(),
			m_saCommand.Field(27).asShort(),
			m_saCommand.Field(28).asShort(),
			m_saCommand.Field(29).asShort(),
			m_saCommand.Field(30).asShort(),
			m_saCommand.Field(31).asShort(),
			m_saCommand.Field(32).asShort(),
			m_saCommand.Field(33).asShort()
			));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}*/
BOOL CScaGallBasDB::delPlot(TRAKT_DATA rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		GetKeyDBQuestion(&sSQL,rec);
		if (exists(sSQL))
		{
			
			sSQL.Format(_T("delete from dbo.sca_plot_table where plot_trakt_forvalt=:1 and plot_trakt_avv_id=:2 and plot_trakt_inv_typ=:3 and plot_trakt_forenklad=:4"),TBL_PLOT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()  = rec.m_nTrakt_forvalt;
			m_saCommand.Param(2).setAsString() = rec.m_sTrakt_avv_id;
			m_saCommand.Param(3).setAsShort() = rec.m_nTrakt_inv_typ;
			m_saCommand.Param(4).setAsShort() = rec.m_nTrakt_forenklad;
			m_saCommand.Execute();			
			m_saConnection.Commit();
			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CScaGallBasDB::addPlot(TRAKT_DATA &trkt, PLOT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		GetKeyPlotDBQuestion(&sSQL,rec);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (\
							plot_id,plot_stam_gy,plot_stam_antal,plot_stubb_gy,plot_stubb_antal,\
							plot_skad_stam,plot_skad_rot,plot_skad_stam_rot,\
							plot_skadandel_tot,plot_skadandel_stam,plot_skadandel_rot,plot_skadandel_rota,\
							plot_x,plot_y,\
							plot_trakt_forvalt,plot_trakt_avv_id,plot_trakt_inv_typ,plot_trakt_forenklad,\
							plot_antal,plot_areal,plot_brosthojdsalder,plot_si,plot_si_tradslag,plot_skad_rota,plot_spar_langd1,\
							plot_spar_langd2,plot_spar_matstracka,plot_stv_avst1,plot_stv_avst2,plot_stv_bredd1,plot_stv_bredd2,plot_ohjd,\
							plot_skad_stam1,plot_skad_stam2,plot_skad_rot1,plot_skad_rot2,plot_skad_stam_rot1,plot_skad_stam_rot2)\
							values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38)"),TBL_PLOT);
			m_saCommand.setCommandText((SAString)sSQL);
			SetSaCommandPlotParam(trkt,rec,0);
			m_saCommand.Prepare();
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		//AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::updPlot(TRAKT_DATA &trkt,PLOT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		GetKeyPlotDBQuestion(&sSQL,rec);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set \
							plot_id=:1,plot_stam_gy=:2,plot_stam_antal=:3,plot_stubb_gy=:4,plot_stubb_antal=:5,\
							plot_skad_stam=:6,plot_skad_rot=:7,plot_skad_stam_rot=:8,\
							plot_skadandel_tot=:9,plot_skadandel_stam=:10,plot_skadandel_rot=:11,plot_skadandel_rota=:12,\
							plot_x=:13,plot_y=:14,\
							plot_trakt_forvalt=:15,plot_trakt_avv_id=:16,plot_trakt_inv_typ=:17 ,plot_trakt_forenklad=:18,\
							plot_antal=:19,plot_areal=:20,plot_brosthojdsalder=:21,plot_si=:22,plot_si_tradslag=:23,plot_skad_rota=:24,plot_spar_langd1=:25,\
							plot_spar_langd2=:26,plot_spar_matstracka=:27,plot_stv_avst1=:28,plot_stv_avst2=:29,plot_stv_bredd1=:30,plot_stv_bredd2=:31,plot_ohjd=:32,\
							plot_skad_stam1=:33,plot_skad_stam2=:34,plot_skad_rot1=:35,plot_skad_rot2=:36,plot_skad_stam_rot1=:37,plot_skad_stam_rot2=:38\
							where plot_id=:39 and plot_trakt_forvalt=:40 and plot_trakt_avv_id=:41 and plot_trakt_inv_typ=:42 and plot_trakt_forenklad=:43"),TBL_PLOT);
			m_saCommand.setCommandText((SAString)sSQL);
			SetSaCommandPlotParam(trkt,rec,1);
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::addTree(TRAKT_DATA &trkt, TREE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		GetKeyTreeDBQuestion(&sSQL,rec);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (\
					 		Tree_Trakt_forvalt,Tree_Trakt_avv_id,Tree_Trakt_inv_typ,Tree_Trakt_forenklad,Tree_PlotId,\
							Tree_TreeId,Tree_SpecId,Tree_Dia,Tree_Anth,Tree_Hgt,Tree_Hgt2,Tree_Typ\
							)\
							values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)"),TBL_TREE);
			m_saCommand.setCommandText((SAString)sSQL);
			SetSaCommandTreeParam(trkt,rec,0);
			m_saCommand.Prepare();
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &)
	{
		 // print error message
//		AfxMessageBox((const char*)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CScaGallBasDB::updTree(TRAKT_DATA &trkt,TREE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		GetKeyTreeDBQuestion(&sSQL,rec);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set \
							Tree_Trakt_forvalt=:1,Tree_Trakt_avv_id=:2,Tree_Trakt_inv_typ=:3,Tree_Trakt_forenklad=:4,Tree_PlotId=:5,\
							Tree_TreeId=:6,Tree_SpecId=:7,Tree_Dia=:8,Tree_Anth=:9,Tree_Hgt=:10,Tree_Hgt2=:11,Tree_Typ=:12\
							where Tree_Trakt_forvalt=:13 and Tree_Trakt_avv_id=:14 and Tree_Trakt_inv_typ=:15 and Tree_Trakt_forenklad=:16 and Tree_TreeId=:17"),TBL_TREE);
			m_saCommand.setCommandText((SAString)sSQL);
			SetSaCommandTreeParam(trkt,rec,1);
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::getTrees(TRAKT_DATA &trkt)
{
	CString str=_T(""),err=_T("");
	try
	{
		trkt.m_vTreeData.clear();
		str.Format(_T("select * from %s where Tree_Trakt_forvalt = %d and Tree_Trakt_avv_id = '%s' and Tree_Trakt_inv_typ  = %d and Tree_Trakt_forenklad = %d order by Tree_PlotId,Tree_TreeId"),
				TBL_TREE,
				trkt.m_nTrakt_forvalt,
				trkt.m_sTrakt_avv_id,
				trkt.m_nTrakt_inv_typ,
				trkt.m_nTrakt_forenklad
				);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			trkt.m_vTreeData.push_back(_tree_data(
			m_saCommand.Field(1).asLong(),
			m_saCommand.Field(2).asString().GetBuffer(0),
			m_saCommand.Field(3).asShort(),
			m_saCommand.Field(4).asShort(),
			m_saCommand.Field(5).asShort(),
			m_saCommand.Field(6).asShort(),
			m_saCommand.Field(7).asShort(),
			m_saCommand.Field(8).asShort(),
			m_saCommand.Field(9).asShort(),
			m_saCommand.Field(10).asShort(),
			m_saCommand.Field(11).asShort(),
			m_saCommand.Field(12).asShort()			
			));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


void CScaGallBasDB::getSQLPlotQuestion(LPTSTR sql)
{
	_stprintf(sql,SQL_PLOT_QUESTION1,TBL_PLOT);
}

void CScaGallBasDB::getSQLTreeQuestion(LPTSTR sql)
{
	_stprintf(sql,SQL_TREE_QUESTION1,TBL_TREE);
}


void CScaGallBasDB::getSQLTraktQuestion(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION1,TBL_TRAKT);
}

void CScaGallBasDB::getSQLTraktQuestionIndex(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION_INDEX_AND_FORENKLAD,TBL_TRAKT);
}

BOOL CScaGallBasDB::delTrakt(TRAKT_DATA rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		GetKeyDBQuestion(&sSQL,rec);
		if (exists(sSQL))
		{

			sSQL.Format(_T("delete from %s where trakt_forvalt=:1 and trakt_avv_id=:2 and trakt_inv_typ =:3 and trakt_forenklad =:4"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()  = rec.m_nTrakt_forvalt;
			m_saCommand.Param(2).setAsString() = rec.m_sTrakt_avv_id;
			m_saCommand.Param(3).setAsShort() = rec.m_nTrakt_inv_typ;
			m_saCommand.Param(4).setAsShort() = rec.m_nTrakt_forenklad;
			m_saCommand.Execute();			
			m_saConnection.Commit();
			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}



void CScaGallBasDB::GetKeyDBQuestion(CString *str,TRAKT_DATA rec)
{
										  
	str->Format(_T("select * from %s where trakt_forvalt = %d and trakt_avv_id = '%s' and trakt_inv_typ = %d and trakt_forenklad = %d"),
		TBL_TRAKT,
		rec.m_nTrakt_forvalt,
		rec.m_sTrakt_avv_id,
		rec.m_nTrakt_inv_typ,
		rec.m_nTrakt_forenklad);
}

void CScaGallBasDB::GetKeyPlotDBQuestion(CString *str,PLOT_DATA rec)
{
	str->Format(_T("select * from %s where plot_id = %d and plot_trakt_forvalt = %d and plot_trakt_avv_id = '%s' and plot_trakt_inv_typ = %d and plot_trakt_forenklad = %d"),TBL_PLOT,
		rec.m_nPlot_id,
		rec.m_nPlot_trakt_forvalt,
		rec.m_sPlot_trakt_avv_id,
		rec.m_nPlot_trakt_inv_typ,
		rec.m_nPlot_trakt_forenklad);
}

void CScaGallBasDB::GetKeyTreeDBQuestion(CString *str,TREE_DATA rec)
{
	str->Format(_T("select * from %s where Tree_Trakt_forvalt = %d and Tree_Trakt_avv_id = '%s' and Tree_Trakt_inv_typ = %d and Tree_Trakt_forenklad= %d and	Tree_TreeId= %d "),TBL_TREE,
		rec.m_nTree_Trakt_forvalt,
		rec.m_sTree_Trakt_avv_id,
		rec.m_nTree_Trakt_inv_typ,
		rec.m_nTree_Trakt_forenklad,
		rec.m_nTree_TreeId);
}

void CScaGallBasDB::SetSaCommandParam(TRAKT_DATA rec,int update)
{
	int spec=0,var=0,i=1;

	m_saCommand.Param(i).setAsLong()	= rec.m_nTrakt_forvalt; i++;

	m_saCommand.Param(i).setAsString()= rec.m_sTrakt_avv_id; i++;

	m_saCommand.Param(i).setAsShort()= rec.m_nTrakt_inv_typ; i++;

	m_saCommand.Param(i).setAsShort()= rec.m_nTrakt_forenklad; i++;

	if(rec.m_nTrakt_driv_enh == MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsLong() = rec.m_nTrakt_driv_enh; i++;

	if(rec.m_nTrakt_avlagg==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsLong()	= rec.m_nTrakt_avlagg;i++;

	if(rec.m_nTrakt_distrikt==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_distrikt;i++;

	if(rec.m_nTrakt_maskinlag==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_maskinlag;i++;

	if(rec.m_nTrakt_ursprung==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_ursprung;i++;

	if(rec.m_nTrakt_prod_led==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_prod_led;i++;

	m_saCommand.Param(i).setAsString()	= rec.m_sTrakt_namn; 
	i++;

	m_saCommand.Param(i).setAsString()	= rec.m_sTrakt_date; 
	i++;

	if(rec.m_fTrakt_areal==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_areal;i++;

	if(rec.m_fTrakt_ant_ytor==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_ant_ytor;i++;

	if(rec.m_fTrakt_medel_ovre_hojd==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_medel_ovre_hojd;i++;

	if(rec.m_nTrakt_si_sp==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_si_sp;i++;

	if(rec.m_fTrakt_si_tot==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_si_tot;i++;

	if(rec.m_fTrakt_alder==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_alder;i++;

	m_saCommand.Param(i).setAsString()		= rec.m_sTrakt_progver; 
	i++;

	if(rec.m_fTrakt_gy_dir_fore==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_gy_dir_fore;i++;

	if(rec.m_fTrakt_gy_dir_mal==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble()	= rec.m_fTrakt_gy_dir_mal;i++;

	if(rec.m_nTrakt_enter_type==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_enter_type;i++;


	m_saCommand.Param(i).setAsCLob()		= rec.m_sTrakt_notes; 
	i++;
	
	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		for(var=0;var<NUM_OF_FORE;var++)
		{			
			if(rec.m_recData.m_recVars.m_recFore[var][spec]==MY_NULL)
			{
				//CString tst=_T("");
				//tst.Format("Null var nr= %d \n Value = %10.3f",i,rec.m_recData.m_recVars.m_recFore[var][spec]);
				//AfxMessageBox(tst);
				m_saCommand.Param(i).setAsNull();
			}
			else
			{
				m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_recFore[var][spec];
			}
			i++;
		}
	}
   
	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		for(var=0;var<NUM_OF_KVARV;var++)
		{			
			if(rec.m_recData.m_recVars.m_recKvarvarande[var][spec]==MY_NULL)
			{
				//CString tst=_T("");
				//tst.Format("Null var nr= %d \n Value = %10.3f",i,rec.m_recData.m_recVars.m_recKvarvarande[var][spec]);
				//AfxMessageBox(tst);
				m_saCommand.Param(i).setAsNull();
			}
			else
			{
				m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_recKvarvarande[var][spec];
			}
			i++;
		}
	}

	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		for(var=0;var<NUM_OF_UTTAG;var++)
		{
			if(rec.m_recData.m_recVars.m_recUttagTot[var][spec]==MY_NULL)
				m_saCommand.Param(i).setAsNull();
			else
				m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_recUttagTot[var][spec];		
			i++;
		}
	}

	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		for(var=0;var<NUM_OF_UTTAG;var++)
		{
			if(rec.m_recData.m_recVars.m_recUttagMstv[var][spec]==MY_NULL)
				m_saCommand.Param(i).setAsNull();
			else
				m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_recUttagMstv[var][spec];		
			i++;
		}
	}
	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		for(var=0;var<NUM_OF_UTTAG;var++)
		{
			if(rec.m_recData.m_recVars.m_recUttagIstv[var][spec]==MY_NULL)
				m_saCommand.Param(i).setAsNull();
			else
				m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_recUttagIstv[var][spec];		
			i++;
		}	
	}

	for(spec=0;spec<NUM_OF_SP+1;spec++)
	{
		if(rec.m_recData.m_recVars.m_recGaKvot[spec]==MY_NULL)
			m_saCommand.Param(i).setAsNull();
		else
			m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_recGaKvot[spec];		
		i++;
	}

	if(rec.m_recData.m_recVars.m_Skadade_Trad==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Skadade_Trad;
	i++;

	if(rec.m_recData.m_recVars.m_Skadade_Stam==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Skadade_Stam;
	i++;

	if(rec.m_recData.m_recVars.m_Skadade_Rot==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Skadade_Rot;
	i++;

	if(rec.m_recData.m_recVars.m_Rota==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Rota;
	i++;

	if(rec.m_recData.m_recVars.m_Zon1_Stam==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Zon1_Stam;
	i++;

	if(rec.m_recData.m_recVars.m_Zon2_Stam==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Zon2_Stam;
	i++;

	if(rec.m_recData.m_recVars.m_Zon1_Rot==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Zon1_Rot;
	i++;

	if(rec.m_recData.m_recVars.m_Zon2_Rot==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Zon2_Rot;
	i++;

	if(rec.m_recData.m_recVars.m_Zon1_Rot_Stam==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Zon1_Rot_Stam;
	i++;

	if(rec.m_recData.m_recVars.m_Zon2_Rot_Stam==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Zon2_Rot_Stam;
	i++;

	if(rec.m_recData.m_recVars.m_Sparstracka==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Sparstracka;
	i++;

	if(rec.m_recData.m_recVars.m_Spardjup==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Spardjup;
	i++;

	if(rec.m_recData.m_recVars.m_Spardjup_Andel==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Spardjup_Andel;
	i++;

	if(rec.m_recData.m_recVars.m_Stv_Avst==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Stv_Avst;
	i++;

	if(rec.m_recData.m_recVars.m_Stv_Bredd==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Stv_Bredd;
	i++;

	if(rec.m_recData.m_recVars.m_Stv_Areal==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Stv_Areal;
	i++;

	if(rec.m_recData.m_recVars.m_Spar_over_20mha==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsDouble() = rec.m_recData.m_recVars.m_Spar_over_20mha;
	i++;

	m_saCommand.Param(i).setAsString()	= rec.m_sTrakt_date2; 
	i++;

	if(rec.m_nTrakt_huggnform==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_huggnform;i++;

	if(rec.m_nTrakt_terrf==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_terrf;i++;


	if(rec.m_nTrakt_partof==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nTrakt_partof;i++;


	for(spec=0;spec<NUM_OF_SP;spec++)
	{
		if(rec.m_nDefaultH25[spec]==MY_NULL)
			m_saCommand.Param(i).setAsNull();
		else
			m_saCommand.Param(i).setAsShort() = rec.m_nDefaultH25[spec];		
		i++;
	}

	m_saCommand.Param(i).setAsCLob()		= rec.m_sTrakt_coord; 
	i++;

	if(update)
	{
		m_saCommand.Param(i).setAsLong()	= rec.m_nTrakt_forvalt;		i++;
		m_saCommand.Param(i).setAsString()= rec.m_sTrakt_avv_id;		i++;
		m_saCommand.Param(i).setAsShort()= rec.m_nTrakt_inv_typ;		i++;
	   	m_saCommand.Param(i).setAsShort()= rec.m_nTrakt_forenklad;
	}
}

void CScaGallBasDB::SetSaCommandPlotParam(TRAKT_DATA &trkt,PLOT_DATA &rec,int update)
{
	int i=1;
 	m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_id;					i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_stam_gy;			i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_stam_antal;		i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_stubb_gy;			i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_stubb_antal;		i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skad_stam;		i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skad_rot;			i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skad_stam_rot;	i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skadandel_tot;	i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skadandel_stam;	i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skadandel_rot;	i++;
	m_saCommand.Param(i).setAsDouble()	= rec.m_fPlot_skadandel_rota;	i++;
	m_saCommand.Param(i).setAsLong()		= rec.m_nPlot_X;					i++;
	m_saCommand.Param(i).setAsLong()		= rec.m_nPlot_Y;					i++;
	m_saCommand.Param(i).setAsLong()		= trkt.m_nTrakt_forvalt;		i++;
	m_saCommand.Param(i).setAsString()		= trkt.m_sTrakt_avv_id;		i++;
	m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_inv_typ;		i++;
	m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_forenklad;		i++;

	if(rec.m_nPlot_antal==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_antal;				i++;
	if(rec.m_nPlot_areal==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_areal;				i++;
	if(rec.m_nPlot_brosthojdsalder==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_brosthojdsalder;i++;
	if(rec.m_nPlot_si==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_si;					i++;
	if(rec.m_nPlot_si_tradslag==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_si_tradslag;		i++;
	if(rec.m_nPlot_skad_rota==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_rota;		i++;
	if(rec.m_nPlot_spar_langd1==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_spar_langd1;		i++;
	if(rec.m_nPlot_spar_langd2==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_spar_langd2;		i++;
	if(rec.m_nPlot_spar_matstracka==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_spar_matstracka;i++;
	if(rec.m_nPlot_stv_avst1==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_stv_avst1;		i++;
	if(rec.m_nPlot_stv_avst2==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_stv_avst2;		i++;
	if(rec.m_nPlot_stv_bredd1==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_stv_bredd1;		i++;
	if(rec.m_nPlot_stv_bredd2==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_stv_bredd2;		i++;
	if(rec.m_nPlot_ohjd==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_ohjd;				i++;

	if(rec.m_nPlot_skad_stam1==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_stam1;			i++;
	if(rec.m_nPlot_skad_stam2==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_stam2;			i++;
	if(rec.m_nPlot_skad_rot1==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_rot1;			i++;
	if(rec.m_nPlot_skad_rot2==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_rot2;			i++;
	if(rec.m_nPlot_skad_stam_rot1==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_stam_rot1;			i++;
	if(rec.m_nPlot_skad_stam_rot2==MY_NULL)
		m_saCommand.Param(i).setAsNull();
	else
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_skad_stam_rot2;			i++;

	if(update)
	{
		m_saCommand.Param(i).setAsShort()	= rec.m_nPlot_id;				i++;
		m_saCommand.Param(i).setAsLong()		= trkt.m_nTrakt_forvalt;		i++;
		m_saCommand.Param(i).setAsString()	= trkt.m_sTrakt_avv_id;		i++;
		m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_inv_typ;	i++;
		m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_forenklad;	i++;
	}

}

void CScaGallBasDB::SetSaCommandTreeParam(TRAKT_DATA &trkt,TREE_DATA &rec,int update)
{
	int i=1;
	m_saCommand.Param(i).setAsLong()		= trkt.m_nTrakt_forvalt;		i++;
	m_saCommand.Param(i).setAsString()	= trkt.m_sTrakt_avv_id;		i++;
	m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_inv_typ;	i++;
	m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_forenklad;	i++;

	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_PlotId;		i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_TreeId;	i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_SpecId;		i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_Dia;	i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_Anth;	i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_Hgt;		i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_Hgt2;	i++;
	m_saCommand.Param(i).setAsShort()	= rec.m_nTree_Typ;	i++;

	if(update)
	{
		m_saCommand.Param(i).setAsLong()		= trkt.m_nTrakt_forvalt;	i++;
		m_saCommand.Param(i).setAsString()	= trkt.m_sTrakt_avv_id;		i++;
		m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_inv_typ;	i++;
		m_saCommand.Param(i).setAsShort()	= trkt.m_nTrakt_forenklad;	i++;
		m_saCommand.Param(i).setAsShort()	= rec.m_nTree_TreeId;		i++;
	}

}

TCHAR *DBVarName[NUM_OF_TRAKT_VARIABLES_TOT]={
  _T("trakt_forvalt")			,_T("trakt_avv_id")				,_T("trakt_inv_typ")		,_T(
  "trakt_forenklad")			,_T("trakt_driv_enh")	,_T("trakt_avlagg")					,_T("trakt_distrikt")			,_T("trakt_maskinlag")	,_T(
  "trakt_ursprung")				,_T("trakt_prod_led")				,_T("trakt_namn")					,_T("trakt_date")			,_T(
  "trakt_areal")					,_T("trakt_ant_ytor")				,_T("trakt_medel_ovre_hojd")	,_T("trakt_si_sp")		,_T(
  "trakt_si_tot")					,_T("trakt_alder")		,_T("trakt_progver")	,_T(
  "trakt_gy_dir_fore")			,_T("trakt_gy_dir_efter")			,_T("trakt_enter_type")			,_T("trakt_notes")		,_T(
  "trakt_fore_tall_m3sk"		),_T("trakt_fore_tall_m3fub"		),_T("trakt_fore_tall_mstamsk"		),_T("trakt_fore_tall_mstamfub"		),_T("trakt_fore_tall_gy"		),_T("trakt_fore_tall_antal"			),_T("trakt_fore_tall_dg"			),_T("trakt_fore_tall_hgv"					),_T("trakt_fore_tall_h25"		),_T("trakt_fore_tall_dgv"),_T(
  "trakt_fore_gran_m3sk"		),_T("trakt_fore_gran_m3fub"		),_T("trakt_fore_gran_mstamsk"		),_T("trakt_fore_gran_mstamfub"		),_T("trakt_fore_gran_gy"		),_T("trakt_fore_gran_antal"			),_T("trakt_fore_gran_dg"			),_T("trakt_fore_gran_hgv"					),_T("trakt_fore_gran_h25"		),_T("trakt_fore_gran_dgv"),_T(
  "trakt_fore_bjork_m3sk"		),_T("trakt_fore_bjork_m3fub"		),_T("trakt_fore_bjork_mstamsk"	),_T("trakt_fore_bjork_mstamfub"		),_T("trakt_fore_bjork_gy"		),_T("trakt_fore_bjork_antal"			),_T("trakt_fore_bjork_dg"			),_T("trakt_fore_bjork_hgv"					),_T("trakt_fore_bjork_h25"		),_T("trakt_fore_bjork_dgv"),_T(
  "trakt_fore_OLov_m3sk"		),_T("trakt_fore_OLov_m3fub"		),_T("trakt_fore_OLov_mstamsk"	),_T("trakt_fore_OLov_mstamfub"		),_T("trakt_fore_OLov_gy"		),_T("trakt_fore_OLov_antal"			),_T("trakt_fore_OLov_dg"			),_T("trakt_fore_OLov_hgv"					),_T("trakt_fore_OLov_h25"		),_T("trakt_fore_OLov_dgv"),_T(
  "trakt_fore_OBarr_m3sk"		),_T("trakt_fore_OBarr_m3fub"		),_T("trakt_fore_OBarr_mstamsk"	),_T("trakt_fore_OBarr_mstamfub"		),_T("trakt_fore_OBarr_gy"		),_T("trakt_fore_OBarr_antal"			),_T("trakt_fore_OBarr_dg"			),_T("trakt_fore_OBarr_hgv"					),_T("trakt_fore_OBarr_h25"		),_T("trakt_fore_OBarr_dgv"),_T(
  "trakt_fore_Conto_m3sk"		),_T("trakt_fore_Conto_m3fub"		),_T("trakt_fore_Conto_mstamsk"	),_T("trakt_fore_Conto_mstamfub"		),_T("trakt_fore_Conto_gy"		),_T("trakt_fore_Conto_antal"			),_T("trakt_fore_Conto_dg"			),_T("trakt_fore_Conto_hgv"					),_T("trakt_fore_Conto_h25"		),_T("trakt_fore_Conto_dgv"),_T(
  "trakt_fore_Torra_m3sk"		),_T("trakt_fore_Torra_m3fub"		),_T("trakt_fore_Torra_mstamsk"	),_T("trakt_fore_Torra_mstamfub"		),_T("trakt_fore_Torra_gy"		),_T("trakt_fore_Torra_antal"			),_T("trakt_fore_Torra_dg"			),_T("trakt_fore_Torra_hgv"					),_T("trakt_fore_Torra_h25"		),_T("trakt_fore_Torra_dgv"),_T(
  "trakt_fore_Totalt_m3sk"		),_T("trakt_fore_Totalt_m3fub"	),_T("trakt_fore_Totalt_mstamsk"	),_T("trakt_fore_Totalt_mstamfub"		),_T("trakt_fore_Totalt_gy"		),_T("trakt_fore_Totalt_antal"			),_T("trakt_fore_Totalt_dg"			),_T("trakt_fore_Totalt_hgv"				),_T("trakt_fore_Totalt_h25"	),_T("trakt_fore_Totalt_dgv"),_T(
  "trakt_kvarv_tall_m3sk"		),_T("trakt_kvarv_tall_m3fub"		),_T("trakt_kvarv_tall_mstamsk"	),_T("trakt_kvarv_tall_mstamfub"		),_T("trakt_kvarv_tall_gy"		),_T("trakt_kvarv_tall_antal"			),_T("trakt_kvarv_tall_dg"			),_T("trakt_kvarv_tall_hgv"					),_T("trakt_kvarv_tall_h25"		),_T("trakt_kvarv_tall_dgv"),_T(
  "trakt_kvarv_gran_m3sk"		),_T("trakt_kvarv_gran_m3fub"		),_T("trakt_kvarv_gran_mstamsk"	),_T("trakt_kvarv_gran_mstamfub"		),_T("trakt_kvarv_gran_gy"		),_T("trakt_kvarv_gran_antal"			),_T("trakt_kvarv_gran_dg"			),_T("trakt_kvarv_gran_hgv"					),_T("trakt_kvarv_gran_h25"		),_T("trakt_kvarv_gran_dgv"),_T(
  "trakt_kvarv_bjork_m3sk"		),_T("trakt_kvarv_bjork_m3fub"	),_T("trakt_kvarv_bjork_mstamsk"	),_T("trakt_kvarv_bjork_mstamfub"		),_T("trakt_kvarv_bjork_gy"		),_T("trakt_kvarv_bjork_antal"			),_T("trakt_kvarv_bjork_dg"			),_T("trakt_kvarv_bjork_hgv"				),_T("trakt_kvarv_bjork_h25"	),_T("trakt_kvarv_bjork_dgv"),_T(
  "trakt_kvarv_OLov_m3sk"		),_T("trakt_kvarv_OLov_m3fub"		),_T("trakt_kvarv_OLov_mstamsk"	),_T("trakt_kvarv_OLov_mstamfub"		),_T("trakt_kvarv_OLov_gy"		),_T("trakt_kvarv_OLov_antal"			),_T("trakt_kvarv_OLov_dg"			),_T("trakt_kvarv_OLov_hgv"					),_T("trakt_kvarv_OLov_h25"		),_T("trakt_kvarv_OLov_dgv"),_T(
  "trakt_kvarv_OBarr_m3sk"		),_T("trakt_kvarv_OBarr_m3fub"	),_T("trakt_kvarv_OBarr_mstamsk"	),_T("trakt_kvarv_OBarr_mstamfub"		),_T("trakt_kvarv_OBarr_gy"		),_T("trakt_kvarv_OBarr_antal"			),_T("trakt_kvarv_OBarr_dg"			),_T("trakt_kvarv_OBarr_hgv"				),_T("trakt_kvarv_OBarr_h25"	),_T("trakt_kvarv_OBarr_dgv"),_T(
  "trakt_kvarv_Conto_m3sk"		),_T("trakt_kvarv_Conto_m3fub"	),_T("trakt_kvarv_Conto_mstamsk"	),_T("trakt_kvarv_Conto_mstamfub"		),_T("trakt_kvarv_Conto_gy"		),_T("trakt_kvarv_Conto_antal"			),_T("trakt_kvarv_Conto_dg"			),_T("trakt_kvarv_Conto_hgv"				),_T("trakt_kvarv_Conto_h25"	),_T("trakt_kvarv_Conto_dgv"),_T(
  "trakt_kvarv_Torra_m3sk"		),_T("trakt_kvarv_Torra_m3fub"	),_T("trakt_kvarv_Torra_mstamsk"	),_T("trakt_kvarv_Torra_mstamfub"		),_T("trakt_kvarv_Torra_gy"		),_T("trakt_kvarv_Torra_antal"			),_T("trakt_kvarv_Torra_dg"			),_T("trakt_kvarv_Torra_hgv"				),_T("trakt_kvarv_Torra_h25"	),_T("trakt_kvarv_Torra_dgv"),_T(
  "trakt_kvarv_Totalt_m3sk"	),_T("trakt_kvarv_Totalt_m3fub"	),_T("trakt_kvarv_Totalt_mstamsk"),_T("trakt_kvarv_Totalt_mstamfub"	),_T("trakt_kvarv_Totalt_gy"	),_T("trakt_kvarv_Totalt_antal"		),_T("trakt_kvarv_Totalt_dg"		),_T("trakt_kvarv_Totalt_hgv"				),_T("trakt_kvarv_Totalt_h25"	),_T("trakt_kvarv_Totalt_dgv"),_T(
  "trakt_uttot_tall_m3sk"		),_T("trakt_uttot_tall_m3fub"		),_T("trakt_uttot_tall_mstamsk"	),_T("trakt_uttot_tall_mstamfub"		),_T("trakt_uttot_tall_gy"		),_T("trakt_uttot_tall_gy_andel"		),_T("trakt_uttot_tall_antal "		),_T("trakt_uttot_tall_antal_andel"		),_T("trakt_uttot_tall_dg"		),_T("trakt_uttot_tall_dgv"),_T(
  "trakt_uttot_gran_m3sk"		),_T("trakt_uttot_gran_m3fub"		),_T("trakt_uttot_gran_mstamsk"	),_T("trakt_uttot_gran_mstamfub"		),_T("trakt_uttot_gran_gy"		),_T("trakt_uttot_gran_gy_andel"		),_T("trakt_uttot_gran_antal "		),_T("trakt_uttot_gran_antal_andel"		),_T("trakt_uttot_gran_dg"		),_T("trakt_uttot_gran_dgv"),_T(
  "trakt_uttot_bjork_m3sk"		),_T("trakt_uttot_bjork_m3fub"	),_T("trakt_uttot_bjork_mstamsk"	),_T("trakt_uttot_bjork_mstamfub"		),_T("trakt_uttot_bjork_gy"		),_T("trakt_uttot_bjork_gy_andel"		),_T("trakt_uttot_bjork_antal "	),_T("trakt_uttot_bjork_antal_andel"		),_T("trakt_uttot_bjork_dg"		),_T("trakt_uttot_bjork_dgv"),_T(
  "trakt_uttot_OLov_m3sk"		),_T("trakt_uttot_OLov_m3fub"		),_T("trakt_uttot_OLov_mstamsk"	),_T("trakt_uttot_OLov_mstamfub"		),_T("trakt_uttot_OLov_gy"		),_T("trakt_uttot_OLov_gy_andel"		),_T("trakt_uttot_OLov_antal "		),_T("trakt_uttot_OLov_antal_andel"		),_T("trakt_uttot_OLov_dg"		),_T("trakt_uttot_OLov_dgv"),_T(
  "trakt_uttot_OBarr_m3sk"		),_T("trakt_uttot_OBarr_m3fub"	),_T("trakt_uttot_OBarr_mstamsk"	),_T("trakt_uttot_OBarr_mstamfub"		),_T("trakt_uttot_OBarr_gy"		),_T("trakt_uttot_OBarr_gy_andel"		),_T("trakt_uttot_OBarr_antal "	),_T("trakt_uttot_OBarr_antal_andel"		),_T("trakt_uttot_OBarr_dg"		),_T("trakt_uttot_OBarr_dgv"),_T(
  "trakt_uttot_Conto_m3sk"		),_T("trakt_uttot_Conto_m3fub"	),_T("trakt_uttot_Conto_mstamsk"	),_T("trakt_uttot_Conto_mstamfub"		),_T("trakt_uttot_Conto_gy"		),_T("trakt_uttot_Conto_gy_andel"		),_T("trakt_uttot_Conto_antal "	),_T("trakt_uttot_Conto_antal_andel"		),_T("trakt_uttot_Conto_dg"		),_T("trakt_uttot_Conto_dgv"),_T(
  "trakt_uttot_Torra_m3sk"		),_T("trakt_uttot_Torra_m3fub"	),_T("trakt_uttot_Torra_mstamsk"	),_T("trakt_uttot_Torra_mstamfub"		),_T("trakt_uttot_Torra_gy"		),_T("trakt_uttot_Torra_gy_andel"		),_T("trakt_uttot_Torra_antal "	),_T("trakt_uttot_Torra_antal_andel"		),_T("trakt_uttot_Torra_dg"		),_T("trakt_uttot_Torra_dgv"),_T(
  "trakt_uttot_Totalt_m3sk"	),_T("trakt_uttot_Totalt_m3fub"	),_T("trakt_uttot_Totalt_mstamsk"),_T("trakt_uttot_Totalt_mstamfub"	),_T("trakt_uttot_Totalt_gy"	),_T("trakt_uttot_Totalt_gy_andel"	),_T("trakt_uttot_Totalt_antal "	),_T("trakt_uttot_Totalt_antal_andel"	),_T("trakt_uttot_Totalt_dg"	),_T("trakt_uttot_Totalt_dgv"),_T(
  "trakt_utmstv_tall_m3sk"		),_T("trakt_utmstv_tall_m3fub"	),_T("trakt_utmstv_tall_mstamsk"	),_T("trakt_utmstv_tall_mstamfub"		),_T("trakt_utmstv_tall_gy"		),_T("trakt_utmstv_tall_gy_andel"		),_T("trakt_utmstv_tall_antal "	),_T("trakt_utmstv_tall_antal_andel"		),_T("trakt_utmstv_tall_dg"		),_T("trakt_utmstv_tall_dgv"),_T(
  "trakt_utmstv_gran_m3sk"		),_T("trakt_utmstv_gran_m3fub"	),_T("trakt_utmstv_gran_mstamsk"	),_T("trakt_utmstv_gran_mstamfub"		),_T("trakt_utmstv_gran_gy"		),_T("trakt_utmstv_gran_gy_andel"		),_T("trakt_utmstv_gran_antal "	),_T("trakt_utmstv_gran_antal_andel"		),_T("trakt_utmstv_gran_dg"		),_T("trakt_utmstv_gran_dgv"),_T(
  "trakt_utmstv_bjork_m3sk"	),_T("trakt_utmstv_bjork_m3fub"	),_T("trakt_utmstv_bjork_mstamsk"),_T("trakt_utmstv_bjork_mstamfub"	),_T("trakt_utmstv_bjork_gy"	),_T("trakt_utmstv_bjork_gy_andel"	),_T("trakt_utmstv_bjork_antal "	),_T("trakt_utmstv_bjork_antal_andel"	),_T("trakt_utmstv_bjork_dg"	),_T("trakt_utmstv_bjork_dgv"),_T(
  "trakt_utmstv_OLov_m3sk"		),_T("trakt_utmstv_OLov_m3fub"	),_T("trakt_utmstv_OLov_mstamsk"	),_T("trakt_utmstv_OLov_mstamfub"		),_T("trakt_utmstv_OLov_gy"		),_T("trakt_utmstv_OLov_gy_andel"		),_T("trakt_utmstv_OLov_antal "	),_T("trakt_utmstv_OLov_antal_andel"		),_T("trakt_utmstv_OLov_dg"		),_T("trakt_utmstv_OLov_dgv"),_T(
  "trakt_utmstv_OBarr_m3sk"	),_T("trakt_utmstv_OBarr_m3fub"	),_T("trakt_utmstv_OBarr_mstamsk"),_T("trakt_utmstv_OBarr_mstamfub"	),_T("trakt_utmstv_OBarr_gy"	),_T("trakt_utmstv_OBarr_gy_andel"	),_T("trakt_utmstv_OBarr_antal "	),_T("trakt_utmstv_OBarr_antal_andel"	),_T("trakt_utmstv_OBarr_dg"	),_T("trakt_utmstv_OBarr_dgv"),_T(
  "trakt_utmstv_Conto_m3sk"	),_T("trakt_utmstv_Conto_m3fub"	),_T("trakt_utmstv_Conto_mstamsk"),_T("trakt_utmstv_Conto_mstamfub"	),_T("trakt_utmstv_Conto_gy"	),_T("trakt_utmstv_Conto_gy_andel"	),_T("trakt_utmstv_Conto_antal "	),_T("trakt_utmstv_Conto_antal_andel"	),_T("trakt_utmstv_Conto_dg"	),_T("trakt_utmstv_Conto_dgv"),_T(
  "trakt_utmstv_Torra_m3sk"	),_T("trakt_utmstv_Torra_m3fub"	),_T("trakt_utmstv_Torra_mstamsk"),_T("trakt_utmstv_Torra_mstamfub"	),_T("trakt_utmstv_Torra_gy"	),_T("trakt_utmstv_Torra_gy_andel"	),_T("trakt_utmstv_Torra_antal "	),_T("trakt_utmstv_Torra_antal_andel"	),_T("trakt_utmstv_Torra_dg"	),_T("trakt_utmstv_Torra_dgv"),_T(
  "trakt_utmstv_Totalt_m3sk"	),_T("trakt_utmstv_Totalt_m3fub"	),_T("trakt_utmstv_Totalt_mstamsk"),_T("trakt_utmstv_Totalt_mstamfub"	),_T("trakt_utmstv_Totalt_gy"	),_T("trakt_utmstv_Totalt_gy_andel"	),_T("trakt_utmstv_Totalt_antal "	),_T("trakt_utmstv_Totalt_antal_andel"	),_T("trakt_utmstv_Totalt_dg"	),_T("trakt_utmstv_Totalt_dgv"),_T(
  "trakt_utistv_tall_m3sk"		),_T("trakt_utistv_tall_m3fub"	),_T("trakt_utistv_tall_mstamsk"	),_T("trakt_utistv_tall_mstamfub"		),_T("trakt_utistv_tall_gy"		),_T("trakt_utistv_tall_gy_andel"		),_T("trakt_utistv_tall_antal "	),_T("trakt_utistv_tall_antal_andel"		),_T("trakt_utistv_tall_dg"		),_T("trakt_utistv_tall_dgv"),_T(
  "trakt_utistv_gran_m3sk"		),_T("trakt_utistv_gran_m3fub"	),_T("trakt_utistv_gran_mstamsk"	),_T("trakt_utistv_gran_mstamfub"		),_T("trakt_utistv_gran_gy"		),_T("trakt_utistv_gran_gy_andel"		),_T("trakt_utistv_gran_antal "	),_T("trakt_utistv_gran_antal_andel"		),_T("trakt_utistv_gran_dg"		),_T("trakt_utistv2_dgv"),_T( 
  "trakt_utistv_bjork_m3sk"	),_T("trakt_utistv_bjork_m3fub"	),_T("trakt_utistv_bjork_mstamsk"),_T("trakt_utistv_bjork_mstamfub"	),_T("trakt_utistv_bjork_gy"	),_T("trakt_utistv_bjork_gy_andel"	),_T("trakt_utistv_bjork_antal "	),_T("trakt_utistv_bjork_antal_andel"	),_T("trakt_utistv_bjork_dg"	),_T("trakt_utistv_bjork_dgv"),_T(
  "trakt_utistv_OLov_m3sk"		),_T("trakt_utistv_OLov_m3fub"	),_T("trakt_utistv_OLov_mstamsk"	),_T("trakt_utistv_OLov_mstamfub"		),_T("trakt_utistv_OLov_gy"		),_T("trakt_utistv_OLov_gy_andel"		),_T("trakt_utistv_OLov_antal "	),_T("trakt_utistv_OLov_antal_andel"		),_T("trakt_utistv_OLov_dg"		),_T("trakt_utistv_OLov_dgv"),_T(
  "trakt_utistv_OBarr_m3sk"	),_T("trakt_utistv_OBarr_m3fub"	),_T("trakt_utistv_OBarr_mstamsk"),_T("trakt_utistv_OBarr_mstamfub"	),_T("trakt_utistv_OBarr_gy"	),_T("trakt_utistv_OBarr_gy_andel"	),_T("trakt_utistv_OBarr_antal "	),_T("trakt_utistv_OBarr_antal_andel"	),_T("trakt_utistv_OBarr_dg"	),_T("trakt_utistv_OBarr_dgv"),_T(
  "trakt_utistv_Conto_m3sk"	),_T("trakt_utistv_Conto_m3fub"	),_T("trakt_utistv_Conto_mstamsk"),_T("trakt_utistv_Conto_mstamfub"	),_T("trakt_utistv_Conto_gy"	),_T("trakt_utistv_Conto_gy_andel"	),_T("trakt_utistv_Conto_antal "	),_T("trakt_utistv_Conto_antal_andel"	),_T("trakt_utistv_Conto_dg"	),_T("trakt_utistv_Conto_dgv"),_T(
  "trakt_utistv_Torra_m3sk"	),_T("trakt_utistv_Torra_m3fub"	),_T("trakt_utistv_Torra_mstamsk"),_T("trakt_utistv_Torra_mstamfub"	),_T("trakt_utistv_Torra_gy"	),_T("trakt_utistv_Torra_gy_andel"	),_T("trakt_utistv_Torra_antal "	),_T("trakt_utistv_Torra_antal_andel"	),_T("trakt_utistv_Torra_dg"	),_T("trakt_utistv_Torra_dgv"),_T(
  "trakt_utistv_Totalt_m3sk"	),_T("trakt_utistv_Totalt_m3fub"	),_T("trakt_utistv_Totalt_mstamsk"),_T("trakt_utistv_Totalt_mstamfub"	),_T("trakt_utistv_Totalt_gy"	),_T("trakt_utistv_Totalt_gy_andel"	),_T("trakt_utistv_Totalt_antal "	),_T("trakt_utistv_Totalt_antal_andel"	),_T("trakt_utistv_Totalt_dg"	),_T("trakt_utistv_Totalt_dgv"),_T(

  "trakt_gakvot_tall"			),_T("trakt_gakvot_gran"),_T("trakt_gakvot_bjork"),_T("trakt_gakvot_Olov"),_T("trakt_gakvot_OBarr"	),_T("trakt_gakvot_Conto"),_T("trakt_gakvot_Torra"),_T("trakt_gakvot_Totalt"),_T(
  "trakt_skadade1_1"				),_T("trakt_skadade1_2"	),_T("trakt_skadade1_3"),_T("trakt_skadade1_4"),_T(
  "trakt_skadade2_1"				),_T("trakt_skadade2_2"	),_T("trakt_skadade2_3"),_T("trakt_skadade2_4"),_T(
  "trakt_skadade2_5"				),_T("trakt_skadade2_6"	),_T(
  "trakt_spar1"					),_T("trakt_spar2"		),_T("trakt_spar3"),_T(
  "trakt_stv1"						),_T("trakt_stv2"			),_T("trakt_stv3"),_T(
  "trakt_spar_over_20mha"     ),_T("trakt_date2"		),_T("trakt_huggnform"			),_T("trakt_terrf"		),
  _T("trakt_partof"     ),
  _T("trakt_defaultH25_1"),
  _T("trakt_defaultH25_2"),
  _T("trakt_defaultH25_3"),
  _T("trakt_defaultH25_4"),
  _T("trakt_defaultH25_5"),
  _T("trakt_defaultH25_6"),
  _T("trakt_defaultH25_7"),
  _T("trakt_coord")

};


BOOL CScaGallBasDB::addTrakt(TRAKT_DATA rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	int i=0;
	CString tst=_T(""),tst2=_T("");
	try
	{
		GetKeyDBQuestion(&sSQL,rec);
		if (!exists(sSQL))
		{
			tst.Format(_T("insert into %s ("),TBL_TRAKT,"(");
			for(i=0;i<NUM_OF_TRAKT_VARIABLES_TOT;i++)
			{	
				if(i==0)
				{
					tst2.Format(_T(" %s"),DBVarName[i]);					
				}
				else
					tst2.Format(_T(",%s"),DBVarName[i]);
				tst+=tst2;
			}
			//tst2.Format(_T("%s"),") values(");
			tst+=_T(") values(");
			for(i=0;i<NUM_OF_TRAKT_VARIABLES_TOT;i++)
			{
				if(i==0)
					tst2.Format(_T(":%d"),i+1);
				else
					tst2.Format(_T(",:%d"),i+1);
			  tst+=tst2;
			}
			tst+=_T(")");
			sSQL=tst;
			m_saCommand.setCommandText((SAString)sSQL);
			SetSaCommandParam(rec,0);
			m_saCommand.Prepare();
			m_saCommand.Execute();
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CScaGallBasDB::updTrakt(TRAKT_DATA rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	CString tst=_T(""),tst2=_T("");
	int i=0;
	try
	{
		GetKeyDBQuestion(&sSQL,rec);
		if (exists(sSQL))
		{
			tst.Format(_T("update %s set"),TBL_TRAKT);
			for(i=0;i<NUM_OF_TRAKT_VARIABLES_TOT;i++)
			//for(i=0;i<22;i++)
			{	
				if(i==0)
					tst2.Format(_T(" %s=:%d"),DBVarName[i],i+1);
				else
					tst2.Format(_T(",%s=:%d"),DBVarName[i],i+1);
				tst+=tst2;
			}
			tst2.Format(_T(" where trakt_forvalt=:%d and trakt_avv_id=:%d and trakt_inv_typ=:%d and trakt_forenklad=:%d"),NUM_OF_TRAKT_VARIABLES_TOT+1,NUM_OF_TRAKT_VARIABLES_TOT+2,NUM_OF_TRAKT_VARIABLES_TOT+3,NUM_OF_TRAKT_VARIABLES_TOT+4);
			tst+=tst2;
			sSQL=tst;			
			//AfxMessageBox(sSQL);
			m_saCommand.setCommandText((SAString)sSQL);
			SetSaCommandParam(rec,1);
			m_saCommand.Prepare();
			m_saCommand.Execute();	
			m_saConnection.Commit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}