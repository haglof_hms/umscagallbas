#pragma once

#ifndef _HMS_SCATRAKTPAGES_H_
#define _HMS_SCATRAKTPAGES_H_
#include "UMHmsMisc.h"
#include "MyProgressBarDlg.h"

typedef struct _max_min_data
{
	int max_fore_var[NUM_OF_FORE];
	int max_kvarv_var[NUM_OF_KVARV];
	int max_uttag_var[NUM_OF_UTTAG];

	int varIdx_fore[NUM_OF_FORE];
	int varIdx_kvarv[NUM_OF_KVARV];
	int varIdx_uttag[NUM_OF_UTTAG];

	int i;
	_max_min_data(void)
	{
		max_kvarv_var[KVARV_VARINDEX_M3SK]		=KVARV_VARINDEX_M3SK_MAX;
		max_kvarv_var[KVARV_VARINDEX_M3FUB]		=KVARV_VARINDEX_M3FUB_MAX;
		max_kvarv_var[KVARV_VARINDEX_M3STAMSK]	=KVARV_VARINDEX_M3STAMSK_MAX;
		max_kvarv_var[KVARV_VARINDEX_M3STAMFUB]=KVARV_VARINDEX_M3STAMFUB_MAX;
		max_kvarv_var[KVARV_VARINDEX_GY]			=KVARV_VARINDEX_GY_MAX;
		max_kvarv_var[KVARV_VARINDEX_ANTAL]		=KVARV_VARINDEX_ANTAL_MAX;
		max_kvarv_var[KVARV_VARINDEX_DG]			=KVARV_VARINDEX_DG_MAX;
		max_kvarv_var[KVARV_VARINDEX_HGV]		=KVARV_VARINDEX_HGV_MAX;
		max_kvarv_var[KVARV_VARINDEX_H25]		=KVARV_VARINDEX_H25_MAX;
		max_kvarv_var[KVARV_VARINDEX_DGV]		=KVARV_VARINDEX_DGV_MAX;

		max_fore_var[FORE_VARINDEX_M3SK]			=FORE_VARINDEX_M3SK_MAX;
		max_fore_var[FORE_VARINDEX_M3FUB]		=FORE_VARINDEX_M3FUB_MAX;
		max_fore_var[FORE_VARINDEX_M3STAMSK]	=FORE_VARINDEX_M3STAMSK_MAX;
		max_fore_var[FORE_VARINDEX_M3STAMFUB]	=FORE_VARINDEX_M3STAMFUB_MAX;
		max_fore_var[FORE_VARINDEX_GY]			=FORE_VARINDEX_GY_MAX;
		max_fore_var[FORE_VARINDEX_ANTAL]		=FORE_VARINDEX_ANTAL_MAX;
		max_fore_var[FORE_VARINDEX_DG]			=FORE_VARINDEX_DG_MAX;
		max_fore_var[FORE_VARINDEX_HGV]			=FORE_VARINDEX_HGV_MAX;
		max_fore_var[FORE_VARINDEX_H25]			=FORE_VARINDEX_H25_MAX;
		max_fore_var[FORE_VARINDEX_DGV]			=FORE_VARINDEX_DGV_MAX;
		
		max_uttag_var[UTTAG_VARINDEX_M3SK]		=UTTAGVARINDEX_M3SK_MAX;
		max_uttag_var[UTTAG_VARINDEX_M3FUB]		=UTTAGVARINDEX_M3FUB_MAX;
		max_uttag_var[UTTAG_VARINDEX_M3STAMSK]	=UTTAGVARINDEX_M3STAMSK_MAX;
		max_uttag_var[UTTAG_VARINDEX_M3STAMFUB]=UTTAGVARINDEX_M3STAMFUB_MAX;
		max_uttag_var[UTTAG_VARINDEX_GY]			=UTTAGVARINDEX_GY_MAX;
		max_uttag_var[UTTAG_VARINDEX_GYANDEL]	=UTTAGVARINDEX_GYANDEL_MAX;
		max_uttag_var[UTTAG_VARINDEX_ANTAL]		=UTTAGVARINDEX_ANTAL_MAX;
		max_uttag_var[UTTAG_VARINDEX_ANTALANDEL]=UTTAGVARINDEX_ANTALANDEL_MAX;
		max_uttag_var[UTTAG_VARINDEX_DG]			=UTTAGVARINDEX_DG_MAX;
		max_uttag_var[UTTAG_VARINDEX_DGV]		=UTTAGVARINDEX_DGV_MAX;

		varIdx_kvarv[1]=KVARV_VARINDEX_M3SK;
		varIdx_kvarv[2]=KVARV_VARINDEX_M3FUB;
		varIdx_kvarv[3]=KVARV_VARINDEX_M3STAMSK;
		varIdx_kvarv[4]=KVARV_VARINDEX_GY;		
		varIdx_kvarv[5]=KVARV_VARINDEX_ANTAL;	
		varIdx_kvarv[6]=KVARV_VARINDEX_DG;		
		varIdx_kvarv[7]=KVARV_VARINDEX_H25;		
		varIdx_kvarv[8]=KVARV_VARINDEX_DGV;		

		varIdx_fore[1]=FORE_VARINDEX_M3SK;
		varIdx_fore[2]=FORE_VARINDEX_M3FUB;
		varIdx_fore[3]=FORE_VARINDEX_M3STAMSK;
		varIdx_fore[4]=FORE_VARINDEX_GY;		
		varIdx_fore[5]=FORE_VARINDEX_ANTAL;	
		varIdx_fore[6]=FORE_VARINDEX_DG;		
		varIdx_fore[7]=FORE_VARINDEX_H25;		
		varIdx_fore[8]=FORE_VARINDEX_DGV;		

		
	}
} MAX_MIN_DATA;



class CMyReportPageBase : public CXTPReportView
{
	DECLARE_DYNCREATE(CMyReportPageBase);
	MAX_MIN_DATA m_nMaxValues;
public:
	int m_nVarArray[NUM_OF_KVARV-1],m_nYtArray[NUM_OF_YTVAR_TOSHOW],m_nYtVarShow[NUM_OF_YTVAR_TOSHOW],m_nReportId[4],m_nSpecArray[NUM_OF_SP+1],m_nTreeArray[NUM_OF_TREE_VAR];
	CString m_sTreeTypes[3];
	CString	m_sLangFN;
public:
	CMyReportPageBase(void);
	void ClearReport(CXTPReportControl *rep);
};

//Klass f�r Kvarvarande fliken i traktf�nstret
class CMyReportPage1 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage1);
public:
	CMyReportPage1(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
};




//Klass f�r Uttag mellan stickv�g fliken i traktf�nstret
class CMyReportPage2 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage2);

public:
	CMyReportPage2(void);
	
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};

	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
};

//Klass f�r Uttag i stickv�g fliken i traktf�nstret
class CMyReportPage3 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage3);

public:
	CMyReportPage3(void);
	
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};

	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
};

//Klass f�r Skador fliken i traktf�nstret
class CMyReportPage4 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage4);

public:
	CMyReportPage4(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};

	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
};

//Klass f�r Ytor fliken i traktf�nstret
class CMyReportPage5 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage5);

public:
	CMyReportPage5(void);
private:
	BOOL m_bDoInit;
	//int m_nShowData[]
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	//void getData(TRAKT_DATA *data);

	DECLARE_MESSAGE_MAP()
};

//Klass f�r Tr�d fliken i traktf�nstret
class CMyReportPage10 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage10);

public:
	virtual ~CMyReportPage10()
	{
		ProgressDlg.DestroyWindow();
	};
	CMyReportPage10(void);
private:
	CMyProgressBarDlg ProgressDlg;
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	//void getData(TRAKT_DATA *data);

	DECLARE_MESSAGE_MAP()
};

//Klass f�r Uttag totalt  fliken i traktf�nstret
class CMyReportPage6 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage6);

public:
	CMyReportPage6(void);
	
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};

	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
};

//Klass f�r F�re fliken i traktf�nstret
class CMyReportPage7 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage7);

public:
	CMyReportPage7(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
};


//Klass f�r den f�renklade fliken i traktf�nstret
class CMyReportPage8 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage8);

public:
	CMyReportPage8(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	DECLARE_MESSAGE_MAP()
};

//Klass f�r den f�renklade fliken f�r stickv�gar
class CMyReportPage9 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage9);

public:
	CMyReportPage9(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA data);
	void getData(TRAKT_VARIABLES *tv);
	BOOL m_bIsDirty2;
	void resetIsDirty(){m_bIsDirty2=FALSE;};
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	DECLARE_MESSAGE_MAP()
};






#endif