#include "stdafx.h"

#include "MyProgressBarDlg.h"

// CMyProgressBarDlg dialog
IMPLEMENT_DYNAMIC(CMyProgressBarDlg, CDialog)

BEGIN_MESSAGE_MAP(CMyProgressBarDlg, CDialog)
END_MESSAGE_MAP()

CMyProgressBarDlg::CMyProgressBarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyProgressBarDlg::IDD, pParent)
{
}

CMyProgressBarDlg::CMyProgressBarDlg(CString cap,CString text,CWnd* pParent /*=NULL*/)
	: CDialog(CMyProgressBarDlg::IDD, pParent)
{
	m_csCaption=cap;
	m_csText=text;


}

CMyProgressBarDlg::~CMyProgressBarDlg()
{
}

void CMyProgressBarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PROGRESSBAR_STATIC, m_wndLbl_Text);
	DDX_Control(pDX, IDC_PROGRESSBAR_STATIC2, m_wndLbl_Text2);
	DDX_Control(pDX, IDC_PROGRESSBAR, m_ProgressBar);
	
	//}}AFX_DATA_MAP

}

CProgressCtrl& CMyProgressBarDlg::GetBar()
{
	return m_ProgressBar;
}
void CMyProgressBarDlg::SetDlgTexts(CString Caption,CString Text)
{
	m_csText=Text;
	m_csCaption=Caption;
	m_wndLbl_Text.SetWindowText(m_csText);
	SetWindowText(m_csCaption);
}

void CMyProgressBarDlg::UpdateText2(CString Text)
{
	m_wndLbl_Text2.SetWindowText(Text);
}

BOOL CMyProgressBarDlg::OnInitDialog()
{

	CDialog::OnInitDialog();
	m_wndLbl_Text2.SetLblFont();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMyProgressBarDlg::OnInitialUpdate()
{
//	m_wndLbl_Text.SetWindowText(m_csText);
//	SetWindowText(m_csCaption);
}