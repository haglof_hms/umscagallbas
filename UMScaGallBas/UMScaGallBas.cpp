// UMScaGallBas.cpp : Defines the initialization routines for the DLL.
//
#include "StdAfx.h"
#include "UMScaGallBas.h"
#include "Resource.h"


#include "MDIFrameDoc.h"
#include "MDIScaGBForvaltFrame.h"
#include "MDIScaGBDistriktFrame.h"
#include "MDIScaGBTraktFrame.h"
#include "MDIScaGBTrakt2Frame.h"

#include "MDIScaGBForvaltFormView.h"
#include "MDIScaGBDistriktFormView.h"
#include "MDIScaGBTraktFormView.h"
#include "MDIScaGBTrakt2FormView.h"

#include "ScaGBForvaltSelectionList.h"
#include "ScaGBDistriktSelectionList.h"
#include "ScaGBTraktSelectList.h"

#include <afxdllx.h>

static AFX_EXTENSION_MODULE UMScaGallBasDLL = { NULL, NULL };

HINSTANCE g_hInstance = NULL;



extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMScaGallBas.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMScaGallBasDLL, hInstance))
		{

			WriteToLog(_T("Kunde inte initialisera UMScaGallbasDll"),true);
			return 0;

		}
		else
		{
			WriteToLog(_T("Initialiserade UMScaGallbasDll"),false);
		}
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMScaGallBas.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMScaGallBasDLL);
	}
	g_hInstance = hInstance;
	return 1;   // ok
}

void DLL_BUILD DoAlterTables(LPCTSTR dbname)
{
#ifdef _DEBUG
	check_if_tables_exists();
#endif
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &info)
{
	
	new CDynLinkLibrary(UMScaGallBasDLL);
	CString sReportSettingsFN;
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	CString sPath;
	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function;  051214 p�d

   WriteToLog(_T("Initialiserar UMScaGallbasDll"),false);


	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIScaGBForvaltFrame),
			RUNTIME_CLASS(CMDIScaGBForvaltFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CScaGBForvaltSelectionListFrame),
			RUNTIME_CLASS(CScaGBForvaltSelectionList)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW1,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIScaGBTraktFrame),
			RUNTIME_CLASS(CMDIScaGBTraktFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW2,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CScaGBTraktSelectListFrame),
			RUNTIME_CLASS(CScaGBTraktSelectList)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIScaGBDistriktFrame),
		RUNTIME_CLASS(CMDIScaGBDistriktFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW4,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CScaGBDistriktSelectionListFrame),
			RUNTIME_CLASS(CScaGBDistriktSelectionList)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW5,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW6,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIScaGBTrakt2Frame),
			RUNTIME_CLASS(CMDIScaGBTrakt2FormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW6,suite,sLangFN,TRUE));
	

	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);

	info.push_back(INFO_TABLE(-999,2 ,
									(TCHAR*)sLangFN.GetBuffer(),
									(TCHAR*)sVersion.GetBuffer(),
									(TCHAR*)sCopyright.GetBuffer(),
									(TCHAR*)sCompany.GetBuffer()));

	// Check if there's a connection set; 080702 p�d
	WriteToLog(_T("Kollar om DB koppling finns"),false);
	if (getIsDBConSet() == 1)
	{
		WriteToLog(_T("Checka tabeller"),false);
		// Testa om tabeller ska skapas
		DoAlterTables(_T(""));
		//check_if_tables_exists();
	}
}

BOOL check_if_tables_exists()
{
	//TCHAR sSQL[127];
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
   //DB_CONNECTION_DATA	m_dbConnectionData;
	CString S;
	CString csT=_T("");
	int nServerConn=0;
	bool check=false;

	WriteToLog(_T("Skall koppla upp f�r att kontrollera tabeller"),false);
			
	
	try
	{
		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
		//Beh�ver inte kolla s� det finns anv�ndarnamn eller l�sen om det �r windows verifiering
		GetAuthentication(&nServerConn);
		if(nServerConn)
			check=(_tcscmp(sUserName,_T("")) != 0 &&	_tcscmp(sPSW,_T("")) != 0);
		else
			check=true;
			if (_tcscmp(sLocation,_T("")) != 0 &&
				_tcscmp(sDBName,_T("")) != 0 &&
				check)
			{
				CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nServerConn,sUserName,sPSW,sLocation,sDBName))
					{
						WriteToLog(_T("Kontrollerar tabeller"),false);

						if (!pDB->existsTableInDB(sDBName,TBL_TRAKT))
						{
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(SQL_CREATE_TRAKT_TABLE);
						}

						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_TRAKT_TABLE_AVVDATUM);
						
						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_TRAKT_TABLE_HUGGNFORM);
						
						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_TRAKT_TABLE_TERRF);

						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_TRAKT_TABLE_PARTOF);

						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_TRAKT_TABLE_DEFH25);

						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_TRAKT_TABLE_COORD);
						
						CString sSQL=_T("");
						BOOL bReturn=FALSE;
						//Kolla original view existerar
						sSQL.Format(_T("select object_id('%s','V') as 'objectid'"),TBL_VIEW_ORIGINAL);
						pDB->setDefaultDatabase(sDBName);
						if (pDB->recordsetQuery(sSQL))
						{
							bReturn = (pDB->getInt(_T("objectid")) > 0);
							pDB->recordsetClose();
						}
						if(!bReturn)
						{
							pDB->commandExecute(SQL_CREATE_VIEW_ORIGINAL);
						}

						//Kolla f�renklad view existerar
						sSQL.Format(_T("select object_id('%s','V') as 'objectid'"),TBL_VIEW_FORENKLAD);
						pDB->setDefaultDatabase(sDBName);
						if (pDB->recordsetQuery(sSQL))
						{
							bReturn = (pDB->getInt(_T("objectid")) > 0);
							pDB->recordsetClose();
						}
						if(!bReturn)
						{
							pDB->commandExecute(SQL_CREATE_VIEW_FORENKLAD);
						}

						if (!pDB->existsTableInDB(sDBName,TBL_PLOT))
						{
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(SQL_CREATE_PLOT_TABLE);
						}

						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(SQL_ADD_PLOT_COLUMNS);
						

    					if (!pDB->existsTableInDB(sDBName,TBL_TREE))
						{
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(SQL_CREATE_TREE_TABLE);
						}


						if (!pDB->existsTableInDB(sDBName,TBL_DISTRIKT))
						{
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(SQL_CREATE_DISTRIKT_TABLE);
						}

						if (!pDB->existsTableInDB(sDBName,TBL_FORVALT))
						{
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(SQL_CREATE_FORVALT_TABLE);
						}
						WriteToLog(_T("Kontrollerat tabeller"),false);

					}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
					delete pDB;
				}	// if (pDB)
			}	// if (_tcscmp(sLocation,"") != 0 &&
			bReturn = TRUE;
		}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


void WriteToLog(CString message2, bool error/*=false*/)
{
	char szTime[256];
	SYSTEMTIME st;
	CStringA message(message2);

	if( !message.IsEmpty() )
	{
		// Set up timestamp prefix
		GetLocalTime(&st);
		_snprintf(szTime, sizeof(szTime), "%d-%02d-%02d %02d:%02d:%02d:\t", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	}
	else
	{
		// Insert empty row with no timestamp if message is empty
		szTime[0] = NULL;
	}
	cout << szTime;
	cout << "UMScaGallBas.dll: ";
	if( error )
	{
	cout << "[ERROR] ";
	}
	cout << message << "\n";
}
