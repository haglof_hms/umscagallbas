#include "stdafx.h"
#include "MDIScaGBTraktFormView.h"
#include "MDIScaGBTraktFrame.h"
#include "ResLangFileReader.h"
#include "HXLHandling.h"
#include "UMScaGallBas.h"
#include "Calculations.h"

IMPLEMENT_DYNCREATE(CMDIScaGBTraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIScaGBTraktFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT, OnValueChanged)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT2, OnValueChanged)
	ON_CBN_EDITCHANGE(IDC_COMBO2_2, &CMDIScaGBTraktFormView::OnCbnEditchangeCombo22)
	ON_CBN_EDITCHANGE(IDC_COMBO2_1, &CMDIScaGBTraktFormView::OnCbnEditchangeCombo21)
	ON_NOTIFY(TCN_SELCHANGE,IDC_TABCONTROL_1,OnSelectedChanged)
	ON_BN_CLICKED(IDC_BUTTON_RECALC, &CMDIScaGBTraktFormView::OnBnClickedButtonRecalc)
END_MESSAGE_MAP()

CMDIScaGBTraktFormView::CMDIScaGBTraktFormView()
	: CXTResizeFormView(CMDIScaGBTraktFormView::IDD)
{
	WriteToLog(_T("CMDIScaGBTraktFormView::CMDIScaGBTraktFormView()"),false);
	m_bConnected = FALSE;
}

CMDIScaGBTraktFormView::~CMDIScaGBTraktFormView()
{
	ProgressDlg.DestroyWindow();
}

void CMDIScaGBTraktFormView::OnClose()
{
}

BOOL CMDIScaGBTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	WriteToLog(_T("CMDIScaGBTraktFormView::PreCreateWindow 1"),false);
	if( !CXTResizeFormView::PreCreateWindow(cs) )
	{
		WriteToLog(_T("CMDIScaGBTraktFormView::PreCreateWindow 2"),true);
		return FALSE;
	}

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIScaGBTraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_STATIC2_1, m_wndLbl_Forvalt);
	DDX_Control(pDX, IDC_STATIC2_2, m_wndLbl_DrivEnh);
	DDX_Control(pDX, IDC_STATIC2_3, m_wndLbl_AvvId);
	DDX_Control(pDX, IDC_STATIC2_4, m_wndLbl_InvTyp);
	DDX_Control(pDX, IDC_STATIC2_5, m_wndLbl_Avlagg);
	DDX_Control(pDX, IDC_STATIC2_6, m_wndLbl_Distrikt);
	DDX_Control(pDX, IDC_STATIC2_7, m_wndLbl_Maskinlag);
	DDX_Control(pDX, IDC_STATIC2_8, m_wndLbl_Ursrpung);
	DDX_Control(pDX, IDC_STATIC2_9, m_wndLbl_ProdLedare);
	DDX_Control(pDX, IDC_STATIC2_10, m_wndLbl_Namn);
	DDX_Control(pDX, IDC_STATIC2_11, m_wndLbl_Datum);
	DDX_Control(pDX, IDC_STATIC2_12, m_wndLbl_Areal);
	DDX_Control(pDX, IDC_STATIC2_13, m_wndLbl_AntYtor);
	DDX_Control(pDX, IDC_STATIC2_14, m_wndLbl_SiAlder);
	DDX_Control(pDX, IDC_STATIC2_15, m_wndLbl_MedOhjd);
	DDX_Control(pDX, IDC_STATIC2_16, m_wndLbl_SiSp);
	DDX_Control(pDX, IDC_STATIC2_17, m_wndLbl_GyDirFore);
	DDX_Control(pDX, IDC_STATIC2_18, m_wndLbl_GyDirEfter);

	DDX_Control(pDX, IDC_STATIC2_19, m_wndLbl_Huggnform);
	DDX_Control(pDX, IDC_STATIC2_20, m_wndLbl_Avvdatum);
	DDX_Control(pDX, IDC_STATIC2_21, m_wndLbl_Terrf);


	DDX_Control(pDX, IDC_COMBO2_1	, m_wndCBoxForvalt);
	DDX_Control(pDX, IDC_EDIT2_2	, m_wndEditDrivEnh);
	DDX_Control(pDX, IDC_EDIT2_3	, m_wndEditAvvId);
	DDX_Control(pDX, IDC_COMBO2_3	, m_wndCBoxInvType);
	DDX_Control(pDX, IDC_EDIT2_8	, m_wndEditAvlagg);
	DDX_Control(pDX, IDC_COMBO2_2, m_wndCBoxDistrikt);
	DDX_Control(pDX, IDC_EDIT2_10	, m_wndEditMaskinlag);
	DDX_Control(pDX, IDC_EDIT2_5	, m_wndEditUrsprung);
	DDX_Control(pDX, IDC_EDIT2_7	, m_wndEditProdLedare);
	DDX_Control(pDX, IDC_EDIT2_4	, m_wndEditNamn);
	DDX_Control(pDX, IDC_EDIT2_22	, m_wndEditDatum);
	DDX_Control(pDX, IDC_EDIT2_11, m_wndEditAreal);
	DDX_Control(pDX, IDC_EDIT2_12, m_wndEditAntYtor);
	DDX_Control(pDX, IDC_EDIT2_13, m_wndEditMedOhjd);
	DDX_Control(pDX, IDC_COMBO2_4, m_wndCBoxSiSp);
	DDX_Control(pDX, IDC_EDIT2_14, m_wndEditSiTot);
	DDX_Control(pDX, IDC_EDIT2_15, m_wndEditSiAlder);
	DDX_Control(pDX, IDC_EDIT2_16, m_wndEditGyDirFore);
	DDX_Control(pDX, IDC_EDIT2_17, m_wndEditGyDirEfter);
	DDX_Control(pDX, IDC_EDIT2_18, m_wndEditNotes);

	DDX_Control(pDX, IDC_EDIT2_23	, m_wndEditDatum2);
	DDX_Control(pDX, IDC_EDIT2_19, m_wndEditHuggnform);
	DDX_Control(pDX, IDC_EDIT2_21, m_wndEditTerrf);

	//DDX_Control(pDX, IDC_COMBO_DATEPICKER_1, m_wndDatePicker);
	//DDX_Control(pDX, IDC_COMBO_DATEPICKER_3, m_wndDatePicker2);
	
	DDX_Control(pDX, IDC_GROUP2_1, m_wndGroup);
	DDX_Control(pDX, IDC_GROUP2_2, m_wndGroup2);
	DDX_Control(pDX, IDC_GROUP2_3, m_wndGroup3);
}


void CMDIScaGBTraktFormView::OnSize(UINT nType,int cx,int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl, 1, rect.top+335, rect.right - 2, rect.bottom-(rect.top+340));
}

void CMDIScaGBTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

   WriteToLog(_T("CMDIScaGBTraktFormView::OnInitialUpdate 1"),false);

	SetScaleToFitSize(CSize(90, 1));
	m_wndEditDrivEnh.SetLimitText(DRIV_ENH_MAX_LEN);
	//m_wndEditAvvId.SetLimitText(DELID_MAX_LEN);
	m_wndEditAvlagg.SetLimitText(AVLAGG_MAX_LEN);
	m_wndEditMaskinlag.SetLimitText(MASKINLAG_MAX_LEN);
	m_wndEditUrsprung.SetLimitText(URSPRUNG_MAX_LEN);
	m_wndEditProdLedare.SetLimitText(PRODLED_MAX_LEN);
	m_wndEditDatum.SetLimitText(DATUM_MAX_LEN);

	m_wndEditHuggnform.SetLimitText(HUGGNFORM_MAX_LEN);
	m_wndEditDatum2.SetLimitText(DATUM_MAX_LEN);
	m_wndEditTerrf.SetLimitText(TERRF_MAX_LEN);


	m_wndEditDrivEnh.SetEnabledColor(BLACK, WHITE );
	m_wndEditDrivEnh.SetDisabledColor(BLACK, INFOBK );
	m_wndEditAvvId.SetEnabledColor(BLACK, WHITE );
	m_wndEditAvvId.SetDisabledColor(BLACK, INFOBK );
	m_wndEditAvlagg.SetEnabledColor(BLACK, WHITE );
	m_wndEditAvlagg.SetDisabledColor(BLACK, INFOBK );
	m_wndEditMaskinlag.SetEnabledColor(BLACK, WHITE );
	m_wndEditMaskinlag.SetDisabledColor(BLACK, INFOBK );
	m_wndEditUrsprung.SetEnabledColor(BLACK, WHITE );
	m_wndEditUrsprung.SetDisabledColor(BLACK, INFOBK );
	m_wndEditProdLedare.SetEnabledColor(BLACK, WHITE );
	m_wndEditProdLedare.SetDisabledColor(BLACK, INFOBK );
	m_wndEditNamn.SetEnabledColor(BLACK, WHITE );
	m_wndEditNamn.SetDisabledColor(BLACK, INFOBK );
	m_wndEditDatum.SetEnabledColor(BLACK, WHITE );
	m_wndEditDatum.SetDisabledColor(BLACK, INFOBK );
	m_wndEditAreal.SetEnabledColor(BLACK, WHITE );
	m_wndEditAreal.SetDisabledColor(BLACK, INFOBK );
	m_wndEditAntYtor.SetEnabledColor(BLACK, WHITE );
	m_wndEditAntYtor.SetDisabledColor(BLACK, INFOBK );
	m_wndEditMedOhjd.SetEnabledColor(BLACK, WHITE );
	m_wndEditMedOhjd.SetDisabledColor(BLACK, INFOBK );
	m_wndEditSiTot.SetEnabledColor(BLACK, WHITE );
	m_wndEditSiTot.SetDisabledColor(BLACK, INFOBK );
	m_wndEditSiAlder.SetEnabledColor(BLACK, WHITE );
	m_wndEditSiAlder.SetDisabledColor(BLACK, INFOBK );
	m_wndEditGyDirFore.SetEnabledColor(BLACK, WHITE );
	m_wndEditGyDirFore.SetDisabledColor(BLACK, INFOBK );
	m_wndEditGyDirEfter.SetEnabledColor(BLACK, WHITE );
	m_wndEditGyDirEfter.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditNotes.SetEnabledColor(BLACK, WHITE );
	m_wndEditNotes.SetDisabledColor(BLACK, INFOBK );


	m_wndEditHuggnform.SetEnabledColor(BLACK, WHITE );
	m_wndEditHuggnform.SetDisabledColor(BLACK, INFOBK );
	m_wndEditDatum2.SetEnabledColor(BLACK, WHITE );
	m_wndEditDatum2.SetDisabledColor(BLACK, INFOBK );
	m_wndEditTerrf.SetEnabledColor(BLACK, WHITE );
	m_wndEditTerrf.SetDisabledColor(BLACK, INFOBK );


	m_wndCBoxForvalt.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxForvalt.SetDisabledColor(BLACK,INFOBK );
	m_wndCBoxDistrikt.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxDistrikt.SetDisabledColor(BLACK,INFOBK );
	m_wndCBoxInvType.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxInvType.SetDisabledColor(BLACK,INFOBK );
	m_wndCBoxSiSp.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxSiSp.SetDisabledColor(BLACK,INFOBK );

	if (!ProgressDlg.Create(CMyProgressBarDlg::IDD,this))
	{
		TRACE0("Failed to create progress window control.\n");
	}

	//m_wndDatePicker.setDateInComboBox();
	//m_wndDatePicker2.setDateInComboBox();

	//setAllReadOnly(FALSE,FALSE);
	setKeysReadOnly();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	WriteToLog(_T("H�mtar data fr�n DB till traktf�nster"),false);
	getForvaltsFromDB();
	addForvaltsToCBox();
	getDistriktFromDB();
	addDistriktToCBox();
	getTraktIndexFromDB();
	//getTraktsFromDB();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);	
	setLanguage();
	getInvTypesFromFile();
	addInvTypesToCBox();
	getSiSpFromFile();
	addSiSpToCBox();
	m_nDBIndex = 0;
	setupTraktTabs();
	populateData(m_nDBIndex);	
	m_bIsDirty = FALSE;
	resetIsDirty();
	m_enumAction = NOTHING;
	
	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl, 1, rect.top+335, rect.right - 2, rect.bottom-(rect.top+340));

	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	WriteToLog(_T("CMDIScaGBTraktFormView::OnInitialUpdate 2"),false);
}

BOOL CMDIScaGBTraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIScaGBTraktFormView::OnSetFocus(CWnd*)
{
	setNavigationButtons2();
	/*
	if (!m_vecTraktIndex.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}*/
}

void CMDIScaGBTraktFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Trakt information
			m_wndLbl_Forvalt.SetWindowText(xml->str(IDS_STRING7550));
			m_wndLbl_DrivEnh.SetWindowText(xml->str(IDS_STRING7551));
			m_wndLbl_AvvId.SetWindowText(xml->str(IDS_STRING7552));
			m_wndLbl_InvTyp.SetWindowText(xml->str(IDS_STRING7553));
			m_wndLbl_Avlagg.SetWindowText(xml->str(IDS_STRING7554));
			m_wndLbl_Distrikt.SetWindowText(xml->str(IDS_STRING7555));
			m_wndLbl_Maskinlag.SetWindowText(xml->str(IDS_STRING7556));
			m_wndLbl_Ursrpung.SetWindowText(xml->str(IDS_STRING7557));
			m_wndLbl_ProdLedare.SetWindowText(xml->str(IDS_STRING7558));
			m_wndLbl_Namn.SetWindowText(xml->str(IDS_STRING7559));
			m_wndLbl_Datum.SetWindowText(xml->str(IDS_STRING7560));
			m_wndLbl_Areal.SetWindowText(xml->str(IDS_STRING7561));
			m_wndLbl_AntYtor.SetWindowText(xml->str(IDS_STRING7562));
			m_wndLbl_SiAlder.SetWindowText(xml->str(IDS_STRING7565));
			m_wndLbl_MedOhjd.SetWindowText(xml->str(IDS_STRING7563));
			m_wndLbl_SiSp.SetWindowText(xml->str(IDS_STRING7564));
			m_wndLbl_GyDirFore.SetWindowText(xml->str(IDS_STRING7567));
			m_wndLbl_GyDirEfter.SetWindowText(xml->str(IDS_STRING7568));

			m_wndLbl_Huggnform.SetWindowText(xml->str(IDS_STRING7662));
			m_wndLbl_Avvdatum.SetWindowText(xml->str(IDS_STRING7663));
			m_wndLbl_Terrf.SetWindowText(xml->str(IDS_STRING7664));

			m_sErrCap = xml->str(IDS_STRING7574);
			m_sErrMsg = xml->str(IDS_STRING7620);
			m_sNotOkToSave.Format(_T("%s\n\n%s\n\n%s"),
									xml->str(IDS_STRING7621),
									xml->str(IDS_STRING7622),
									xml->str(IDS_STRING7623));
			m_sSaveData.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7624),
									xml->str(IDS_STRING7629));
			m_sUpdateTraktMsg.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7626),
									//xml->str(IDS_STRING7627),
									//xml->str(IDS_STRING7628),
									xml->str(IDS_STRING7629));
		}
		delete xml;
	}
}

void CMDIScaGBTraktFormView::showData()
{
	int idx=-1;
	//m_wndCBoxForvalt.SetItemData(0,m_recActiveTrakt.m_nTrakt_forvalt);
	idx=getForvaltIndex(m_recActiveTrakt.m_nTrakt_forvalt);
	//if(idx!=-1)
		m_wndCBoxForvalt.SetCurSel(idx);
	m_wndCBoxForvalt.SetWindowText(getForvaltSelected(m_recActiveTrakt.m_nTrakt_forvalt));
				
	//m_wndCBoxDistrikt.SetItemData(0,m_recActiveTrakt.m_nTrakt_distrikt);
	idx=getDistriktIndex(m_recActiveTrakt.m_nTrakt_distrikt);
	//if(idx!=-1)
		m_wndCBoxDistrikt.SetCurSel(idx);
	m_wndCBoxDistrikt.SetWindowText(getDistriktSelected(m_recActiveTrakt.m_nTrakt_distrikt));
	
	m_wndCBoxInvType.SetWindowText(getInvTypeSelected(m_recActiveTrakt.m_nTrakt_inv_typ));		
	//m_wndCBoxInvType.SetItemData(0,m_recActiveTrakt.m_nTrakt_inv_typ);
	m_wndCBoxInvType.SetCurSel(getInvTypeIndex(m_recActiveTrakt.m_nTrakt_inv_typ));

	m_wndCBoxSiSp.SetWindowText(getSiSpSelected(m_recActiveTrakt.m_nTrakt_si_sp));		
	//m_wndCBoxSiSp.SetItemData(0,m_recActiveTrakt.m_nTrakt_si_sp);
	m_wndCBoxSiSp.SetCurSel(getSiSpIndex(m_recActiveTrakt.m_nTrakt_si_sp));
	//m_wndEdit1.setInt(m_recActiveTrakt.m_nTrakt_id_typ);
	
	m_wndEditDrivEnh.setInt(m_recActiveTrakt.m_nTrakt_driv_enh);
	m_wndEditAvvId.SetWindowText(m_recActiveTrakt.m_sTrakt_avv_id);
	m_wndEditAvlagg.setInt(m_recActiveTrakt.m_nTrakt_avlagg);	
	m_wndEditMaskinlag.setInt(m_recActiveTrakt.m_nTrakt_maskinlag);
	m_wndEditUrsprung.setInt(m_recActiveTrakt.m_nTrakt_ursprung);
	m_wndEditProdLedare.setInt(m_recActiveTrakt.m_nTrakt_prod_led);
	m_wndEditNamn.SetWindowText(m_recActiveTrakt.m_sTrakt_namn);
	CString testdate;
	if(m_recActiveTrakt.m_sTrakt_date.GetLength() > 0)
	{
		testdate.Format(_T("%8.8s"),m_recActiveTrakt.m_sTrakt_date);
		m_wndEditDatum.SetWindowText(testdate);
	}
	//m_wndEditDatum.SetWindowText(m_recActiveTrakt.m_sTrakt_date);

	//m_wndDatePicker.SetWindowText(m_recActiveTrakt.m_sTrakt_date);
	m_wndEditAreal.setFloat(m_recActiveTrakt.m_fTrakt_areal,1);
	m_wndEditAntYtor.setFloat(m_recActiveTrakt.m_fTrakt_ant_ytor,0);
	m_wndEditMedOhjd.setFloat(m_recActiveTrakt.m_fTrakt_medel_ovre_hojd,1);
	m_wndEditSiTot.setFloat(m_recActiveTrakt.m_fTrakt_si_tot,1);
	m_wndEditSiAlder.setFloat(m_recActiveTrakt.m_fTrakt_alder,1);
	m_wndEditGyDirFore.setFloat(m_recActiveTrakt.m_fTrakt_gy_dir_fore,1);
	m_wndEditGyDirEfter.setFloat(m_recActiveTrakt.m_fTrakt_gy_dir_mal,1);
	m_wndEditNotes.SetWindowText(m_recActiveTrakt.m_sTrakt_notes);

	//m_wndDatePicker2.SetWindowText(m_recActiveTrakt.m_sTrakt_date2);
	
	if(m_recActiveTrakt.m_sTrakt_date2.GetLength() > 0)
	{
		testdate.Format(_T("%8.8s"),m_recActiveTrakt.m_sTrakt_date2);
		m_wndEditDatum2.SetWindowText(testdate);
	}
	//m_wndEditDatum2.SetWindowText(m_recActiveTrakt.m_sTrakt_date2);
	m_wndEditHuggnform.setInt(m_recActiveTrakt.m_nTrakt_huggnform);
	m_wndEditTerrf.setInt(m_recActiveTrakt.m_nTrakt_terrf);

	showTabData(m_recActiveTrakt);
}


void CMDIScaGBTraktFormView::showTabData(TRAKT_DATA data)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage7 *wndReport7;
				wndReport7 = DYNAMIC_DOWNCAST(CMyReportPage7,CWnd::FromHandle(pItem->GetHandle()));
				wndReport7->showData(data);
				break;
			case 1:
				CMyReportPage1 *wndReport1;
				wndReport1 = DYNAMIC_DOWNCAST(CMyReportPage1,CWnd::FromHandle(pItem->GetHandle()));
				wndReport1->showData(data);
				break;
			case 2:
				CMyReportPage6 *wndReport6;
				wndReport6 = DYNAMIC_DOWNCAST(CMyReportPage6,CWnd::FromHandle(pItem->GetHandle()));
				wndReport6->showData(data);
				break;
			case 3:
				CMyReportPage2 *wndReport2;
				wndReport2 = DYNAMIC_DOWNCAST(CMyReportPage2,CWnd::FromHandle(pItem->GetHandle()));
				wndReport2->showData(data);
				break;
			case 4:
				CMyReportPage3 *wndReport3;
				wndReport3 = DYNAMIC_DOWNCAST(CMyReportPage3,CWnd::FromHandle(pItem->GetHandle()));
				wndReport3->showData(data);
				break;
			case 5:
				CMyReportPage4 *wndReport4;
				wndReport4 = DYNAMIC_DOWNCAST(CMyReportPage4,CWnd::FromHandle(pItem->GetHandle()));
				wndReport4->showData(data);
				break;
			case 6:
				CMyReportPage5 *wndReport5;
				wndReport5 = DYNAMIC_DOWNCAST(CMyReportPage5,CWnd::FromHandle(pItem->GetHandle()));
				wndReport5->showData(data);
				break;
			case 7:
				MySelectedChanged(1,data);
				// Visa inte tr�d fr�n b�rjan utan n�r fliken aktiveras, se funk OnSelectedChanged
				/*
				CMyReportPage10 *wndReport10;
				wndReport10 = DYNAMIC_DOWNCAST(CMyReportPage10,CWnd::FromHandle(pItem->GetHandle()));
				wndReport10->showData(data);*/
				break;

			}			
		}
	}
}

void CMDIScaGBTraktFormView::MySelectedChanged(int show,TRAKT_DATA data)
{
	int nPage=0;

	nPage=m_wndTabControl.GetCurSel();
	switch(nPage)
	{
	case 7:
		if(!data.m_nShownTreeData || show)
		{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);		
		CMyReportPage10 *wndReport10;
		wndReport10 = DYNAMIC_DOWNCAST(CMyReportPage10,CWnd::FromHandle(pItem->GetHandle()));
		wndReport10->showData(data);
		data.m_nShownTreeData=1;
		}
		break;
	}
}

void CMDIScaGBTraktFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	MySelectedChanged(0,m_recActiveTrakt);	
}

void CMDIScaGBTraktFormView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	
	/*
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	int idx=m_nDBIndex;
	RLFReader *xml = new RLFReader;
	CMDIScaGBTraktFrame *pView = (CMDIScaGBTraktFrame *)getFormViewByID(IDD_FORMVIEW2)->GetParent();
	if (xml->Load(m_sLangFN))
		tst3=_T(xml->str(IDS_STRING752));
	tst+=tst3;
	
	if (!m_vecTraktIndex.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktIndex.size())
	{
		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(" (%d/%d)",m_nDBIndex+1,m_vecTraktIndex.size());
				tst+=tst2;
				pDoc->SetTitle(tst);			
			}
		}
	}
	else
	{
		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(" (%d/%d)",0,0);
				tst+=tst2;
				pDoc->SetTitle(tst);						
			}
		}
	}*/
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CMDIScaGBTraktFormView::populateData(UINT idx)
{
	DISTRIKT_DATA data;
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	RLFReader *xml = new RLFReader;
	CMDIScaGBTraktFrame *pView = (CMDIScaGBTraktFrame *)getFormViewByID(IDD_FORMVIEW2)->GetParent();

	if (xml->Load(m_sLangFN))
		tst3=xml->str(IDS_STRING752);
	
	if (!m_vecTraktIndex.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktIndex.size())
	{
	//m_recActiveTrakt = m_vecTraktData[idx];
		LoadActiveTrakt(idx);
		showData();
		if (pView)
		{
			CXTResizeGroupBox * grp= (CXTResizeGroupBox *)GetDlgItem(IDC_GROUP2_1);
   		tst2.Format(_T(" (%d/%d)"),m_nDBIndex+1,m_vecTraktIndex.size());
			tst.Format(_T("%s%s"),tst3,tst2);
			grp->SetWindowText(tst);
			/*CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				CXTResizeGroupBox * grp= GetDlgItem(IDD_FORMVIEW2,IDC_GROUP2_1);
				tst2.Format(_T(" (%d/%d)"),m_nDBIndex+1,m_vecTraktIndex.size());
				tst.Format(_T("%s%s"),tst3,tst2);
				pDoc->SetTitle(tst);		
			}*/
		}
	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setAllReadOnly( TRUE, TRUE );
		clearAll();
		if (pView)
		{
			CXTResizeGroupBox * grp= (CXTResizeGroupBox *)GetDlgItem(IDC_GROUP2_1);
			grp->SetWindowText(_T("0/0"));
			/*CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),0,0);
				tst+=tst2;
				pDoc->SetTitle(tst);						
			}*/
		}
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIScaGBTraktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{

	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			m_enumAction = MANUALLY;
			addTraktManually();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			m_enumAction = FROM_FILE;
			addTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			if (getIsDirty())
				saveTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			if (getIsDirty())
				isDataChanged();
			else
				deleteTrakt();
			//resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (m_nDBIndex != 0)
			{
				if (!isDataChanged())
				{
					m_nDBIndex = 0;
					setNavigationButtons2();
					//setNavigationButtons(FALSE,TRUE);
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			if (m_nDBIndex != 0)
			{
				if (!isDataChanged())
				{
					m_nDBIndex--;
					if (m_nDBIndex < 0)
						m_nDBIndex = 0;
					setNavigationButtons2();
					/*
					if (m_nDBIndex == 0)
					{
						setNavigationButtons(FALSE,TRUE);
					}
					else
					{
						setNavigationButtons(TRUE,TRUE);
					}*/
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			if (m_nDBIndex != ((int)m_vecTraktIndex.size() - 1))
			{
				if (!isDataChanged())
				{
					m_nDBIndex++;
					if (m_nDBIndex > ((int)m_vecTraktIndex.size() - 1))
						m_nDBIndex = (int)m_vecTraktIndex.size() - 1;
					setNavigationButtons2();
					/*
					if (m_nDBIndex == (UINT)m_vecTraktIndex.size() - 1)
					{
						setNavigationButtons(TRUE,FALSE);
					}
					else
					{
						setNavigationButtons(TRUE,TRUE);
					}*/
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			if (m_nDBIndex != ((int)m_vecTraktIndex.size() - 1))
			{
				if (!isDataChanged())
				{
					m_nDBIndex = (int)m_vecTraktIndex.size()-1;
					setNavigationButtons2();
					//setNavigationButtons(TRUE,FALSE);	
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}	// case ID_NEW_ITEM :
		case ID_PREVIEW_ITEM :
		{
			/*if(!m_vecTraktIndex.empty())
			{
				makeDummyReport();
			}*/
		}
	};
	return 0L;
}

void CMDIScaGBTraktFormView::makeDummyReport()
{
	/*CString csReport=_T(""),csLang=_T(""),csArgs=_T("GruppId - object_id=1;");
	void *ddd=0;
	csReport.Format(_T("%s"),_T("C:\\HMS_5205.rpt"));
	//Documents and Settings\\jonorn-6.HAGLOF\\Mina dokument\\HMS Intr�ng - Rapporter\\F�rdiga Rapporter\\Intr�ngsv�rdering\\
   //csReport.Format(_T("%s"),_T("C:\\Program\\Haglof Sweden AB\\Haglof Management Systems\\Reports\\SVE\\HMS_5000.rpt"));
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(333, 
		_T("OpenSuiteEx"), 
		_T("Reports2.dll"),
		csReport,
		_T(""),
		csArgs,
		ddd));*/

/*
int m_nIndex; // See ShellData <ExecItem> or <ReportItem>
TCHAR m_szFunc[128]; // See ShellData <ExecItem> or <ReportItem>
TCHAR m_szSuite[128]; // See ShellData <ExecItem> or <ReportItem>
TCHAR m_szName[128]; // See ShellData <ExecItem> or <ReportItem>
TCHAR m_szFileName[MAX_PATH]; // See ShellData <ExecItem> or <ReportItem>
TCHAR m_szArgStr[2048]; // Can containe a string of arguments, added to a
								// reports "variables". E.g. arg1;arg2;arg3;etc..; 070207 p�d
void *m_pBuf; // void* for generic use*/


	/*
   CString csReport=_T(""),csLang=_T(""),csArgs=_T("");
	csLang.Format("%s",getLangSet());
	csReport.Format(_T("%s\%s\\ScaTestRapportSVE.rpt"), getReportsDir(), csLang);
	if(!fileExists(csReport))
	{
		csReport.Format(_T("%s\\ENU\\ScaTestRapportENU.rpt"), getReportsDir());
		
	}
	//csArgs.Format("%d;%d;%d;%d;",m_vecTraktIndex[m_nDBIndex].m_nTrakt_forvalt,m_vecTraktIndex[m_nDBIndex].m_nTrakt_driv_enh,m_vecTraktIndex[m_nDBIndex].m_nTrakt_del_id,m_vecTraktIndex[m_nDBIndex].m_nTrakt_inv_typ);
	//csArgs.Format("%d;;;;",m_vecTraktIndex[m_nDBIndex].m_nTrakt_forvalt);
	// tell the report-suite to open the report
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(333, 
		"OpenSuiteEx", 
		"Reports2.dll",
		csReport,
		"",
		csArgs));	*/
}


void CMDIScaGBTraktFormView::getTraktIndexFromDB(void)
{
	if (m_bConnected)
	{
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTraktIndex(m_vecTraktIndex,0);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTraktFormView::LoadActiveTrakt(int idx)
{
TRAKT_INDEX trkt_idx=m_vecTraktIndex[idx];
	if (m_bConnected)
	{
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakt(trkt_idx,m_recActiveTrakt);
			m_recActiveTrakt.m_nShownTreeData=0;
			pDB->getPlots(m_recActiveTrakt);
			pDB->getTrees(m_recActiveTrakt);
			m_structTraktIdentifer.m_nTrakt_forvalt	= m_recActiveTrakt.m_nTrakt_forvalt;
			m_structTraktIdentifer.m_nTrakt_driv_enh	= m_recActiveTrakt.m_nTrakt_driv_enh;
			m_structTraktIdentifer.m_sTrakt_avv_id		= m_recActiveTrakt.m_sTrakt_avv_id;
			m_structTraktIdentifer.m_nTrakt_inv_typ	= m_recActiveTrakt.m_nTrakt_inv_typ;		
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}




void CMDIScaGBTraktFormView::getForvaltsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getForvalts(m_vecForvaltData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTraktFormView::getDistriktFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistrikts(m_vecDistriktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTraktFormView::getInvTypesFromFile(void)
{
	if (fileExists(m_sLangFN))
	{
		m_vecInvTypeData.clear();
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		m_vecInvTypeData.push_back(_inv_type_data(0,xml->str(IDS_STRING7610)));
		m_vecInvTypeData.push_back(_inv_type_data(1,xml->str(IDS_STRING7611)));
		m_vecInvTypeData.push_back(_inv_type_data(2,xml->str(IDS_STRING7612)));
		}
	}
}

void CMDIScaGBTraktFormView::getSiSpFromFile(void)
{
	if (fileExists(m_sLangFN))
	{
		m_vecSiSpData.clear();
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		m_vecSiSpData.push_back(_inv_type_data(1,xml->str(IDS_STRING7600)));
		m_vecSiSpData.push_back(_inv_type_data(2,xml->str(IDS_STRING7601)));
		m_vecSiSpData.push_back(_inv_type_data(3,xml->str(IDS_STRING7609)));
		}
	}
}

void CMDIScaGBTraktFormView::addTraktManually(void)
{
	setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d
	TRAKT_DATA fileTrakt= TRAKT_DATA();
	m_recActiveTrakt=fileTrakt;
	clearAll();
	//m_wndListMachineBtn.EnableWindow( TRUE );
	m_bIsDirty = TRUE;
}

void CMDIScaGBTraktFormView::addTrakt(void)
{
	unsigned int i=0;
	int count=0;
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	RLFReader *xml = new RLFReader;
	CMDIScaGBTraktFrame *pView = (CMDIScaGBTraktFrame *)getFormViewByID(IDD_FORMVIEW2)->GetParent();
	CFileDialog fdlg(true,NULL,NULL,OFN_ENABLESIZING | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_ALLOWMULTISELECT,HXL_FILE_FILTER,this ,0);
	CString sFileName=_T(""),buf_filename;
	TRAKT_DATA fileTrakt=TRAKT_DATA();		

	fdlg.GetOFN().lpstrFile = buf_filename.GetBuffer(2048*(_MAX_PATH + 1) +1);
	fdlg.GetOFN().nMaxFile = 2048;

	if( fdlg.DoModal ()==IDOK )
	{		
		count=0;

		POSITION pos = fdlg.GetStartPosition();
		while(pos)
		{
			fdlg.GetNextPathName(pos);
			count++;
		}
		if(count>1)
		{
			Batch_Open_Several_Files(&fdlg,count);
		}
		else
		{
			if(count>0 && count ==1)
			{
				pos = fdlg.GetStartPosition();
				sFileName = fdlg.GetNextPathName(pos);
				if(HXL_FileOpen(sFileName.GetBuffer())==0)
				{
					HXL_MakeData(&fileTrakt);				
				}
				else
				{
					AfxMessageBox(_T("Lyckades ej l�sa fil!"));
					return;
				}
			}		
			setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d
			//clearAll();
			m_recActiveTrakt.m_vPlotData.clear();
			m_recActiveTrakt.m_vTreeData.clear();
			m_recActiveTrakt=fileTrakt;
			m_recActiveTrakt.m_nTrakt_enter_type=0;
			m_bIsDirty = TRUE;
			if (xml->Load(m_sLangFN))
				tst3=xml->str(IDS_STRING752);
			tst+=tst3;
			if (pView)
			{
				CDocument *pDoc = pView->GetActiveDocument();
				if (pDoc != NULL)
				{
					tst2.Format(_T(" (Ny Trakt)"));
					tst+=tst2;
					pDoc->SetTitle(tst);			
				}
			}
			//Check and correct wrong plot numbers
			CheckAndCorrectPlotNumbers(&m_recActiveTrakt);
			showData();
			//m_wndListMachineBtn.EnableWindow( TRUE );
		}

	}
	buf_filename.ReleaseBuffer();


}

// PUBLIC
int CMDIScaGBTraktFormView::saveTrakt(void)
{
	int nSize,nRet=0;
	CString cs_Update=_T(""),cs_Msg=_T("");
	CString sForvalt1=_T(""),sDrivEnh1=_T(""),sAvvId1=_T(""),sInvTyp1=_T(""),sAreal1=_T("");
	CString sForvalt=_T(""),sDrivEnh=_T(""),sAvvId=_T(""),sInvTyp=_T(""),sAreal=_T(""),sMsg=_T("");

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sForvalt1.Format(_T("%-15.15s"),xml->str(IDS_STRING7550));
			sDrivEnh1.Format(_T("%-15.15s"),xml->str(IDS_STRING7551));
			sAvvId1.Format(_T("%-15.15s"),xml->str(IDS_STRING7552));
			sInvTyp1.Format(_T("%-15.15s"),xml->str(IDS_STRING7553));
			sAreal1.Format(_T("%-15.15s"),xml->str(IDS_STRING7561));
		}
	}

	if(m_recActiveTrakt.m_nTrakt_forvalt==MY_NULL)
		sForvalt=_T("");
	else
		sForvalt.Format(_T("%d"),m_recActiveTrakt.m_nTrakt_forvalt);

	if(m_recActiveTrakt.m_nTrakt_driv_enh==MY_NULL)
		sDrivEnh=_T("");
	else
		sDrivEnh.Format(_T("%d"),m_recActiveTrakt.m_nTrakt_driv_enh);

	sAvvId = m_recActiveTrakt.m_sTrakt_avv_id;

	if(m_recActiveTrakt.m_nTrakt_inv_typ==MY_NULL)
		sInvTyp=_T("");
	else
		sInvTyp=getInvTypeSelected(m_recActiveTrakt.m_nTrakt_inv_typ);

	if(m_recActiveTrakt.m_fTrakt_areal==MY_NULL)
		sAreal=_T("");
	else
		sAreal.Format(_T("%.1f"),m_recActiveTrakt.m_fTrakt_areal);


	cs_Msg.Format(_T("%s\n\n%-15.15s : %s \n%-15.15s : %s \n%-15.15s : %s \n%-15.15s : %s\n%-15.15s : %s\n\n"),
		m_sUpdateTraktMsg,
		sForvalt1,sForvalt,
		sDrivEnh1,sDrivEnh,
		sAvvId1,sAvvId,
		sInvTyp1,sInvTyp,
		sAreal1,sAreal);






	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			::SetCursor(::LoadCursor(NULL,IDC_WAIT));
			getEnteredData();
			// Setup index information; 061207 p�d			
			if (!pDB->addTrakt( m_recNewTrakt ))
			{				
				if ((::MessageBox(0,cs_Msg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
				{
					pDB->updTrakt( m_recNewTrakt );
					m_structTraktIdentifer.m_nTrakt_forvalt	= m_recNewTrakt.m_nTrakt_forvalt;
					m_structTraktIdentifer.m_nTrakt_driv_enh	= m_recNewTrakt.m_nTrakt_driv_enh;
					m_structTraktIdentifer.m_sTrakt_avv_id		= m_recNewTrakt.m_sTrakt_avv_id;
					m_structTraktIdentifer.m_nTrakt_inv_typ	= m_recNewTrakt.m_nTrakt_inv_typ;		
					if(m_recNewTrakt.m_vPlotData.size()>0)
					{
						pDB->delPlot(m_recNewTrakt);
						for(unsigned int pi=0;pi<m_recNewTrakt.m_vPlotData.size();pi++)
						{
							if(!pDB->addPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]))
								pDB->updPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]);
						}
					}

					if(m_recNewTrakt.m_vTreeData.size()>0)
					{

						nSize=m_recNewTrakt.m_vTreeData.size();
						
						ProgressDlg.GetBar().SetRange(0,nSize);
						ProgressDlg.GetBar().SetStep(1);
						ProgressDlg.GetBar().SetPos(0);
						ProgressDlg.ShowWindow(SW_NORMAL);
						ProgressDlg.SetDlgTexts(_T("Status"),_T("Sparar tr�d"));

						for(unsigned int ti=0;ti<m_recNewTrakt.m_vTreeData.size();ti++)
						{
							if(!pDB->addTree(m_recNewTrakt,m_recNewTrakt.m_vTreeData[ti]))
								pDB->updTree(m_recNewTrakt,m_recNewTrakt.m_vTreeData[ti]);

							ProgressDlg.GetBar().StepIt();
							cs_Update.Format(_T("%d/%d"),ti,nSize);
							ProgressDlg.UpdateText2(cs_Update);
						}
						ProgressDlg.ShowWindow(SW_HIDE);
					}
					nRet=2;
				}
				else 
					nRet=3;
			}
			else
			{
				if(m_recNewTrakt.m_vPlotData.size()>0)
				{
					pDB->delPlot(m_recNewTrakt);
					for(unsigned int pi=0;pi<m_recNewTrakt.m_vPlotData.size();pi++)
						if(!pDB->addPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]))
							pDB->updPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]);
				}

				if(m_recNewTrakt.m_vTreeData.size()>0)
				{
					for(unsigned int ti=0;ti<m_recNewTrakt.m_vTreeData.size();ti++)
						if(!pDB->addTree(m_recNewTrakt,m_recNewTrakt.m_vTreeData[ti]))
							pDB->updTree(m_recNewTrakt,m_recNewTrakt.m_vTreeData[ti]);
				}
				m_structTraktIdentifer.m_nTrakt_forvalt	= m_recNewTrakt.m_nTrakt_forvalt;
				m_structTraktIdentifer.m_nTrakt_driv_enh	= m_recNewTrakt.m_nTrakt_driv_enh;
				m_structTraktIdentifer.m_sTrakt_avv_id		= m_recNewTrakt.m_sTrakt_avv_id;
				m_structTraktIdentifer.m_nTrakt_inv_typ	= m_recNewTrakt.m_nTrakt_inv_typ;		
				nRet=1;
			}
			::SetCursor(::LoadCursor(NULL,IDC_ARROW));
			delete pDB;
		}	// if (pDB != NULL)
		else
			nRet=0;
		resetIsDirty();
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
		m_enumAction = NOTHING;
	}	// if (isOKToSave())
	else
		nRet=0;
	return nRet;
}

void CMDIScaGBTraktFormView::deleteTrakt(void)
{
	TRAKT_DATA dataTrakt;
	CString sMsg=_T(""),tmp=_T("");
	CString sText1;	//	Ta bort trakt
	CString sText2;	// OBS! Om trakt tas bort, kommer all underliggande information att tas bort
	CString sText3;	// Dvs. att alla Ytor raderas
	CString sText4;	// Skall denna trakt tas bort ... ?
	CString sForvalt1,sDrivEnh1,sAvvId1,sInvTyp1,sAvlagg1,sDistrikt1,sMaskin1,sUrsprung1,sProdLed1,sNamn1,sDate1,sAreal1,sAntYt1;
	CString sForvalt,sDrivEnh,sAvvId,sInvTyp,sAvlagg,sDistrikt,sMaskin,sUrsprung,sProdLed,sNamn,sDate,sAreal,sAntYt;
	CString sDate21,sHuggnform1,sTerrf1;
	CString sDate2,sHuggnform,sTerrf;
	CString sCaption;	// Meddelande
	CString sOKBtn;	// Ta bort
	CString sCancelBtn;// Avbryt
	BOOL bDidDel=FALSE;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING7570);			sText2 = xml->str(IDS_STRING7571);
			sText3 = xml->str(IDS_STRING7572);			sText4 = xml->str(IDS_STRING7573);
			sForvalt1.Format(_T("%-15.15s"),xml->str(IDS_STRING7550));
			sDrivEnh1.Format(_T("%-15.15s"),xml->str(IDS_STRING7551));
			sAvvId1.Format(_T("%-15.15s"),xml->str(IDS_STRING7552));
			sInvTyp1.Format(_T("%-15.15s"),xml->str(IDS_STRING7553));
			sAvlagg1.Format(_T("%-15.15s"),xml->str(IDS_STRING7554));
			sDistrikt1.Format(_T("%-15.15s"),xml->str(IDS_STRING7555));
			sMaskin1.Format(_T("%-15.15s"),xml->str(IDS_STRING7556));
			sUrsprung1.Format(_T("%-15.15s"),xml->str(IDS_STRING7557));
			sProdLed1.Format(_T("%-15.15s"),xml->str(IDS_STRING7558));
			sNamn1.Format(_T("%-15.15s"),xml->str(IDS_STRING7559));
			sDate1.Format(_T("%-15.15s"),xml->str(IDS_STRING7560));
			sAreal1.Format(_T("%-15.15s"),xml->str(IDS_STRING7561));
			sAntYt1.Format(_T("%-15.15s"),xml->str(IDS_STRING7562));
			sHuggnform1.Format(_T("%-15.15s"),xml->str(IDS_STRING7662));
			sDate21.Format(_T("%-15.15s"),xml->str(IDS_STRING7663));
			sTerrf1.Format(_T("%-15.15s"),xml->str(IDS_STRING7664));
			sCaption.Format(_T("%s"),xml->str(IDS_STRING7574));
			sOKBtn = xml->str(IDS_STRING7575);
			sCancelBtn = xml->str(IDS_STRING7576);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktIndex.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					dataTrakt = m_recActiveTrakt;
					// Setup a message for user upon deleting 
					if(dataTrakt.m_nTrakt_forvalt==MY_NULL)
						sForvalt=_T("");
					else
						sForvalt.Format(_T("%d"),dataTrakt.m_nTrakt_forvalt);

					if(dataTrakt.m_nTrakt_driv_enh==MY_NULL)
						sDrivEnh=_T("");
					else
						sDrivEnh.Format(_T("%d"),dataTrakt.m_nTrakt_driv_enh);

					sAvvId = dataTrakt.m_sTrakt_avv_id;

					if(dataTrakt.m_nTrakt_inv_typ==MY_NULL)
						sInvTyp=_T("");
					else
						sInvTyp=getInvTypeSelected(dataTrakt.m_nTrakt_inv_typ);

					if(dataTrakt.m_nTrakt_avlagg==MY_NULL)
						sAvlagg=_T("");
					else
						sAvlagg.Format(_T("%d"),dataTrakt.m_nTrakt_avlagg);

					if(dataTrakt.m_nTrakt_distrikt==MY_NULL)
						sDistrikt=_T("");
					else
						sDistrikt.Format(_T("%d"),dataTrakt.m_nTrakt_distrikt);

					if(dataTrakt.m_nTrakt_maskinlag==MY_NULL)
						sMaskin=_T("");
					else
						sMaskin.Format(_T("%d"),dataTrakt.m_nTrakt_maskinlag);	

					if(dataTrakt.m_nTrakt_ursprung==MY_NULL)
						sUrsprung=_T("");
					else
						sUrsprung.Format(_T("%d"),dataTrakt.m_nTrakt_ursprung);


					if(dataTrakt.m_nTrakt_prod_led==MY_NULL)
						sDistrikt=_T("");
					else
						sProdLed.Format(_T("%d"),dataTrakt.m_nTrakt_prod_led);	

					sNamn=dataTrakt.m_sTrakt_namn;	
					sDate=dataTrakt.m_sTrakt_date;
					sDate2=dataTrakt.m_sTrakt_date2;

					if(dataTrakt.m_fTrakt_areal==MY_NULL)
						sAreal=_T("");
					else
						sAreal.Format(_T("%.1f"),dataTrakt.m_fTrakt_areal);	

					if(dataTrakt.m_fTrakt_ant_ytor==MY_NULL)
						sAntYt=_T("");
					else
						sAntYt.Format(_T("%.0f"),dataTrakt.m_fTrakt_ant_ytor);	


					if(dataTrakt.m_nTrakt_huggnform==MY_NULL)
						sHuggnform=_T("");
					else
						sHuggnform.Format(_T("%d"),dataTrakt.m_nTrakt_huggnform);	

					if(dataTrakt.m_nTrakt_terrf==MY_NULL)
						sTerrf=_T("");
					else
						sTerrf.Format(_T("%d"),dataTrakt.m_nTrakt_terrf);	

					sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><br>%-15.15s : <b>%s</b><hr><br><center>%s</center><br>"),
						sText1,
						sForvalt1,sForvalt,
						sDrivEnh1,sDrivEnh,
						sAvvId1,sAvvId,
						sInvTyp1,sInvTyp,
						sAvlagg1,sAvlagg,
						sDistrikt1,sDistrikt,
						sMaskin1,sMaskin,
						sUrsprung1,sUrsprung,
						sProdLed1,sProdLed,
						sNamn1,sNamn,
						sDate1,sDate,
						sHuggnform1,sHuggnform,
						sTerrf1,sTerrf,
						sDate21,sDate2,
						sAreal1,sAreal,
						sAntYt1,sAntYt,
						sText4);
				
					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						::SetCursor(::LoadCursor(NULL,IDC_WAIT));
						pDB->delTrakt(dataTrakt);
						bDidDel=TRUE;
						::SetCursor(::LoadCursor(NULL,IDC_ARROW));
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	if(bDidDel==TRUE)
		resetTrakt(RESET_TO_LAST_SET_NB);
	else
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
}

// Get manually entered data; 061011 p�d
void CMDIScaGBTraktFormView::getEnteredData(void)
{
	int nTabPages=0,nPage=0;
	unsigned int i=0;
	CString tst=_T("");
	m_recNewTrakt=m_recActiveTrakt;
	/*
	for(i=0;i<m_recActiveTrakt.m_vPlotData.size();i++)
	{
		m_recNewTrakt.m_vPlotData.push_back(m_recActiveTrakt.m_vPlotData[i]);
	}*/	
	CString csStr,sDate=_T(""),sDate2=_T("");
	// Identification information
	m_recNewTrakt.m_nTrakt_forvalt			= getForvaltID();
	m_recNewTrakt.m_nTrakt_driv_enh			= m_wndEditDrivEnh.getInt();
	m_recNewTrakt.m_sTrakt_avv_id				= m_wndEditAvvId.getText();
	m_recNewTrakt.m_nTrakt_inv_typ			= getInvTypeID();
	
	m_recNewTrakt.m_nTrakt_avlagg				= m_wndEditAvlagg.getInt();
	m_recNewTrakt.m_nTrakt_distrikt			= getDistriktID();
	//m_recNewTrakt.m_nTrakt_distrikt		= m_wndEditDistrikt.getInt();
	m_recNewTrakt.m_nTrakt_maskinlag			= m_wndEditMaskinlag.getInt();
	m_recNewTrakt.m_nTrakt_ursprung			= m_wndEditUrsprung.getInt();
	m_recNewTrakt.m_nTrakt_prod_led			= m_wndEditProdLedare.getInt();
	m_recNewTrakt.m_sTrakt_namn				= m_wndEditNamn.getText();
		
	if(m_wndEditDatum.getText().GetLength() > 0)
		m_recNewTrakt.m_sTrakt_date.Format(_T("%8.8s"),m_wndEditDatum.getText());
	else
		m_recNewTrakt.m_sTrakt_date = _T("");
	//m_wndDatePicker.GetWindowText(sDate);
	//m_wndDatePicker2.GetWindowText(sDate2);
	//m_recNewTrakt.m_sTrakt_date				= sDate;
	//m_recNewTrakt.m_sTrakt_date2				= sDate2;
	m_recNewTrakt.m_fTrakt_areal				= m_wndEditAreal.getFloat();
	m_recNewTrakt.m_fTrakt_ant_ytor			= m_wndEditAntYtor.getInt();
	m_recNewTrakt.m_fTrakt_medel_ovre_hojd	= m_wndEditMedOhjd.getFloat();
	m_recNewTrakt.m_fTrakt_alder				= m_wndEditSiAlder.getFloat();
	m_recNewTrakt.m_fTrakt_si_tot				= m_wndEditSiTot.getFloat();
	m_recNewTrakt.m_nTrakt_si_sp				= getSiSpID();
	m_recNewTrakt.m_fTrakt_gy_dir_fore		= m_wndEditGyDirFore.getFloat();
	m_recNewTrakt.m_fTrakt_gy_dir_mal		= m_wndEditGyDirEfter.getFloat();
	m_recNewTrakt.m_sTrakt_notes				= m_wndEditNotes.getText();
	
	if(m_wndEditDatum2.getText().GetLength() > 0)
		m_recNewTrakt.m_sTrakt_date2.Format(_T("%8.8s"),m_wndEditDatum2.getText());
	else
		m_recNewTrakt.m_sTrakt_date2 = _T("");
	//m_recNewTrakt.m_sTrakt_date2				= m_wndEditDatum2.getText();
	m_recNewTrakt.m_nTrakt_huggnform			=  m_wndEditHuggnform.getInt();
	m_recNewTrakt.m_nTrakt_terrf				=  m_wndEditTerrf.getInt();

	nTabPages=m_wndTabControl.getNumOfTabPages();
	for(nPage = 0; nPage<nTabPages; nPage++)
	{		
		//H�mta flik
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage7 *wndReport7;
				wndReport7 = DYNAMIC_DOWNCAST(CMyReportPage7,CWnd::FromHandle(pItem->GetHandle()));
				wndReport7->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;
			case 1:
				CMyReportPage1 *wndReport1;
				wndReport1 = DYNAMIC_DOWNCAST(CMyReportPage1,CWnd::FromHandle(pItem->GetHandle()));
				wndReport1->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;
			case 2:
				CMyReportPage6 *wndReport6;
				wndReport6 = DYNAMIC_DOWNCAST(CMyReportPage6,CWnd::FromHandle(pItem->GetHandle()));
				wndReport6->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;
			case 3:
				CMyReportPage2 *wndReport2;
				wndReport2 = DYNAMIC_DOWNCAST(CMyReportPage2,CWnd::FromHandle(pItem->GetHandle()));
				wndReport2->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;
			case 4:
				CMyReportPage3 *wndReport3;
				wndReport3 = DYNAMIC_DOWNCAST(CMyReportPage3,CWnd::FromHandle(pItem->GetHandle()));
				wndReport3->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;
			case 5:
				CMyReportPage4 *wndReport4;
				wndReport4 = DYNAMIC_DOWNCAST(CMyReportPage4,CWnd::FromHandle(pItem->GetHandle()));
				wndReport4->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;
				//Beh�ver aldrig h�mta ytdata d� den aldrig editeras
				/*
			case 6:
				CMyReportPage5 *wndReport5;
				wndReport5 = DYNAMIC_DOWNCAST(CMyReportPage5,CWnd::FromHandle(pItem->GetHandle()));
				wndReport5->getData(&m_recNewTrakt);
				break;*/
			}	
		}
	}
	// Setup who data's entered, Maually or by file; 061201 p�d
	if (m_enumAction == MANUALLY)
		m_recNewTrakt.m_nTrakt_enter_type = 1;
	//Variabel som s�tter om stor eller f�renklad modell anv�nts = stor
	m_recNewTrakt.m_nTrakt_forenklad= 0;
	//	csStr.Format("%5.1f",m_recNewTrakt.m_recData.m_recVars.m_Skadade_Trad);
	//	AfxMessageBox(csStr);
	// Trakt information
	//CString sDate;
	//m_wndDatePicker.GetWindowText(sDate);
	//m_recNewTrakt.m_sTraktDate		= sDate;
	// Check RegionID and DistriktID. If they eq. -1 then
	// Set to m_recActiveTrakt values; 061011 p�d
	/*
	if (m_recNewMachine.m_nRegionID == -1 && m_recNewMachine.m_nDistriktID == -1)
	{
		m_recNewMachine.m_nRegionID = m_recActiveTrakt.m_nRegionID;
		m_recNewMachine.m_nDistriktID = m_recActiveTrakt.m_nDistriktID;
	}*/
}

// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/Distrikt is entered
// using a Dialog; 061011 p�d
void CMDIScaGBTraktFormView::clearAll(void)
{
	m_wndEditDrivEnh.SetWindowText(_T(""));
	m_wndEditAvvId.SetWindowText(_T(""));
	m_wndEditAvlagg.SetWindowText(_T(""));
	m_wndEditMaskinlag.SetWindowText(_T(""));
	m_wndEditUrsprung.SetWindowText(_T(""));
	m_wndEditProdLedare.SetWindowText(_T(""));
	m_wndEditNamn.SetWindowText(_T(""));
	m_wndEditDatum.SetWindowText(_T(""));
	//m_wndDatePicker.SetWindowText(_T(""));
	//m_wndDatePicker2.SetWindowText(_T(""));
	m_wndEditAreal.SetWindowText(_T(""));
	m_wndEditAntYtor.SetWindowText(_T(""));
	m_wndEditMedOhjd.SetWindowText(_T(""));
	m_wndEditSiTot.SetWindowText(_T(""));
	m_wndEditSiAlder.SetWindowText(_T(""));
	m_wndEditGyDirFore.SetWindowText(_T(""));
	m_wndEditGyDirEfter.SetWindowText(_T(""));
	m_wndEditNotes.SetWindowText(_T(""));
	m_wndEditDatum2.SetWindowText(_T(""));
	m_wndEditHuggnform.SetWindowText(_T(""));
	m_wndEditTerrf.SetWindowText(_T(""));

	m_wndCBoxForvalt.SetCurSel(-1);
	m_wndCBoxDistrikt.SetCurSel(-1);
	m_wndCBoxInvType.SetCurSel(-1);
	m_wndCBoxSiSp.SetCurSel(-1);
	CTraktVariables data = CTraktVariables();
	TRAKT_DATA data2 = _trakt_data();
	data2.m_recData=data;
	showTabData(data2);
	//m_wndDatePicker.SetWindowText(_T(""));
	// Set m_recNewTrakt key-values to -1
	SetKeyValuesToNegative(m_recNewTrakt);
}

void CMDIScaGBTraktFormView::SetKeyValuesToNegative(TRAKT_DATA rec)
{
	rec.m_nTrakt_forvalt		= MY_NULL;
	rec.m_nTrakt_driv_enh	= MY_NULL;
	rec.m_sTrakt_avv_id		= _T("");
	rec.m_nTrakt_inv_typ		= MY_NULL;	
	rec.m_fTrakt_areal		= 0.0;
}

// Set all Editboxes to read or read only, depending on
// if data comes from database and holds Compartmant(s) and Plot(s); 061010 p�d
void CMDIScaGBTraktFormView::setAllReadOnly(BOOL ro1,BOOL ro2)
{
	m_wndCBoxForvalt.SetReadOnly( ro2);
	m_wndEditDrivEnh.SetReadOnly( ro1);
	m_wndEditAvvId.SetReadOnly( ro1);
	m_wndCBoxInvType.SetReadOnly( ro2);
	m_wndEditAvlagg.SetReadOnly( ro1);
	m_wndEditMaskinlag.SetReadOnly( ro1);
	m_wndEditUrsprung.SetReadOnly( ro1);
	m_wndEditProdLedare.SetReadOnly( ro1);
	m_wndEditNamn.SetReadOnly( ro1);
//	m_wndEditDatum.SetReadOnly( ro1);
	m_wndEditAreal.SetReadOnly( ro1);
	m_wndEditAntYtor.SetReadOnly( ro1);
	m_wndEditMedOhjd.SetReadOnly( ro1);
	m_wndEditSiTot.SetReadOnly( ro1);
	m_wndEditSiAlder.SetReadOnly( ro1);
	m_wndEditGyDirFore.SetReadOnly( ro1);
	m_wndEditGyDirEfter.SetReadOnly( ro1);
	m_wndEditNotes.SetReadOnly( ro1);
	m_wndEditHuggnform.SetReadOnly( ro1);
	m_wndEditTerrf.SetReadOnly( ro1);
	m_wndCBoxDistrikt.SetReadOnly( ro2);
	m_wndCBoxSiSp.SetReadOnly( ro2);
}

void CMDIScaGBTraktFormView::setKeysReadOnly()
{
	m_wndCBoxForvalt.SetReadOnly(TRUE);
	m_wndEditDrivEnh.SetReadOnly(TRUE);
	m_wndEditAvvId.SetReadOnly(TRUE);
	m_wndCBoxInvType.SetReadOnly(TRUE);
	m_wndEditAvlagg.SetReadOnly(FALSE);
	m_wndEditMaskinlag.SetReadOnly(FALSE);
	m_wndEditUrsprung.SetReadOnly(FALSE);
	m_wndEditProdLedare.SetReadOnly(FALSE);
	m_wndEditNamn.SetReadOnly(FALSE);
//	m_wndEditDatum.SetReadOnly(FALSE);
	m_wndEditAreal.SetReadOnly(FALSE);
	m_wndEditAntYtor.SetReadOnly(FALSE);
	m_wndEditMedOhjd.SetReadOnly(FALSE);
	m_wndEditSiTot.SetReadOnly(FALSE);
	m_wndEditSiAlder.SetReadOnly(FALSE);
	m_wndEditGyDirFore.SetReadOnly(FALSE);
	m_wndEditGyDirEfter.SetReadOnly(FALSE);
	m_wndEditNotes.SetReadOnly(FALSE);
	m_wndEditHuggnform.SetReadOnly(FALSE);
	m_wndEditTerrf.SetReadOnly(FALSE);
	m_wndCBoxDistrikt.SetReadOnly(FALSE);
	m_wndCBoxSiSp.SetReadOnly(FALSE);
}


// Add Inv Types to ComboBox3
void CMDIScaGBTraktFormView::addInvTypesToCBox(void)
{
	CString sText;
	if (m_vecInvTypeData.size() > 0)
	{
		m_wndCBoxInvType.ResetContent();
		for (UINT i = 0;i < m_vecInvTypeData.size();i++)
		{
			INV_TYPE_DATA data = m_vecInvTypeData[i];
			sText.Format(_T("%s"),data.m_sInvTypeName);
			m_wndCBoxInvType.AddString(sText);
			m_wndCBoxInvType.SetItemData(i, data.m_nInvTypeID);
		}	
	}	
}

CString CMDIScaGBTraktFormView::getInvTypeSelected(int inv_type)
{
	CString sText;
	if (m_vecInvTypeData.size() > 0)
	{
		for (UINT i = 0;i < m_vecInvTypeData.size();i++)
		{
			INV_TYPE_DATA data = m_vecInvTypeData[i];
			if (data.m_nInvTypeID == inv_type)
			{
				sText.Format(_T("%s"),data.m_sInvTypeName);
				return sText;
			}
		}
	}	
	return _T("");
}

CString CMDIScaGBTraktFormView::getSiSpSelected(int sisp)
{
	CString sText;
	if (m_vecSiSpData.size() > 0)
	{
		for (UINT i = 0;i < m_vecSiSpData.size();i++)
		{
			INV_TYPE_DATA data = m_vecSiSpData[i];
			if (data.m_nInvTypeID == sisp)
			{
				sText.Format(_T("%s"),data.m_sInvTypeName);
				return sText;
			}
		}
	}	
	return _T("");
}

void CMDIScaGBTraktFormView::addSiSpToCBox(void)
{
	CString sText;
	if (m_vecSiSpData.size() > 0)
	{
		m_wndCBoxSiSp.ResetContent();
		for (UINT i = 0;i < m_vecSiSpData.size();i++)
		{
			INV_TYPE_DATA data = m_vecSiSpData[i];
			sText.Format(_T("%s"),data.m_sInvTypeName);
			m_wndCBoxSiSp.AddString(sText);
			m_wndCBoxSiSp.SetItemData(i, data.m_nInvTypeID);
		}	
	}	
}

// Try to find index of inv type in m_vecInvType;
int CMDIScaGBTraktFormView::getInvTypeID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected inv type is used on New item; 061205 p�d
	if (m_vecInvTypeData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_vecTraktIndex[m_nDBIndex].m_nTrakt_inv_typ;
	}
	else
	{
		int nIdx = m_wndCBoxInvType.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return (int)m_wndCBoxInvType.GetItemData(nIdx);
		}
	}
	return -1;
}

int CMDIScaGBTraktFormView::getInvTypeIndex(int inv_type)
{
	if (m_vecInvTypeData.size() > 0)
	{
		for (UINT i = 0;i < m_vecInvTypeData.size();i++)
		{
			INV_TYPE_DATA data = m_vecInvTypeData[i];
			if (data.m_nInvTypeID == inv_type)
					return i;
		}
	}	
	return -1;
}


// Try to find index of inv type in m_vecInvType;
int CMDIScaGBTraktFormView::getSiSpID(void)
{
	if (m_vecSiSpData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_recActiveTrakt.m_nTrakt_si_sp;
	}
	else
	{
		int nIdx = m_wndCBoxSiSp.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return (int)m_wndCBoxSiSp.GetItemData(nIdx);
		}
	}
	return -1;
}

int CMDIScaGBTraktFormView::getSiSpIndex(int sisp)
{
	if (m_vecSiSpData.size() > 0)
	{
		for (UINT i = 0;i < m_vecSiSpData.size();i++)
		{
			INV_TYPE_DATA data = m_vecSiSpData[i];
			if (data.m_nInvTypeID == sisp)
					return i;
		}
	}	
	return -1;
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CMDIScaGBTraktFormView::addForvaltsToCBox(void)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		m_wndCBoxForvalt.ResetContent();
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			sText.Format(_T("%d - %s"),data.m_nForvaltID,data.m_sForvaltName);
			m_wndCBoxForvalt.AddString(sText);
			m_wndCBoxForvalt.SetItemData(i, data.m_nForvaltID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

CString CMDIScaGBTraktFormView::getForvaltSelected(int forvalt)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			if (data.m_nForvaltID == forvalt)
			{
				sText.Format(_T("%d - %s"),data.m_nForvaltID,data.m_sForvaltName);
				return sText;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	if(forvalt==MY_NULL)
		sText.Format(_T("%s"),"");
	else
		sText.Format(_T("%d"),forvalt);
	return sText;
}


int CMDIScaGBTraktFormView::getForvaltID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Forvaltn is used on New item; 061205 p�d
	/*if (m_vecTraktIndex.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_recActiveTrakt.m_nTrakt_forvalt;
	}
	else
	{
		int nIdx = m_wndCBoxForvalt.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBoxForvalt.GetItemData(nIdx);
		}
	}*/
	int nIdx = m_wndCBoxForvalt.GetCurSel(),value=-1;
	CString csTest=_T("");
	if (nIdx != CB_ERR)
	{
		return (int)m_wndCBoxForvalt.GetItemData(nIdx);
	}
	else
	{
		m_wndCBoxForvalt.GetWindowText(csTest);
		value=_tstoi(csTest.GetBuffer(0));
		if(value>0 && value <999)
			return value;
		else
			return -1;
	}
	return -1;
}

int CMDIScaGBTraktFormView::getForvaltIndex(int forvalt)
{
	if (m_vecForvaltData.size() > 0)
	{
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			if (data.m_nForvaltID == forvalt)
			{
				return i;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	return -1;
}

// Add Distrikts into Combobox2
void CMDIScaGBTraktFormView::addDistriktToCBox(void)
{
	CString sText;
	if (m_vecDistriktData.size() > 0)
	{
		m_wndCBoxDistrikt.ResetContent();
		for (UINT i = 0;i < m_vecDistriktData.size();i++)
		{
			DISTRIKT_DATA data = m_vecDistriktData[i];
			sText.Format(_T("%d - %s"),data.m_nDistriktID,data.m_sDistriktName);
			m_wndCBoxDistrikt.AddString(sText);
			m_wndCBoxDistrikt.SetItemData(i, data.m_nDistriktID);
		}	// for (UINT i = 0;i < m_vecDistriktData.size();i++)
	}	// if (m_vecDistriktData.size() > 0)
}

// Try to find distrikt, based on index;
CString CMDIScaGBTraktFormView::getDistriktSelected(int distrikt)
{
	CString sText;
	if (m_vecDistriktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistriktData.size();i++)
		{
			DISTRIKT_DATA data = m_vecDistriktData[i];
			if (data.m_nDistriktID == distrikt)
			{
				sText.Format(_T("%d - %s"),data.m_nDistriktID,data.m_sDistriktName);
				return sText;
			}	// if (data.m_nDistriktID == distrikt)
		}	// for (UINT i = 0;i < m_vecDistriktData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	if(distrikt==MY_NULL)
	sText.Format(_T("%s"),"");
	else
	sText.Format(_T("%d"),distrikt);
	return sText;
}

// Try to find index of regionnumber in m_vecDistriktData; 061204 p�d
int CMDIScaGBTraktFormView::getDistriktID(void)
{
	CString tst=_T("");
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Distrikt is used on New item; 061205 p�d
	/*if (m_vecTraktIndex.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_recActiveTrakt.m_nTrakt_distrikt;
	}
	else
	{
		int nIdx = m_wndCBoxDistrikt.GetCurSel();
		if (nIdx != CB_ERR)
		{
			//tst.Format("Distriktselected = %d\n Distrikt Id = %d",nIdx,m_wndCBoxDistrikt.GetItemData(nIdx));
			//AfxMessageBox(tst);
			return m_wndCBoxDistrikt.GetItemData(nIdx);
		}
	}*/
	int nIdx = m_wndCBoxDistrikt.GetCurSel(),value=-1;
	CString csTest=_T("");
	if (nIdx != CB_ERR)
	{
		return (int)m_wndCBoxDistrikt.GetItemData(nIdx);
	}	
	else
	{
		m_wndCBoxDistrikt.GetWindowText(csTest);
		value=_tstoi(csTest.GetBuffer(0));
		if(value>0 && value<999)
			return value;
		else
			return -1;
	}
	return -1;
}

int CMDIScaGBTraktFormView::getDistriktIndex(int distrikt)
{
	if (m_vecDistriktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistriktData.size();i++)
		{
			DISTRIKT_DATA data = m_vecDistriktData[i];
			if (data.m_nDistriktID == distrikt)
				return i;
		}	// for (UINT i = 0;i < m_vecDistriktData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	return -1;
}


void CMDIScaGBTraktFormView::setNavigationButtons2(void)
{
	BOOL start=FALSE,end=FALSE;
	if (!m_vecTraktIndex.empty())
	{
		if(m_nDBIndex > 0)
			start=TRUE;

		if(m_nDBIndex < ((int)m_vecTraktIndex.size()-1))
			end=TRUE;
	}
	setNavigationButtons(start,end);	
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIScaGBTraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

void CMDIScaGBTraktFormView::setNavigationButtonSave(BOOL on)
{
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,on);
}

// Check that information needed to be abel to save Trakt, is enterd
// by user; 061113 p�d
BOOL CMDIScaGBTraktFormView::isOKToSave(void)
{
	if (	_tcscmp(m_wndCBoxInvType.getText().Trim(),_T("")) == 0 
		|| _tcscmp(m_wndCBoxForvalt.getText().Trim(),_T("")) == 0 
		|| m_wndEditDrivEnh.getInt() <= MY_NULL 
		|| _tcscmp(m_wndEditAvvId.getText().Trim(),_T("")) == 0
		|| m_wndEditAreal.getFloat() <= 0.0
		) 
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}

	//Kolla produktionsledare numeriskt och antal tecken
	if(m_wndEditProdLedare.getText().GetLength()>0)
	{
		if(!isInt(m_wndEditProdLedare.getText().GetBuffer(0)))
		{
			AfxMessageBox(_T("Fel i Prod.Ledare data\n Ej numerisk (0-9)"));
			return FALSE;
		}
		if(m_wndEditProdLedare.getText().GetLength()>9)
		{
			AfxMessageBox(_T("Fel i Prod.Ledare data\n F�r m�nga tecken! Max 9"));
			return FALSE;
		}
	}
	//Kolla maskinlag numeriskt och antal tecken
	if(m_wndEditMaskinlag.getText().GetLength()>0)
	{
		if(!isInt(m_wndEditMaskinlag.getText().GetBuffer(0)))
		{
			AfxMessageBox(_T("Fel i Maskinlag data\n Ej numerisk (0-9)"));
			return FALSE;
		}
		if(m_wndEditMaskinlag.getText().GetLength()>9)
		{
			AfxMessageBox(_T("Fel i Maskinlag data\n F�r m�nga tecken! Max 9"));
			return FALSE;
		}
	}
	if(m_wndEditDrivEnh.getText().GetLength()>0)
	{
		if(!isInt(m_wndEditDrivEnh.getText().GetBuffer(0)))
		{
			AfxMessageBox(_T("Fel i Avl�gg/Drivn.enhet\n Ej numerisk (0-9)"));
			return FALSE;
		}
		if(m_wndEditDrivEnh.getText().GetLength()>5)
		{
			AfxMessageBox(_T("Fel i Avl�gg/Drivn.enhet\n F�r m�nga tecken! Max 5"));
			return FALSE;
		}
	}
	return TRUE;
}


// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CMDIScaGBTraktFormView::isDataChanged(void)
{
	if (getIsDirty())
	{
		if (::MessageBox(0,m_sSaveData,m_sErrCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		{
			m_enumAction = CHANGED;
			saveTrakt();
			resetIsDirty();
		   return TRUE;
		}
		else
			resetIsDirty();
	}
	return FALSE;
}

void CMDIScaGBTraktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons2();
	//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIScaGBTraktFormView::resetTrakt(enumRESET reset)
{
	CMDIScaGBTraktFrame *pView = (CMDIScaGBTraktFrame *)getFormViewByID(IDD_FORMVIEW2)->GetParent();
	RLFReader *xml = new RLFReader;
	// Get updated data from Database
	getTraktIndexFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktIndex.size() > 0)
	{
		if (xml->Load(m_sLangFN))
			if (pView)
			{
				CDocument *pDoc = pView->GetActiveDocument();
				if (pDoc != NULL)
					pDoc->SetTitle(xml->str(IDS_STRING752));			
			}


		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons( m_nDBIndex > 0, m_nDBIndex < (m_vecTraktIndex.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = (int)m_vecTraktIndex.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons( m_nDBIndex > 0, m_nDBIndex < (m_vecTraktIndex.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = (int)m_vecTraktIndex.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktIndex.size();i++)
			{
				//TRAKT_INDEX data = m_vecTraktIndex[i];
				if (m_vecTraktIndex[i].m_nTrakt_forvalt == m_structTraktIdentifer.m_nTrakt_forvalt &&
					m_vecTraktIndex[i].m_nTrakt_driv_enh == m_structTraktIdentifer.m_nTrakt_driv_enh &&
					m_vecTraktIndex[i].m_sTrakt_avv_id == m_structTraktIdentifer.m_sTrakt_avv_id &&
					m_vecTraktIndex[i].m_nTrakt_inv_typ == m_structTraktIdentifer.m_nTrakt_inv_typ)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons2();
				//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons2();
				//setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons2();
		//setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}


BOOL CMDIScaGBTraktFormView::getIsHeaderDirty(void)
{
//	CString sDate;
//	int dirtyDate=0,date1=0,date2=0;

	TRAKT_DATA m_recTempTrakt=TRAKT_DATA();

	/*m_wndDatePicker.GetWindowText(sDate);
	date1=_tstoi(sDate.GetBuffer(0));
	date2=_tstoi(m_recActiveTrakt.m_sTrakt_date.GetBuffer(0));
	if(date1!=date2)
		dirtyDate=1;

	m_wndDatePicker2.GetWindowText(sDate);
	date1=_tstoi(sDate.GetBuffer(0));
	date2=_tstoi(m_recActiveTrakt.m_sTrakt_date2.GetBuffer(0));
	if(date1!=date2)
		dirtyDate=1;*/

	m_recTempTrakt.m_nTrakt_forvalt			= getForvaltID();
	m_recTempTrakt.m_nTrakt_driv_enh			= m_wndEditDrivEnh.getInt();
	m_recTempTrakt.m_sTrakt_avv_id			= m_wndEditAvvId.getText();
	m_recTempTrakt.m_nTrakt_inv_typ			= getInvTypeID();
	
	m_recTempTrakt.m_nTrakt_avlagg			= m_wndEditAvlagg.getInt();
	m_recTempTrakt.m_nTrakt_distrikt			= getDistriktID();
	m_recTempTrakt.m_nTrakt_maskinlag		= m_wndEditMaskinlag.getInt();
	m_recTempTrakt.m_nTrakt_ursprung			= m_wndEditUrsprung.getInt();
	m_recTempTrakt.m_nTrakt_prod_led			= m_wndEditProdLedare.getInt();
	m_recTempTrakt.m_sTrakt_namn				= m_wndEditNamn.getText();
		
	/*m_wndDatePicker.GetWindowText(sDate);
	m_recTempTrakt.m_sTrakt_date				= sDate;
	m_wndDatePicker2.GetWindowText(sDate);
	m_recTempTrakt.m_sTrakt_date2				= sDate;*/

	if(m_wndEditDatum.getText().GetLength() > 0)
		m_recTempTrakt.m_sTrakt_date.Format(_T("%8.8s"),m_wndEditDatum.getText());
	else
		m_recTempTrakt.m_sTrakt_date = _T("");

	if(m_wndEditDatum2.getText().GetLength() > 0)
		m_recTempTrakt.m_sTrakt_date2.Format(_T("%8.8s"),m_wndEditDatum2.getText());
	else
		m_recTempTrakt.m_sTrakt_date2 = _T("");

	m_recTempTrakt.m_fTrakt_areal				= m_wndEditAreal.getFloat();
	m_recTempTrakt.m_fTrakt_ant_ytor			= m_wndEditAntYtor.getInt();
	m_recTempTrakt.m_fTrakt_medel_ovre_hojd	= m_wndEditMedOhjd.getFloat();
	m_recTempTrakt.m_fTrakt_alder				= m_wndEditSiAlder.getFloat();
	m_recTempTrakt.m_fTrakt_si_tot			= m_wndEditSiTot.getFloat();
	m_recTempTrakt.m_nTrakt_si_sp				= getSiSpID();
	m_recTempTrakt.m_fTrakt_gy_dir_fore		= m_wndEditGyDirFore.getFloat();
	m_recTempTrakt.m_fTrakt_gy_dir_mal		= m_wndEditGyDirEfter.getFloat();

	m_recTempTrakt.m_nTrakt_huggnform		=  m_wndEditHuggnform.getInt();
	m_recTempTrakt.m_nTrakt_terrf				=  m_wndEditTerrf.getInt();

	m_recTempTrakt.m_sTrakt_notes				= m_wndEditNotes.getText();

	if(//dirtyDate ||
		m_recTempTrakt.m_nTrakt_forvalt			!= m_recTempTrakt.m_nTrakt_forvalt	||
		m_recTempTrakt.m_nTrakt_driv_enh			!= m_recActiveTrakt.m_nTrakt_driv_enh	||		 
		m_recTempTrakt.m_sTrakt_avv_id			!= m_recActiveTrakt.m_sTrakt_avv_id	||		
		m_recTempTrakt.m_nTrakt_inv_typ			!= m_recActiveTrakt.m_nTrakt_inv_typ	||		

		m_recTempTrakt.m_nTrakt_avlagg			!= m_recActiveTrakt.m_nTrakt_avlagg	||		 
		m_recTempTrakt.m_nTrakt_distrikt			!= m_recActiveTrakt.m_nTrakt_distrikt	||		 
		m_recTempTrakt.m_nTrakt_maskinlag		!= m_recActiveTrakt.m_nTrakt_maskinlag||		 
		m_recTempTrakt.m_nTrakt_ursprung			!= m_recActiveTrakt.m_nTrakt_ursprung	||		 
		m_recTempTrakt.m_nTrakt_prod_led			!= m_recActiveTrakt.m_nTrakt_prod_led	||		 
		m_recTempTrakt.m_sTrakt_namn				!= m_recActiveTrakt.m_sTrakt_namn		||		 

		m_recTempTrakt.m_sTrakt_date				!= m_recActiveTrakt.m_sTrakt_date		||
		m_recTempTrakt.m_sTrakt_date2				!= m_recActiveTrakt.m_sTrakt_date2		||
		m_recTempTrakt.m_fTrakt_areal				!= m_recActiveTrakt.m_fTrakt_areal		||		 
		m_recTempTrakt.m_fTrakt_ant_ytor			!= m_recActiveTrakt.m_fTrakt_ant_ytor	||		 
		m_recTempTrakt.m_fTrakt_medel_ovre_hojd!= m_recActiveTrakt.m_fTrakt_medel_ovre_hojd ||
		m_recTempTrakt.m_fTrakt_alder				!= m_recActiveTrakt.m_fTrakt_alder		||
		m_recTempTrakt.m_fTrakt_si_tot			!= m_recActiveTrakt.m_fTrakt_si_tot	||		 
		m_recTempTrakt.m_nTrakt_si_sp				!= m_recActiveTrakt.m_nTrakt_si_sp		||		 
		m_recTempTrakt.m_fTrakt_gy_dir_fore		!= m_recActiveTrakt.m_fTrakt_gy_dir_fore ||		 
		m_recTempTrakt.m_fTrakt_gy_dir_mal		!= m_recActiveTrakt.m_fTrakt_gy_dir_mal	||	 
		m_recTempTrakt.m_sTrakt_notes				!= m_recActiveTrakt.m_sTrakt_notes ||
		m_recTempTrakt.m_nTrakt_huggnform		!= m_recActiveTrakt.m_nTrakt_huggnform	||	 
		m_recTempTrakt.m_nTrakt_terrf				!= m_recActiveTrakt.m_nTrakt_terrf
		)
		return TRUE;
	else
		return FALSE;
}


BOOL CMDIScaGBTraktFormView::getIsDirty(void)
{
	if(m_bIsDirty)
		return TRUE;

	if(getIsHeaderDirty())
		return TRUE;
	/*
	if(m_wndEditDrivEnh.isDirty()		||
	m_wndEditAvvId.isDirty()		||
	m_wndEditAvlagg.isDirty()		||
	m_wndEditMaskinlag.isDirty()	||
	m_wndEditUrsprung.isDirty()	||
	m_wndEditProdLedare.isDirty() ||
	m_wndEditNamn.isDirty()			||
	//m_wndEditDatum.isDirty()		||
	dirtyDate							||
	m_wndEditAreal.isDirty()		||
	m_wndEditAntYtor.isDirty()		||
	m_wndEditMedOhjd.isDirty()		||
	m_wndEditSiTot.isDirty()		||
	m_wndEditSiAlder.isDirty()		||
	m_wndEditGyDirFore.isDirty()	||
	m_wndEditGyDirEfter.isDirty() ||
	m_wndEditNotes.isDirty()		||
	m_wndCBoxForvalt.isDirty()		|| 
	m_wndCBoxDistrikt.isDirty()	|| 
	m_wndCBoxInvType.isDirty()		||
	m_wndCBoxSiSp.isDirty())
	{
		return TRUE;
	}*/
	int nTabPages = m_wndTabControl.getNumOfTabPages();
	BOOL bDirty=FALSE;
	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage7 *wndReport7;
				wndReport7 = DYNAMIC_DOWNCAST(CMyReportPage7,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport7->m_bIsDirty2;
				break;
			case 1:
				CMyReportPage1 *wndReport1;
				wndReport1 = DYNAMIC_DOWNCAST(CMyReportPage1,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport1->m_bIsDirty2;
				break;
			case 2:
				CMyReportPage6 *wndReport6;
				wndReport6 = DYNAMIC_DOWNCAST(CMyReportPage6,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport6->m_bIsDirty2;
				break;
			case 3:
				CMyReportPage2 *wndReport2;
				wndReport2 = DYNAMIC_DOWNCAST(CMyReportPage2,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport2->m_bIsDirty2;
				break;
			case 4:
				CMyReportPage3 *wndReport3;
				wndReport3 = DYNAMIC_DOWNCAST(CMyReportPage3,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport3->m_bIsDirty2;
				break;
			case 5:
				CMyReportPage4 *wndReport4;
				wndReport4 = DYNAMIC_DOWNCAST(CMyReportPage4,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport4->m_bIsDirty2;
				break;
			}			
		}
		if(bDirty)
			return TRUE;
	}

	return FALSE;
}

void CMDIScaGBTraktFormView::resetIsDirty(void)
{
	m_bIsDirty = FALSE;

	m_wndEditDrivEnh.resetIsDirty();
	m_wndEditAvvId.resetIsDirty();
	m_wndEditAvlagg.resetIsDirty();
	m_wndEditMaskinlag.resetIsDirty();
	m_wndEditUrsprung.resetIsDirty();
	m_wndEditProdLedare.resetIsDirty();
	m_wndEditNamn.resetIsDirty();
	m_wndEditDatum.resetIsDirty();
	m_wndEditAreal.resetIsDirty();
	m_wndEditAntYtor.resetIsDirty();
	m_wndEditMedOhjd.resetIsDirty();
	m_wndEditSiTot.resetIsDirty();
	m_wndEditSiAlder.resetIsDirty();
	m_wndEditGyDirFore.resetIsDirty();
	m_wndEditGyDirEfter.resetIsDirty();
	
	m_wndEditDatum2.resetIsDirty();
	m_wndEditHuggnform.resetIsDirty();
	m_wndEditTerrf.resetIsDirty();

	m_wndEditNotes.resetIsDirty();
	m_wndCBoxForvalt.resetIsDirty(); 
	m_wndCBoxDistrikt.resetIsDirty();
	m_wndCBoxInvType.resetIsDirty();
	m_wndCBoxSiSp.resetIsDirty();
	m_wndCBoxForvalt.resetIsDirty();
	m_wndCBoxDistrikt.resetIsDirty();	
	m_wndCBoxInvType.resetIsDirty();	
	m_wndCBoxSiSp.resetIsDirty();	

	int nTabPages = m_wndTabControl.getNumOfTabPages();

	BOOL bDirty=FALSE;
	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage7 *wndReport7;
				wndReport7 = DYNAMIC_DOWNCAST(CMyReportPage7,CWnd::FromHandle(pItem->GetHandle()));
				wndReport7->resetIsDirty();
				break;
			case 1:
				CMyReportPage1 *wndReport1;
				wndReport1 = DYNAMIC_DOWNCAST(CMyReportPage1,CWnd::FromHandle(pItem->GetHandle()));
				wndReport1->resetIsDirty();
				break;
			case 2:
				CMyReportPage6 *wndReport6;
				wndReport6 = DYNAMIC_DOWNCAST(CMyReportPage6,CWnd::FromHandle(pItem->GetHandle()));
				wndReport6->resetIsDirty();
				break;
			case 3:
				CMyReportPage2 *wndReport2;
				wndReport2 = DYNAMIC_DOWNCAST(CMyReportPage2,CWnd::FromHandle(pItem->GetHandle()));
				wndReport2->resetIsDirty();
				break;
			case 4:
				CMyReportPage3 *wndReport3;
				wndReport3 = DYNAMIC_DOWNCAST(CMyReportPage3,CWnd::FromHandle(pItem->GetHandle()));
				wndReport3->resetIsDirty();
				break;
			case 5:
				CMyReportPage4 *wndReport4;
				wndReport4 = DYNAMIC_DOWNCAST(CMyReportPage4,CWnd::FromHandle(pItem->GetHandle()));
				wndReport4->resetIsDirty();
				break;
			}			
		}
	}

}

BOOL CMDIScaGBTraktFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;
	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
				 rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
				 TRACE0( "Warning: couldn't create client tab for view.\n" );
				 // pWnd will be cleaned up by PostNcDestroy
				 return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	return TRUE;
}



BOOL CMDIScaGBTraktFormView::setupTraktTabs(void)
{
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,CRect(0,0,0,0),this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	//m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS,NULL,0,CSize(16, 16),xtpImageNormal);
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Create tabpages
			AddView(RUNTIME_CLASS(CMyReportPage7),xml->str(IDS_STRING7586),8);	// Kvarv
			AddView(RUNTIME_CLASS(CMyReportPage1),xml->str(IDS_STRING7582),8);	// Kvarv
			AddView(RUNTIME_CLASS(CMyReportPage6),xml->str(IDS_STRING7585),8);	// Uttag Tot
			AddView(RUNTIME_CLASS(CMyReportPage2),xml->str(IDS_STRING7583),8);	// Uttag mellan stv
			AddView(RUNTIME_CLASS(CMyReportPage3),xml->str(IDS_STRING7584),8);	// Uttag i stv	
			AddView(RUNTIME_CLASS(CMyReportPage4),xml->str(IDS_STRING7524),8);	// Skador	
			AddView(RUNTIME_CLASS(CMyReportPage5),xml->str(IDS_STRING7520),8);	// Ytor
			AddView(RUNTIME_CLASS(CMyReportPage10),xml->str(IDS_STRING7679),8);	// Tr�d
		}
		delete xml;
	}
	return TRUE;
}
void CMDIScaGBTraktFormView::OnCbnEditchangeCombo22()
{
	// TODO: Add your control notification handler code here
		CString csTest=_T("");
	int nIdx = m_wndCBoxDistrikt.GetCurSel(),value=-1;

	if (nIdx != CB_ERR)
	{
		return;
	}	
	else
	{
		m_wndCBoxDistrikt.GetWindowText(csTest);
		value=_tstoi(csTest.GetBuffer(0));
		if(!isInt(csTest.GetBuffer(0)))
		{
			AfxMessageBox(_T("Distrikt bara numeriska tecken!"));
			csTest=_T("");
			m_wndCBoxDistrikt.SetWindowText(csTest);
		}
		else
		{		
			if(value>0 && value<99)
				return;
			else
			{
				AfxMessageBox(_T("Distrikt bara 2 tecken!"));
				csTest.Delete(2,csTest.GetLength()-2);
				m_wndCBoxDistrikt.SetWindowText(csTest);
			}
		}
		return;
	}
}

void CMDIScaGBTraktFormView::OnCbnEditchangeCombo21()
{
	// TODO: Add your control notification handler code here
		int nIdx = m_wndCBoxForvalt.GetCurSel(),value=-1;
	CString csTest=_T("");
	if (nIdx != CB_ERR)
	{
		return;
	}
	else
	{
		m_wndCBoxForvalt.GetWindowText(csTest);
		value=_tstoi(csTest.GetBuffer(0));
		if(!isInt(csTest.GetBuffer(0)))
		{
			AfxMessageBox(_T("F�rvaltning bara numeriska tecken!"));
			csTest=_T("");
			m_wndCBoxForvalt.SetWindowText(csTest);
		}
		else
		{		
			if(value>0 && value<999)
				return;
			else
			{
				AfxMessageBox(_T("F�rvaltning bara 3 tecken!"));
				csTest.Delete(3,csTest.GetLength()-3);
				m_wndCBoxForvalt.SetWindowText(csTest);
			}
		}
	}
}

void CMDIScaGBTraktFormView::OnBnClickedButtonRecalc()
{
}


/*BOOL CMDIScaGBTraktFormView::CheckAndCorrectPlotNumbers(TRAKT_DATA *trakt)
{
	unsigned int p=0,q=0;
	float test=0.0;
	//     Id , Gy
	vector<PLOT_CORRECTION> vecTreePlotId;


	//Check if there are any trakts at all
	if(!m_vecTraktIndex.empty())
	{
		//Check if the trakt can be corrected, ie that it has been created with a version that
		//has saved the tree and plot data.
		if(trakt->m_vTreeData.size()>0)
		{
			if(trakt->m_sTrakt_progver==_T("ESTIMSCA  1.3"))
			{
				vecTreePlotId.clear();
				//Get PlotId:s from tree connected to the Trakt
				if(!getCorrectPlotNumbers(*trakt,vecTreePlotId))
					return false;

				//Check if number of PlotId:s from trees match number of existing plots in trakt
				if(vecTreePlotId.size()==trakt->m_vPlotData.size())
				{	
					//Change the plotnumbers in the trakt to the correct ones from the trees
					for(p=0;p<vecTreePlotId.size();p++)
						trakt->m_vPlotData[p].m_nPlot_id=vecTreePlotId[p].n_Id;
					make_thinning_result(&m_recActiveTrakt);
					showData();
					return true;
				}
				else
				{
					AfxMessageBox(_T("Misslyckades att r�kna om: Antal ytor st�mmer ej med antal ytor p� inl�sta tr�d"));

				}
			}
		}
	}
	return false;
}*/

BOOL CMDIScaGBTraktFormView::CheckAndCorrectPlotNumbers(TRAKT_DATA *trakt)
{
	unsigned int ti=0,pi=0;
	float test=0.0;
	//     Id , Gy
	vector<PLOT_CORRECTION> vecTreePlotId;


	//Check if the trakt can be corrected, ie that it has been created with a version that
	//has saved the tree and plot data.
	if(trakt->m_vTreeData.size()>0)
	{
		if(trakt->m_sTrakt_progver==_T("ESTIMSCA  1.3"))
		{
			for(ti=0;ti<trakt->m_vTreeData.size();ti++)
			{
				//Check if current plot number on the tree can be found in the plot vector
				if((trakt->m_vTreeData[ti].m_nTree_PlotId-1)<trakt->m_vPlotData.size())
				{
					//Check if the tree:s plotnumber is not equal to the plot number in the plot vector
					if(trakt->m_vTreeData[ti].m_nTree_PlotId!=trakt->m_vPlotData[trakt->m_vTreeData[ti].m_nTree_PlotId-1].m_nPlot_id)
						trakt->m_vTreeData[ti].m_nTree_PlotId=trakt->m_vPlotData[trakt->m_vTreeData[ti].m_nTree_PlotId-1].m_nPlot_id;
				}
			}
			make_thinning_result(&m_recActiveTrakt);
			showData();
			return true;
		}
	}
	return false;
}

BOOL CMDIScaGBTraktFormView::getCorrectPlotNumbers(TRAKT_DATA t,vector<PLOT_CORRECTION> &v)
{
	int pi=0,ti=0,found=0;
	BOOL ret=false;
	PLOT_CORRECTION pc;
	for(ti=0;ti<t.m_vTreeData.size();ti++)
	{
		found=0;
		for(pi=0;pi<v.size();pi++)
		{
			if(v[pi].n_Id==t.m_vTreeData[ti].m_nTree_PlotId)
			{
				found=1;
				break;
			}
		}
		if(!found)
		{
			pc.f_Gy=0.0;
			pc.n_Id=t.m_vTreeData[ti].m_nTree_PlotId;
			v.resize(v.size()+1,pc);
			ret=true;
		}
	}
	return ret;
}




void CMDIScaGBTraktFormView::Batch_Open_Several_Files(CFileDialog *fdlg,int num_files)
{
	CString sFileName=_T("");
	TRAKT_DATA trakt_data=TRAKT_DATA();
	RLFReader *xml = new RLFReader;
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	CString sForvalt1=_T(""),sDrivEnh1=_T(""),sAvvId1=_T(""),sInvTyp1=_T(""),sAreal1=_T("");
	CString sForvalt=_T(""),sDrivEnh=_T(""),sAvvId=_T(""),sInvTyp=_T(""),sAreal=_T(""),sMsg=_T(""),sMsg2=_T("");
	int num=0,nErr=0,nRet=0;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sForvalt1.Format(_T("%-15.15s"),xml->str(IDS_STRING7550));
			sDrivEnh1.Format(_T("%-15.15s"),xml->str(IDS_STRING7551));
			sAvvId1.Format(_T("%-15.15s"),xml->str(IDS_STRING7552));
			sInvTyp1.Format(_T("%-15.15s"),xml->str(IDS_STRING7553));
			sAreal1.Format(_T("%-15.15s"),xml->str(IDS_STRING7561));
		}
	}
	
	
	ProgressDlg.GetBar().SetRange(0,num_files);
	ProgressDlg.GetBar().SetStep(1);
	ProgressDlg.GetBar().SetPos(0);
	ProgressDlg.ShowWindow(SW_NORMAL);
	ProgressDlg.SetDlgTexts(_T("Status"),_T("L�ser in och skapar trakter"));
	POSITION pos = fdlg->GetStartPosition();

	sMsg2.Format(_T("%s"),_T("Lyckades ej l�sa fil(er)!\n"));

	while (pos)
	{
		doEvents();
		num++;
		sFileName = fdlg->GetNextPathName(pos);
		if(HXL_FileOpen(sFileName.GetBuffer())==0)
		{
			trakt_data=TRAKT_DATA();
			//Create and save trakt
			if(HXL_MakeData(&trakt_data))				
			{
				setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d
				//clearAll();
				m_recActiveTrakt.m_vPlotData.clear();
				m_recActiveTrakt.m_vTreeData.clear();
				m_recActiveTrakt=trakt_data;

				if(m_recActiveTrakt.m_nTrakt_forvalt==MY_NULL)
					sForvalt=_T("");
				else
					sForvalt.Format(_T("%d"),m_recActiveTrakt.m_nTrakt_forvalt);

				if(m_recActiveTrakt.m_nTrakt_driv_enh==MY_NULL)
					sDrivEnh=_T("");
				else
					sDrivEnh.Format(_T("%d"),m_recActiveTrakt.m_nTrakt_driv_enh);

				sAvvId = m_recActiveTrakt.m_sTrakt_avv_id;

				if(m_recActiveTrakt.m_nTrakt_inv_typ==MY_NULL)
					sInvTyp=_T("");
				else
					sInvTyp=getInvTypeSelected(m_recActiveTrakt.m_nTrakt_inv_typ);

				if(m_recActiveTrakt.m_fTrakt_areal==MY_NULL)
					sAreal=_T("");
				else
					sAreal.Format(_T("%.1f"),m_recActiveTrakt.m_fTrakt_areal);

				sMsg.Format(_T("%-15.15s : %s \n%-15.15s : %s \n%-15.15s : %s \n%-15.15s : %s\n%-15.15s : %s"),
					sForvalt1,sForvalt,
					sDrivEnh1,sDrivEnh,
					sAvvId1,sAvvId,
					sInvTyp1,sInvTyp,
					sAreal1,sAreal);

				m_recActiveTrakt.m_nTrakt_enter_type=0;
				m_bIsDirty = TRUE;
				//Check and correct wrong plot numbers
				CheckAndCorrectPlotNumbers(&m_recActiveTrakt);
				showData();
				nRet=saveTrakt();
				ProgressDlg.UpdateText2(sMsg);
				sMsg.Format(_T("L�ser in och skapar trakter (%d/%d)"),num,num_files);
				ProgressDlg.SetDlgTexts(_T("Status"),sMsg);
				ProgressDlg.GetBar().StepIt();
				switch(nRet)
				{
				case 0://Trakt not saved
					sMsg.Format(_T("%s\n\n"),sFileName);
					sMsg2+=sMsg;
					nErr=1;
					break;
				case 1://Trakt saved
					break;
				case 2://Trakt updated
					doEvents();
					ProgressDlg.GetBar().SetRange(0,num_files);
					ProgressDlg.GetBar().SetStep(1);
					ProgressDlg.GetBar().SetPos(num);
					ProgressDlg.ShowWindow(SW_NORMAL);
					sMsg.Format(_T("L�ser in och skapar trakter (%d/%d)"),num,num_files);
					ProgressDlg.SetDlgTexts(_T("Status"),sMsg);
					break;
				case 3://Trakt exists but choosed not to update/replace that trakt
					break;
				}
			}
		}
		else
		{
			sMsg.Format(_T("%s\n"),sFileName);
			sMsg2+=sMsg;
			nErr=1;
		}
	}	
	if(nErr)
		AfxMessageBox(sMsg2);
	ProgressDlg.ShowWindow(SW_HIDE);
}

