#pragma once

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDIScaTestFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIScaTestFrame)
	
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:


	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;

	//void setupFiles(void);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;


public:
	CMDIScaTestFrame();   // standard constructor
	virtual ~CMDIScaTestFrame();
	static XTPDockingPanePaintTheme m_themeCurrent;
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
