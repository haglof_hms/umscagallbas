typedef struct _tagLICENSE
{
	CString csModID;			// module id
	CString csModName;		// module name
	int nType;					// 1 = User, 2 = Computer, 3 = Counter, 4 = Yes/No, 5 = Item, 6 = Concurrent User (only possible if the necessary licence exists)
	int nLicenses;				// Number of users or computers etc. In a Yes/No-Module 1 for yes and 0 for no.
	BOOL bDemo;					// demo?
	DATE dtExpires;			// Date when this module will expire, 0 for unlimited
	int nNoOfDays;				// Number of days this module will run after the first usage
	DATE dtMaxDate;			// Max date when this module will no longer work, even if there are open days, 0 for unlimited
	CString csTag;				// The tag value of that module
	BOOL bAllowDeactivate;	// Can users, computers or items be deactivated In that module? 
	int nWebActivation;		// WebActivation State of the new module (0-3). See Web Activation Documentation. 
	BOOL bYesNo;				// TRUE = yes, FALSE = no
	int nRemDays;				// amount of days left
	int nRemLic;				// amount of licenses left
} LICENSE;

BOOL License(void);

const LPCTSTR LIC_GETLIC		=_T("GetLicense");
typedef int (*LIC_PROC_GET)(TCHAR *szName, LICENSE *licRet);
