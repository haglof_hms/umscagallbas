#include "stdafx.h"
#include "UMHmsMisc.h"
#include "ResLangFileReader.h"
#include "FileVersionInfo.h"

//////////////////////////////////////////////////////////////////////////////////
// 	CMyTabControl22

CMyTabControl2::CMyTabControl2()
	: CXTPTabControl()
{
}
/*
void CMyTabControl2::OnItemClick(CXTPTabManagerItem* pItem)
{
	CXTPTabControl::OnItemClick(pItem);

	CXTPTabManager *pTabManager = pItem->GetTabManager();
	CXTPTabManagerItem *pTabPage = NULL;
	if (pTabManager)
	{
		pTabPage = GetSelectedItem();
		if (pTabPage)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, 
										 ID_WPARAM_VALUE_FROM + 0xFA,
										 pTabPage->GetIndex());
		}	// if (pTabPage)
	}	// if (pTabManager)
	
}*/

CXTPTabManagerItem *CMyTabControl2::getSelectedTabPage(void)
{

	CXTPTabManagerItem *pTabPage = NULL;
	pTabPage = GetSelectedItem();
	if (pTabPage)
	{
		return pTabPage;
	}

	return NULL;
}

CXTPTabManagerItem *CMyTabControl2::getTabPage(int idx)
{
	// Make sure the index (idx) is less than tabs; 060405 p�d
	if (idx < GetItemCount())
	{
		CXTPTabManagerItem *pTabPage = NULL;
		pTabPage = GetItem(idx);
		if (pTabPage)
		{
			return pTabPage;
		}
	}
	return NULL;
}

int CMyTabControl2::getNumOfTabPages(void)
{
	return GetItemCount();
}


/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit2

BEGIN_MESSAGE_MAP(CMyExtEdit2, CXTEdit)
	//{{AFX_MSG_MAP(CMyExtEdit2)
	ON_WM_KEYUP()
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyExtEdit2::CMyExtEdit2()
{
  // defaults
  m_crFGEnabled = ENABLED_FG;
  m_crBGEnabled = ENABLED_BG;
  m_crFGDisabled = DISABLED_FG;
  m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
  m_pbrushEnabled = new CBrush;
  m_pbrushDisabled = new CBrush;
  m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);

	m_bModified = FALSE;
	m_bIsNumeric = FALSE;
//	m_bIsAsH100	= FALSE;

	//m_nItemData = -1;
	m_sItemData = _T("");
}

CMyExtEdit2::~CMyExtEdit2()
{
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}
/*

BOOL CMyExtEdit2::PreTranslateMessage(MSG *pMSG)
{
	UINT  nKeyCode = pMSG->wParam; // virtual key code of the key pressed
	if (pMSG->message == WM_KEYDOWN)
	{
		if (nKeyCode < 65)
			return 0;

	  // CTRL+C, CTRL+X, or, CTRL+V?
	  if ( (nKeyCode == _T('C') || 
			nKeyCode == _T('X') || 
		  nKeyCode == _T('V')) && 
		  (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
			return 0;
	  }

	  if (m_bIsNumeric)
	  {
			if (isNumeric(nKeyCode))
				return 0;
			else
				return 1;
	  }
	  else
			return 0;

	}
	return 0;
}*/



/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit2 message handlers

void CMyExtEdit2::PreSubclassWindow() 
{
	CXTEdit::PreSubclassWindow();
}

void CMyExtEdit2::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
//	TCHAR tmp[120];
	m_bModified = TRUE;
	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	CXTEdit::OnKeyUp(nChar,nRepCnt,nFlags);
}


void CMyExtEdit2::SetAsNumeric(void)
{
	m_bIsNumeric = TRUE;
}

/*
void CMyExtEdit2::SetAsH100(void)
{
	m_bIsAsH100 = TRUE;
}*/

HBRUSH CMyExtEdit2::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(GetStyle() & ES_READONLY)
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
}

void CMyExtEdit2::SetReadOnly(BOOL flag)
{
		if (flag == TRUE)
		{
			ModifyStyle(WS_TABSTOP,0);
		}
		else
		{
			ModifyStyle(0,WS_TABSTOP);
		}
		Invalidate(TRUE);

		CEdit::SetReadOnly( flag );
}

void CMyExtEdit2::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyExtEdit2::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGDisabled = crFG;
   m_crBGDisabled = crBG;
   delete m_pbrushDisabled;
   m_pbrushDisabled = new CBrush;
   m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

void CMyExtEdit2::SetFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic2

CMyExtStatic2::CMyExtStatic2()
{
	m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing background color to the system face color.
	m_crTextColor = BLACK; // Initializing text color to black
	m_brBkgnd.CreateSolidBrush(m_crBkColor); // Creating the Brush Color For the Edit Box Background
	m_fnt1 = new CFont();
}

CMyExtStatic2::~CMyExtStatic2()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
}


BEGIN_MESSAGE_MAP(CMyExtStatic2, CStatic)
	//{{AFX_MSG_MAP(CMyExtStatic2)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic2 message handlers

void CMyExtStatic2::SetTextColor(COLORREF crColor)
{
	m_crTextColor = crColor; // Passing the value passed by the dialog to the member varaible for Text Color
	RedrawWindow();
}

void CMyExtStatic2::SetBkColor(COLORREF crColor)
{
	m_crBkColor = crColor; // Passing the value passed by the dialog to the member varaible for Backgound Color
	m_brBkgnd.DeleteObject(); // Deleting any Previous Brush Colors if any existed.
	m_brBkgnd.CreateSolidBrush(crColor); // Creating the Brush Color For the Edit Box Background
	RedrawWindow();
}

HBRUSH CMyExtStatic2::CtlColor(CDC* pDC, UINT nCtlColor)
{
	HBRUSH hbr;
	hbr = (HBRUSH)m_brBkgnd; // Passing a Handle to the Brush
	pDC->SetBkColor(m_crBkColor); // Setting the Color of the Text Background to the one passed by the Dialog
	pDC->SetTextColor(m_crTextColor); // Setting the Text Color to the one Passed by the Dialog

	if (nCtlColor)       // To get rid of compiler warning
      nCtlColor += 0;

	return hbr;
}

void CMyExtStatic2::SetLblFont()
{
		LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = 14;
	_tcscpy(lf.lfFaceName,_T("Courier New"));
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

void CMyExtStatic2::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;

	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

void CMyExtStatic2::SetLblFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

CString getHxlDllDir(void)
{
	CString sPath;
	CString sModulesPath;
	
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s"),sPath,HXL_DLL_NAME);

	return sModulesPath;
}


//////////////////////////////////////////////////////////////////////////
// CMyReportCtrl2; derived from CXTPReportControl
IMPLEMENT_DYNCREATE(CMyReportCtrl2, CXTPReportControl)

BEGIN_MESSAGE_MAP(CMyReportCtrl2, CXTPReportControl)
	//ON_WM_CHAR()
	//ON_WM_KEYUP()
 //ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT, OnValueChanged1)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT2, OnValueChanged2)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT3, OnValueChanged3)
END_MESSAGE_MAP()


CMyReportCtrl2::CMyReportCtrl2()
	: CXTPReportControl()
{
	/*bIsDirty = FALSE;
	m_bDoValidateCol = FALSE;
	m_fValidateValue = 0.0;
	m_bDoEditFirstColumn = TRUE;
	m_bRunMetrics = TRUE;*/
}

// Copy constructor
CMyReportCtrl2::CMyReportCtrl2(const CMyReportCtrl2 &c)
{
	*this = c;
}

BOOL CMyReportCtrl2::Create(CWnd* pParentWnd,UINT nID,BOOL add_hscroll,BOOL run_metrics)
{
	CXTPReportControl::Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,
														CRect(0,0,0,0),
														pParentWnd,
														nID);
	if (add_hscroll)
	{
		GetReportHeader()->SetAutoColumnSizing( FALSE );
		EnableScrollBar(SB_HORZ, TRUE );
		EnableScrollBar(SB_VERT, TRUE );
	}
	else
	{
		GetReportHeader()->SetAutoColumnSizing( TRUE );
		EnableScrollBar(SB_HORZ, FALSE );
		EnableScrollBar(SB_VERT, FALSE );
	}

//	m_bRunMetrics = run_metrics;
//	m_nID = nID;
	return TRUE;
}

/*
//Kvarvarande �ndring i data
void CMyReportCtrl2::OnValueChanged1(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  CXTPReportControl *rep = this;
  CTraktViewData *pRec;

		if (rep->GetSafeHwnd() != NULL)
		{
			CXTPReportRecords *pRecs = rep->GetRecords();
			CXTPReportColumn  *pCols = rep->GetFocusedColumn();
			CXTPReportRow		*pRow	 = rep->GetFocusedRow();

			int nRowIndex = pRow->GetIndex();
			int nColIndex = pCols->GetIndex();
			switch(nColIndex)
			{
				//M3sk
			default:
					pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
					value=pRec->getColumnDouble(nColIndex);
					if(value<0 || value>4000.0)
					{
						value=0.0;
						pRec->setColumnDouble(nColIndex,value);
					}
			break;
			}
		}
}*/

/*
// virtual method; 070205 p�d
void CMyReportCtrl2::GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
{
	CDC *pDC = pDrawArgs->pDC; //GetDC();
	CXTPReportRecords *pRecs = NULL;
	CString sMsg;
	int nCol = pDrawArgs->pColumn->GetIndex();
	double fValue = 0.0;
	if (m_bRunMetrics)
	{
		if (nCol == 0)
		{
			pDrawArgs->pColumn->SetEditable( m_bDoEditFirstColumn );
		}

		if (nCol > 0 && isDoValidate())
		{
			pRecs = GetRecords();
			if (pRecs != NULL)
			{
				fValue = 0.0;
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CXTPReportRecord *pRecord = pRecs->GetAt(i);
					if (pRecord != NULL)
					{
						CXTPReportRecordItem *pRec = pRecord->GetItem(pDrawArgs->pColumn);
						fValue += atof(pRec->GetCaption(pDrawArgs->pColumn));
					}	// if (pRecord != NULL)
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs != NULL)
			sMsg.Format("%.1f",fValue);
			pDrawArgs->pColumn->SetFooterText(sMsg);

			if (fValue != getValidateValue())
			{
				pItemMetrics->clrForeground = RGB(255,0,0);
			}
			else
			{
				pItemMetrics->clrBackground = RGB(255,255,255);
			}
		}	// if (nCol > 0 && isDoValidate())
	}	// if (m_bRunMetrics)
}

BOOL CMyReportCtrl2::isDirty(void)
{
	isDataValid();
	return bIsDirty;
}

void CMyReportCtrl2::setIsDirty(BOOL val)
{
	bIsDirty = val;
}*/

BOOL CMyReportCtrl2::ClearReport(void)
{
	CXTPReportControl *rep = this;
	if (rep->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords* pRecords = rep->GetRecords();

		if (pRecords)
		{
			pRecords->RemoveAll();
			return TRUE;
		}
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
// CMyComboBox2

BEGIN_MESSAGE_MAP(CMyComboBox2, CComboBox)
  //{{AFX_MSG_MAP(CMyComboBox2)
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ENABLE()
	ON_CONTROL_REFLECT_EX(CBN_EDITCHANGE, OnCBoxChange)
	ON_CONTROL_REFLECT_EX(CBN_DROPDOWN, OnCBoxDropDown)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyComboBox2::CMyComboBox2(BOOL bEditable)
{
	// defaults
	m_bIsDirty		= FALSE;
	m_bEditable		= bEditable;
	m_crFGEnabled	= ENABLED_FG;
	m_crBGEnabled	= ENABLED_BG;
	m_crFGDisabled = DISABLED_FG;
	m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
	m_pbrushEnabled	= new CBrush;
	m_pbrushDisabled	= new CBrush;
	m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
	m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

CMyComboBox2::~CMyComboBox2()
{
	m_font.DeleteObject();
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}

// CMyComboBox2 message handlers

HBRUSH CMyComboBox2::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(IsWindowEnabled())
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
}

HBRUSH CMyComboBox2::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(IsWindowEnabled())
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   return NULL;
}

void CMyComboBox2::OnEnable(BOOL bEnable)
{
   CComboBox::OnEnable(bEnable);

   // TODO: Add your message handler code here

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);

   // Setting the edit ctrl always to enable
   pComboEdit->EnableWindow(TRUE);
   pComboEdit->SetReadOnly(!(m_bEditable && bEnable));
   Invalidate();
}

void CMyComboBox2::SetReadOnly(BOOL bReadOnly)
{
   m_bEditable = !bReadOnly;

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);

	 EnableWindow( !bReadOnly );
   // Setting the edit ctrl always to enable
   pComboEdit->EnableWindow(TRUE);
   pComboEdit->SetReadOnly(!(m_bEditable && IsWindowEnabled()));
   Invalidate();
}

void CMyComboBox2::SetReadOnlyEx(BOOL bReadOnly)
{
   m_bEditable = !bReadOnly;

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);

   // Setting the edit ctrl always to enable
   pComboEdit->EnableWindow(TRUE);
   pComboEdit->SetReadOnly(!m_bEditable);
   Invalidate();
}

void CMyComboBox2::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyComboBox2::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGDisabled = crFG;
   m_crBGDisabled = crBG;
   delete m_pbrushDisabled;
   m_pbrushDisabled = new CBrush;
   m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

CString CMyComboBox2::getText(void)
{
	int nIndex = GetCurSel();
	int nLength;
	CString sBuffer;
	if (nIndex != CB_ERR)	
	{
		nLength = GetLBTextLen(nIndex);
		GetLBText(nIndex,sBuffer.GetBuffer(nLength));
		sBuffer.ReleaseBuffer();
		return sBuffer;
	}
	else
	{
		GetWindowText(sBuffer);
		return sBuffer;
	}
		return _T("");
}

void CMyComboBox2::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_font.CreateFontIndirect(&lf);

	SetFont(&m_font);
}

// Events
BOOL CMyComboBox2::OnCBoxChange()
{
	m_bIsDirty = TRUE;

	return FALSE;
}

BOOL CMyComboBox2::OnCBoxDropDown()
{
	m_bIsDirty = TRUE;

	return FALSE;
}


BOOL isInt(TCHAR *str)
{
	BOOL bOk = FALSE;
	for (TCHAR *p = str;p < str + _tcsclen(str);p++)
	{
		if (isInteger(*p))
		{
			bOk = TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	return bOk;
}

BOOL isInteger(UINT value)
{
	if ((value >= 48 && value <= 57))
		return TRUE;
	return FALSE;
}
void showFormView(int idd,LPCTSTR lang_fn)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(ID_UPDATE_ITEM);
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
				}

				break;
			}
		}
	}
}


CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

BOOL messageDialog(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg)
{
	BOOL bReturn = FALSE;
	CMyMessageDlg *dlg = new CMyMessageDlg(cap,ok_btn,cancel_btn,msg);

	bReturn = (dlg->DoModal() == IDOK);

	delete dlg;

	return bReturn;
}

BOOL isTal(TCHAR *str)
{
	BOOL bOk = FALSE;
	for (TCHAR *p = str;p < str + _tcsclen(str);p++)
	{
		if (isTalet(*p))
		{
			bOk = TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	return bOk;
}

BOOL isTalet(UINT value)
{
	if ((value >= 48 && value <= 57))	// 0-9
		return TRUE;
	if(value==44 || value==46)  //punkt eller komma
		return TRUE;
	return FALSE;
}

/*
// Modify this rutine, to check if file exists; 051213 p�d
BOOL fileExists(LPCTSTR fn)
{
	BOOL bFound = FALSE;
	CFileFind find;
	bFound = find.FindFile(fn);
	find.Close();
	return bFound;
}


void regSetInt(LPCTSTR root,LPCTSTR key, LPCTSTR item,DWORD value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);
  if (RegCreateKeyEx(HKEY_CURRENT_USER,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_WRITE|KEY_SET_VALUE,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{
		RegSetValueEx(hk, 
								item, 
								NULL, 
								REG_DWORD, 
								(LPBYTE)&value, 
								sizeof(DWORD));
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}

DWORD regGetInt(LPCTSTR root,LPCTSTR key, LPCTSTR item, int nDefault)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];
	DWORD dwKeySize = 255;
	DWORD nValue = 0;

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{

		if(RegQueryValueEx(hk, item, 0,NULL, 
			(LPBYTE)&nValue, 
			&dwKeySize) != ERROR_SUCCESS)
		{
			nValue = nDefault;
		}
	}

	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return nValue;
}


//	const LPCTSTR REG_ROOT								= _T("SOFTWARE\\HaglofManagmentSystem\\");
//	const LPCTSTR REG_KEY_USERNAME				= _("USER_DATA");
//	const LPCTSTR REG_STR_USERPRIV				= _("UserPriviliges");
//	const LPCTSTR REG_STR_SELDB					= _("SelectedDataBase");

void regSetStr(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);
  if (RegCreateKeyEx(HKEY_CURRENT_USER,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_WRITE|KEY_SET_VALUE,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{
		_stprintf(szValue,_T("%s"),value);
		RegSetValueEx(hk,
				          item,
									0,
									REG_SZ,
									(PBYTE)&szValue,
									(DWORD)_tcslen(szValue));
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}

CString regGetStr(LPCTSTR root,LPCTSTR key, LPCTSTR item, LPCTSTR szDefault)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 510;

	memset(szValue, 0, 511);

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"), root, key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		// Retrieve the value of the Left key
		if(RegQueryValueEx(hk,
			item,
			0,
			NULL,//&dwType,
			(PBYTE)&szValue,
			&dwKeySize) != ERROR_SUCCESS)
		{
			RegCloseKey(hk);
			return szDefault;
		}
	}

	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return szValue;
}

// Functions used to keep track of window size and placement, set by user; 060125 p�d
// Key's and items in registry
const LPCTSTR REG_WINDOW_POS		= _T("WINDOW_POS");
const LPCTSTR REG_WINDOW_CMDSHOW = _T("CMDSHOW");
const LPCTSTR REG_WINDOW_LEFT		= _T("LEFT");
const LPCTSTR REG_WINDOW_TOP		= _T("TOP");
const LPCTSTR REG_WINDOW_WIDTH	= _T("WIDTH");
const LPCTSTR REG_WINDOW_HEIGHT	= _T("HEIGHT");
const LPCTSTR REG_WINDOW_FLAGS	= _T("FLAGS");
const LPCTSTR REG_WINDOW_			= _T("FLAGS");

void getWindowPlacement(LPCTSTR root,					
						WINDOWPLACEMENT *wp,
						RECT def_placement
						)
{
	wp->length = sizeof(WINDOWPLACEMENT);
	wp->showCmd = regGetInt(root,REG_WINDOW_POS,REG_WINDOW_CMDSHOW, SW_SHOWNORMAL);
	wp->rcNormalPosition.left = regGetInt(root,REG_WINDOW_POS,REG_WINDOW_LEFT, def_placement.left);
	wp->rcNormalPosition.top = regGetInt(root,REG_WINDOW_POS,REG_WINDOW_TOP, def_placement.top);
	wp->rcNormalPosition.right = regGetInt(root,REG_WINDOW_POS,REG_WINDOW_WIDTH, def_placement.right);
	wp->rcNormalPosition.bottom = regGetInt(root,REG_WINDOW_POS,REG_WINDOW_HEIGHT, def_placement.bottom);
	wp->flags = regGetInt(root,REG_WINDOW_POS,REG_WINDOW_FLAGS, 0);
	wp->ptMaxPosition.x = regGetInt(root,REG_WINDOW_POS,_T("MaxX"), 0);
	wp->ptMaxPosition.y = regGetInt(root,REG_WINDOW_POS,_T("MaxY"), 0);
	wp->ptMinPosition.x = regGetInt(root,REG_WINDOW_POS,_T("MinX"), 0);
	wp->ptMinPosition.y = regGetInt(root,REG_WINDOW_POS,_T("MinY"), 0);
}

void setWindowPlacement(LPCTSTR root,		// Root in registry
						CWnd *wnd			// Window to save settings
						)
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(WINDOWPLACEMENT);
	wnd->GetWindowPlacement(&wp);

	regSetInt(root,REG_WINDOW_POS,REG_WINDOW_CMDSHOW,(DWORD)wp.showCmd);
	regSetInt(root,REG_WINDOW_POS,REG_WINDOW_FLAGS,(DWORD)wp.flags);
	regSetInt(root,REG_WINDOW_POS,REG_WINDOW_HEIGHT,(DWORD)wp.rcNormalPosition.bottom);
	regSetInt(root,REG_WINDOW_POS,REG_WINDOW_WIDTH,(DWORD)wp.rcNormalPosition.right);
	regSetInt(root,REG_WINDOW_POS,REG_WINDOW_LEFT,(DWORD)wp.rcNormalPosition.left);
	regSetInt(root,REG_WINDOW_POS,REG_WINDOW_TOP,(DWORD)wp.rcNormalPosition.top);
	regSetInt(root,REG_WINDOW_POS,_T("MaxX"),(DWORD)wp.ptMaxPosition.x);
	regSetInt(root,REG_WINDOW_POS,_T("MaxY"),(DWORD)wp.ptMaxPosition.y);
	regSetInt(root,REG_WINDOW_POS,_T("MinX"),(DWORD)wp.ptMinPosition.x);
	regSetInt(root,REG_WINDOW_POS,_T("MinY"),(DWORD)wp.ptMinPosition.y);
}

BOOL regGetBin(LPCTSTR root,LPCTSTR key, LPCTSTR item,
	BYTE** ppData, UINT* pBytes)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	//TCHAR szValue[512];
	DWORD dwKeySize = 510;

	*ppData = NULL;
	*pBytes = 0;

	_stprintf(szRoot,_T("%s\\%s"), root, key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		DWORD dwType, dwCount;
		LONG lResult = RegQueryValueEx(hk, item, NULL, &dwType,
			NULL, &dwCount);
		*pBytes = dwCount;

		if (lResult == ERROR_SUCCESS)
		{
			ASSERT(dwType == REG_BINARY);
			*ppData = new BYTE[*pBytes];
			lResult = RegQueryValueEx(hk, item, NULL, &dwType,
				*ppData, &dwCount);
		}
		RegCloseKey(hk);

		if (lResult == ERROR_SUCCESS)
		{
			ASSERT(dwType == REG_BINARY);
			return TRUE;
		}
		else
		{
			delete [] *ppData;
			*ppData = NULL;
		}
	}
	return FALSE;
}


BOOL regSetBin(LPCTSTR root,LPCTSTR key, LPCTSTR item,
	LPBYTE pData, UINT nBytes)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
//	TCHAR szValue[512];
	DWORD dwKeySize = 510;

	_stprintf(szRoot,_T("%s\\%s"), root, key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_WRITE|KEY_SET_VALUE,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		LONG lResult;

		lResult = RegSetValueEx(hk, item, NULL, REG_BINARY,
			pData, nBytes);

		RegCloseKey(hk);
		return lResult == ERROR_SUCCESS;
	}
	else
		return FALSE;
	}


void LoadPlacement(CWnd *pwnd, LPCTSTR pszSection)
{
	UINT nBytes = 0;
	BYTE* pBytes = 0;

	regGetBin(pszSection, _T(""), REG_WINDOW_POS, &pBytes, &nBytes);

	if (nBytes == sizeof(WINDOWPLACEMENT))
	{
		//GetDesktopWindow()
		WINDOWPLACEMENT wp;
		::GetWindowPlacement(GetDesktopWindow(), &wp);
		
		if(((WINDOWPLACEMENT*)pBytes)->rcNormalPosition.left > wp.rcNormalPosition.right ||
			((WINDOWPLACEMENT*)pBytes)->rcNormalPosition.top > wp.rcNormalPosition.bottom)
		{
		}
		else
		{
			pwnd->SetWindowPlacement((WINDOWPLACEMENT*) pBytes);
		}
	}
	if (pBytes && nBytes) delete[] pBytes;

}

void SavePlacement(CWnd *pwnd,LPCTSTR pszSection)
{
	WINDOWPLACEMENT wp;
	pwnd->GetWindowPlacement(&wp);

	if(wp.showCmd != SW_HIDE)
		regSetBin(pszSection, _T(""), REG_WINDOW_POS, (BYTE*)&wp, sizeof(wp));
}

CString getVersionInfo(HMODULE hLib, LPCTSTR csEntry)
{
	CFileVersionInfo verinfo;

	if (verinfo.Create(hLib))
	{
		if (_tcscmp(csEntry,VER_NUMBER) == 0)
			return verinfo.GetFileVersion();

		if (_tcscmp(csEntry,VER_COMPANY) == 0)
			return verinfo.GetCompanyName();

		if (_tcscmp(csEntry,VER_COPYRIGHT) == 0)
			return verinfo.GetLegalCopyright();
	}
	return _T("");
}


CString getVersionInfo2(HMODULE hLib, CString csEntry)
{
	CString csRet;

	if (hLib == NULL)
		hLib = AfxGetResourceHandle();

	HRSRC hVersion = ::FindResource( hLib, MAKEINTRESOURCE(VS_VERSION_INFO), RT_VERSION );
	if (hVersion != NULL)
	{
		HGLOBAL hGlobal = LoadResource( hLib, hVersion ); 
		if ( hGlobal != NULL)  
		{  
			LPVOID versionInfo  = LockResource(hGlobal);  
			if (versionInfo != NULL)
			{
				DWORD vLen,langD;
				BOOL retVal;    

				LPVOID retbuf=NULL;
				static char fileEntry[256];

				sprintf_s(fileEntry,"\\VarFileInfo\\Translation");
				retVal = VerQueryValue(versionInfo,fileEntry,&retbuf,(UINT *)&vLen);
				if (retVal && vLen==4) 
				{
					memcpy(&langD,retbuf,4);            
					sprintf_s(fileEntry, "\\StringFileInfo\\%02X%02X%02X%02X\\%s",
						(langD & 0xff00)>>8,langD & 0xff,(langD & 0xff000000)>>24, 
						(langD & 0xff0000)>>16, csEntry);            
				}
				else 
					sprintf_s(fileEntry, "\\StringFileInfo\\%04X04B0\\%s",  GetUserDefaultLangID(), csEntry);

				if (VerQueryValue(versionInfo,fileEntry,&retbuf,(UINT *)&vLen)) 
					csRet = (char*)retbuf;
			}
		}

		UnlockResource( hGlobal );  
		FreeResource( hGlobal );  
	}
	return csRet;
}


CString getLangSet()
{
	CString sValue;
	sValue = regGetStr(REG_ROOT,REG_LANG_KEY,REG_LANG_ITEM,_T(""));
	return sValue;
}

CString getLanguageDir(void)
{
	CString sPath;
	CString sModulesPath;
	
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"),sPath,SUBDIR_LANGUAGE);

	return sModulesPath;
}

CString getReportsDir(void)
{
	CString sPath;
	CString sModulesPath;
	VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"), sPath, SUBDIR_REPORTS);

	return sModulesPath;
}



// Check if data in Report grid is valid; 070205 p�d
BOOL CMyReportCtrl2::isDataValid(void)
{
	double fValue;
	CString sValue;
	if (isDoValidate())
	{
		CXTPReportControl *rep = this;
		if (rep->GetSafeHwnd() != NULL)
		{
			CXTPReportRecords *pRecs = rep->GetRecords();
			CXTPReportColumns* pCols = rep->GetColumns();

			if (pCols != NULL && pRecs != NULL)
			{
				for (int col = 0;col < pCols->GetCount();col++)
				{
					if (col > 0)
					{
						fValue = 0.0;
						for (int i = 0;i < pRecs->GetCount();i++)
						{
							CXTPReportRecord *pRecord = pRecs->GetAt(i);
							if (pRecord != NULL)
							{
								CXTPReportRecordItem *pRec = pRecord->GetItem(pCols->GetAt(col));
								sValue = pRec->GetCaption(pCols->GetAt(col));
								fValue += atof(sValue);
							}	// if (pRecord != NULL)
						}	// for (int i = 0;i < pRecs->GetCount();i++)
						if (fValue != getValidateValue())
							return FALSE;
					}	// if (col >= 0)
				}	// for (int col = 0;col < pCols->GetCount();col++)
			}	// if (pCols != NULL && pRecs != NULL)
		}	// if (rep->GetSafeHwnd() != NULL)
	}	// if (isDoValidate())

	return TRUE;

}

void CMyReportCtrl2::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	bIsDirty = TRUE;
	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
	CXTPReportControl::OnChar(nChar,nRepCnt,nFlags);
}

void CMyReportCtrl2::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	bIsDirty = TRUE;
	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
	CXTPReportControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

void setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}	// if (pWnd)
}




CString getDateEx(void)
{
	CString sDate;
	CTime t = CTime::GetCurrentTime();
	sDate.Format(_T("%04d%02d%02d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay());

	return sDate;
}

BOOL GetUserDBInRegistry(LPTSTR db_name)
{
	// Save to registry; 060303 p�d
	_tcscpy(db_name,regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_SELDB));

	return TRUE;
}


int GetAuthentication()
{
	CString sAuthenticaction;
	sAuthenticaction = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_DB_AUTHENTICATION_ITEM);
	return _tstoi(sAuthenticaction);
}

BOOL GetAdminIniData(TCHAR* db_serv,TCHAR* location,TCHAR* user,TCHAR* psw,TCHAR* dsn_name,TCHAR* client,int *idx)
{
	CString sDBServer;
	CString sDBLocation;
	CString sDBUser;
	CString sDBPsw;
	CString sDSNName;
	CString sDBClient;
	CString sAuthenticaction;

	sDBLocation = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBLOCATION);
	sDBServer = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBSERVER);
	sDBUser = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_USERNAME);
	sDBPsw = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_PSW);
	sDSNName = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DSN);
	sDBClient = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_CLIENT);
	sAuthenticaction = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_DB_AUTHENTICATION_ITEM);

	_tcscpy(db_serv,sDBServer.GetBuffer());
	_tcscpy(location,sDBLocation.GetBuffer());
	_tcscpy(user,sDBUser.GetBuffer());
	_tcscpy(psw,sDBPsw.GetBuffer());
	_tcscpy(dsn_name,sDSNName.GetBuffer());
	_tcscpy(client,sDBClient.GetBuffer());
	*idx = _tstoi(sAuthenticaction);

	return TRUE;
}

// Check what DBServer client is used, and setup dbname
// according to spec. in SQLApi++; 060310 p�d
CString setDBServerName(SAClient_t &client,LPCTSTR db_name,LPCTSTR db_selected)
{
	CString sDBName;
	switch(client)
	{
		case SA_MySQL_Client:
		case SA_SQLServer_Client:
		{
			if (db_selected == _T(""))
				sDBName.Format(_T("%s@"),db_name);
			else
				sDBName.Format(_T("%s@%s"),db_name,db_selected);
			return sDBName;
		}
	}
	return _T("");
}

BOOL getDBLocation(LPTSTR db_location)
{
	CString sAdminPath;
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBClient[32];
	int nIsWindowsAuthentication;
	// Get data from admin.ini file
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	
	// Setup return values; 060317 p�d
	_tcscpy(db_location,szDBLocation);

	return TRUE;
}

BOOL getDBUserInfo(LPTSTR db_path,LPTSTR user_name,LPTSTR psw,LPTSTR dsn_name,LPTSTR location,LPTSTR db_name,SAClient_t *client)
{
	CString S;
	TCHAR szDBServerPath[128];
	TCHAR szDBLocation[128];
	TCHAR szDB[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	SAClient_t m_client;

	if(getDBInfo(ADMIN_DB_NAME,szDBServerPath,szDBUser,szDBPsw,szDSNName,&m_client) && 
		 getDBLocation(szDBLocation))
	{
		GetUserDBInRegistry(szDB);
		
		S.Format(_T("%s%s"),setDBServerName(m_client,szDBLocation),szDB);

		*client = m_client;
		_tcscpy(db_path,S);
		_tcscpy(user_name,szDBUser);
		_tcscpy(psw,szDBPsw);
		_tcscpy(dsn_name,szDSNName);
		_tcscpy(location,szDBLocation);
		_tcscpy(db_name,szDB);

		return TRUE;
	}
	return FALSE;
}

BOOL getDBInfo(LPCTSTR schema,LPTSTR db_path,LPTSTR db_user,LPTSTR db_psw,LPTSTR dsn_name,SAClient_t *client)
{
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBServerPath[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBClient[32];
	SAClient_t saClient;
	int nIsWindowsAuthentication;
	// Get data from admin.ini file
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	// Set client foe SQLApi

	if (_tcscmp(szDBClient,MySQL) == 0)	// MySQL Server
	{
		saClient = SA_MySQL_Client;
	}
	else if (_tcscmp(szDBClient,SQLServer) == 0)	// MySQL Server
	{
		saClient = SA_SQLServer_Client;
	}

		
	_stprintf(szDBServerPath,_T("%s%s"),setDBServerName(saClient,szDBLocation),schema);
	// Setup return values; 060217 p�d
	*client  = saClient;
	_tcscpy(db_path,szDBServerPath);
	_tcscpy(db_user,szDBUser);
	_tcscpy(dsn_name,szDSNName);
	_tcscpy(db_psw,szDBPsw);

	return TRUE;
}





*/