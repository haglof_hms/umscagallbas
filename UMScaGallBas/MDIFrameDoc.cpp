
#include "StdAfx.h"
#include "MDIFrameDoc.h"
#include "License.h"
#include "UMScaGallBas.h"

/***********************************************************************************************/
// CMDIFrameDoc
/***********************************************************************************************/
IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)
BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction
CMDIFrameDoc::CMDIFrameDoc()
{
}
CMDIFrameDoc::~CMDIFrameDoc()
{
}
BOOL CMDIFrameDoc::OnNewDocument()
{
	WriteToLog(_T("CMDIFrameDoc::OnNewDocument 1"),false);
	//check license
	if(!License())
	{
		WriteToLog(_T("CMDIFrameDoc::OnNewDocument 2"),true);
		return FALSE;
	}
	if (!CDocument::OnNewDocument())
	{
		WriteToLog(_T("CMDIFrameDoc::OnNewDocument 3"),true);
		return FALSE;
	}
   WriteToLog(_T("CMDIFrameDoc::OnNewDocument 4"),false);
	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization
void CMDIFrameDoc::Serialize(CArchive& ar)
{
}
/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics
#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}
void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG
// CMDIFrameDoc commands
