#pragma once

#ifndef _HMS_UMHMSMISC_H_
#define _HMS_UMHMSMISC_H_

#include "MyMessageDlg.h"


//#include <wincrypt.h>
//#include <vector>

//////////////////////////////////////////////////////////////////////////////
// CMyTabControl22; derived from CXTPTabControl; 060216 p�d

class CMyTabControl2 : public CXTPTabControl
{
//protected:
//	void OnItemClick(CXTPTabManagerItem* pItem);

public:
	CMyTabControl2();

	// Return the page that's active in control; 060324 p�d
	CXTPTabManagerItem *getSelectedTabPage(void);

	// Return the page set by index; 060405 p�d
	CXTPTabManagerItem *getTabPage(int idx);

	// Return number of tabs; 060405 p�d
	int getNumOfTabPages(void);
};

class CMyExtEdit2 : public CXTEdit 
{
//private:
  // default colors
  enum
  {
     ENABLED_FG = RGB(0,0,0), // black
     ENABLED_BG = RGB(255,255,255), // white
     DISABLED_FG = RGB(0,0,0), // black
     DISABLED_BG = RGB(192,192,192), // light grey
  };

	//int m_nItemData;
	CString m_sItemData;

	TCHAR szValue[128];
	//TCHAR szIdentifer[128];

	BOOL m_bIsNumeric;	// TRUE if only numeric values are allowed

	//BOOL m_bIsAsH100;		// TRUE if the editbox'll set value according to H100; 070508 p�d

	
	/*
	CString setAsH100(LPCTSTR str)
	{
		TCHAR szBuffer[32];
		strcpy(szBuffer,str);
		// If empty string, return; 070510 p�d
		if (strcmp(szBuffer,"") == 0)
			return "";

		// Check first character in szBuffer, to see which
		// Specie's selected. I.e. 1 = Pine, 2 = Spruce all rest
		// will be Birch, if not IMP (Impediment) is set; 070508 p�d
		if (strcmp(szBuffer,"IMP") == 0 || strcmp(szBuffer,"imp") == 0)
			return "IMP";

		if (szBuffer[0] == '1' || szBuffer[0] == 'T' || szBuffer[0] == 't')
			szBuffer[0] = 'T';
		else	if (szBuffer[0] == '2' || szBuffer[0] == 'G' || szBuffer[0] == 'g')
			szBuffer[0] = 'G';
		else
			szBuffer[0] = 'B';
		return szBuffer;
	}*/

	// Background brush
  CBrush *m_pbrushDisabled;
  // Foreground brush
  CBrush *m_pbrushEnabled;

	CFont *m_fnt1;

	BOOL	m_bModified;
// Construction
public:
	CMyExtEdit2();
/*
	void setItemIntData(int v)
	{
		m_nItemData = v;
	}
	int getItemIntData(void)
	{
		return m_nItemData;
	}

	void setItemStrData(LPCTSTR v)
	{
		m_sItemData = v;
	}

	CString getItemStrData(void)
	{
		return m_sItemData;
	}*/
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtEdit2)
	virtual void PreSubclassWindow();
	//virtual BOOL PreTranslateMessage(MSG *);
	//}}AFX_VIRTUAL

	void SetReadOnly(BOOL bReadOnly = TRUE);
	void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
	void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);

//	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
//	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetAsNumeric(void);
	//void SetAsH100(void);

	virtual ~CMyExtEdit2();

	double getFloat(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		if(_tcslen(szValue)==0)
			return MY_NULL;
		else
			return _tstof(szValue);
	}
	void setFloat(double value,int dec)
	{
		if(value==MY_NULL)
			_stprintf(szValue,_T("%s"),"");
		else
			_stprintf(szValue,_T("%.*f"),dec,value);
		SetWindowText((LPTSTR)szValue);
	}

	int getInt(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		if(_tcslen(szValue)==0)
			return MY_NULL;
		else
			return _tstoi(szValue);
	}
	void setInt(int value)
	{
		if(value==MY_NULL)
			_stprintf(szValue,_T("%s"),"");
		else
			_stprintf(szValue,_T("%d"),value);
		SetWindowText((LPCTSTR)szValue);
	}

	CString getText(void)
	{
		
		GetWindowText(m_sItemData);
		return m_sItemData;
		/*
		if (m_bIsAsH100)
		{
			return setAsH100(szValue);
		}
		else
		{
			return (LPTSTR)szValue;
		}*/
	}
/*
	void setIdentifer(LPCTSTR id)
	{
		_tcscpy_s(szIdentifer,128,id);
	}

	LPCTSTR getIdentifer(void)
	{
		return (LPCTSTR)szIdentifer;
	}*/

	BOOL isDirty(void)
	{
		return m_bModified;
	}

	void setIsDirty(void)
	{
		m_bModified = TRUE;
	}

	void resetIsDirty(void)
	{
		m_bModified = FALSE;
	}

	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crFGEnabled;			// Holds the Background Color for the Text for Enabled window
	COLORREF m_crBGEnabled;			// Holds the Color for the Text for Enabled window
	COLORREF m_crFGDisabled;		// Holds the Background Color for the Text for Disabled window
	COLORREF m_crBGDisabled;		// Holds the Color for the Text for Disabled window
	//{{AFX_MSG(CMyExtEdit2)
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic2 window

class CMyExtStatic2 : public CStatic
{
// Construction
public:
	CMyExtStatic2();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtStatic2)
	//}}AFX_VIRTUAL

	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetLblFont(int,int,LPTSTR font_name = _T("Arial"));
	void SetLblFont();
	// Set font items except fontface; 071130 p�d
	void SetLblFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);
	virtual ~CMyExtStatic2();

protected:
	CString m_sText;
	CFont *m_fnt1;
	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crBkColor; // Holds the Background Color for the Text
	COLORREF m_crTextColor; // Holds the Color for the Text
	//{{AFX_MSG(CMyExtStatic2)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor); // This Function Gets Called Every Time Your Window Gets Redrawn.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};



//////////////////////////////////////////////////////////////////////////
// CMyReportCtrl2; derived from CXTPReportControl
class CMyReportCtrl2 : public CXTPReportControl
{
	DECLARE_DYNCREATE(CMyReportCtrl2)
//private:	
	/*BOOL bIsDirty;
	BOOL m_bDoValidateCol;
	BOOL m_bDoEditFirstColumn;
	BOOL m_bRunMetrics;
	CList<int,int&> m_vecDcls;
	CList<int,int&> m_vecStart;
	CList<int,int&> m_vecNumOf;
	int m_nID;
	int m_nFromColumn;
	CBrush m_hBrush;
	CBrush *m_pBrush;
 
	double m_fValidateValue;*/
public:
	CMyReportCtrl2();
	CMyReportCtrl2(const CMyReportCtrl2 &);

	BOOL Create(CWnd* pParentWnd,UINT nID,BOOL add_hscroll = FALSE,BOOL run_metrics = TRUE);
		// Methods
	BOOL ClearReport(void);

	CMyReportCtrl2 operator=(const CMyReportCtrl2 &c)
	{
		return c;
	}

	//afx_msg void OnValueChanged1(NMHDR * pNotifyStruct, LRESULT * /*result*/);
/*
	// Virtual method
	virtual void GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics);



	BOOL isDirty(void);

	void setIsDirty(BOOL val);

	// Do edit column method
	void setDoEditFirstColumn(BOOL val)
	{
		m_bDoEditFirstColumn = val;
	}
	
	// Validation methods
	void setValidateColumns(BOOL validate,int from_col,double validate_value)
	{
		m_bDoValidateCol = validate;
		m_nFromColumn = from_col;
		m_fValidateValue = validate_value;
	}

	BOOL isDoValidate(void)
	{
		return m_bDoValidateCol;
	}

	double getValidateValue(void)
	{
		return m_fValidateValue;
	}

	BOOL isDataValid(void);

	CXTPReportColumn* AddMyColumn(CXTPReportColumn* pColumn,int dcls,int start,int num_of)
	{
		CXTPReportControl::AddColumn(pColumn);
		m_vecDcls.AddTail(dcls);
		m_vecStart.AddTail(start);
		m_vecNumOf.AddTail(num_of);
		return pColumn;
	}

	// Column value methods
	int getFromColumn(void)
	{
		return m_nFromColumn;
	}


	// Always retrive the Last item in list
	BOOL getItemData(int idx,int *dcls,int *start = 0,int *num_of = 0)
	{
		BOOL bIsDCLS = FALSE;
		BOOL bIsStart = FALSE;
		BOOL bIsNumOf = FALSE;
		POSITION posDcls = m_vecDcls.FindIndex(idx);
		if (posDcls)
		{
			*dcls = (int)m_vecDcls.GetAt(posDcls);
			bIsDCLS = TRUE;
		}

		POSITION posStart = m_vecStart.FindIndex(idx);
		if (posStart)
		{
			*start = (int)m_vecStart.GetAt(posStart);
			bIsStart = TRUE;
		}

		POSITION posNumOf = m_vecNumOf.FindIndex(idx);
		if (posNumOf)
		{
			*num_of = (int)m_vecNumOf.GetAt(posNumOf);
			bIsNumOf = TRUE;
		}

		return (bIsDCLS && bIsStart && bIsNumOf);
	}

	// Always retrive the Last item in list
	BOOL getLastItemData(int *dcls,int *start = 0,int *num_of = 0)
	{
		BOOL bIsDCLS = FALSE;
		BOOL bIsStart = FALSE;
		BOOL bIsNumOf = FALSE;
		POSITION posDcls = m_vecDcls.GetTailPosition();
		if (posDcls)
		{
			*dcls = (int)m_vecDcls.GetAt(posDcls);
			bIsDCLS = TRUE;
		}

		POSITION posStart = m_vecStart.GetTailPosition();
		if (posStart)
		{
			*start = (int)m_vecStart.GetAt(posStart);
			bIsStart = TRUE;
		}

		POSITION posNumOf = m_vecNumOf.GetTailPosition();
		if (posNumOf)
		{
			*num_of = (int)m_vecNumOf.GetAt(posNumOf);
			bIsNumOf = TRUE;
		}

		return (bIsDCLS && bIsStart && bIsNumOf);
	}

	void delLastItemData(void)
	{
		if (m_vecDcls.GetCount() > 0)
		{
			m_vecDcls.RemoveTail();
		}
		if (m_vecStart.GetCount() > 0)
		{
			m_vecStart.RemoveTail();
		}
		if (m_vecNumOf.GetCount() > 0)
		{
			m_vecNumOf.RemoveTail();
		}
	}

	void delAllItemData(void)
	{
		if (m_vecDcls.GetCount() > 0)
		{
			m_vecDcls.RemoveAll();
		}
		if (m_vecStart.GetCount() > 0)
		{
			m_vecStart.RemoveAll();
		}
		if (m_vecNumOf.GetCount() > 0)
		{
			m_vecNumOf.RemoveAll();
		}
	}

protected:
	//{{AFX_MSG(CMDIDBFormFrame)

	afx_msg void OnChar(UINT,UINT,UINT);
	afx_msg void OnKeyUp(UINT,UINT,UINT);

	//}}AFX_MSG*/

	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////
// CMyComboBox2 window

// Author: Robert Cremer

// A ComboBox with easy to switch between edit mode and non edit mode.
// (i. e. works like a drop down or a drop list)
// The ComboBox has real coloring, working in drop down and drop list mode.
// !!! The Box must not be a drop list (CBS_DROPDOWNLIST) !!!
// Working as a drop list can be done with a call to SetReadOnly()
// or with the CTor param false.
class CMyComboBox2 : public CComboBox
{
// Attributes
private:
   // default colors
   enum
   {
      ENABLED_FG = RGB(0,0,0), // black
      ENABLED_BG = RGB(255,255,255), // white
      DISABLED_FG = RGB(0,0,0), // black
      DISABLED_BG = RGB(192,192,192), // light grey
   };

   // edit mode
   bool m_bEditable;

   // the actual colors
   COLORREF m_crFGEnabled;
   COLORREF m_crBGEnabled;
   COLORREF m_crFGDisabled;
   COLORREF m_crBGDisabled;

	 CFont m_font;
   // Background brush
   CBrush *m_pbrushDisabled;
   // Foreground brush
   CBrush *m_pbrushEnabled;

	 BOOL m_bIsDirty;
// Operations
public:
  void SetReadOnly(BOOL bReadOnly = true);
  void SetReadOnlyEx(BOOL bReadOnly = true);
  void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
  void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetLblFont(int size,int weight,LPTSTR font_name = _T("Arial"));
	CString getText(void);


// Overrides
// ClassWizard generated virtual function overrides
//{{AFX_VIRTUAL(CMyComboBox2)
//}}AFX_VIRTUAL

	BOOL isDirty(void)
	{
		return m_bIsDirty;
	}

	void resetIsDirty(void)
	{
		m_bIsDirty = FALSE;
	}

// Implementation
public:
   // Construction
   CMyComboBox2(BOOL bEditable = TRUE);
   virtual ~CMyComboBox2();

   // Generated message map functions
protected:
  //{{AFX_MSG(CMyComboBox2)
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
  afx_msg void OnEnable(BOOL bEnable);
	afx_msg BOOL OnCBoxChange();
	afx_msg BOOL OnCBoxDropDown();
  //}}AFX_MSG

   DECLARE_MESSAGE_MAP()
};

CString getHxlDllDir(void);
BOOL isInt(TCHAR *str);
BOOL isInteger(UINT value);
// Open the formview for selecting e.g. F�rvaltning, distrikt etc
void showFormView(int idd,LPCTSTR lang_fn);
// Get CView fy ID of form. E.g. IDD_FORMVIEW8; 061020 p�d
CView *getFormViewByID(int idd);
BOOL messageDialog(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg);
BOOL isTal(TCHAR *str);
BOOL isTalet(UINT value);

/*class _user_msg
{
//private:
	int m_nIndex;									// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szFunc[128];					// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szSuite[128];					// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szName[128];					// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szFileName[MAX_PATH];	// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szArgStr[MAX_PATH];				// Can containe a string of arguments, added to a
																// reports "variables". E.g. arg1;arg2;arg3;etc..; 070207 p�d
public:
	_user_msg(void)
	{
		m_nIndex = -1;
		_tcscpy_s(m_szFunc,128,(const TCHAR *)(""));
		_tcscpy_s(m_szSuite,128,(const TCHAR *)(""));
		_tcscpy_s(m_szName,128,(const TCHAR *)(""));
		_tcscpy_s(m_szFileName,MAX_PATH,(const TCHAR *)(""));
		_tcscpy_s(m_szArgStr,MAX_PATH,(const TCHAR *)(""));
	}
	_user_msg(int i1,LPCTSTR str1,LPCTSTR str2,LPCTSTR str3,LPCTSTR str4,LPCTSTR str5)
	{
		m_nIndex = i1;
		_tcscpy_s(m_szFunc,128,(const TCHAR *)(str1));
		_tcscpy_s(m_szSuite,128,(const TCHAR *)(str2));
		_tcscpy_s(m_szName,128,(const TCHAR *)(str3));
		_tcscpy_s(m_szFileName,MAX_PATH,(const TCHAR *)(str4));
		_tcscpy_s(m_szArgStr,MAX_PATH,(const TCHAR *)(str5));
	}
	_user_msg(const _user_msg &c)
	{
		*this = c;
	}

	int getIndex(void)				{ return m_nIndex; }

	LPCTSTR getFunc(void)			{ return m_szFunc; }
	LPCTSTR getSuite(void)			{ return m_szSuite; }
	LPCTSTR getName(void)			{ return m_szName; }
	LPCTSTR getFileName(void)	{ return m_szFileName; }
	LPCTSTR getArgStr(void)		{ return m_szArgStr; }
};

#define BLACK				RGB(  0,  0,  0)
#define WHITE				RGB(255,255,255)
#define GRAY				RGB(192,192,192)
#define BLUE				RGB(0,0,255)
#define GREEN				RGB(0,255,0)
#define RED					RGB(255,0,0)
#define INFOBK			::GetSysColor(COLOR_INFOBK)
#define COL3DFACE		::GetSysColor(COLOR_3DFACE)


BOOL fileExists(LPCTSTR fn);

void regSetInt(LPCTSTR root,LPCTSTR key,LPCTSTR item,DWORD value);
DWORD regGetInt(LPCTSTR root,LPCTSTR key,LPCTSTR item, int nDefault);

void regSetStr(LPCTSTR root,LPCTSTR key,LPCTSTR item,LPCTSTR value);
CString regGetStr(LPCTSTR root,LPCTSTR key,LPCTSTR item,LPCTSTR szDefault);
CString regGetStr(LPCTSTR root,LPCTSTR key,LPCTSTR item,LPCTSTR def_str = _T(""));

BOOL regGetBin(LPCTSTR root,LPCTSTR key,LPCTSTR item, BYTE** ppData, UINT* pBytes);
BOOL regSetBin(LPCTSTR root,LPCTSTR key,LPCTSTR item, LPBYTE pData, UINT nBytes);

void getWindowPlacement(LPCTSTR root,						// Root in registry
												WINDOWPLACEMENT *wp,		// Placement for window structure
												RECT def_placement			// A default value, if there's no entry in registry
												);

void setWindowPlacement(LPCTSTR root,				// Root in registry
												CWnd *wnd						// Window to save settings
												);

void LoadPlacement(CWnd *pwnd,LPCTSTR pszSection);
void SavePlacement(CWnd *pwnd,LPCTSTR pszSection);

 Constants for version information
const LPCTSTR VER_NUMBER			= _T("FileVersion");
const LPCTSTR VER_COMPANY		= _T("CompanyName");
const LPCTSTR VER_COPYRIGHT		= _T("LegalCopyright");

const LPCTSTR SUBDIR_SCRIPTS	= _T("SQL script");

// get info about versions
CString getVersionInfo(HMODULE hLib, LPCTSTR csEntry);
CString getLangSet();
CString getLanguageDir(void);
CString getReportsDir(void);


void setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos = FALSE);

CString getDateEx(void);

const LPCTSTR REG_KEY_DATABASE						= _T("Database");
const LPCTSTR REG_STR_DBLOCATION					= _T("DBLocation");
const LPCTSTR REG_STR_DBSERVER						= _T("DBServer");
const LPCTSTR REG_STR_USERNAME						= _T("UserName");
const LPCTSTR REG_STR_PSW							= _T("Password");
const LPCTSTR REG_STR_DSN							= _T("DSN");
const LPCTSTR REG_STR_CLIENT						= _T("Client");
const LPCTSTR REG_STR_SELDB							= _T("DBSelected");
const LPCTSTR REG_DB_QUALIFIED_ITEM			= _T("IsQualified");	// Tell us if user is qualified to change database settings; 
const LPCTSTR REG_DB_CONNECTED_ITEM			= _T("IsConnected");	// Set a key on if there's a conenction to a Database server or not; 070329 p�d
const LPCTSTR REG_DB_AUTHENTICATION_ITEM	= _T("IsWindowsAuthentication");	// Set a key on if Windows Authentication  = 0 or Server Authentication = 1

// Name of the database holding tabels for administration; 060217 p�d
const LPCTSTR ADMIN_DB_NAME					= _T("hms_administrator");

const LPCTSTR MySQL									= _T("MySQL");
const LPCTSTR SQLServer								= _T("SQLServer");


BOOL GetAdminIniData(TCHAR* db_serv,TCHAR* location,TCHAR* user,TCHAR* psw,TCHAR* dsn_name,TCHAR* client,int *idx);

BOOL GetUserDBInRegistry(LPTSTR db_name);

CString setDBServerName(SAClient_t &client,LPCTSTR db_name,LPCTSTR db_selected = _T(""));

BOOL getDBInfo(LPCTSTR schema,LPTSTR db_path,LPTSTR db_user,LPTSTR db_psw,LPTSTR dsn_name,SAClient_t *client);
BOOL getDBLocation(LPTSTR db_location);
BOOL getDBUserInfo(LPTSTR db_path,LPTSTR user_name,LPTSTR psw,LPTSTR dsn_name,LPTSTR location,LPTSTR db_name,SAClient_t *client);
int GetAuthentication();

*/
#endif