#pragma once
#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"

#define NUM_OF_LIST_COLS 14
extern int nForenklad_global;		


class CScaGBTraktSelectList : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CScaGBTraktSelectList)
	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CScaGBTraktSelectList();   // standard constructor
	virtual ~CScaGBTraktSelectList();

	CMyReportCtrl2 m_wndTraktReport;
	BOOL setupReport(void);
	void populateReport(void);
	//int m_nForenklad;

	vecTraktData m_vecTraktData;

	CString m_sEnteredManual;
	CString m_sEnteredFromFile;

	void getTraktsFromDB(int forenklad);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

// Dialog Data
	enum { IDD = IDD_FORMVIEW3 };

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	//void forenklad(int forenklad);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
