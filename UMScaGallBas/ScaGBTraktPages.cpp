#include "stdafx.h"
#include "ScaGBTraktPages.h"
#include "ResLangFileReader.h"
#include "MyProgressBarDlg.h"
	
IMPLEMENT_DYNCREATE(CMyReportPageBase,CXTPReportView)
//------------------------------------------------------------------------------------------------------
//		BASKLASS FLIKAR
//------------------------------------------------------------------------------------------------------
CMyReportPageBase::CMyReportPageBase()
	:CXTPReportView()
{
		m_nSpecArray[0]=IDS_STRING7600;
		m_nSpecArray[1]=IDS_STRING7601;
		m_nSpecArray[2]=IDS_STRING7602;
		m_nSpecArray[3]=IDS_STRING7603;
		m_nSpecArray[4]=IDS_STRING7604;
		m_nSpecArray[5]=IDS_STRING7605;
		m_nSpecArray[6]=IDS_STRING7606;
		//m_nSpecArray[7]=IDS_STRING7607;
		m_nSpecArray[7]=IDS_STRING7608;

		m_nReportId[0]=IDC_TRAKT_DATA_REPORT;
		m_nReportId[1]=IDC_TRAKT_DATA_REPORT2;
		m_nReportId[2]=IDC_TRAKT_DATA_REPORT3;
		m_nReportId[3]=IDC_TRAKT_DATA_REPORT4;
		
		m_nVarArray[0]=IDS_STRING7590;
		m_nVarArray[1]=IDS_STRING7591;
		m_nVarArray[2]=IDS_STRING7592;
		m_nVarArray[3]=IDS_STRING7593;
		m_nVarArray[4]=IDS_STRING7594;
		m_nVarArray[5]=IDS_STRING7595;
		m_nVarArray[6]=IDS_STRING7596;
		m_nVarArray[7]=IDS_STRING7597;
		m_nVarArray[8]=IDS_STRING7598;

		m_nYtArray[0]=IDS_STRING7630;
		m_nYtArray[1]=IDS_STRING7631;
		m_nYtArray[2]=IDS_STRING7632;
		m_nYtArray[3]=IDS_STRING7690;
		m_nYtArray[4]=IDS_STRING7633;
		m_nYtArray[5]=IDS_STRING7634;
		m_nYtArray[6]=IDS_STRING7691;
		m_nYtArray[7]=IDS_STRING7635;
		m_nYtArray[8]=IDS_STRING7636;
		m_nYtArray[9]=IDS_STRING7637;
		m_nYtArray[10]=IDS_STRING7638;
		m_nYtArray[11]=IDS_STRING7639;
		m_nYtArray[12]=IDS_STRING7640;
		m_nYtArray[13]=IDS_STRING7641;
		m_nYtArray[14]=IDS_STRING7642;
		m_nYtArray[15]=IDS_STRING7643;

		m_nYtArray[16]=IDS_STRING7665;
		m_nYtArray[17]=IDS_STRING7666;
		m_nYtArray[18]=IDS_STRING7667;
		m_nYtArray[19]=IDS_STRING7668;
		m_nYtArray[20]=IDS_STRING7669;
		m_nYtArray[21]=IDS_STRING7670;
		m_nYtArray[22]=IDS_STRING7671;
		m_nYtArray[23]=IDS_STRING7672;
		m_nYtArray[24]=IDS_STRING7673;
		m_nYtArray[25]=IDS_STRING7674;
		m_nYtArray[26]=IDS_STRING7675;
		m_nYtArray[27]=IDS_STRING7676;
		m_nYtArray[28]=IDS_STRING7677;
		m_nYtArray[29]=IDS_STRING7678;


		m_nYtVarShow[0]=1;
		m_nYtVarShow[1]=1;
		m_nYtVarShow[2]=1;
		m_nYtVarShow[3]=1;
		m_nYtVarShow[4]=1;
		m_nYtVarShow[5]=1;
		m_nYtVarShow[6]=1;
		m_nYtVarShow[7]=1;
		m_nYtVarShow[8]=1;
		m_nYtVarShow[9]=1;
		m_nYtVarShow[10]=1;
		m_nYtVarShow[11]=1;
		m_nYtVarShow[12]=1;
		m_nYtVarShow[13]=1;
		m_nYtVarShow[14]=1;
		m_nYtVarShow[15]=1;

		m_nYtVarShow[16]=0;
		m_nYtVarShow[17]=1;
		m_nYtVarShow[18]=1;
		m_nYtVarShow[19]=1;
		m_nYtVarShow[20]=1;
		m_nYtVarShow[21]=1;
		m_nYtVarShow[22]=1;
		m_nYtVarShow[23]=1;
		m_nYtVarShow[24]=1;
		m_nYtVarShow[25]=1;
		m_nYtVarShow[26]=1;
		m_nYtVarShow[27]=1;
		m_nYtVarShow[28]=1;
		m_nYtVarShow[29]=1;

		m_nTreeArray[0]=IDS_STRING7680;
		m_nTreeArray[1]=IDS_STRING7681;
		m_nTreeArray[2]=IDS_STRING7682;
		m_nTreeArray[3]=IDS_STRING7683;
		m_nTreeArray[4]=IDS_STRING7684;
		m_nTreeArray[5]=IDS_STRING7685;
		m_nTreeArray[6]=IDS_STRING7686;

		m_nMaxValues=MAX_MIN_DATA();

		// Setup language filename; 051214 p�d
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);


		//Stubbe i skog Kvarv   Stubbe i v�g
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{					
				m_sTreeTypes[0]=xml->str(IDS_STRING7687);
				m_sTreeTypes[1]=xml->str(IDS_STRING7688);
				m_sTreeTypes[2]=xml->str(IDS_STRING7689);
			}
		}
}

void CMyReportPageBase::ClearReport(CXTPReportControl *rep)
{
	if (rep->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords* pRecords = rep->GetRecords();
		if (pRecords)
		{
			pRecords->RemoveAll();
			return;
		}
	}		
}


//------------------------------------------------------------------------------------------------------
//		KVARVARANDE
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage1, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage1, CMyReportPageBase)
	//ON_WM_CHAR()
	//ON_WM_KEYUP()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT2, OnValueChanged2)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT3, OnValueChanged3)
END_MESSAGE_MAP()

CMyReportPage1::CMyReportPage1()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

// Initiera kolumnheaders i fliken
void CMyReportPage1::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_KVARV_TO_SHOW+1;nCol++)
			{
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nVarArray[nCol]), COL_SIZE_TAB));
				if(nCol!=0)
				{
					if(ALLOW_EDIT_KVARV)
					{
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
					pCol->GetEditOptions()->m_dwEditStyle  = ES_CENTER;	
					pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			pCol->SetHeaderAlignment( DT_CENTER );
			pCol->SetAlignment( DT_CENTER );

			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);

			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}
//---------------------------
// Visa Data f�r kvarvarande
//---------------------------
void CMyReportPage1::showData(TRAKT_DATA data)
{
	
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			for(nSpec=0;nSpec<NUM_OF_SP+1;nSpec++)
				m_ReportCtrl->AddRecord(new CTraktViewData(nSpec,PAGE_NR_KVARV,data,xml->str(m_nSpecArray[nSpec])));

			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r kvarvarande
//---------------------------
void CMyReportPage1::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0,tempCol=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_KVARV_TO_SHOW;nCol++)
		{
			//value=pRec->getColumnDouble(nCol+1);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol+1);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			tempCol=m_nMaxValues.varIdx_kvarv[nCol+1];
			data->m_recKvarvarande[tempCol][nRow]=value;
			
			/*switch(nCol+1)
			{
			case 1:	tempCol=KVARV_VARINDEX_M3SK;		data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 2:	tempCol=KVARV_VARINDEX_M3FUB;		data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 3:	tempCol=KVARV_VARINDEX_M3STAMSK;	data->m_recKvarvarande[tempCol][nRow]=value;break;	
			case 4:	tempCol=KVARV_VARINDEX_GY;			data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 5:	tempCol=KVARV_VARINDEX_ANTAL;		data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 6:	tempCol=KVARV_VARINDEX_DG;			data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 7:	tempCol=KVARV_VARINDEX_H25;		data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 8:	tempCol=KVARV_VARINDEX_DGV;		data->m_recKvarvarande[tempCol][nRow]=value;break;
			}*/			
		}
	}
}

//---------------------------
// Data f�r kvarvarande har �ndrats
//---------------------------
void CMyReportPage1::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
	int max_value=0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	
	m_ReportCtrl=&GetReportCtrl();
  
	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();

		pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
		tst=pRec->getColumnText(nColIndex);
		if(!tst.IsEmpty())
		{
			if(!isTal(tst.GetBuffer(0)))
			{
				AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
				tst.Format(_T("%s"),"");
				pRec->setColumnText(nColIndex,tst);
			}
			else
			{
				value=(float)_tstof(tst);		
				max_value=m_nMaxValues.max_kvarv_var[m_nMaxValues.varIdx_kvarv[nColIndex]];
				if(value<0 || value>max_value)
				{
					tst.Format(_T("%s %d"),"0<= V�rde <=",max_value);
					AfxMessageBox(tst);
					value=0.0;
					tst.Format(_T("%10.1f"),value);
					pRec->setColumnText(nColIndex,tst);
				}
			}
		}			
		m_bIsDirty2=TRUE;
	}
}


//------------------------------------------------------------------------------------------------------
//		F�RE
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage7, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage7, CMyReportPageBase)
	//ON_WM_CHAR()
	//ON_WM_KEYUP()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT2, OnValueChanged2)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT3, OnValueChanged3)
END_MESSAGE_MAP()

CMyReportPage7::CMyReportPage7()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

// Initiera kolumnheaders i fliken
void CMyReportPage7::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();




	ClearReport(m_ReportCtrl);
	
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_FORE_TO_SHOW+1;nCol++)
			{
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nVarArray[nCol]),COL_SIZE_TAB));
				if(nCol!=0)
				{
					if(ALLOW_EDIT_FORE)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle  = ES_CENTER;	
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			pCol->SetHeaderAlignment( DT_CENTER );
			pCol->SetAlignment( DT_CENTER );

			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);

			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
	   	m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}
//---------------------------
// Visa Data f�r f�re
//---------------------------
void CMyReportPage7::showData(TRAKT_DATA data)
{
	
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			for(nSpec=0;nSpec<NUM_OF_SP+1;nSpec++)
				m_ReportCtrl->AddRecord(new CTraktViewData(nSpec,PAGE_NR_FORE,data,xml->str(m_nSpecArray[nSpec])));

			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r f�re
//---------------------------
void CMyReportPage7::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0,tempCol=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_KVARV_TO_SHOW;nCol++)
		{
			//value=pRec->getColumnDouble(nCol+1);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol+1);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			tempCol=m_nMaxValues.varIdx_fore[nCol+1];
			data->m_recFore[tempCol][nRow]=value;
			/*switch(nCol+1)
			{
			case 1:	tempCol=FORE_VARINDEX_M3SK;		data->m_recFore[tempCol][nRow]=value;break;
			case 2:	tempCol=FORE_VARINDEX_M3FUB;		data->m_recFore[tempCol][nRow]=value;break;
			case 3:	tempCol=FORE_VARINDEX_M3STAMSK;	data->m_recFore[tempCol][nRow]=value;break;	
			case 4:	tempCol=FORE_VARINDEX_GY;			data->m_recFore[tempCol][nRow]=value;break;
			case 5:	tempCol=FORE_VARINDEX_ANTAL;		data->m_recFore[tempCol][nRow]=value;break;
			case 6:	tempCol=FORE_VARINDEX_DG;			data->m_recFore[tempCol][nRow]=value;break;
			case 7:	tempCol=FORE_VARINDEX_H25;		data->m_recFore[tempCol][nRow]=value;break;
			case 8:	tempCol=FORE_VARINDEX_DGV;		data->m_recFore[tempCol][nRow]=value;break;
			}*/			
		}
	}
}

//---------------------------
// Data f�r f�re har �ndrats
//---------------------------
void CMyReportPage7::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0,max_value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	
	m_ReportCtrl=&GetReportCtrl();
  
	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();
		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
		tst=pRec->getColumnText(nColIndex);
		if(!tst.IsEmpty())
		{
			if(!isTal(tst.GetBuffer(0)))
			{
				AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
				tst.Format(_T("%s"),"");
				pRec->setColumnText(nColIndex,tst);
			}
			else
			{
				value=(float)_tstof(tst);		
				max_value=(float)m_nMaxValues.max_fore_var[m_nMaxValues.varIdx_fore[nColIndex]];
				if(value<0 || value>max_value)
				{
					tst.Format(_T("%s %d"),"0<= V�rde <=",max_value);
					AfxMessageBox(tst);
					value=0.0;
					tst.Format(_T("%10.1f"),value);
					pRec->setColumnText(nColIndex,tst);
				}
			}
		}			
		m_bIsDirty2=TRUE;
	}
}


//------------------------------------------------------------------------------------------------------
//		UTTAG MELLAN STICKV�G
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage2, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage2, CMyReportPageBase)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
END_MESSAGE_MAP()

CMyReportPage2::CMyReportPage2()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

void CMyReportPage2::OnInitialUpdate()
{
	int nCol=0,tempCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_UTTAG_TO_SHOW+1;nCol++)
			{
				tempCol=nCol;
				if(nCol==7)
					tempCol=8;
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nVarArray[tempCol]), COL_SIZE_TAB));
				if(nCol!=0)
				{
					if(ALLOW_EDIT_UTTMST)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle  = ES_CENTER;	
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}			
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}
//---------------------------
// Visa Data f�r uttag mellan stv
//---------------------------
void CMyReportPage2::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			for(nSpec=0;nSpec<NUM_OF_SP+1;nSpec++)
				m_ReportCtrl->AddRecord(new CTraktViewData(nSpec,PAGE_NR_UTMSTV,data,xml->str(m_nSpecArray[nSpec])));

			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r utt mstv
//---------------------------
void CMyReportPage2::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0,tempCol=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_UTTAG_TO_SHOW;nCol++)
		{
			//value=pRec->getColumnDouble(nCol+1);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol+1);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			switch(nCol)
			{
			case 0:	tempCol=UTTAG_VARINDEX_M3SK;		data->m_recUttagMstv[tempCol][nRow]=value;break;
			case 1:	tempCol=UTTAG_VARINDEX_M3FUB;		data->m_recUttagMstv[tempCol][nRow]=value;break;
			case 2:	tempCol=UTTAG_VARINDEX_M3STAMSK;	data->m_recUttagMstv[tempCol][nRow]=value;break;
			case 3:	tempCol=UTTAG_VARINDEX_GY;			data->m_recUttagMstv[tempCol][nRow]=value;break;
			case 4:	tempCol=UTTAG_VARINDEX_ANTAL;		data->m_recUttagMstv[tempCol][nRow]=value;break;
			case 5:	tempCol=UTTAG_VARINDEX_DG;			data->m_recUttagMstv[tempCol][nRow]=value;break;
			case 6:	tempCol=UTTAG_VARINDEX_DGV;		data->m_recUttagMstv[tempCol][nRow]=value;break;
			}
			
		}
	}
}

//---------------------------
// Data f�r utt mstv har �ndrats
//---------------------------
void CMyReportPage2::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	
	m_ReportCtrl=&GetReportCtrl();
  
	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		//Switcha Column/Variabel
		//switch(nColIndex)
		//{				
		//default:
		pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
		tst=pRec->getColumnText(nColIndex);
		if(!tst.IsEmpty())
		{
			if(!isTal(tst.GetBuffer(0)))
			{
				AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
				tst.Format(_T("%s"),"");
				pRec->setColumnText(nColIndex,tst);
			}
			else
			{
				value=(float)_tstof(tst);		
				if(value<0 || value>4000.0)
				{
					AfxMessageBox(_T(" 0<= V�rde <= 4000 "));
					value=0.0;
					tst.Format(_T("%10.1f"),value);
					pRec->setColumnText(nColIndex,tst);
				}
			}
		}			
		//break;
		//}
		m_bIsDirty2=TRUE;
	}
}

//------------------------------------------------------------------------------------------------------
//		UTTAG I STICKV�G
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage3, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage3, CMyReportPageBase)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
END_MESSAGE_MAP()

CMyReportPage3::CMyReportPage3()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

void CMyReportPage3::OnInitialUpdate()
{
	int nCol=0,tempCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_UTTAG_TO_SHOW+1;nCol++)
			{
				tempCol=nCol;
				if(nCol==7)
					tempCol=8;
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nVarArray[tempCol]), COL_SIZE_TAB));
				if(nCol!=0)
				{
					if(ALLOW_EDIT_UTTIST)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle  = ES_CENTER;	
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}			
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}
//---------------------------
// Visa Data f�r uttag i stv
//---------------------------
void CMyReportPage3::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			for(nSpec=0;nSpec<NUM_OF_SP+1;nSpec++)
				m_ReportCtrl->AddRecord(new CTraktViewData(nSpec,PAGE_NR_UTISTV,data,xml->str(m_nSpecArray[nSpec])));

			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r utt i stv
//---------------------------
void CMyReportPage3::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0,tempCol=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_UTTAG_TO_SHOW;nCol++)
		{
			//value=pRec->getColumnDouble(nCol+1);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol+1);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			switch(nCol)
			{
			case 0:	tempCol=UTTAG_VARINDEX_M3SK;		data->m_recUttagIstv[tempCol][nRow]=value;break;
			case 1:	tempCol=UTTAG_VARINDEX_M3FUB;		data->m_recUttagIstv[tempCol][nRow]=value;break;
			case 2:	tempCol=UTTAG_VARINDEX_M3STAMSK;	data->m_recUttagIstv[tempCol][nRow]=value;break;
			case 3:	tempCol=UTTAG_VARINDEX_GY;			data->m_recUttagIstv[tempCol][nRow]=value;break;
			case 4:	tempCol=UTTAG_VARINDEX_ANTAL;		data->m_recUttagIstv[tempCol][nRow]=value;break;
			case 5:	tempCol=UTTAG_VARINDEX_DG;			data->m_recUttagIstv[tempCol][nRow]=value;break;
			case 6:	tempCol=UTTAG_VARINDEX_DGV;		data->m_recUttagIstv[tempCol][nRow]=value;break;
			}			
		}
	}
}

//---------------------------
// Data f�r utt istv har �ndrats
//---------------------------
void CMyReportPage3::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	
	m_ReportCtrl=&GetReportCtrl();
  
	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		//Switcha Column/Variabel
		//switch(nColIndex)
		//{				
		//default:
		pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
		tst=pRec->getColumnText(nColIndex);
		if(!tst.IsEmpty())
		{
			if(!isTal(tst.GetBuffer(0)))
			{
				AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
				tst.Format(_T("%s"),"");
				pRec->setColumnText(nColIndex,tst);
			}
			else
			{
				value=(float)_tstof(tst);		
				if(value<0 || value>4000.0)
				{
					AfxMessageBox(_T(" 0<= V�rde <= 4000 "));
					value=0.0;
					tst.Format(_T("%10.1f"),value);
					pRec->setColumnText(nColIndex,tst);
				}
			}
		}
		//break;
		//}
		m_bIsDirty2=TRUE;
	}
}

//------------------------------------------------------------------------------------------------------
//		SKADOR
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage4, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage4, CMyReportPageBase)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
END_MESSAGE_MAP()

CMyReportPage4::CMyReportPage4()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

void CMyReportPage4::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CString csStr=_T("");
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_SKAD_COLS;nCol++)
			{
				switch(nCol)
				{
				case 0:
				case 1:
				case 2:
					csStr=_T("");										
					break;
				case 3:
					csStr=xml->str(IDS_STRING7524);
					break;
				case 4:
					csStr=xml->str(IDS_STRING7536);
					break;		
				case 5:
					csStr=xml->str(IDS_STRING7537);
					break;											
				}
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol,csStr, COL_SIZE_TAB));
				if(nCol==1 || nCol==4 || nCol==5)
				{
					if(ALLOW_EDIT_SKAD)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle = ES_CENTER;	
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);
						
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}

//---------------------------
// Visa Data f�r skador
//---------------------------
void CMyReportPage4::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_ReportCtrl->AddRecord(new CTraktViewData(0,data,xml->str(IDS_STRING7525),xml->str(IDS_STRING7539)));
			m_ReportCtrl->AddRecord(new CTraktViewData(1,(TRAKT_DATA)data,xml->str(IDS_STRING7526),xml->str(IDS_STRING7540)));
			m_ReportCtrl->AddRecord(new CTraktViewData(2,(TRAKT_DATA)data,xml->str(IDS_STRING7527),xml->str(IDS_STRING7541)));
			m_ReportCtrl->AddRecord(new CTraktViewData(3,(TRAKT_DATA)data,xml->str(IDS_STRING7528),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(4,(TRAKT_DATA)data,xml->str(IDS_STRING7529),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(5,(TRAKT_DATA)data,xml->str(IDS_STRING7530),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(6,(TRAKT_DATA)data,xml->str(IDS_STRING7531),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(7,data,xml->str(IDS_STRING7532),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(8,data,xml->str(IDS_STRING7533),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(9,data,xml->str(IDS_STRING7534),_T("")));		
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r skador
//---------------------------
void CMyReportPage4::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		nCol=1;
		//value=pRec->getColumnDouble(nCol);
		csStr=_T("");
		csStr=pRec->getColumnText(nCol);
		if(csStr.IsEmpty())
			value=MY_NULL;
		else
			value=_tstof(csStr);
		switch(nRow)
		{
		case 0:
			data->m_Skadade_Trad=value;
			break;
		case 1:
			data->m_Skadade_Stam=value;
			break;
		case 2:
			data->m_Skadade_Rot=value;
			break;
		case 3:
			data->m_Rota=value;
			break;
		case 4:
			data->m_Sparstracka=value;
			break;
		case 5:
			data->m_Spardjup=value;
			break;
		case 6:
			data->m_Spardjup_Andel=value;
			break;
		case 7:
			data->m_Stv_Avst=value;
			break;
		case 8:
			data->m_Stv_Bredd=value;
			break;
		case 9:
			data->m_Stv_Areal=value;
			break;
		}
		switch(nRow)
		{
		case 0:
			nCol=4;	
			//value=pRec->getColumnDouble(nCol);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon1_Stam=value;
			break;
		case 1:
			nCol=4;	
			//value=pRec->getColumnDouble(nCol);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon1_Rot=value;
			break;
		case 2:
			nCol=4;	
			//value=pRec->getColumnDouble(nCol);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon1_Rot_Stam=value;
			break;
		}
		switch(nRow)
		{
		case 0:
			nCol=5;	
			//value=pRec->getColumnDouble(nCol);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);

			data->m_Zon2_Stam=value;
			break;
		case 1:
			nCol=5;	
			//value=pRec->getColumnDouble(nCol);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon2_Rot=value;
			break;
		case 2:
			nCol=5;	
			//value=pRec->getColumnDouble(nCol);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon2_Rot_Stam=value;
			break;
		}
	}
}
//---------------------------
// Data f�r skador har �ndrats
//---------------------------
void CMyReportPage4::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	m_ReportCtrl=&GetReportCtrl();

	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		//Switcha Column/Variabel
		switch(nColIndex)
		{				
		case 1:
		case 4:
		case 5:
			pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
			tst=pRec->getColumnText(nColIndex);
			if(!tst.IsEmpty())
			{
				if(!isTal(tst.GetBuffer(0)))
				{
					AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
					tst.Format(_T("%s"),"");
					pRec->setColumnText(nColIndex,tst);
				}
				else
				{

					value=(float)_tstof(tst);		
					if(value<0 || value>4000.0)
					{
						AfxMessageBox(_T(" 0<= V�rde <= 4000 "));
						value=0.0;
						tst.Format(_T("%10.1f"),value);
						pRec->setColumnText(nColIndex,tst);
					}
				}
			}
			break;
		}
		m_bIsDirty2=TRUE;
	}
}

//------------------------------------------------------------------------------------------------------
//		YTOR
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage5, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage5, CMyReportPageBase)	
END_MESSAGE_MAP()

CMyReportPage5::CMyReportPage5()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
}

void CMyReportPage5::OnInitialUpdate()
{
	int nCol=0,nCol2=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			nCol2=0;
			for(nCol=0;nCol<NUM_OF_YTVAR_TOSHOW;nCol++)
			{
				if(m_nYtVarShow[nCol])
				{
					
					pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol2, xml->str(m_nYtArray[nCol]), COL_SIZE_TAB));
					
					if(ALLOW_EDIT_YTOR)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle = ES_CENTER;	   //only allow numbers
						pCol->SetEditable( TRUE );
					}					
					else
						pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					nCol2++;
				}
			}
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(TRUE);
			m_ReportCtrl->AllowEdit(FALSE);						
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);			
		}			
	} 
	m_bDoInit=FALSE;
	}
}

//---------------------------
// Visa Data f�r ytor
//---------------------------
void CMyReportPage5::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_ReportCtrl=&GetReportCtrl();
			ClearReport(m_ReportCtrl);
			for(unsigned int pi=0;pi<data.m_vPlotData.size();pi++)
			{							
				if(data.m_vPlotData[pi].m_nPlot_si_tradslag<0)
					m_ReportCtrl->AddRecord(new CTraktViewData(data.m_vPlotData[pi],m_nYtVarShow,_T("")));		
				else
					m_ReportCtrl->AddRecord(new CTraktViewData(data.m_vPlotData[pi],m_nYtVarShow,xml->str(m_nSpecArray[data.m_vPlotData[pi].m_nPlot_si_tradslag])));		
			}
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();	
		}
	}
}

//---------------------------
// H�mta Data f�r ytor
//---------------------------
/*
void CMyReportPage5::getData(TRAKT_DATA *data)
{
	int nCol=0,nRow=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;
	PLOT_DATA pData;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_YTVAR_TOSHOW;nCol++)
		{
			value=pRec->getColumnDouble(nCol+1);
			switch(nCol)
			{
			case 0:  pData.m_nPlot_id					=value;	break;
			case 1:  pData.m_fPlot_stam_gy			=value;	break;
			case 2:  pData.m_fPlot_stam_antal		=value;	break;
			case 3:  pData.m_fPlot_stubb_gy			=value;	break;
			case 4:  pData.m_fPlot_stubb_antal		=value;	break;
			case 5:  pData.m_fPlot_skad_stam			=value;	break;
			case 6:  pData.m_fPlot_skad_rot			=value;	break;
			case 7:  pData.m_fPlot_skad_stam_rot	=value;	break;
			case 8:  pData.m_fPlot_skadandel_tot	=value;	break;
			case 9:  pData.m_fPlot_skadandel_stam	=value;	break;
			case 10: pData.m_fPlot_skadandel_rot	=value;	break;
			case 11: pData.m_fPlot_skadandel_rota	=value;	break;
			case 12: pData.m_fPlot_lat					=value;	break;
			case 13: pData.m_fPlot_lon					=value;	break;	
			}
			data->m_vPlotData.push_back(pData);
		}
	}
}
*/

//------------------------------------------------------------------------------------------------------
//		TR�D
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage10, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage10, CMyReportPageBase)	
END_MESSAGE_MAP()

CMyReportPage10::CMyReportPage10()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
}

void CMyReportPage10::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (!ProgressDlg.Create(CMyProgressBarDlg::IDD,this))
	{
		TRACE0("Failed to create progress window control.\n");
	}

	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_TREE_VAR;nCol++)
			{
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nTreeArray[nCol]), COL_SIZE_TAB));
				
				if(ALLOW_EDIT_TREE)
				{
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
					pCol->GetEditOptions()->m_dwEditStyle = ES_CENTER;	   //only allow numbers
					pCol->SetEditable( TRUE );
				}
				else
						pCol->SetEditable( FALSE );
				
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(TRUE);
			m_ReportCtrl->AllowEdit(FALSE);						
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);			
		}			
	} 
	m_bDoInit=FALSE;
	}
}

//---------------------------
// Visa Data f�r tr�d
//---------------------------
void CMyReportPage10::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	CString Update=_T("");

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			int size=data.m_vTreeData.size();

			m_ReportCtrl=&GetReportCtrl();
			ClearReport(m_ReportCtrl);

			
			CProgressCtrl &Bar=ProgressDlg.GetBar();
			Bar.SetRange(0,data.m_vTreeData.size());
			Bar.SetStep(1);
			Bar.SetPos(0);
			if(size>0)
			{
				ProgressDlg.ShowWindow(SW_NORMAL);
				ProgressDlg.SetDlgTexts(_T("Status"),_T("L�ser in tr�d"));
			}

			
			for(unsigned int ti=0;ti<data.m_vTreeData.size();ti++)
			{							
				m_ReportCtrl->AddRecord(new CTraktViewData(data.m_vTreeData[ti],xml->str(m_nSpecArray[data.m_vTreeData[ti].m_nTree_SpecId-1]),m_sTreeTypes[data.m_vTreeData[ti].m_nTree_Typ]));		
				Bar.StepIt();
				Update.Format(_T("%d/%d"),ti,size);
				ProgressDlg.UpdateText2(Update);
			}
			if(size>0)
				ProgressDlg.ShowWindow(SW_HIDE);
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();	
		}
	}
}

//------------------------------------------------------------------------------------------------------
//		UTTAG TOTALT
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage6, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage6, CMyReportPageBase)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
END_MESSAGE_MAP()

CMyReportPage6::CMyReportPage6()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

void CMyReportPage6::OnInitialUpdate()
{
	int nCol=0,tempCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			//Lagt till en extra kolumn(NUM_OF_UTTAG_TO_SHOW+2) f�r ga kvot aktivt i v1.0.13
			for(nCol=0;nCol<NUM_OF_UTTAG_TO_SHOW+2;nCol++)
			{
				tempCol=nCol;
				if(nCol==7)
					tempCol=8;
				if(nCol==8)
					pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(IDS_STRING7692), COL_SIZE_TAB));
				else
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nVarArray[tempCol]), COL_SIZE_TAB));
				if(nCol!=0)
				{
					if(ALLOW_EDIT_UTTTOT)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle  = ES_CENTER;	
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}			
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}
//---------------------------
// Visa Data f�r uttag totalt
//---------------------------
void CMyReportPage6::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			for(nSpec=0;nSpec<NUM_OF_SP+1;nSpec++)
				m_ReportCtrl->AddRecord(new CTraktViewData(nSpec,PAGE_NR_UTTOT,data,xml->str(m_nSpecArray[nSpec])));

			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r utt totalt
//---------------------------
void CMyReportPage6::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0,tempCol=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_UTTAG_TO_SHOW;nCol++)
		{
			//value=pRec->getColumnDouble(nCol+1);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol+1);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			switch(nCol)
			{
			case 0:	tempCol=UTTAG_VARINDEX_M3SK;		data->m_recUttagTot[tempCol][nRow]=value;break;
			case 1:	tempCol=UTTAG_VARINDEX_M3FUB;		data->m_recUttagTot[tempCol][nRow]=value;break;
			case 2:	tempCol=UTTAG_VARINDEX_M3STAMSK;	data->m_recUttagTot[tempCol][nRow]=value;break;
			case 3:	tempCol=UTTAG_VARINDEX_GY;			data->m_recUttagTot[tempCol][nRow]=value;break;
			case 4:	tempCol=UTTAG_VARINDEX_ANTAL;		data->m_recUttagTot[tempCol][nRow]=value;break;
			case 5:	tempCol=UTTAG_VARINDEX_DG;			data->m_recUttagTot[tempCol][nRow]=value;break;
			case 6:	tempCol=UTTAG_VARINDEX_DGV;		data->m_recUttagTot[tempCol][nRow]=value;break;			
			}			
		}
	}
}

//---------------------------
// Data f�r utt totalt har �ndrats
//---------------------------
void CMyReportPage6::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	
	m_ReportCtrl=&GetReportCtrl();
  
	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		//Switcha Column/Variabel
		//switch(nColIndex)
		//{				
		//default:
		pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
		/*
		value=pRec->getColumnDouble(nColIndex);
		if(value<0 || value>4000.0)
		{
			value=0.0;
			pRec->setColumnDouble(nColIndex,value);
		}*/				
		tst=pRec->getColumnText(nColIndex);
		if(!tst.IsEmpty())
		{
			if(!isTal(tst.GetBuffer(0)))
			{
				AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
				tst.Format(_T("%s"),"");
				pRec->setColumnText(nColIndex,tst);
			}
			else
			{
				value=(float)_tstof(tst);		
				if(value<0 || value>4000.0)
				{
					AfxMessageBox(_T(" 0<= V�rde <= 4000 "));
					value=0.0;
					tst.Format(_T("%10.1f"),value);
					pRec->setColumnText(nColIndex,tst);
				}
			}
		}

		//break;
		//}
		m_bIsDirty2=TRUE;
	}
}



//------------------------------------------------------------------------------------------------------
//		F�RENKLADE
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage8, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage8, CMyReportPageBase)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
END_MESSAGE_MAP()

CMyReportPage8::CMyReportPage8()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

void CMyReportPage8::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CString csStr=_T("");
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<4;nCol++)
			{
				switch(nCol)
				{
				case 0://Tr�dslag
					csStr=xml->str(IDS_STRING7590); break;
				case 1://Gy
					csStr=xml->str(IDS_STRING7594); break;
				case 2://Dgv
					csStr=xml->str(IDS_STRING7598);	break;
				case 3://Hgv
					csStr=xml->str(IDS_STRING7599);
					break;
				}
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol,csStr, COL_SIZE_TAB));
				if(nCol!=0)
				{
					if(ALLOW_EDIT_FORENKLAD)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle = ES_CENTER;	//only allow numbers
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);
						
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}

//---------------------------
// Visa Data f�r f�renklad
//---------------------------
void CMyReportPage8::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			for(nSpec=0;nSpec<NUM_OF_SP+1;nSpec++)
				m_ReportCtrl->AddRecord(new CTraktViewData(nSpec,PAGE_NR_FORENKLAD,data,xml->str(m_nSpecArray[nSpec])));
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r f�renklad
//---------------------------
void CMyReportPage8::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0,tempCol=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		for(nCol=0;nCol<NUM_OF_FORENKLAD_TO_SHOW;nCol++)
		{
			//value=pRec->getColumnDouble(nCol+1);
			csStr=_T("");
			csStr=pRec->getColumnText(nCol+1);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			switch(nCol)
			{
			case 0:	tempCol=KVARV_VARINDEX_GY;		data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 1:	tempCol=KVARV_VARINDEX_DGV;	data->m_recKvarvarande[tempCol][nRow]=value;break;
			case 2:	tempCol=KVARV_VARINDEX_HGV;	data->m_recKvarvarande[tempCol][nRow]=value;break;			
			}			
		}
	}
}
//---------------------------
// Data f�r f�renklade har �ndrats
//---------------------------
void CMyReportPage8::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	
	m_ReportCtrl=&GetReportCtrl();
  
	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		//switch(nColIndex)
		//{
		//M3sk
		//default:
		pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
		tst=pRec->getColumnText(nColIndex);
		if(!tst.IsEmpty())
		{
			if(!isTal(tst.GetBuffer(0)))
			{
				AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
				tst.Format(_T("%s"),"");
				pRec->setColumnText(nColIndex,tst);
			}
			else
			{
				value=(float)_tstof(tst);		
				if(value<0 || value>4000.0)
				{
					AfxMessageBox(_T(" 0<= V�rde <= 4000 "));
					value=0.0;
					tst.Format(_T("%10.1f"),value);
					pRec->setColumnText(nColIndex,tst);
				}
			}
		}			
		//break;
		//}
		m_bIsDirty2=TRUE;
	}
}






//------------------------------------------------------------------------------------------------------
//		�VRIGT F�RENKAD
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage9, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage9, CMyReportPageBase)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
END_MESSAGE_MAP()

CMyReportPage9::CMyReportPage9()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
m_bIsDirty2=FALSE;
}

void CMyReportPage9::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CString csStr=_T("");
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_STICKVAG_COLS;nCol++)
			{
				switch(nCol)
				{
				case 0:	csStr=xml->str(IDS_STRING7519);	break;	
				default:	csStr=_T("");									break;
				}
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol,csStr, COL_SIZE_TAB));
				if(nCol==1)
				{
					if(ALLOW_EDIT_FORENKLAD2)
					{
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
						pCol->GetEditOptions()->m_dwEditStyle = ES_CENTER;	   //only allow numbers
						pCol->SetEditable( TRUE );
					}
					else
						pCol->SetEditable( FALSE );
				}
				else
					pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(TRUE);
						
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);
		}			
	}
	m_bDoInit=FALSE;
	}
}

//---------------------------
// Visa Data f�r �vrigt, f�renklad
//---------------------------
void CMyReportPage9::showData(TRAKT_DATA data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_ReportCtrl->AddRecord(new CTraktViewData(7,data,xml->str(IDS_STRING7532),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(8,data,xml->str(IDS_STRING7533),_T("")));
			m_ReportCtrl->AddRecord(new CTraktViewData(9,data,xml->str(IDS_STRING7534),_T("")));	
			m_ReportCtrl->AddRecord(new CTraktViewData(10,data,xml->str(IDS_STRING7526),_T("")));	
			m_ReportCtrl->AddRecord(new CTraktViewData(11,data,xml->str(IDS_STRING7530),_T("")));	
			m_ReportCtrl->AddRecord(new CTraktViewData(12,data,xml->str(IDS_STRING7542),_T("")));	
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}

//---------------------------
// H�mta Data f�r �vrigt, f�renklad
//---------------------------
void CMyReportPage9::getData(TRAKT_VARIABLES *data)
{
	int nCol=0,nRow=0;
	CTraktViewData *pRec;
	double value=0.0;
	CString csStr;
	CXTPReportControl *m_ReportCtrl;

	m_ReportCtrl=&GetReportCtrl();

	// H�mta antalet rader i rapporten
	CXTPReportRecords *pRecords = m_ReportCtrl->GetRecords();
	// Stega igneom raderna i rapporten
	for (nRow = 0;nRow < pRecords->GetCount();nRow++)
	{
		// H�mta specifika raden
		pRec = (CTraktViewData *)pRecords->GetAt(nRow);
		//S�tt kolumn till 1:a
		nCol=1;
		csStr=_T("");
		csStr=pRec->getColumnText(nCol);
		if(csStr.IsEmpty())
			value=MY_NULL;
		else
			value=_tstof(csStr);
		switch(nRow)
		{
		case 0:
			data->m_Stv_Avst=value;
			break;
		case 1:
			data->m_Stv_Bredd=value;
			break;
		case 2:
			data->m_Stv_Areal=value;
			break;
		case 3:
			data->m_Skadade_Stam=value;
			break;
		case 4:
			data->m_Spardjup=value;
			break;
		case 5:
			data->m_Spar_over_20mha=value;
			break;			
		}
		/*
		switch(nRow)
		{
		case 0:
			nCol=4;	
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon1_Stam=value;
			break;
		case 1:
			nCol=4;	
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon1_Rot=value;
			break;
		case 2:
			nCol=4;	
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon1_Rot_Stam=value;
			break;
		}
		switch(nRow)
		{
		case 0:
			nCol=5;	
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon2_Stam=value;
			break;
		case 1:
			nCol=5;	
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon2_Rot=value;
			break;
		case 2:
			nCol=5;	
			csStr=_T("");
			csStr=pRec->getColumnText(nCol);
			if(csStr.IsEmpty())
				value=MY_NULL;
			else
				value=_tstof(csStr);
			data->m_Zon2_Rot_Stam=value;
			break;
		}*/
	}
}
//---------------------------
// Data f�r stickv�g, f�renklad har �ndrats
//---------------------------
void CMyReportPage9::OnValueChanged(NMHDR * pNotifyStruct, LRESULT *)
{
	float value=0.0;
  	CXTPReportControl *m_ReportCtrl;
	CTraktViewData *pRec;
	CString tst=_T("");
	m_ReportCtrl=&GetReportCtrl();

	if (m_ReportCtrl->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords *pRecs = m_ReportCtrl->GetRecords();
		CXTPReportColumn  *pCols = m_ReportCtrl->GetFocusedColumn();
		CXTPReportRow		*pRow	 = m_ReportCtrl->GetFocusedRow();

		int nRowIndex = pRow->GetIndex();
		int nColIndex = pCols->GetIndex();
		//Switcha Column/Variabel
		switch(nColIndex)
		{				
		case 1:
			pRec = (CTraktViewData *)pRecs->GetAt(nRowIndex);
			tst=pRec->getColumnText(nColIndex);
			m_bIsDirty2=TRUE;
			if(!tst.IsEmpty())
			{
   			if(!isTal(tst.GetBuffer(0)))
				{
					AfxMessageBox(_T("Fel i Data\n Ej numerisk (0-9)"));
					tst.Format(_T("%s"),"");
					pRec->setColumnText(nColIndex,tst);
				}
				else
				{

					value=(float)_tstof(tst);		
					if(value<0 || value>4000.0)
					{
						AfxMessageBox(_T(" 0<= V�rde <= 4000 "));
						value=0.0;
						tst.Format(_T("%10.1f"),value);
						pRec->setColumnText(nColIndex,tst);
					}
				}
			}
			break;
		}
		
	}
}