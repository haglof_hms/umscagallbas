
#include "stdafx.h"
#include "Calculations.h"



STAND_TABLE stbl;
vecSampleType vSampleType;


float H25inv(int spec,float diam,float h25)
{
   float logD;
   if(diam<((float) MINDIA/10.0)||h25<5.0)
      return (float)1.4; // bdh-h�jd, unvik matherr
   logD=log((float)diam)/log((float)10);
   if(spec==1||spec==4||spec==6)
      return (float)9.022-(float)6.454*logD-(float)1.256*h25+(float)1.613*h25*logD ;
   else
      return (float)1.518-(float)1.086*logD-(float)0.518*h25+(float)1.086*h25*logD;
}

void CalH25(CALCVolType *V,TRAKT_DATA *Trakt)
{
   float dd[NUM_OF_SP+1],height,diam,log10=log((float)10.0);
   int	count,i,samples_tot[NUM_OF_SP+1];
   char	spec;
	int sp=0;
   
	for(sp=0;sp<NUM_OF_SP+1;sp++)
	{
		samples_tot[sp]=0;
		dd[sp]=0.0;
	}
	
	for(i=0;i<vSampleType.size();i++)
   {
      if(vSampleType[i].hgt1>=70 && vSampleType[i].dia>=100)
      {
         spec=vSampleType[i].spc;
         height=vSampleType[i].hgt1/(float)10.0 ; //till m
         diam=vSampleType[i].dia/(float)10.0 ;        //cm
         samples_tot[spec]++ ;
         samples_tot[NUM_OF_SP]++ ;
         V->h25[spec]+=height*diam*diam;
         V->h25[NUM_OF_SP]+=height*diam*diam;
         dd[spec]+=diam*diam;
         dd[NUM_OF_SP]+=diam*diam;
      }
   }

   for(count=0;count<=NUM_OF_SP;count++ )
   {
      if(dd[spec]>0.0&&samples_tot[count]>0)
      {
         V->h25[count]/= dd[count];
         dd[count]=sqrt(dd[count]/samples_tot[count]);
         if(count==1||count==4||count==6)//gran H25
            V->h25[count]=( V->h25[count]-(float)9.022+(float)6.454*( log( dd[count] )/log10 ))/( (float)1.613*( log( dd[count] )/log10 )-(float)1.256);
         else
            V->h25[count]= ( V->h25[count]-(float)1.518+(float)1.086*( log( dd[count])/log10 ))/( (float)1.086*( log( dd[count] )/log10 )-(float)0.518 );
      }
   }

   for(count=0;count<NUM_OF_SP;count++ )
      if(!samples_tot[count])
         V->h25[count]=Trakt->m_nDefaultH25[count]/(float)10.0;
   if(!samples_tot[NUM_OF_SP])
      V->h25[NUM_OF_SP]=18.0;
   return;
}


int Vol(TRAKT_DATA *Trakt,CALCVolType *V,int type)
{

   const float m3sk[2][8][5]=
   {
      /*       a        b        c        d        e                           */

      {
         {(float)  -1.38903,(float) 1.84493,(float) 0.06563,(float) 2.02122,(float)-1.01095 },  /* Tall    S 100-01   */
         {(float)  -1.02039,(float) 2.00128,(float)-0.47473,(float) 2.87138,(float)-1.61803 },  /* Gran    S 100-01   */
         {(float)  -0.89359,(float) 2.27954,(float)-1.18672,(float) 7.07362,(float)-5.45175 },  /* L�v     S 100-01   */
         {(float)  -0.89359,(float) 2.27954,(float)-1.18672,(float) 7.07362,(float)-5.45175 },  /* L�v �l�v    S 100-01   */
         {(float)  -1.02039,(float) 2.00128,(float)-0.47473,(float) 2.87138,(float)-1.61803 },  /* Gran �barr   S 100-01   */
         {(float)  -1.38903,(float) 1.84493,(float) 0.06563,(float) 2.02122,(float)-1.01095 },  /* Tall cont    S 100-01   */
         {(float)  -1.02039,(float) 2.00128,(float)-0.47473,(float) 2.87138,(float)-1.61803 },  /* Gran torr   S 100-01   */
         {(float)  -0.89359,(float) 2.27954,(float)-1.18672,(float) 7.07362,(float)-5.45175 },  /* L�v  Ek   S 100-01   */
      },

      /*       a        b        c        d        e                           */
      {
         {(float) -1.20914,(float) 1.94740,(float)-0.05947,(float) 1.40958,(float)-0.45810 },  /* Tall  N 100-01     */
         {(float) -0.79783,(float) 2.07157,(float)-0.73882,(float) 3.16332,(float)-1.82622 },  /* Gran  N 100-01     */
         {(float) -0.44224,(float) 2.47580,(float)-1.40854,(float) 5.16863,(float)-3.77147 },  /* L�v   N 100-01     */
         {(float) -0.44224,(float) 2.47580,(float)-1.40854,(float) 5.16863,(float)-3.77147 },  /* L�v  �l�v  N 100-01     */
         {(float) -0.79783,(float) 2.07157,(float)-0.73882,(float) 3.16332,(float)-1.82622 },  /* Gran  �barr N 100-01     */
         {(float) -1.20914,(float) 1.94740,(float)-0.05947,(float) 1.40958,(float)-0.45810 },  /* Tall cont N 100-01     */
         {(float) -0.79783,(float) 2.07157,(float)-0.73882,(float) 3.16332,(float)-1.82622 },  /* Gran torr N 100-01     */
         {(float) -0.44224,(float) 2.47580,(float)-1.40854,(float) 5.16863,(float)-3.77147 },  /* L�v ek  N 100-01     */
      }
   };
   const  float fub[2][8][5]=
   {
      /*       a        b        c        d        e                          */
      {
         {(float) -1.52761,(float) 1.82928,(float) 0.07454,(float) 1.43792,(float)-0.35559 },  /* Tall    S 300-01   */
         {(float) -1.06019,(float) 2.04239,(float)-0.54292,(float) 2.80843,(float)-1.52110 },  /* Gran    S 300-01   */
         {(float) -0.93631,(float) 2.30212,(float)-1.40378,(float) 8.01817,(float)-6.18825 },  /* L�v     S 300-01   */
         {(float) -0.93631,(float) 2.30212,(float)-1.40378,(float) 8.01817,(float)-6.18825 },  /* L�v �l�v    S 300-01   */
         {(float)-1.06019,(float) 2.04239,(float)-0.54292,(float) 2.80843,(float)-1.52110 },  /* Gran �barr   S 300-01   */
         {(float) -1.52761,(float) 1.82928,(float) 0.07454,(float) 1.43792,(float)-0.35559 },  /* Tall  cont  S 300-01   */
         {(float) -1.06019,(float) 2.04239,(float)-0.54292,(float) 2.80843,(float)-1.52110 },  /* Gran torr   S 300-01   */
         {(float) -0.93631,(float) 2.30212,(float)-1.40378,(float) 8.01817,(float)-6.18825 },  /* L�v ek    S 300-01   */

      },
      /*       a        b        c        d        e                            */
      {
         {(float)  -1.25246,(float) 1.98244,(float)-0.13118,(float) 1.03781,(float)-0.03482 },  /* Tall     N 300-01  */
         {(float)  -0.82249,(float) 2.11094,(float)-0.89626,(float) 3.51812,(float)-2.05567 },  /* Gran     N 300-01  */
         {(float)  -0.35394,(float) 2.52141,(float)-1.54257,(float) 4.88165,(float)-3.47422 },  /* L�v      N 300-01  */
         {(float)  -0.35394,(float) 2.52141,(float)-1.54257,(float) 4.88165,(float)-3.47422 },  /* L�v  �l�v   N 300-01  */
         {(float)  -0.82249,(float) 2.11094,(float)-0.89626,(float) 3.51812,(float)-2.05567 },  /* Gran �barr    N 300-01  */
         {(float)  -1.25246,(float) 1.98244,(float)-0.13118,(float) 1.03781,(float)-0.03482 },  /* Tall cont   N 300-01  */
         {(float)  -0.82249,(float) 2.11094,(float)-0.89626,(float) 3.51812,(float)-2.05567 },  /* Gran torr   N 300-01  */
         {(float)  -0.35394,(float) 2.52141,(float)-1.54257,(float) 4.88165,(float)-3.47422 },  /* L�v    ek  N 300-01  */

      }
   };

   int i,j,Part;
   float diam,height,volsk,volfub;

   float log10=log((float)10.0),logD,tfactor;
   float a,b,c,d,e;
   float a1,b1,c1,d1,e1;

//   float dub,voltimb,volklentimb,volgagnv,volmass,timb_part,klen_timb_part,gagn_part;
	CString csBuf=_T("");
   CalH25(V,Trakt);
   tfactor=GetArealFactor(Trakt);

   for(i=0;i<NUM_OF_SP;i++)
   {
		Part=Trakt->m_nTrakt_partof;
      
	  a=m3sk[Part][i][0];
	  b=m3sk[Part][i][1];
	  c=m3sk[Part][i][2];
	  d=m3sk[Part][i][3];
	  e=m3sk[Part][i][4];
	  a1=fub[Part][i][0];
	  b1=fub[Part][i][1];
	  c1=fub[Part][i][2];
	  d1=fub[Part][i][3];
	  e1=fub[Part][i][4];


      for(j=0;j<MAXDIA/DIACLASS;j++)
      {
         if(stbl.StandTable[type][i][j] && (V->h25[i]>0.0) )
         {
            diam=(j*DIACLASS+DIACLASS/2)/(float)10.0;
            logD=log(diam)/log10 ;

            if(i==1||i==4||i==6) /* gran �barr torra */
               height=(float)9.022-(float)6.454*logD-(float)1.256*V->h25[i]+(float)1.613*V->h25[i]*logD ;
            else
               height=(float)1.518-(float)1.086*logD-(float)0.518*V->h25[i]+(float)1.086*V->h25[i]*logD;

            volsk=Brandel(diam,height,a,b,c,d,e)*stbl.StandTable[type][i][j];
            volfub=Brandel(diam,height,a1,b1,c1,d1,e1)*stbl.StandTable[type][i][j];
            V->volumem3sk[i]+=volsk;
            V->volumefub[i]+=volfub;
            V->volumem3sk[NUM_OF_SP]+=volsk;
            V->volumefub[NUM_OF_SP]+=volfub;
         }
      }
   }

   for(i=0;i<NUM_OF_SP+1;i++)
   {
      V->volumem3sk[i]/=1000.0;
      V->volumefub[i]/=1000.0;
   }
   V->factor=tfactor;
   return 1;
}

void CountVol(TRAKT_DATA *Trakt,int type,CALCVolType *V)
{
   
   int i,j;
   float dia,n;
	CString csBuf=_T("");

	*V=CALCVolType();

	//ClearVolType(&V);
   V->factor=1.0;

   Vol(Trakt,V,type);                                       /* h25 and Brandel */

   for(i=0;i<NUM_OF_SP;i++)                       /* Calculate volume */
   {
      for(j=0;j<MAXDIA/DIACLASS;j++)
      {
         dia=(DIACLASS*j+DIACLASS/2)/(float)10.0;
         n=stbl.StandTable[type][i][j];
         if(n)
         {
				V->gr[i]+=dia*dia*n;
				V->gr[NUM_OF_SP]+=dia*dia*n;

            V->da[i]+=dia*n;
            V->da[NUM_OF_SP]+=dia*n;

            V->dg[i]+=dia*dia*n;
            V->dg[NUM_OF_SP]+=dia*dia*n;

            V->dgv[i]+=dia*dia*dia*n;
            V->dgv[NUM_OF_SP]+=dia*dia*dia*n;

            V->stems[i]+=n;
            V->stems[NUM_OF_SP]+=n;
         }
      }
   }


   for(i=0;i<=NUM_OF_SP;i++)                       /* Calculate avg */
   {
       V->gr[i]=V->gr[i]*(float)3.1415/(float)40000.0;
      if(V->dg[i]>0.0)
         V->dgv[i]=V->dgv[i]/V->dg[i];
      if(V->stems[i]>0.0)
      {
         V->mstamm3fub[i]=V->volumefub[i]/V->stems[i];       /* Average stem */
         V->mstamm3sk[i]=V->volumem3sk[i]/V->stems[i];         /* Average stem */
         V->dg[i]=sqrt(V->dg[i]/V->stems[i]);
         V->da[i]=V->da[i]/V->stems[i];
      }


      if(V->dgv[i]>MINDIA/10.0)
         V->hgv[i]=H25inv(i,V->dgv[i],V->h25[i]);
   }
   //return V;
}



int make_thinning_result(TRAKT_DATA *Trakt)
{
	int line=0,nNump=0,q=0,Err_Msg=0;
	unsigned int i=0,pl=0;
	float tot_stems=0.0,tot_rots=0.0,StemGy=0.0,StubbGy=0.0,StemNr=0.0,StubbNr=0.0;
	CString csTst=_T("");
	CString csBuf=_T(""),csOut=_T("");
	CALCVolType Vutt=CALCVolType();
	CALCVolType Vkvar=CALCVolType();
	CALCVolType Vuttstk=CALCVolType();
	CALCVolType Vytdata=CALCVolType();

	CountStandtable(Trakt);
	nNump=CountPlot(Trakt,&Vytdata);

	CountVol(Trakt,AVV_IN_FOREST,&Vutt);
	CountVol(Trakt,REMAINED,&Vkvar);
	CountVol(Trakt,AVK_IN_ROAD,&Vuttstk);
	
	tot_stems	= (float)1.0*Vkvar.stems[NUM_OF_SP];
	tot_rots		= (float)1.0*Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP];
	if(tot_stems>0.0)
		Vytdata.hurttrees=(float)100.0*Vytdata.hurttrees/tot_stems; // antal skadade tr�d/kvarst
	if(tot_stems>0.0)
		Vytdata.hurttree=(float)100.0*Vytdata.hurttree/tot_stems;	// antal skadade tr�d/kvarst
	if(tot_stems>0.0)
		Vytdata.hurtrot=(float)100.0*Vytdata.hurtrot/tot_stems;		// antal skadade r�t/kvarst
	if(tot_rots>0.0)
		Vytdata.hurtrots=(float)100.0*Vytdata.hurtrots/tot_rots;    // antal r�t/stubbar

	Err_Msg++;   
		
	DEFINED_SPEC defined_species= DEFINED_SPEC();
	
	Trakt->m_nTrakt_si_sp=defined_species.SpecNr[Vytdata.SIspec];
	Trakt->m_fTrakt_si_tot=Vytdata.SItot/10.0;
	Trakt->m_fTrakt_alder=Vytdata.SI_Age;
	Trakt->m_fTrakt_medel_ovre_hojd=Vytdata.overheight/10.0;	
	
	
	for(i=0;i<Trakt->m_vPlotData.size();i++)
	{
		//         memset(&pdata,0,sizeof(pdata));
		Count_Plot_Thinning(Trakt,Trakt->m_vPlotData[i],&StemNr,&StubbNr,&StemGy,&StubbGy);
		//"Yta Stammar    Stubbar    Skador         Skador%"
		//"Nr  Gyta Antal Gyta Antal Stam Rot St&Rt Tot Stam Rot R�t"
		Trakt->m_vPlotData[i].m_fPlot_stam_gy=StemGy;
		Trakt->m_vPlotData[i].m_fPlot_stam_antal=StemNr;
		Trakt->m_vPlotData[i].m_fPlot_stubb_gy=StubbGy;
		Trakt->m_vPlotData[i].m_fPlot_stubb_antal=StubbNr;

		//G�r om StemNr & StubbNr fr�n /ha till antal f�r vidare % ber�kningar
		StemNr*=(float)((float)Trakt->m_vPlotData[i].m_nPlot_areal/10000.0);
		StubbNr*=(float)((float)Trakt->m_vPlotData[i].m_nPlot_areal/10000.0);

		Trakt->m_vPlotData[i].m_fPlot_skad_stam=Trakt->m_vPlotData[i].m_nPlot_skad_stam1+Trakt->m_vPlotData[i].m_nPlot_skad_stam2;
		Trakt->m_vPlotData[i].m_fPlot_skad_rot=Trakt->m_vPlotData[i].m_nPlot_skad_rot1+Trakt->m_vPlotData[i].m_nPlot_skad_rot2;
		Trakt->m_vPlotData[i].m_fPlot_skad_stam_rot=Trakt->m_vPlotData[i].m_nPlot_skad_stam_rot1+Trakt->m_vPlotData[i].m_nPlot_skad_stam_rot2;
		//Skador %
		if(StemNr>0.0)
		{
			//�ndrat 20071105 r�knar inmatade skadade tr�d som enskilda tr�d, dvs plussar ihop alla...aktivt till version 1.4
			//IoPrintf(protocol,"%3.0f ",100.0*(((float)(P[i].hurttree+P[i].hurttree2+P[i].hurtrot+P[i].hurtrot2-P[i].hurtrot_tree-P[i].hurtrot_tree2))/StemNr));
			Trakt->m_vPlotData[i].m_fPlot_skadandel_tot=100.0*(Trakt->m_vPlotData[i].m_fPlot_skad_stam+Trakt->m_vPlotData[i].m_fPlot_skad_rot+Trakt->m_vPlotData[i].m_fPlot_skad_stam_rot)/StemNr;
			Trakt->m_vPlotData[i].m_fPlot_skadandel_stam=100.0*Trakt->m_vPlotData[i].m_fPlot_skad_stam/StemNr;
			Trakt->m_vPlotData[i].m_fPlot_skadandel_rot=100.0*Trakt->m_vPlotData[i].m_fPlot_skad_rot/StemNr;
		}			
		if(StubbNr>0.0)
			Trakt->m_vPlotData[i].m_fPlot_skadandel_rota=100.0*((float)Trakt->m_vPlotData[i].m_nPlot_skad_rota/StubbNr);
	}
   
	

	//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  M3Sk
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recFore[0][i]=(Vutt.volumem3sk[i]+Vkvar.volumem3sk[i]+Vuttstk.volumem3sk[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recFore[0][NUM_OF_SP]=(Vutt.volumem3sk[NUM_OF_SP]+Vkvar.volumem3sk[NUM_OF_SP]+Vuttstk.volumem3sk[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  M3Fub
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recFore[1][i]=(Vutt.volumefub[i]+Vkvar.volumefub[i]+Vuttstk.volumefub[i])*Vutt.factor;		
	Trakt->m_recData.m_recVars.m_recFore[1][NUM_OF_SP]=(Vutt.volumefub[NUM_OF_SP]+Vkvar.volumefub[NUM_OF_SP]+Vuttstk.volumefub[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  F�RE GALLRING ------------------------------------------	
	//											  MStamSk
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recFore[2][i]=(Vutt.volumem3sk[i]+Vkvar.volumem3sk[i]+Vuttstk.volumem3sk[i])/(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]);		
	if(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[2][NUM_OF_SP]=(Vutt.volumem3sk[NUM_OF_SP]+Vkvar.volumem3sk[NUM_OF_SP]+Vuttstk.volumem3sk[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  F�RE GALLRING ------------------------------------------	
	//											  MStamFub
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recFore[3][i]=(Vutt.volumefub[i]+Vkvar.volumefub[i]+Vuttstk.volumefub[i])/(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[3][NUM_OF_SP]=(Vutt.volumefub[NUM_OF_SP]+Vkvar.volumefub[NUM_OF_SP]+Vuttstk.volumefub[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  GY
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recFore[4][i]=(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i])*Vutt.factor;		
	Trakt->m_recData.m_recVars.m_recFore[4][NUM_OF_SP]=(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP])*Vutt.factor;  //3
	//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  Antal
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recFore[5][i]=(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recFore[5][NUM_OF_SP]=(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  Dg
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recFore[6][i]=(Vutt.dg[i]*Vutt.stems[i]+Vkvar.dg[i]*Vkvar.stems[i]+Vuttstk.dg[i]*Vuttstk.stems[i])/(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[6][NUM_OF_SP]=(Vutt.dg[NUM_OF_SP]*Vutt.stems[NUM_OF_SP]+Vkvar.dg[NUM_OF_SP]*Vkvar.stems[NUM_OF_SP]+Vuttstk.dg[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);   

		//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  Hgv
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[7][i]=(Vutt.hgv[i]*Vutt.stems[i]+Vkvar.hgv[i]*Vkvar.stems[i]+Vuttstk.hgv[i]*Vuttstk.stems[i])/(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[7][NUM_OF_SP]=(Vutt.hgv[NUM_OF_SP]*Vutt.stems[NUM_OF_SP]+Vkvar.hgv[NUM_OF_SP]*Vkvar.stems[NUM_OF_SP]+Vuttstk.hgv[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);   
	//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  H25
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[8][i]=(Vutt.h25[i]*Vutt.stems[i]+Vkvar.h25[i]*Vkvar.stems[i]+Vuttstk.h25[i]*Vuttstk.stems[i])/(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[8][NUM_OF_SP]=(Vutt.h25[NUM_OF_SP]*Vutt.stems[NUM_OF_SP]+Vkvar.h25[NUM_OF_SP]*Vkvar.stems[NUM_OF_SP]+Vuttstk.h25[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);   
//-------------------------------  F�RE GALLRING ------------------------------------------
	//											  Dgv
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recFore[9][i]=(Vutt.dgv[i]*Vutt.stems[i]+Vkvar.dgv[i]*Vkvar.stems[i]+Vuttstk.dgv[i]*Vuttstk.stems[i])/(Vutt.stems[i]+Vkvar.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recFore[9][NUM_OF_SP]=(Vutt.dgv[NUM_OF_SP]*Vutt.stems[NUM_OF_SP]+Vkvar.dgv[NUM_OF_SP]*Vkvar.stems[NUM_OF_SP]+Vuttstk.dgv[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);   

	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  M3Sk
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[0][i]=Vkvar.volumem3sk[i]*Vutt.factor;		
	Trakt->m_recData.m_recVars.m_recKvarvarande[0][NUM_OF_SP]=Vkvar.volumem3sk[NUM_OF_SP]*Vutt.factor; // 13
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  M3Fub
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[1][i]=Vkvar.volumefub[i]*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recKvarvarande[1][NUM_OF_SP]=Vkvar.volumefub[NUM_OF_SP]*Vutt.factor;
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  M3StamSk
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[2][i]=Vkvar.mstamm3sk[i];
	Trakt->m_recData.m_recVars.m_recKvarvarande[2][NUM_OF_SP]=Vkvar.mstamm3sk[NUM_OF_SP];
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  M3StamFub
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[3][i]=Vkvar.mstamm3fub[i];		
	Trakt->m_recData.m_recVars.m_recKvarvarande[3][NUM_OF_SP]=Vkvar.mstamm3fub[NUM_OF_SP];
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  GY
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[4][i]=Vkvar.gr[i]*Vutt.factor;		
	Trakt->m_recData.m_recVars.m_recKvarvarande[4][NUM_OF_SP]=Vkvar.gr[NUM_OF_SP]*Vutt.factor;			// 4
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  Antal
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[5][i]=Vkvar.stems[i]*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recKvarvarande[5][NUM_OF_SP]=Vkvar.stems[NUM_OF_SP]*Vutt.factor;		//7
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  Dg
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[6][i]=Vkvar.dg[i];
	Trakt->m_recData.m_recVars.m_recKvarvarande[6][NUM_OF_SP]=Vkvar.dg[NUM_OF_SP]; // 8
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  Hgv
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[7][i]=Vkvar.hgv[i];
	Trakt->m_recData.m_recVars.m_recKvarvarande[7][NUM_OF_SP]=Vkvar.hgv[NUM_OF_SP];
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  H25
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[8][i]=Vkvar.h25[i];
	Trakt->m_recData.m_recVars.m_recKvarvarande[8][NUM_OF_SP]=Vkvar.h25[NUM_OF_SP];// 9
	//-------------------------------  KVARVARANDE --------------------------------------------
	//											  Dgv
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recKvarvarande[9][i]=Vkvar.dgv[i];
	Trakt->m_recData.m_recVars.m_recKvarvarande[9][NUM_OF_SP]=Vkvar.dgv[NUM_OF_SP];


	
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  M3Sk	
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagMstv[0][i]=(Vutt.volumem3sk[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagMstv[0][NUM_OF_SP]=(Vutt.volumem3sk[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  M3Fub
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagMstv[1][i]=(Vutt.volumefub[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagMstv[1][NUM_OF_SP]=(Vutt.volumefub[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  M3StamSk
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i])
			Trakt->m_recData.m_recVars.m_recUttagMstv[2][i]=(Vutt.volumem3sk[i])/(Vutt.stems[i]);
	if(Vutt.stems[NUM_OF_SP])
		Trakt->m_recData.m_recVars.m_recUttagMstv[2][NUM_OF_SP]=(Vutt.volumem3sk[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  M3StamFub
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i])
			Trakt->m_recData.m_recVars.m_recUttagMstv[3][i]=(Vutt.volumefub[i])/(Vutt.stems[i]);
	if(Vutt.stems[NUM_OF_SP])
		Trakt->m_recData.m_recVars.m_recUttagMstv[3][NUM_OF_SP]=(Vutt.volumefub[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  Gy
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagMstv[4][i]=(Vutt.gr[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagMstv[4][NUM_OF_SP]=(Vutt.gr[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  Gy %
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagMstv[5][i]=(float)100.0*(Vutt.gr[i])/(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i]);
	if(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagMstv[5][NUM_OF_SP]=(float)100.0*(Vutt.gr[NUM_OF_SP])/(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP]);
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  Antal
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagMstv[6][i]=(Vutt.stems[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagMstv[6][NUM_OF_SP]=(Vutt.stems[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  Antal %
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i]+Vkvar.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagMstv[7][i]=(float)100.0*(Vutt.stems[i])/(Vutt.stems[i]+Vuttstk.stems[i]+Vkvar.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagMstv[7][NUM_OF_SP]=(float)100.0*(Vutt.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  Dg
	/*for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]>0.0)
			Res->utmstv.Dg[i]=(Vutt.dg[i]*Vutt.stems[i])/(Vutt.stems[i]);
	if(Vutt.stems[NUM_OF_SP]>0.0)
		Res->utmstv.Dg[NUM_OF_SP]=(Vutt.dg[NUM_OF_SP]*Vutt.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]);*/
	for(i=0;i<NUM_OF_SP;i++)
			Trakt->m_recData.m_recVars.m_recUttagMstv[8][i]=Vutt.dg[i];
	Trakt->m_recData.m_recVars.m_recUttagMstv[8][NUM_OF_SP]=Vutt.dg[NUM_OF_SP];

	//-------------------------------  UTTAG MELLAN STV --------------------------------------------
	//											  Dgv
	for(i=0;i<NUM_OF_SP;i++)
			Trakt->m_recData.m_recVars.m_recUttagMstv[9][i]=Vutt.dgv[i];
	Trakt->m_recData.m_recVars.m_recUttagMstv[9][NUM_OF_SP]=Vutt.dgv[NUM_OF_SP];


	
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  M3Sk	
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagIstv[0][i]=(Vuttstk.volumem3sk[i])*Vuttstk.factor;
	Trakt->m_recData.m_recVars.m_recUttagIstv[0][NUM_OF_SP]=(Vuttstk.volumem3sk[NUM_OF_SP])*Vuttstk.factor;
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  M3Fub
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagIstv[1][i]=(Vuttstk.volumefub[i])*Vuttstk.factor;
	Trakt->m_recData.m_recVars.m_recUttagIstv[1][NUM_OF_SP]=(Vuttstk.volumefub[NUM_OF_SP])*Vuttstk.factor;
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  M3StamSk
	for(i=0;i<NUM_OF_SP;i++)
		if(Vuttstk.stems[i])
			Trakt->m_recData.m_recVars.m_recUttagIstv[2][i]=(Vuttstk.volumem3sk[i])/(Vuttstk.stems[i]);
	if(Vuttstk.stems[NUM_OF_SP])
		Trakt->m_recData.m_recVars.m_recUttagIstv[2][NUM_OF_SP]=(Vuttstk.volumem3sk[NUM_OF_SP])/(Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  M3StamFub
	for(i=0;i<NUM_OF_SP;i++)
		if(Vuttstk.stems[i])
			Trakt->m_recData.m_recVars.m_recUttagIstv[3][i]=(Vuttstk.volumefub[i])/(Vuttstk.stems[i]);
	if(Vuttstk.stems[NUM_OF_SP])
		Trakt->m_recData.m_recVars.m_recUttagIstv[3][NUM_OF_SP]=(Vuttstk.volumefub[NUM_OF_SP])/(Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  Gy
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagIstv[4][i]=(Vuttstk.gr[i])*Vuttstk.factor;
	Trakt->m_recData.m_recVars.m_recUttagIstv[4][NUM_OF_SP]=(Vuttstk.gr[NUM_OF_SP])*Vuttstk.factor;
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  Gy %
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagIstv[5][i]=(float)100.0*(Vuttstk.gr[i])/(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i]);
	if(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagIstv[5][NUM_OF_SP]=(float)100.0*(Vuttstk.gr[NUM_OF_SP])/(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP]);
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  Antal
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagIstv[6][i]=(Vuttstk.stems[i])*Vuttstk.factor;
	Trakt->m_recData.m_recVars.m_recUttagIstv[6][NUM_OF_SP]=(Vuttstk.stems[NUM_OF_SP])*Vuttstk.factor;
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  Antal %
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i]+Vkvar.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagIstv[7][i]=(float)100.0*(Vuttstk.stems[i])/(Vutt.stems[i]+Vuttstk.stems[i]+Vkvar.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagIstv[7][NUM_OF_SP]=(float)100.0*(Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  Dg
	/*
	for(i=0;i<NUM_OF_SP;i++)
		if(Vuttstk.stems[i]>0.0)
			Res->utistv.Dg[i]=(Vuttstk.dg[i]*Vuttstk.stems[i])/(Vuttstk.stems[i]);
	if(Vuttstk.stems[NUM_OF_SP]>0.0)
		Res->utistv.Dg[NUM_OF_SP]=(Vuttstk.dg[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vuttstk.stems[NUM_OF_SP]);*/
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagIstv[8][i]=Vuttstk.dg[i];
	if(Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagIstv[8][NUM_OF_SP]=Vuttstk.dg[NUM_OF_SP];
	//-------------------------------  UTTAG I STV --------------------------------------------
	//											  Dgv
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagIstv[9][i]=Vuttstk.dgv[i];
	if(Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagIstv[9][NUM_OF_SP]=Vuttstk.dgv[NUM_OF_SP];


	
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  M3Sk	
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagTot[0][i]=(Vutt.volumem3sk[i]+Vuttstk.volumem3sk[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagTot[0][NUM_OF_SP]=(Vutt.volumem3sk[NUM_OF_SP]+Vuttstk.volumem3sk[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  M3Fub
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagTot[1][i]=(Vutt.volumefub[i]+Vuttstk.volumefub[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagTot[1][NUM_OF_SP]=(Vutt.volumefub[NUM_OF_SP]+Vuttstk.volumefub[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  M3StamSk
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i])
			Trakt->m_recData.m_recVars.m_recUttagTot[2][i]=(Vutt.volumem3sk[i]+Vuttstk.volumem3sk[i])/(Vutt.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP])
		Trakt->m_recData.m_recVars.m_recUttagTot[2][NUM_OF_SP]=(Vutt.volumem3sk[NUM_OF_SP]+Vuttstk.volumem3sk[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  M3StamFub
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i])
			Trakt->m_recData.m_recVars.m_recUttagTot[3][i]=(Vutt.volumefub[i]+Vuttstk.volumefub[i])/(Vutt.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP])
		Trakt->m_recData.m_recVars.m_recUttagTot[3][NUM_OF_SP]=(Vutt.volumefub[NUM_OF_SP]+Vuttstk.volumefub[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  Gy
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagTot[4][i]=(Vutt.gr[i]+Vuttstk.gr[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagTot[4][NUM_OF_SP]=(Vutt.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  Gy %
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagTot[5][i]=(float)100.0*(Vutt.gr[i]+Vuttstk.gr[i])/(Vutt.gr[i]+Vkvar.gr[i]+Vuttstk.gr[i]);
	if(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagTot[5][NUM_OF_SP]=(float)100.0*(Vutt.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP])/(Vutt.gr[NUM_OF_SP]+Vkvar.gr[NUM_OF_SP]+Vuttstk.gr[NUM_OF_SP]);
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  Antal
	for(i=0;i<NUM_OF_SP;i++)
		Trakt->m_recData.m_recVars.m_recUttagTot[6][i]=(Vutt.stems[i]+Vuttstk.stems[i])*Vutt.factor;
	Trakt->m_recData.m_recVars.m_recUttagTot[6][NUM_OF_SP]=(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP])*Vutt.factor;
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  Antal %
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i]+Vkvar.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagTot[7][i]=(float)100.0*(Vutt.stems[i]+Vuttstk.stems[i])/(Vutt.stems[i]+Vuttstk.stems[i]+Vkvar.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagTot[7][NUM_OF_SP]=(float)100.0*(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]+Vkvar.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  Dg
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagTot[8][i]=(Vutt.dg[i]*Vutt.stems[i]+Vuttstk.dg[i]*Vuttstk.stems[i])/(Vutt.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagTot[8][NUM_OF_SP]=(Vutt.dg[NUM_OF_SP]*Vutt.stems[NUM_OF_SP]+Vuttstk.dg[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);
	//-------------------------------  UTTAG TOTALT --------------------------------------------
	//											  Dgv
	for(i=0;i<NUM_OF_SP;i++)
		if(Vutt.stems[i]+Vuttstk.stems[i]>0.0)
			Trakt->m_recData.m_recVars.m_recUttagTot[9][i]=(Vutt.dgv[i]*Vutt.stems[i]+Vuttstk.dgv[i]*Vuttstk.stems[i])/(Vutt.stems[i]+Vuttstk.stems[i]);
	if(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recUttagTot[9][NUM_OF_SP]=(Vutt.dgv[NUM_OF_SP]*Vutt.stems[NUM_OF_SP]+Vuttstk.dgv[NUM_OF_SP]*Vuttstk.stems[NUM_OF_SP])/(Vutt.stems[NUM_OF_SP]+Vuttstk.stems[NUM_OF_SP]);

	
	//-------------------------------  GALLRINGSKVOT --------------------------------------------
	//											  Ga.kvot
	for(i=0;i<NUM_OF_SP;i++)
		if(Vkvar.dg[i]>0.0)
			Trakt->m_recData.m_recVars.m_recGaKvot[i]=Vutt.dg[i]/Vkvar.dg[i];
	if(Vkvar.dg[NUM_OF_SP]>0.0)
		Trakt->m_recData.m_recVars.m_recGaKvot[NUM_OF_SP]=Vutt.dg[NUM_OF_SP]/Vkvar.dg[NUM_OF_SP];
	
	// Skadade tr�d  
	Trakt->m_recData.m_recVars.m_Skadade_Trad=Vytdata.hurttrees;	// 17
	   
	// Skadade stam  
	Trakt->m_recData.m_recVars.m_Skadade_Stam=Vytdata.hurttree;
	   
	// Skadade r�tter
	Trakt->m_recData.m_recVars.m_Skadade_Rot=Vytdata.hurtrot;
	   
	// R�ta          
	Trakt->m_recData.m_recVars.m_Rota=Vytdata.hurtrots;
	   
	// Stickv�gsavst�nd 
	Trakt->m_recData.m_recVars.m_Stv_Avst=Vytdata.skidrowdist/(float)10.0;	//293
	   
	// Stickv�gsbredd   
	Trakt->m_recData.m_recVars.m_Stv_Bredd=Vytdata.skidrowwith/(float)10.0;	//294
	   
	// Stickv�gsareal   
	Trakt->m_recData.m_recVars.m_Stv_Areal=Vytdata.skidrowarea;					//295
	   
	// Sp�rstr�cka      
	Trakt->m_recData.m_recVars.m_Sparstracka=Vytdata.spardjuplen/(float)10.0;
	   
	// Sp�rdjup         
	Trakt->m_recData.m_recVars.m_Spardjup=Vytdata.spardjup/(float)10.0;
	   
	// Sp�rdjup %  
	Trakt->m_recData.m_recVars.m_Spardjup_Andel=Vytdata.spardjup_proc;


	//Skador
	// Stam
	if(Vytdata.skad_stam_tot>0)
	{
		Trakt->m_recData.m_recVars.m_Zon1_Stam=((float)Vytdata.skad_stam_zon1/(float)Vytdata.skad_stam_tot)*(float)100.0;
		Trakt->m_recData.m_recVars.m_Zon2_Stam=((float)Vytdata.skad_stam_zon2/(float)Vytdata.skad_stam_tot)*(float)100.0;
	}     

	// Rot
	if(Vytdata.skad_rot_tot>0)
	{
		Trakt->m_recData.m_recVars.m_Zon1_Rot=((float)Vytdata.skad_rot_zon1/(float)Vytdata.skad_rot_tot)*(float)100.0;
		Trakt->m_recData.m_recVars.m_Zon2_Rot=((float)Vytdata.skad_rot_zon2/(float)Vytdata.skad_rot_tot)*(float)100.0;
	}     

	// Stam & Rot
	if(Vytdata.skad_stamrot_tot>0)
	{
		Trakt->m_recData.m_recVars.m_Zon1_Rot_Stam=((float)Vytdata.skad_stamrot_zon1/(float)Vytdata.skad_stamrot_tot)*(float)100.0;
		Trakt->m_recData.m_recVars.m_Zon2_Rot_Stam=((float)Vytdata.skad_stamrot_zon2/(float)Vytdata.skad_stamrot_tot)*(float)100.0;
	}          
	Err_Msg=0;
	return Err_Msg;
}


void Count_Plot_Thinning(TRAKT_DATA *Trakt,PLOT_DATA Plot,float *stemnr,float *stubbnr,float *stemgy,float *stubbgy)
{
float gy=0.0,StemGy=0.0,StubbGy=0.0,StemNr=0.0,StubbNr=0.0;
unsigned int i=0;

if(Trakt->m_vTreeData.size()<=0)
    return;

  while(i<Trakt->m_vTreeData.size())
  {
	  
	  if((Trakt->m_vTreeData[i].m_nTree_PlotId==Plot.m_nPlot_id)&&Trakt->m_vTreeData[i].m_nTree_Dia>0)
      {
        gy=(float)3.1415*((float)Trakt->m_vTreeData[i].m_nTree_Dia/(float)2000.0)*((float)Trakt->m_vTreeData[i].m_nTree_Dia/(float)2000.0);
		  switch(Trakt->m_vTreeData[i].m_nTree_Typ)
        {
        case REMAINED:
          StemNr++;
          StemGy+=gy;
          break;
        case AVV_IN_FOREST:
        case AVK_IN_ROAD:
          StubbNr++;
          StubbGy+=gy;
          break;
        }
      }    
	 i++;
  }
  StemGy/=(float)Plot.m_nPlot_areal/(float)10000.0;
  StubbGy/=(float)Plot.m_nPlot_areal/(float)10000.0;
  StemNr/=(float)Plot.m_nPlot_areal/(float)10000.0;
  StubbNr/=(float)Plot.m_nPlot_areal/(float)10000.0;

  *stemnr=StemNr;
  *stubbnr=StubbNr;
  *stemgy=StemGy;
  *stubbgy=StubbGy;
}

int CountPlot(TRAKT_DATA *Trakt,CALCVolType *V)
{
   int num;
	unsigned int pIndex=0;
   //int i;
   num=0;
	*V=CALCVolType();
	//ClearVolType(V);
   
	if(Trakt->m_vPlotData.size()<=0)
         return 0;
   
   for(pIndex=0; pIndex < Trakt->m_vPlotData.size(); pIndex++)
	{
		
		num++;

		//if(plot.hurttree+plot.hurttree2+plot.hurtrot+plot.hurtrot2-plot.hurtrot_tree-plot.hurtrot_tree2>0)
		V->hurttrees+=((float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam1 + (float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam2 + 
							(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot1 + (float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot2 + 
							(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam_rot1 + (float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam_rot2); //DAMAGE TREES
		V->hurttree+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam1+(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam2; //DAMAGESTEM
		V->hurtrot+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot1+(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot2;//DAMAGEROTS

		V->hurtrots+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rota;//DAMAGEDECAY

		V->skad_stam_tot+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam1+(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam2;
		V->skad_stam_zon1+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam1;
		V->skad_stam_zon2+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam2;
		V->skad_rot_tot+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot1+(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot2;
		V->skad_rot_zon1+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot1;
		V->skad_rot_zon2+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_rot2;
		V->skad_stamrot_tot+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam_rot1+(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam_rot2;
		V->skad_stamrot_zon1+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam_rot1;
		V->skad_stamrot_zon2+=(float)Trakt->m_vPlotData[pIndex].m_nPlot_skad_stam_rot2;

		if(Trakt->m_vPlotData[pIndex].m_nPlot_ohjd>0)
		{
			V->overheightn++;
			V->overheight+=Trakt->m_vPlotData[pIndex].m_nPlot_ohjd;
		}
		if(Trakt->m_vPlotData[pIndex].m_nPlot_si>0)
		{
			V->SI[Trakt->m_vPlotData[pIndex].m_nPlot_si_tradslag]+=Trakt->m_vPlotData[pIndex].m_nPlot_si;
			V->SIn[Trakt->m_vPlotData[pIndex].m_nPlot_si_tradslag]++;
		}
		//�ndrat 20061117 R�knar �lder ocks�
		if(Trakt->m_vPlotData[pIndex].m_nPlot_brosthojdsalder>0)
		{
			V->SI_Agen++;
			V->SI_Age+=Trakt->m_vPlotData[pIndex].m_nPlot_brosthojdsalder;
		}


		if(Trakt->m_vPlotData[pIndex].m_nPlot_stv_bredd1>0)
		{
			V->skidrowwithn++;
			V->skidrowwith+=Trakt->m_vPlotData[pIndex].m_nPlot_stv_bredd1;
		}
		if(Trakt->m_vPlotData[pIndex].m_nPlot_stv_bredd2>0)
		{
			V->skidrowwithn++;
			V->skidrowwith+=Trakt->m_vPlotData[pIndex].m_nPlot_stv_bredd2;
		}

		if(Trakt->m_vPlotData[pIndex].m_nPlot_stv_avst1>0)
		{
			V->skidrowdistn++;
			V->skidrowdist+=Trakt->m_vPlotData[pIndex].m_nPlot_stv_avst1;
		}
		if(Trakt->m_vPlotData[pIndex].m_nPlot_stv_avst2>0)
		{
			V->skidrowdistn++;
			V->skidrowdist+=Trakt->m_vPlotData[pIndex].m_nPlot_stv_avst2;
		}


		// Lagt till 2070608 Sp�rdjup
		if((Trakt->m_vPlotData[pIndex].m_nPlot_spar_langd1>0) || (Trakt->m_vPlotData[pIndex].m_nPlot_spar_matstracka>0))
		{
			V->spardjupn++;
			V->spardjup+=Trakt->m_vPlotData[pIndex].m_nPlot_spar_langd1;
		}
		if((Trakt->m_vPlotData[pIndex].m_nPlot_spar_langd2>0) || (Trakt->m_vPlotData[pIndex].m_nPlot_spar_matstracka>0))
		{
			V->spardjupn++;
			V->spardjup+=Trakt->m_vPlotData[pIndex].m_nPlot_spar_langd2;
		}
		if(Trakt->m_vPlotData[pIndex].m_nPlot_spar_matstracka>0)
		{
			V->spardjuplenn++;
			V->spardjuplen+=2*Trakt->m_vPlotData[pIndex].m_nPlot_spar_matstracka;
		}		
   }
   
   if(V->overheightn>0.0)
      V->overheight/=V->overheightn;
   if(V->skidrowwithn>0.0)
      V->skidrowwith/=V->skidrowwithn;
   if(V->skidrowdistn)
      V->skidrowdist/=V->skidrowdistn;
   if(V->skidrowdist)
      V->skidrowarea=(float)100.0/V->skidrowdist*V->skidrowwith; //%
   // Lagt till 2070608 Sp�rdjup
   //if(V->spardjuplenn>0.0)
   //  V->spardjuplen/=V->spardjuplenn;
   //if(V->spardjupn>0.0)
   //  V->spardjup/=V->spardjupn;
   if((V->spardjup>0.0) && (V->spardjuplen>0.0))
     V->spardjup_proc=(float)100.0*(V->spardjup/V->spardjuplen);



   if(V->SIn[0])
      V->SI[0]/=V->SIn[0];
   if(V->SIn[1])
      V->SI[1]/=V->SIn[1];
   if(V->SIn[2])
      V->SI[2]/=V->SIn[2];
   //Lagt till 20061117 �lder
   if(V->SI_Agen)
      V->SI_Age/=V->SI_Agen;

   V->SIspec=GetMax(V->SIn,3);
   V->SItot=V->SI[V->SIspec];

   return num;
}

int GetMax(float *f,int n)
{
   int maxi=0;
   float max=f[0];
   for(int i=1;i<n;i++)
      if(f[i]>max)
      {
         max=f[i];
         maxi=i;
      }
   return maxi;
}


int CountStandtable(TRAKT_DATA *Trakt)
{
   int totnum,s=0;
	unsigned int i=0,nSpec=0;
	CALCSAMPLETYPE SampleType;
	stbl=_stand_table();
	//ClearStandTabe();
	vSampleType.clear();
   
	if(Trakt->m_vTreeData.size()<0)
		return 0;

	totnum=Trakt->m_vTreeData.size();

   while( i < Trakt->m_vTreeData.size() )
   {
		// Lagt till en koll h�r som omvandlar Tr�dslagsnummer till position i tr�dslagsarray
		nSpec=Get_Defined_SpeciePlace(Trakt->m_vTreeData[i].m_nTree_SpecId);
		if(nSpec!=-1)
		{
			if(Trakt->m_vTreeData[i].m_nTree_Dia>=MINDIA&&Trakt->m_vTreeData[i].m_nTree_Dia<MAXDIA&&nSpec<NUM_OF_SP)
			{
				stbl.StandTable[Trakt->m_vTreeData[i].m_nTree_Typ][nSpec][Trakt->m_vTreeData[i].m_nTree_Dia/DIACLASS]++;
				stbl.StandTable[Trakt->m_vTreeData[i].m_nTree_Typ][nSpec][MAXDIA/DIACLASS]++;
				stbl.StandTable[Trakt->m_vTreeData[i].m_nTree_Typ][NUM_OF_SP][Trakt->m_vTreeData[i].m_nTree_Dia/DIACLASS]++;
				stbl.StandTable[Trakt->m_vTreeData[i].m_nTree_Typ][NUM_OF_SP][MAXDIA/DIACLASS]++;

				if(Trakt->m_vTreeData[i].m_nTree_Typ!=REMAINED)
				{
					stbl.StandTable[NUM_OF_TYPES-1][nSpec][Trakt->m_vTreeData[i].m_nTree_Dia/DIACLASS]++;
					stbl.StandTable[NUM_OF_TYPES-1][nSpec][MAXDIA/DIACLASS]++;
					stbl.StandTable[NUM_OF_TYPES-1][NUM_OF_SP][Trakt->m_vTreeData[i].m_nTree_Dia/DIACLASS]++;
					stbl.StandTable[NUM_OF_TYPES-1][NUM_OF_SP][MAXDIA/DIACLASS]++;
				}

				if((Trakt->m_vTreeData[i].m_nTree_Hgt>0||Trakt->m_vTreeData[i].m_nTree_Hgt2>0))
				{
					SampleType= CALCSAMPLETYPE();
					SampleType.spc=nSpec;
					SampleType.dia=Trakt->m_vTreeData[i].m_nTree_Dia;
					SampleType.hgt1=Trakt->m_vTreeData[i].m_nTree_Hgt;
					SampleType.hgt2=Trakt->m_vTreeData[i].m_nTree_Hgt2;
					vSampleType.push_back(SampleType);
				}
			}
		}
		i++;
   }
  
   return stbl.StandTable[NUM_OF_TYPES-1][NUM_OF_SP][MAXDIA/DIACLASS]+stbl.StandTable[REMAINED][NUM_OF_SP][MAXDIA/DIACLASS]+stbl.StandTable[AVK_IN_ROAD][NUM_OF_SP][MAXDIA/DIACLASS];
}

float GetArealFactor(TRAKT_DATA *Trakt)
{
   int num=0;
	unsigned int i=0;
   float tfactor=1.0;
	   
	if(Trakt->m_vPlotData.size()<=0)
      return 1.0;

	while( i < Trakt->m_vPlotData.size())
	{
			tfactor+=(float)Trakt->m_vPlotData[i].m_nPlot_areal;
		i++;
   }
	if(tfactor>0.0)
		tfactor=(float)10000.0/(tfactor);
	else
		tfactor=(float)1.0;
	return tfactor;
}
float Brandel(float dia,float height,float a,float b,float c,float d,float e)
{
	return 
		pow((float)10.0,(float)a) * 
		pow((float)dia,(float)b) * 
		pow((float)dia+(float)20.0,(float)c) * 
		pow((float)height,(float)d) * 
		pow((float)height-(float)1.3,(float)e);
}

int Get_Defined_SpeciePlace(int _SpecNr)
{
   switch(_SpecNr)
	{
	case IDI_SPEC1: return 0;
	case IDI_SPEC2: return 1;
	case IDI_SPEC3: return 2;
	case IDI_SPEC4: return 3;
	case IDI_SPEC5: return 4;
	case IDI_SPEC6: return 5;
	case IDI_SPEC7: return 6;
	//case IDI_SPEC8: return 7;
	case IDI_SPEC9: return NUM_OF_SP;
	}
	return -1;
}