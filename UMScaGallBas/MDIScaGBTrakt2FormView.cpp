#include "stdafx.h"
#include "MDIScaGBTrakt2FormView.h"
#include "MDIScaGBTrakt2Frame.h"
#include "ResLangFileReader.h"
#include "HXLHandling.h"

IMPLEMENT_DYNCREATE(CMDIScaGBTrakt2FormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIScaGBTrakt2FormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT, OnValueChanged)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_TRAKT_DATA_REPORT2, OnValueChanged)
	ON_CBN_EDITCHANGE(IDC_COMBO6_3, &CMDIScaGBTrakt2FormView::OnCbnEditchangeCombo63)
	ON_CBN_EDITCHANGE(IDC_COMBO6_1, &CMDIScaGBTrakt2FormView::OnCbnEditchangeCombo61)
END_MESSAGE_MAP()

CMDIScaGBTrakt2FormView::CMDIScaGBTrakt2FormView()
	: CXTResizeFormView(CMDIScaGBTrakt2FormView::IDD)
{
	m_bConnected = FALSE;
}

CMDIScaGBTrakt2FormView::~CMDIScaGBTrakt2FormView()
{
}

void CMDIScaGBTrakt2FormView::OnClose()
{
}

BOOL CMDIScaGBTrakt2FormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIScaGBTrakt2FormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_STATIC6_1, m_wndLbl_Forvalt);
	DDX_Control(pDX, IDC_STATIC6_2, m_wndLbl_DrivEnh);
	DDX_Control(pDX, IDC_STATIC6_3, m_wndLbl_AvvId);
	DDX_Control(pDX, IDC_STATIC6_4, m_wndLbl_InvTyp);
	DDX_Control(pDX, IDC_STATIC6_5, m_wndLbl_Avlagg);
	DDX_Control(pDX, IDC_STATIC6_6, m_wndLbl_Distrikt);
	DDX_Control(pDX, IDC_STATIC6_7, m_wndLbl_Maskinlag);
	DDX_Control(pDX, IDC_STATIC6_8, m_wndLbl_Ursrpung);
	DDX_Control(pDX, IDC_STATIC6_9, m_wndLbl_ProdLedare);
	DDX_Control(pDX, IDC_STATIC6_10, m_wndLbl_Namn);
	DDX_Control(pDX, IDC_STATIC6_11, m_wndLbl_Datum);
	DDX_Control(pDX, IDC_STATIC6_12, m_wndLbl_Areal);
	DDX_Control(pDX, IDC_STATIC6_13, m_wndLbl_AntYtor);
	DDX_Control(pDX, IDC_STATIC6_14, m_wndLbl_SiAlder);
	DDX_Control(pDX, IDC_STATIC6_15, m_wndLbl_MedOhjd);
	DDX_Control(pDX, IDC_STATIC6_16, m_wndLbl_SiSp);
	DDX_Control(pDX, IDC_STATIC6_17, m_wndLbl_GyDirFore);
	DDX_Control(pDX, IDC_STATIC6_18, m_wndLbl_GyDirEfter);
	//DDX_Control(pDX, IDC_STATIC6_19, m_wndLbl_Notes);

	DDX_Control(pDX, IDC_COMBO6_1	, m_wndCBoxForvalt);
	DDX_Control(pDX, IDC_EDIT6_1	, m_wndEditDrivEnh);
	DDX_Control(pDX, IDC_EDIT6_2	, m_wndEditAvvId);
	DDX_Control(pDX, IDC_COMBO6_2	, m_wndCBoxInvType);
	DDX_Control(pDX, IDC_EDIT6_3	, m_wndEditAvlagg);
	DDX_Control(pDX, IDC_COMBO6_3 , m_wndCBoxDistrikt);
	DDX_Control(pDX, IDC_EDIT6_4	, m_wndEditMaskinlag);
	DDX_Control(pDX, IDC_EDIT6_5	, m_wndEditUrsprung);
	DDX_Control(pDX, IDC_EDIT6_6	, m_wndEditProdLedare);
	DDX_Control(pDX, IDC_EDIT6_7	, m_wndEditNamn);
	//DDX_Control(pDX, IDC_EDIT6_8	, m_wndEditDatum);

	DDX_Control(pDX, IDC_EDIT6_9	,	m_wndEditAreal);
	DDX_Control(pDX, IDC_EDIT6_10	, m_wndEditAntYtor);
	DDX_Control(pDX, IDC_EDIT6_11	, m_wndEditSiAlder);
	DDX_Control(pDX, IDC_EDIT6_12	, m_wndEditMedOhjd);
	DDX_Control(pDX, IDC_COMBO6_4, m_wndCBoxSiSp);
	DDX_Control(pDX, IDC_EDIT6_13, m_wndEditSiTot);
	DDX_Control(pDX, IDC_EDIT6_14, m_wndEditGyDirFore);
	DDX_Control(pDX, IDC_EDIT6_15, m_wndEditGyDirEfter);
	DDX_Control(pDX, IDC_EDIT6_16, m_wndEditNotes);

	DDX_Control(pDX, IDC_GROUP6_1, m_wndGroup);
	DDX_Control(pDX, IDC_GROUP6_2, m_wndGroup2);
	DDX_Control(pDX, IDC_GROUP6_3, m_wndGroup3);

	DDX_Control(pDX, IDC_COMBO_DATEPICKER_2, m_wndDatePicker);
}


void CMDIScaGBTrakt2FormView::OnSize(UINT nType,int cx,int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl,1,rect.top+305,rect.right - 2,rect.bottom-(rect.top+310));
}

void CMDIScaGBTrakt2FormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));


	m_wndEditDrivEnh.SetLimitText(DRIV_ENH_MAX_LEN);
	//m_wndEditAvvId.SetLimitText(DELID_MAX_LEN);
	m_wndEditAvlagg.SetLimitText(AVLAGG_MAX_LEN);
	m_wndEditMaskinlag.SetLimitText(MASKINLAG_MAX_LEN);
	m_wndEditUrsprung.SetLimitText(URSPRUNG_MAX_LEN);
	m_wndEditProdLedare.SetLimitText(PRODLED_MAX_LEN);
	//m_wndEditDatum.SetLimitText(DATUM_MAX_LEN);
	
	m_wndEditDrivEnh.SetEnabledColor(BLACK, WHITE );
	m_wndEditDrivEnh.SetDisabledColor(BLACK, INFOBK );
	
	

	m_wndEditAvvId.SetEnabledColor(BLACK, WHITE );
	m_wndEditAvvId.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEditAvlagg.SetEnabledColor(BLACK, WHITE );
	m_wndEditAvlagg.SetDisabledColor(BLACK, INFOBK );

	
	m_wndEditMaskinlag.SetEnabledColor(BLACK, WHITE );
	m_wndEditMaskinlag.SetDisabledColor(BLACK, INFOBK );

	
	m_wndEditUrsprung.SetEnabledColor(BLACK, WHITE );
	m_wndEditUrsprung.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEditProdLedare.SetEnabledColor(BLACK, WHITE );
	m_wndEditProdLedare.SetDisabledColor(BLACK, INFOBK );
	
	
	m_wndEditNamn.SetEnabledColor(BLACK, WHITE );
	m_wndEditNamn.SetDisabledColor(BLACK, INFOBK );
	
	//m_wndEditDatum.SetEnabledColor(BLACK, WHITE );
	//m_wndEditDatum.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditAreal.SetEnabledColor(BLACK, WHITE );
	m_wndEditAreal.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditAntYtor.SetEnabledColor(BLACK, WHITE );
	m_wndEditAntYtor.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditMedOhjd.SetEnabledColor(BLACK, WHITE );
	m_wndEditMedOhjd.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditSiTot.SetEnabledColor(BLACK, WHITE );
	m_wndEditSiTot.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditSiAlder.SetEnabledColor(BLACK, WHITE );
	m_wndEditSiAlder.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEditGyDirFore.SetEnabledColor(BLACK, WHITE );
	m_wndEditGyDirFore.SetDisabledColor(BLACK, INFOBK );
	m_wndEditGyDirEfter.SetEnabledColor(BLACK, WHITE );
	m_wndEditGyDirEfter.SetDisabledColor(BLACK, INFOBK );
	m_wndEditNotes.SetEnabledColor(BLACK, WHITE );
	m_wndEditNotes.SetDisabledColor(BLACK, INFOBK );

	m_wndCBoxForvalt.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxForvalt.SetDisabledColor(BLACK,INFOBK );
	m_wndCBoxDistrikt.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxDistrikt.SetDisabledColor(BLACK,INFOBK );
	m_wndCBoxInvType.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxInvType.SetDisabledColor(BLACK,INFOBK );
	m_wndCBoxSiSp.SetEnabledColor(BLACK,WHITE );
	m_wndCBoxSiSp.SetDisabledColor(BLACK,INFOBK );

	m_wndDatePicker.setDateInComboBox();


	//setAllReadOnly(FALSE,FALSE);
	setKeysReadOnly();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getForvaltsFromDB();
	addForvaltsToCBox();

	getDistriktFromDB();
	addDistriktToCBox();

	getTraktIndexFromDB();
	//getTraktsFromDB();
	

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	setLanguage();

	getInvTypesFromFile();
	addInvTypesToCBox();

	getSiSpFromFile();
	addSiSpToCBox();

	m_nDBIndex = 0;
	setupTraktTabs();
	populateData(m_nDBIndex);
	
	m_bIsDirty = FALSE;
	resetIsDirty();
	m_enumAction = NOTHING;
	
	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl,1,rect.top+305,rect.right - 2,rect.bottom-(rect.top+310));

}

BOOL CMDIScaGBTrakt2FormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIScaGBTrakt2FormView::setNavigationButtons2(void)
{
	BOOL start=FALSE,end=FALSE;
	if (!m_vecTraktIndex.empty())
	{
		if(m_nDBIndex > 0)
			start=TRUE;

		if(m_nDBIndex < ((int)m_vecTraktIndex.size()-1))
			end=TRUE;
	}
	setNavigationButtons(start,end);	
}

void CMDIScaGBTrakt2FormView::OnSetFocus(CWnd*)
{
	setNavigationButtons2();
/*
	if (!m_vecTraktIndex.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTraktIndex.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}*/
}

void CMDIScaGBTrakt2FormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Trakt information
			m_wndLbl_Forvalt.SetWindowText(xml->str(IDS_STRING7550));
			m_wndLbl_DrivEnh.SetWindowText(xml->str(IDS_STRING7551));
			m_wndLbl_AvvId.SetWindowText(xml->str(IDS_STRING7552));
			m_wndLbl_InvTyp.SetWindowText(xml->str(IDS_STRING7553));
			m_wndLbl_Avlagg.SetWindowText(xml->str(IDS_STRING7554));
			m_wndLbl_Distrikt.SetWindowText(xml->str(IDS_STRING7555));
			m_wndLbl_Maskinlag.SetWindowText(xml->str(IDS_STRING7556));
			m_wndLbl_Ursrpung.SetWindowText(xml->str(IDS_STRING7557));
			m_wndLbl_ProdLedare.SetWindowText(xml->str(IDS_STRING7558));
			m_wndLbl_Namn.SetWindowText(xml->str(IDS_STRING7559));
			m_wndLbl_Datum.SetWindowText(xml->str(IDS_STRING7560));
			m_wndLbl_Areal.SetWindowText(xml->str(IDS_STRING7561));
			m_wndLbl_AntYtor.SetWindowText(xml->str(IDS_STRING7562));
			m_wndLbl_SiAlder.SetWindowText(xml->str(IDS_STRING7565));
			m_wndLbl_MedOhjd.SetWindowText(xml->str(IDS_STRING7563));
			m_wndLbl_SiSp.SetWindowText(xml->str(IDS_STRING7564));
			m_wndLbl_GyDirFore.SetWindowText(xml->str(IDS_STRING7567));
			m_wndLbl_GyDirEfter.SetWindowText(xml->str(IDS_STRING7568));



			m_sErrCap = xml->str(IDS_STRING7574);
			m_sErrMsg = xml->str(IDS_STRING7620);
			m_sNotOkToSave.Format(_T("%s\n\n%s\n\n%s"),
									xml->str(IDS_STRING7621),
									xml->str(IDS_STRING7622),
									xml->str(IDS_STRING7623));
			m_sSaveData.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7624),
									xml->str(IDS_STRING7629));
			m_sUpdateTraktMsg.Format(_T("%s\n\n%s\n%s\n\n%s"),
									xml->str(IDS_STRING7626),
									xml->str(IDS_STRING7627),
									xml->str(IDS_STRING7628),
									xml->str(IDS_STRING7629));
		}
		delete xml;
	}
}

void CMDIScaGBTrakt2FormView::showData()
{
	int idx=-1;
	//m_wndCBoxForvalt.SetItemData(0,m_recActiveTrakt.m_nTrakt_forvalt);
	idx=getForvaltIndex(m_recActiveTrakt.m_nTrakt_forvalt);
	//if(idx!=-1)
		m_wndCBoxForvalt.SetCurSel(idx);
	m_wndCBoxForvalt.SetWindowText(getForvaltSelected(m_recActiveTrakt.m_nTrakt_forvalt));
	
			
	//m_wndCBoxDistrikt.SetItemData(0,m_recActiveTrakt.m_nTrakt_distrikt);
	idx=getDistriktIndex(m_recActiveTrakt.m_nTrakt_distrikt);
	//if(idx!=-1)
		m_wndCBoxDistrikt.SetCurSel(idx);
	m_wndCBoxDistrikt.SetWindowText(getDistriktSelected(m_recActiveTrakt.m_nTrakt_distrikt));
	
	m_wndCBoxInvType.SetWindowText(getInvTypeSelected(m_recActiveTrakt.m_nTrakt_inv_typ));		
	//m_wndCBoxInvType.SetItemData(0,m_recActiveTrakt.m_nTrakt_inv_typ);
	m_wndCBoxInvType.SetCurSel(getInvTypeIndex(m_recActiveTrakt.m_nTrakt_inv_typ));

	m_wndCBoxSiSp.SetWindowText(getSiSpSelected(m_recActiveTrakt.m_nTrakt_si_sp));		
	//m_wndCBoxSiSp.SetItemData(0,m_recActiveTrakt.m_nTrakt_si_sp);
	m_wndCBoxSiSp.SetCurSel(getSiSpIndex(m_recActiveTrakt.m_nTrakt_si_sp));
	//m_wndEdit1.setInt(m_recActiveTrakt.m_nTrakt_id_typ);
	
	
	m_wndEditDrivEnh.setInt(m_recActiveTrakt.m_nTrakt_driv_enh);
	m_wndEditAvvId.SetWindowText(m_recActiveTrakt.m_sTrakt_avv_id);
	m_wndEditAvlagg.setInt(m_recActiveTrakt.m_nTrakt_avlagg);
	
	m_wndEditMaskinlag.setInt(m_recActiveTrakt.m_nTrakt_maskinlag);
	m_wndEditUrsprung.setInt(m_recActiveTrakt.m_nTrakt_ursprung);
	m_wndEditProdLedare.setInt(m_recActiveTrakt.m_nTrakt_prod_led);
	m_wndEditNamn.SetWindowText(m_recActiveTrakt.m_sTrakt_namn);
	//m_wndEditDatum.SetWindowText(m_recActiveTrakt.m_sTrakt_date);
	m_wndDatePicker.SetWindowText(m_recActiveTrakt.m_sTrakt_date);
	m_wndEditAreal.setFloat(m_recActiveTrakt.m_fTrakt_areal,1);

	m_wndEditAntYtor.setFloat(m_recActiveTrakt.m_fTrakt_ant_ytor,0);
	m_wndEditMedOhjd.setFloat(m_recActiveTrakt.m_fTrakt_medel_ovre_hojd,1);
	m_wndEditSiTot.setFloat(m_recActiveTrakt.m_fTrakt_si_tot,1);
	m_wndEditSiAlder.setFloat(m_recActiveTrakt.m_fTrakt_alder,1);
	m_wndEditGyDirFore.setFloat(m_recActiveTrakt.m_fTrakt_gy_dir_fore,1);
	m_wndEditGyDirEfter.setFloat(m_recActiveTrakt.m_fTrakt_gy_dir_mal,1);
	m_wndEditNotes.SetWindowText(m_recActiveTrakt.m_sTrakt_notes);

	

	showTabData(m_recActiveTrakt);
}


void CMDIScaGBTrakt2FormView::showTabData(TRAKT_DATA data)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage8 *wndReport8;
				wndReport8 = DYNAMIC_DOWNCAST(CMyReportPage8,CWnd::FromHandle(pItem->GetHandle()));
				wndReport8->showData(data);
				break;			
			case 1:
				CMyReportPage9 *wndReport9;
				wndReport9 = DYNAMIC_DOWNCAST(CMyReportPage9,CWnd::FromHandle(pItem->GetHandle()));
				wndReport9->showData(data);
				break;			
			}	
		}
	}
}


void CMDIScaGBTrakt2FormView::populateData(UINT idx)
{
	DISTRIKT_DATA data;
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	RLFReader *xml = new RLFReader;
	CMDIScaGBTrakt2Frame *pView = (CMDIScaGBTrakt2Frame *)getFormViewByID(IDD_FORMVIEW6)->GetParent();
	if (xml->Load(m_sLangFN))
		tst3=xml->str(IDS_STRING756);
	tst+=tst3;

	if (!m_vecTraktIndex.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktIndex.size())
	{
	//m_recActiveTrakt = m_vecTraktData[idx];
		LoadActiveTrakt(idx);
		showData();
		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),m_nDBIndex+1,m_vecTraktIndex.size());
				tst+=tst2;
				pDoc->SetTitle(tst);						
			}
		}
	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setAllReadOnly( TRUE, TRUE );
		clearAll();
		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),0,0);
				tst+=tst2;
				pDoc->SetTitle(tst);						
			}
		}
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIScaGBTrakt2FormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			m_enumAction = MANUALLY;
			addTraktManually();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			m_enumAction = FROM_FILE;
			addTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			if (getIsDirty())
				saveTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteTrakt();
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (m_nDBIndex != 0)
			{
				if (!isDataChanged())
				{
					m_nDBIndex = 0;
					setNavigationButtons2();
					//setNavigationButtons(FALSE,TRUE);
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			if (m_nDBIndex != 0)
			{
				if (!isDataChanged())
				{
					m_nDBIndex--;
					if (m_nDBIndex < 0)
						m_nDBIndex = 0;
					setNavigationButtons2();
					/*
					if (m_nDBIndex == 0)
					{
						setNavigationButtons(false,true);
					}
					else
					{
						setNavigationButtons(true,true);
					}*/
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			if (m_nDBIndex != ((int)m_vecTraktIndex.size() - 1))
			{
				if (!isDataChanged())
				{

					m_nDBIndex++;
					if (m_nDBIndex > ((int)m_vecTraktIndex.size() - 1))
						m_nDBIndex = (int)m_vecTraktIndex.size() - 1;

					setNavigationButtons2();
					/*
					if (m_nDBIndex == m_vecTraktIndex.size() - 1)
					{
						setNavigationButtons(true,false);
					}
					else
					{
						setNavigationButtons(true,false);
					}*/
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			if (m_nDBIndex != ((int)m_vecTraktIndex.size() - 1))
			{
				if (!isDataChanged())
				{
					m_nDBIndex = (int)m_vecTraktIndex.size()-1;
					setNavigationButtons2();
					//setNavigationButtons(TRUE,FALSE);	
					populateData(m_nDBIndex);
					setKeysReadOnly();
				}
			}
			break;
		}	// case ID_NEW_ITEM :
		case ID_PREVIEW_ITEM :
		{
			if(!m_vecTraktIndex.empty())
			{
				//makeDummyReport();
			}
		}
	};
	return 0L;
}

/*
void CMDIScaGBTrakt2FormView::makeDummyReport()
{
	CString csReport=_T(""),csLang=_T(""),csArgs=_T("");

	csLang.Format("%s",getLangSet());
	csReport.Format(_T("%s\%s\\test%s.rpt"), getReportsDir(), csLang, csLang);
	if(!fileExists(csReport))
	{
		csReport.Format(_T("%s\\ENU\\testENU.rpt"), getReportsDir());
	}
	csArgs.Format("%d;%d;%d;%d;",m_vecTraktIndex[m_nDBIndex].m_nTrakt_forvalt,m_vecTraktIndex[m_nDBIndex].m_nTrakt_driv_enh,m_vecTraktIndex[m_nDBIndex].m_nTrakt_del_id,m_vecTraktIndex[m_nDBIndex].m_nTrakt_inv_typ);
	//csArgs.Format("%d;;;;",m_vecTraktIndex[m_nDBIndex].m_nTrakt_forvalt);
	// tell the report-suite to open the report
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(333, 
		"OpenSuiteEx", 
		"Reports2.dll",
		csReport,
		"",
		csArgs));	
}*/


void CMDIScaGBTrakt2FormView::getTraktIndexFromDB(void)
{
	if (m_bConnected)
	{
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTraktIndex(m_vecTraktIndex,1);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTrakt2FormView::LoadActiveTrakt(int idx)
{
TRAKT_INDEX trkt_idx=m_vecTraktIndex[idx];
	if (m_bConnected)
	{
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakt(trkt_idx,m_recActiveTrakt);
			pDB->getPlots(m_recActiveTrakt);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}



void CMDIScaGBTrakt2FormView::getForvaltsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getForvalts(m_vecForvaltData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTrakt2FormView::getDistriktFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistrikts(m_vecDistriktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTrakt2FormView::getInvTypesFromFile(void)
{
	if (fileExists(m_sLangFN))
	{
		m_vecInvTypeData.clear();
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		m_vecInvTypeData.push_back(_inv_type_data(0,xml->str(IDS_STRING7610)));
		m_vecInvTypeData.push_back(_inv_type_data(1,xml->str(IDS_STRING7611)));
		m_vecInvTypeData.push_back(_inv_type_data(2,xml->str(IDS_STRING7612)));
		}
	}
}

void CMDIScaGBTrakt2FormView::getSiSpFromFile(void)
{
	if (fileExists(m_sLangFN))
	{
		m_vecSiSpData.clear();
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		m_vecSiSpData.push_back(_inv_type_data(1,xml->str(IDS_STRING7600)));
		m_vecSiSpData.push_back(_inv_type_data(2,xml->str(IDS_STRING7601)));
		m_vecSiSpData.push_back(_inv_type_data(3,xml->str(IDS_STRING7609)));
		}
	}
}

void CMDIScaGBTrakt2FormView::addTraktManually(void)
{
	setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d
	TRAKT_DATA fileTrakt= TRAKT_DATA();
	m_recActiveTrakt=fileTrakt;
	clearAll();
	//m_wndListMachineBtn.EnableWindow( TRUE );
	m_bIsDirty = TRUE;
}

void CMDIScaGBTrakt2FormView::addTrakt(void)
{
	unsigned int i=0;
	TRAKT_DATA fileTrakt=TRAKT_DATA();
	if (HXL_Open(m_dbConnectionData,m_sLangFN,this,&fileTrakt)!=1)
	{
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);		
	}
	else
	{
	setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d
	//clearAll();
	m_recActiveTrakt.m_vPlotData.clear();
	m_recActiveTrakt=fileTrakt;
	m_recActiveTrakt.m_nTrakt_enter_type=0;
	showData();
	//m_wndListMachineBtn.EnableWindow( TRUE );
	m_bIsDirty = TRUE;
	}
}

// PUBLIC
void CMDIScaGBTrakt2FormView::saveTrakt(void)
{
	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			getEnteredData();
			// Setup index information; 061207 p�d
			
			if (!pDB->addTrakt( m_recNewTrakt ))
			{
				if ((::MessageBox(0,m_sUpdateTraktMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
				{
					pDB->updTrakt( m_recNewTrakt );
					m_structTraktIdentifer.m_nTrakt_forvalt	= m_recNewTrakt.m_nTrakt_forvalt;
					m_structTraktIdentifer.m_nTrakt_driv_enh	= m_recNewTrakt.m_nTrakt_driv_enh;
					m_structTraktIdentifer.m_sTrakt_avv_id		= m_recNewTrakt.m_sTrakt_avv_id;
					m_structTraktIdentifer.m_nTrakt_inv_typ	= m_recNewTrakt.m_nTrakt_inv_typ;		
					if(m_recNewTrakt.m_vPlotData.size()>0)
					{
						for(unsigned int pi=0;pi<m_recNewTrakt.m_vPlotData.size();pi++)
							if(!pDB->addPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]))
								pDB->updPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]);
					}
				}
			}
			else
			{
				if(m_recNewTrakt.m_vPlotData.size()>0)
				{
					for(unsigned int pi=0;pi<m_recNewTrakt.m_vPlotData.size();pi++)
						if(!pDB->addPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]))
							pDB->updPlot(m_recNewTrakt,m_recNewTrakt.m_vPlotData[pi]);
				}
				m_structTraktIdentifer.m_nTrakt_forvalt	= m_recNewTrakt.m_nTrakt_forvalt;
				m_structTraktIdentifer.m_nTrakt_driv_enh	= m_recNewTrakt.m_nTrakt_driv_enh;
				m_structTraktIdentifer.m_sTrakt_avv_id		= m_recNewTrakt.m_sTrakt_avv_id;
				m_structTraktIdentifer.m_nTrakt_inv_typ	= m_recNewTrakt.m_nTrakt_inv_typ;		
			}
			delete pDB;
		}	// if (pDB != NULL)
		resetIsDirty();
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
		m_enumAction = NOTHING;
	}	// if (isOKToSave())
}

void CMDIScaGBTrakt2FormView::deleteTrakt(void)
{
	TRAKT_DATA dataTrakt;
	CString sMsg=_T(""),tmp=_T("");
	CString sText1;	//	Ta bort trakt
	CString sText2;	// OBS! Om trakt tas bort, kommer all underliggande information att tas bort
	CString sText3;	// Dvs. att alla Ytor raderas
	CString sText4;	// Skall denna trakt tas bort ... ?
	CString sForvalt1,sDrivEnh1,sAvvId1,sInvTyp1,sAvlagg1,sDistrikt1,sMaskin1,sUrsprung1,sProdLed1,sNamn1,sDate1,sAreal1,sAntYt1;
	CString sForvalt,sDrivEnh,sAvvId,sInvTyp,sAvlagg,sDistrikt,sMaskin,sUrsprung,sProdLed,sNamn,sDate,sAreal,sAntYt;

	

	CString sCaption;	// Meddelande
	CString sOKBtn;	// Ta bort
	CString sCancelBtn;// Avbryt
	BOOL bDidDel=FALSE;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING7570);			sText2 = xml->str(IDS_STRING7571);
			sText3 = xml->str(IDS_STRING7572);			sText4 = xml->str(IDS_STRING7573);

			sForvalt1	= xml->str(IDS_STRING7550);
			sDrivEnh1	= xml->str(IDS_STRING7551);
			sAvvId1		= xml->str(IDS_STRING7552);
			sInvTyp1		= xml->str(IDS_STRING7553);
			sAvlagg1		= xml->str(IDS_STRING7554);
			sDistrikt1	= xml->str(IDS_STRING7555);
			sMaskin1		= xml->str(IDS_STRING7556);
			sUrsprung1	= xml->str(IDS_STRING7557);
			sProdLed1	= xml->str(IDS_STRING7558);
			sNamn1		= xml->str(IDS_STRING7559);
			sDate1		= xml->str(IDS_STRING7560);
			sAreal1		= xml->str(IDS_STRING7561);
			sAntYt1		= xml->str(IDS_STRING7562);
			
			
			sCaption = xml->str(IDS_STRING7574);
			sOKBtn = xml->str(IDS_STRING7575);
			sCancelBtn = xml->str(IDS_STRING7576);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktIndex.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					dataTrakt = m_recActiveTrakt;
					// Setup a message for user upon deleting 
					if(dataTrakt.m_nTrakt_forvalt==MY_NULL)
						sForvalt=_T("");
					else
						sForvalt.Format(_T("%d"),dataTrakt.m_nTrakt_forvalt);

					if(dataTrakt.m_nTrakt_driv_enh==MY_NULL)
						sDrivEnh=_T("");
					else
						sDrivEnh.Format(_T("%d"),dataTrakt.m_nTrakt_driv_enh);

					sAvvId = dataTrakt.m_sTrakt_avv_id;

					if(dataTrakt.m_nTrakt_inv_typ==MY_NULL)
						sInvTyp=_T("");
					else
						sInvTyp=getInvTypeSelected(dataTrakt.m_nTrakt_inv_typ);

					if(dataTrakt.m_nTrakt_avlagg==MY_NULL)
						sAvlagg=_T("");
					else
						sAvlagg.Format(_T("%d"),dataTrakt.m_nTrakt_avlagg);

					if(dataTrakt.m_nTrakt_distrikt==MY_NULL)
						sDistrikt=_T("");
					else
						sDistrikt.Format(_T("%d"),dataTrakt.m_nTrakt_distrikt);

					if(dataTrakt.m_nTrakt_maskinlag==MY_NULL)
						sMaskin=_T("");
					else
						sMaskin.Format(_T("%d"),dataTrakt.m_nTrakt_maskinlag);	

					if(dataTrakt.m_nTrakt_ursprung==MY_NULL)
						sUrsprung=_T("");
					else
						sUrsprung.Format(_T("%d"),dataTrakt.m_nTrakt_ursprung);


					if(dataTrakt.m_nTrakt_prod_led==MY_NULL)
						sDistrikt=_T("");
					else
						sProdLed.Format(_T("%d"),dataTrakt.m_nTrakt_prod_led);	


					sNamn=dataTrakt.m_sTrakt_namn;	
					sDate=dataTrakt.m_sTrakt_date;

					if(dataTrakt.m_fTrakt_areal==MY_NULL)
						sAreal=_T("");
					else
						sAreal.Format(_T("%.1f"),dataTrakt.m_fTrakt_areal);	

					if(dataTrakt.m_fTrakt_ant_ytor==MY_NULL)
						sAntYt=_T("");
					else
						sAntYt.Format(_T("%d"),dataTrakt.m_fTrakt_ant_ytor);	
					
					sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
						sText1,
						sForvalt1,sForvalt,
						sDrivEnh1,sDrivEnh,
						sAvvId1,sAvvId,
						sInvTyp1,sInvTyp,
						sAvlagg1,sAvlagg,
						sDistrikt1,sDistrikt,
						sMaskin1,sMaskin,
						sUrsprung1,sUrsprung,
						sProdLed1,sProdLed,
						sNamn1,sNamn,
						sDate1,sDate,
						sAreal1,sAreal,
						sAntYt1,sAntYt,
						sText2,
						sText3,
						sText4);
				
					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delTrakt(dataTrakt);
						bDidDel=TRUE;
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	if(bDidDel==TRUE)
		resetTrakt(RESET_TO_LAST_SET_NB);
	else
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
}

// Get manually entered data; 061011 p�d
void CMDIScaGBTrakt2FormView::getEnteredData(void)
{
	int nTabPages=0,nPage=0;
	unsigned int i=0;
	CString tst=_T(""),sDate=_T("");

	m_recNewTrakt=m_recActiveTrakt;
	/*
	for(i=0;i<m_recActiveTrakt.m_vPlotData.size();i++)
	{
		m_recNewTrakt.m_vPlotData.push_back(m_recActiveTrakt.m_vPlotData[i]);
	}*/	
	CString csStr;

	// Identification information
	m_recNewTrakt.m_nTrakt_forvalt			= getForvaltID();
	m_recNewTrakt.m_nTrakt_driv_enh			= m_wndEditDrivEnh.getInt();
	m_recNewTrakt.m_sTrakt_avv_id				= m_wndEditAvvId.getText();
	m_recNewTrakt.m_nTrakt_inv_typ			= getInvTypeID();
	
	m_recNewTrakt.m_nTrakt_avlagg				= m_wndEditAvlagg.getInt();
	m_recNewTrakt.m_nTrakt_distrikt			= getDistriktID();
	//m_recNewTrakt.m_nTrakt_distrikt			= m_wndEditDistrikt.getInt();
	m_recNewTrakt.m_nTrakt_maskinlag			= m_wndEditMaskinlag.getInt();
	m_recNewTrakt.m_nTrakt_ursprung			= m_wndEditUrsprung.getInt();
	m_recNewTrakt.m_nTrakt_prod_led			= m_wndEditProdLedare.getInt();
	m_recNewTrakt.m_sTrakt_namn				= m_wndEditNamn.getText();
		
	//m_recNewTrakt.m_sTrakt_date				= m_wndEditDatum.getText();
	m_wndDatePicker.GetWindowText(sDate);
	m_recNewTrakt.m_sTrakt_date	= sDate;
	m_recNewTrakt.m_fTrakt_areal				= m_wndEditAreal.getFloat();
	m_recNewTrakt.m_fTrakt_ant_ytor			= m_wndEditAntYtor.getInt();
	m_recNewTrakt.m_fTrakt_medel_ovre_hojd	= m_wndEditMedOhjd.getFloat();
	m_recNewTrakt.m_fTrakt_alder				= m_wndEditSiAlder.getFloat();
	m_recNewTrakt.m_fTrakt_si_tot				= m_wndEditSiTot.getFloat();
	m_recNewTrakt.m_nTrakt_si_sp				= getSiSpID();
	m_recNewTrakt.m_fTrakt_gy_dir_fore		= m_wndEditGyDirFore.getFloat();
	m_recNewTrakt.m_fTrakt_gy_dir_mal		= m_wndEditGyDirEfter.getFloat();
	m_recNewTrakt.m_sTrakt_notes				= m_wndEditNotes.getText();
	nTabPages=m_wndTabControl.getNumOfTabPages();
	for(nPage = 0; nPage<nTabPages; nPage++)
	{		
		//H�mta flik
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage8 *wndReport8;
				wndReport8 = DYNAMIC_DOWNCAST(CMyReportPage8,CWnd::FromHandle(pItem->GetHandle()));
				wndReport8->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;			
			case 1:
				CMyReportPage9 *wndReport9;
				wndReport9 = DYNAMIC_DOWNCAST(CMyReportPage9,CWnd::FromHandle(pItem->GetHandle()));
				wndReport9->getData(&m_recNewTrakt.m_recData.m_recVars);
				break;			
			}
		}
	}
					
	// Setup who data's entered, Maually or by file; 061201 p�d
	if (m_enumAction == MANUALLY)
		m_recNewTrakt.m_nTrakt_enter_type = 1;

	m_recNewTrakt.m_nTrakt_forenklad= 1;



	//	csStr.Format("%5.1f",m_recNewTrakt.m_recData.m_recVars.m_Skadade_Trad);
	//	AfxMessageBox(csStr);

	// Trakt information
	//CString sDate;
	//m_wndDatePicker.GetWindowText(sDate);
	//m_recNewTrakt.m_sTraktDate		= sDate;

	// Check RegionID and DistriktID. If they eq. -1 then
	// Set to m_recActiveTrakt values; 061011 p�d
	/*
	if (m_recNewMachine.m_nRegionID == -1 && m_recNewMachine.m_nDistriktID == -1)
	{
		m_recNewMachine.m_nRegionID = m_recActiveTrakt.m_nRegionID;
		m_recNewMachine.m_nDistriktID = m_recActiveTrakt.m_nDistriktID;
	}*/
		
}

// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/Distrikt is entered
// using a Dialog; 061011 p�d
void CMDIScaGBTrakt2FormView::clearAll(void)
{
	m_wndEditDrivEnh.SetWindowText(_T(""));
	m_wndEditAvvId.SetWindowText(_T(""));
	m_wndEditAvlagg.SetWindowText(_T(""));
	m_wndEditMaskinlag.SetWindowText(_T(""));
	m_wndEditUrsprung.SetWindowText(_T(""));
	m_wndEditProdLedare.SetWindowText(_T(""));
	m_wndEditNamn.SetWindowText(_T(""));
	//m_wndEditDatum.SetWindowText(_T(""));
	m_wndDatePicker.SetWindowText(_T(""));
	m_wndEditAreal.SetWindowText(_T(""));
	m_wndEditAntYtor.SetWindowText(_T(""));
	m_wndEditMedOhjd.SetWindowText(_T(""));
	m_wndEditSiTot.SetWindowText(_T(""));
	m_wndEditSiAlder.SetWindowText(_T(""));
	m_wndEditGyDirFore.SetWindowText(_T(""));
	m_wndEditGyDirEfter.SetWindowText(_T(""));
	m_wndEditNotes.SetWindowText(_T(""));
	
	m_wndCBoxForvalt.SetCurSel(-1);
	m_wndCBoxDistrikt.SetCurSel(-1);
	m_wndCBoxInvType.SetCurSel(-1);
	m_wndCBoxSiSp.SetCurSel(-1);

	CTraktVariables data = CTraktVariables();
	TRAKT_DATA data2 = _trakt_data();
	data2.m_recData=data;
	showTabData(data2);
	
	

	// Set m_recNewTrakt key-values to -1
	SetKeyValuesToNegative(m_recNewTrakt);
}

void CMDIScaGBTrakt2FormView::SetKeyValuesToNegative(TRAKT_DATA rec)
{
	rec.m_nTrakt_forvalt		= MY_NULL;
	rec.m_nTrakt_driv_enh	= MY_NULL;
	rec.m_sTrakt_avv_id		= _T("");
	rec.m_nTrakt_inv_typ		= MY_NULL;	
	rec.m_fTrakt_areal		= 0.0;
}

// Set all Editboxes to read or read only, depending on
// if data comes from database and holds Compartmant(s) and Plot(s); 061010 p�d
void CMDIScaGBTrakt2FormView::setAllReadOnly(BOOL ro1,BOOL ro2)
{
	m_wndEditDrivEnh.SetReadOnly( ro1);
	m_wndEditAvvId.SetReadOnly( ro1);
	m_wndEditAvlagg.SetReadOnly( ro1);
	m_wndEditMaskinlag.SetReadOnly( ro1);
	m_wndEditUrsprung.SetReadOnly( ro1);
	m_wndEditProdLedare.SetReadOnly( ro1);
	m_wndEditNamn.SetReadOnly( ro1);
//	m_wndEditDatum.SetReadOnly( ro1);
	m_wndEditAreal.SetReadOnly( ro1);
	m_wndEditAntYtor.SetReadOnly( ro1);
	m_wndEditMedOhjd.SetReadOnly( ro1);
	m_wndEditSiTot.SetReadOnly( ro1);
	m_wndEditSiAlder.SetReadOnly( ro1);
	m_wndEditGyDirFore.SetReadOnly( ro1);
	m_wndEditGyDirEfter.SetReadOnly( ro1);
	m_wndEditNotes.SetReadOnly( ro1);
	m_wndCBoxForvalt.SetReadOnly( ro2);
	m_wndCBoxDistrikt.SetReadOnly( ro2);
	m_wndCBoxInvType.SetReadOnly( ro2);
	m_wndCBoxSiSp.SetReadOnly( ro2);
}

void CMDIScaGBTrakt2FormView::setKeysReadOnly()
{
	m_wndCBoxForvalt.SetReadOnly(TRUE);
	m_wndEditDrivEnh.SetReadOnly(TRUE);
	m_wndEditAvvId.SetReadOnly(TRUE);
	m_wndCBoxInvType.SetReadOnly(TRUE);

	m_wndEditAvlagg.SetReadOnly(FALSE);
	m_wndEditMaskinlag.SetReadOnly(FALSE);
	m_wndEditUrsprung.SetReadOnly(FALSE);
	m_wndEditProdLedare.SetReadOnly(FALSE);
	m_wndEditNamn.SetReadOnly(FALSE);
	//m_wndEditDatum.SetReadOnly(FALSE);
	m_wndEditAreal.SetReadOnly(FALSE);
	m_wndEditAntYtor.SetReadOnly(FALSE);
	m_wndEditMedOhjd.SetReadOnly(FALSE);
	m_wndEditSiTot.SetReadOnly(FALSE);
	m_wndEditSiAlder.SetReadOnly(FALSE);
	m_wndEditGyDirFore.SetReadOnly(FALSE);
	m_wndEditGyDirEfter.SetReadOnly(FALSE);
	m_wndEditNotes.SetReadOnly(FALSE);

	
	m_wndCBoxDistrikt.SetReadOnly(FALSE);
	m_wndCBoxSiSp.SetReadOnly(FALSE);
}

// Add Inv Types to ComboBox3
void CMDIScaGBTrakt2FormView::addInvTypesToCBox(void)
{
	CString sText;
	if (m_vecInvTypeData.size() > 0)
	{
		m_wndCBoxInvType.ResetContent();
		for (UINT i = 0;i < m_vecInvTypeData.size();i++)
		{
			INV_TYPE_DATA data = m_vecInvTypeData[i];
			sText.Format(_T("%s"),data.m_sInvTypeName);
			m_wndCBoxInvType.AddString(sText);
			m_wndCBoxInvType.SetItemData(i, data.m_nInvTypeID);
		}	
	}	
}

CString CMDIScaGBTrakt2FormView::getInvTypeSelected(int inv_type)
{
	CString sText;
	if (m_vecInvTypeData.size() > 0)
	{
		for (UINT i = 0;i < m_vecInvTypeData.size();i++)
		{
			INV_TYPE_DATA data = m_vecInvTypeData[i];
			if (data.m_nInvTypeID == inv_type)
			{
				sText.Format(_T("%s"),data.m_sInvTypeName);
				return sText;
			}
		}
	}	
	return _T("");
}

CString CMDIScaGBTrakt2FormView::getSiSpSelected(int sisp)
{
	CString sText;
	if (m_vecSiSpData.size() > 0)
	{
		for (UINT i = 0;i < m_vecSiSpData.size();i++)
		{
			INV_TYPE_DATA data = m_vecSiSpData[i];
			if (data.m_nInvTypeID == sisp)
			{
				sText.Format(_T("%s"),data.m_sInvTypeName);
				return sText;
			}
		}
	}	
	return _T("");
}

void CMDIScaGBTrakt2FormView::addSiSpToCBox(void)
{
	CString sText;
	if (m_vecSiSpData.size() > 0)
	{
		m_wndCBoxSiSp.ResetContent();
		for (UINT i = 0;i < m_vecSiSpData.size();i++)
		{
			INV_TYPE_DATA data = m_vecSiSpData[i];
			sText.Format(_T("%s"),data.m_sInvTypeName);
			m_wndCBoxSiSp.AddString(sText);
			m_wndCBoxSiSp.SetItemData(i, data.m_nInvTypeID);
		}	
	}	
}

// Try to find index of inv type in m_vecInvType;
int CMDIScaGBTrakt2FormView::getInvTypeID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected inv type is used on New item; 061205 p�d
	if (m_vecInvTypeData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < (int)m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_vecTraktIndex[m_nDBIndex].m_nTrakt_inv_typ;
	}
	else
	{
		int nIdx = m_wndCBoxInvType.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return (int)m_wndCBoxInvType.GetItemData(nIdx);
		}
	}
	return -1;
}

int CMDIScaGBTrakt2FormView::getInvTypeIndex(int inv_type)
{
	if (m_vecInvTypeData.size() > 0)
	{
		for (UINT i = 0;i < m_vecInvTypeData.size();i++)
		{
			INV_TYPE_DATA data = m_vecInvTypeData[i];
			if (data.m_nInvTypeID == inv_type)
					return i;
	
		}
	}	
	return -1;
}



// Try to find index of inv type in m_vecInvType;
int CMDIScaGBTrakt2FormView::getSiSpID(void)
{
	
	if (m_vecSiSpData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < (int)m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_recActiveTrakt.m_nTrakt_si_sp;
	}
	else
	{
		int nIdx = m_wndCBoxSiSp.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return (int)m_wndCBoxSiSp.GetItemData(nIdx);
		}
	}
	return -1;
}

int CMDIScaGBTrakt2FormView::getSiSpIndex(int sisp)
{
	if (m_vecSiSpData.size() > 0)
	{
		for (UINT i = 0;i < m_vecSiSpData.size();i++)
		{
			INV_TYPE_DATA data = m_vecSiSpData[i];
			if (data.m_nInvTypeID == sisp)
					return i;
		}
	}	
	return -1;
}



// Add regions in registry, into ComboBox3; 061204 p�d
void CMDIScaGBTrakt2FormView::addForvaltsToCBox(void)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		m_wndCBoxForvalt.ResetContent();
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			sText.Format(_T("%d - %s"),data.m_nForvaltID,data.m_sForvaltName);
			m_wndCBoxForvalt.AddString(sText);
			m_wndCBoxForvalt.SetItemData(i, data.m_nForvaltID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

CString CMDIScaGBTrakt2FormView::getForvaltSelected(int forvalt)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			if (data.m_nForvaltID == forvalt)
			{
				sText.Format(_T("%d - %s"),data.m_nForvaltID,data.m_sForvaltName);
				return sText;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	if(forvalt==MY_NULL)
	sText.Format(_T("%s"),"");
	else
	sText.Format(_T("%d"),forvalt);
	return sText;
}


int CMDIScaGBTrakt2FormView::getForvaltID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Forvaltn is used on New item; 061205 p�d
	/*if (m_vecTraktIndex.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_recActiveTrakt.m_nTrakt_forvalt;
	}
	else
	{
		int nIdx = m_wndCBoxForvalt.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBoxForvalt.GetItemData(nIdx);
		}
	}*/
		int nIdx = m_wndCBoxForvalt.GetCurSel(),value=-1;
		CString csTest=_T("");
		if (nIdx != CB_ERR)
		{
			return (int)m_wndCBoxForvalt.GetItemData(nIdx);
		}
		else
		{
			m_wndCBoxForvalt.GetWindowText(csTest);
			value=_tstoi(csTest.GetBuffer(0));
			if(value>0 && value <999)
			return value;
			else
				return -1;

		}
	return -1;
}

int CMDIScaGBTrakt2FormView::getForvaltIndex(int forvalt)
{
	if (m_vecForvaltData.size() > 0)
	{
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			if (data.m_nForvaltID == forvalt)
			{
				return i;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	return -1;
}

// Add Distrikts into Combobox2
void CMDIScaGBTrakt2FormView::addDistriktToCBox(void)
{
	CString sText;
	if (m_vecDistriktData.size() > 0)
	{
		m_wndCBoxDistrikt.ResetContent();
		for (UINT i = 0;i < m_vecDistriktData.size();i++)
		{
			DISTRIKT_DATA data = m_vecDistriktData[i];
			sText.Format(_T("%d - %s"),data.m_nDistriktID,data.m_sDistriktName);
			m_wndCBoxDistrikt.AddString(sText);
			m_wndCBoxDistrikt.SetItemData(i, data.m_nDistriktID);
		}	// for (UINT i = 0;i < m_vecDistriktData.size();i++)
	}	// if (m_vecDistriktData.size() > 0)
}

// Try to find distrikt, based on index;
CString CMDIScaGBTrakt2FormView::getDistriktSelected(int distrikt)
{
	CString sText;
	if (m_vecDistriktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistriktData.size();i++)
		{
			DISTRIKT_DATA data = m_vecDistriktData[i];
			if (data.m_nDistriktID == distrikt)
			{
				sText.Format(_T("%d - %s"),data.m_nDistriktID,data.m_sDistriktName);
				return sText;
			}	// if (data.m_nDistriktID == distrikt)
		}	// for (UINT i = 0;i < m_vecDistriktData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	if(distrikt==MY_NULL)
		sText.Format(_T("%s"),"");
	else
		sText.Format(_T("%d"),distrikt);
	return sText;
}

// Try to find index of regionnumber in m_vecDistriktData; 061204 p�d
int CMDIScaGBTrakt2FormView::getDistriktID(void)
{
	CString tst=_T("");
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Distrikt is used on New item; 061205 p�d
	/*if (m_vecTraktIndex.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktIndex.size() && m_enumAction == NOTHING)
	{
		return m_recActiveTrakt.m_nTrakt_distrikt;
	}
	else
	{
		int nIdx = m_wndCBoxDistrikt.GetCurSel();
		if (nIdx != CB_ERR)
		{
			//tst.Format("Distriktselected = %d\n Distrikt Id = %d",nIdx,m_wndCBoxDistrikt.GetItemData(nIdx));
			//AfxMessageBox(tst);
			return m_wndCBoxDistrikt.GetItemData(nIdx);
		}
	}*/

		int nIdx = m_wndCBoxDistrikt.GetCurSel(),value=-1;
		CString csTest=_T("");
		if (nIdx != CB_ERR)
		{
			return (int)m_wndCBoxDistrikt.GetItemData(nIdx);
		}	
		else
		{
			m_wndCBoxDistrikt.GetWindowText(csTest);
			value=_tstoi(csTest.GetBuffer(0));
			if(value>0 && value<999)
				return value;
			else
				return -1;
		}

	return -1;
}

int CMDIScaGBTrakt2FormView::getDistriktIndex(int distrikt)
{
	if (m_vecDistriktData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistriktData.size();i++)
		{
			DISTRIKT_DATA data = m_vecDistriktData[i];
			if (data.m_nDistriktID == distrikt)
				return i;
		}	// for (UINT i = 0;i < m_vecDistriktData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	return -1;
}


// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIScaGBTrakt2FormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

void CMDIScaGBTrakt2FormView::setNavigationButtonSave(BOOL on)
{
AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,on);
}



// Check that information needed to be abel to save Trakt, is enterd
// by user; 061113 p�d
BOOL CMDIScaGBTrakt2FormView::isOKToSave(void)
{
	
	if (	_tcscmp(m_wndCBoxInvType.getText().Trim(),_T("")) == 0 
		|| _tcscmp(m_wndCBoxForvalt.getText().Trim(),_T("")) == 0 
		|| m_wndEditDrivEnh.getInt() <= MY_NULL 
		|| _tcscmp(m_wndEditAvvId.getText().Trim(),_T("")) == 0
		|| m_wndEditAreal.getFloat() <= MY_NULL) 
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}

	//Kolla produktionsledare numeriskt och antal tecken
	if(m_wndEditProdLedare.getText().GetLength()>0)
	{
		if(!isInt(m_wndEditProdLedare.getText().GetBuffer(0)))
		{
			AfxMessageBox(_T("Fel i Prod.Ledare data\n Ej numerisk (0-9)"));
			return FALSE;
		}
		if(m_wndEditProdLedare.getText().GetLength()>9)
		{
			AfxMessageBox(_T("Fel i Prod.Ledare data\n F�r m�nga tecken! Max 9"));
			return FALSE;
		}
	}
	//Kolla maskinlag numeriskt och antal tecken
	if(m_wndEditMaskinlag.getText().GetLength()>0)
	{
		if(!isInt(m_wndEditMaskinlag.getText().GetBuffer(0)))
		{
			AfxMessageBox(_T("Fel i Maskinlag data\n Ej numerisk (0-9)"));
			return FALSE;
		}
		if(m_wndEditMaskinlag.getText().GetLength()>9)
		{
			AfxMessageBox(_T("Fel i Maskinlag data\n F�r m�nga tecken! Max 9"));
			return FALSE;
		}
	}

	
	if(m_wndEditDrivEnh.getText().GetLength()>0)
	{
		if(!isInt(m_wndEditDrivEnh.getText().GetBuffer(0)))
		{
			AfxMessageBox(_T("Fel i Avl�gg/Drivn.enhet\n Ej numerisk (0-9)"));
			return FALSE;
		}
		if(m_wndEditDrivEnh.getText().GetLength()>5)
		{
			AfxMessageBox(_T("Fel i Avl�gg/Drivn.enhet\n F�r m�nga tecken! Max 5"));
			return FALSE;
		}
	}
	
	return TRUE;
}



// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CMDIScaGBTrakt2FormView::isDataChanged(void)
{
	if (getIsDirty())
	{
		if (::MessageBox(0,m_sSaveData,m_sErrCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		{
			m_enumAction = CHANGED;
			saveTrakt();
			resetIsDirty();
		   return TRUE;
		}
		else
			resetIsDirty();
	}
	return FALSE;
}

void CMDIScaGBTrakt2FormView::doPopulate(int index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons2();
	//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTraktIndex.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIScaGBTrakt2FormView::resetTrakt(enumRESET reset)
{
	// Get updated data from Database
	getTraktIndexFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktIndex.size() > 0)
	{
		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons( m_nDBIndex > 0, m_nDBIndex < ((int)m_vecTraktIndex.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = (int)m_vecTraktIndex.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons( m_nDBIndex > 0, m_nDBIndex < ((int)m_vecTraktIndex.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = (int)m_vecTraktIndex.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktIndex.size();i++)
			{
				TRAKT_INDEX data = m_vecTraktIndex[i];
				if (data.m_nTrakt_forvalt == m_structTraktIdentifer.m_nTrakt_forvalt &&
					data.m_nTrakt_driv_enh == m_structTraktIdentifer.m_nTrakt_driv_enh &&
					data.m_sTrakt_avv_id == m_structTraktIdentifer.m_sTrakt_avv_id &&
					data.m_nTrakt_inv_typ == m_structTraktIdentifer.m_nTrakt_inv_typ)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons2();
				//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTraktIndex.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons2();
				//setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons2();
		//setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}

BOOL CMDIScaGBTrakt2FormView::getIsHeaderDirty(void)
{
	CString sDate;
	int dirtyDate=0,date1=0,date2=0;
	m_wndDatePicker.GetWindowText(sDate);
	TRAKT_DATA m_recTempTrakt=TRAKT_DATA();

	date1=_tstoi(sDate.GetBuffer(0));
	date2=_tstoi(m_recActiveTrakt.m_sTrakt_date.GetBuffer(0));
	if(date1!=date2)
		dirtyDate=1;

	m_recTempTrakt.m_nTrakt_forvalt			= getForvaltID();
	m_recTempTrakt.m_nTrakt_driv_enh			= m_wndEditDrivEnh.getInt();
	m_recTempTrakt.m_sTrakt_avv_id				= m_wndEditAvvId.getText();
	m_recTempTrakt.m_nTrakt_inv_typ			= getInvTypeID();
	
	m_recTempTrakt.m_nTrakt_avlagg				= m_wndEditAvlagg.getInt();
	m_recTempTrakt.m_nTrakt_distrikt			= getDistriktID();
	m_recTempTrakt.m_nTrakt_maskinlag			= m_wndEditMaskinlag.getInt();
	m_recTempTrakt.m_nTrakt_ursprung			= m_wndEditUrsprung.getInt();
	m_recTempTrakt.m_nTrakt_prod_led			= m_wndEditProdLedare.getInt();
	m_recTempTrakt.m_sTrakt_namn				= m_wndEditNamn.getText();
		
	m_wndDatePicker.GetWindowText(sDate);
	m_recTempTrakt.m_sTrakt_date	= sDate;
	m_recTempTrakt.m_fTrakt_areal				= m_wndEditAreal.getFloat();
	m_recTempTrakt.m_fTrakt_ant_ytor			= m_wndEditAntYtor.getInt();
	m_recTempTrakt.m_fTrakt_medel_ovre_hojd	= m_wndEditMedOhjd.getFloat();
	m_recTempTrakt.m_fTrakt_alder				= m_wndEditSiAlder.getFloat();
	m_recTempTrakt.m_fTrakt_si_tot				= m_wndEditSiTot.getFloat();
	m_recTempTrakt.m_nTrakt_si_sp				= getSiSpID();
	m_recTempTrakt.m_fTrakt_gy_dir_fore		= m_wndEditGyDirFore.getFloat();
	m_recTempTrakt.m_fTrakt_gy_dir_mal		= m_wndEditGyDirEfter.getFloat();
	m_recTempTrakt.m_sTrakt_notes				= m_wndEditNotes.getText();


	if(dirtyDate ||
		m_recTempTrakt.m_nTrakt_forvalt			!= m_recTempTrakt.m_nTrakt_forvalt	||
		m_recTempTrakt.m_nTrakt_driv_enh			!= m_recActiveTrakt.m_nTrakt_driv_enh	||		 
		m_recTempTrakt.m_sTrakt_avv_id			!= m_recActiveTrakt.m_sTrakt_avv_id	||		
		m_recTempTrakt.m_nTrakt_inv_typ			!= m_recActiveTrakt.m_nTrakt_inv_typ	||		

		m_recTempTrakt.m_nTrakt_avlagg			!= m_recActiveTrakt.m_nTrakt_avlagg	||		 
		m_recTempTrakt.m_nTrakt_distrikt			!= m_recActiveTrakt.m_nTrakt_distrikt	||		 
		m_recTempTrakt.m_nTrakt_maskinlag		!= m_recActiveTrakt.m_nTrakt_maskinlag||		 
		m_recTempTrakt.m_nTrakt_ursprung			!= m_recActiveTrakt.m_nTrakt_ursprung	||		 
		m_recTempTrakt.m_nTrakt_prod_led			!= m_recActiveTrakt.m_nTrakt_prod_led	||		 
		m_recTempTrakt.m_sTrakt_namn				!= m_recActiveTrakt.m_sTrakt_namn		||		 

		m_recTempTrakt.m_sTrakt_date				!= m_recActiveTrakt.m_sTrakt_date		||
		m_recTempTrakt.m_fTrakt_areal				!= m_recActiveTrakt.m_fTrakt_areal		||		 
		m_recTempTrakt.m_fTrakt_ant_ytor			!= m_recActiveTrakt.m_fTrakt_ant_ytor	||		 
		m_recTempTrakt.m_fTrakt_medel_ovre_hojd!= m_recActiveTrakt.m_fTrakt_medel_ovre_hojd ||
		m_recTempTrakt.m_fTrakt_alder				!= m_recActiveTrakt.m_fTrakt_alder		||
		m_recTempTrakt.m_fTrakt_si_tot			!= m_recActiveTrakt.m_fTrakt_si_tot	||		 
		m_recTempTrakt.m_nTrakt_si_sp				!= m_recActiveTrakt.m_nTrakt_si_sp		||		 
		m_recTempTrakt.m_fTrakt_gy_dir_fore		!= m_recActiveTrakt.m_fTrakt_gy_dir_fore ||		 
		m_recTempTrakt.m_fTrakt_gy_dir_mal		!= m_recActiveTrakt.m_fTrakt_gy_dir_mal	||	 
		m_recTempTrakt.m_sTrakt_notes				!= m_recActiveTrakt.m_sTrakt_notes)
		return TRUE;
	else
		return FALSE;

}

BOOL CMDIScaGBTrakt2FormView::getIsDirty(void)
{
	if (m_bIsDirty==TRUE)
		return m_bIsDirty;

		if(getIsHeaderDirty())
		return TRUE;

		/*
	if(m_wndEditDrivEnh.isDirty()		||
	m_wndEditAvvId.isDirty()		||
	m_wndEditAvlagg.isDirty()		||
	m_wndEditMaskinlag.isDirty()	||
	m_wndEditUrsprung.isDirty()	||
	m_wndEditProdLedare.isDirty() ||
	m_wndEditNamn.isDirty()			||
	dirtyDate							||
	m_wndEditAreal.isDirty()		||
	m_wndEditAntYtor.isDirty()		||
	m_wndEditMedOhjd.isDirty()		||
	m_wndEditSiTot.isDirty()		||
	m_wndEditSiAlder.isDirty()		||
	m_wndEditGyDirFore.isDirty()	||
	m_wndEditGyDirEfter.isDirty() ||
	m_wndEditNotes.isDirty() ||
	m_wndCBoxForvalt.isDirty()		|| 
	m_wndCBoxDistrikt.isDirty()	|| 
	m_wndCBoxInvType.isDirty()		||
	m_wndCBoxSiSp.isDirty())
	{
		return TRUE;
	}*/
	int nTabPages = m_wndTabControl.getNumOfTabPages();
	BOOL bDirty=FALSE;
	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage8 *wndReport8;
				wndReport8 = DYNAMIC_DOWNCAST(CMyReportPage8,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport8->m_bIsDirty2;
				break;
			case 1:
				CMyReportPage9 *wndReport9;
				wndReport9 = DYNAMIC_DOWNCAST(CMyReportPage9,CWnd::FromHandle(pItem->GetHandle()));
				bDirty=wndReport9->m_bIsDirty2;
				break;
			}			
		}
		if(bDirty)
			return TRUE;
	}

	return FALSE;
}

void CMDIScaGBTrakt2FormView::resetIsDirty(void)
{
	m_bIsDirty = FALSE;

	m_wndEditDrivEnh.resetIsDirty();
	m_wndEditAvvId.resetIsDirty();
	m_wndEditAvlagg.resetIsDirty();
	m_wndEditMaskinlag.resetIsDirty();
	m_wndEditUrsprung.resetIsDirty();
	m_wndEditProdLedare.resetIsDirty();
	m_wndEditNamn.resetIsDirty();
//	m_wndEditDatum.resetIsDirty();
	m_wndEditAreal.resetIsDirty();
	m_wndEditAntYtor.resetIsDirty();
	m_wndEditMedOhjd.resetIsDirty();
	m_wndEditSiTot.resetIsDirty();
	m_wndEditSiAlder.resetIsDirty();
	m_wndEditGyDirFore.resetIsDirty();
	m_wndEditGyDirEfter.resetIsDirty();
	m_wndEditNotes.resetIsDirty();
	m_wndCBoxForvalt.resetIsDirty(); 
	m_wndCBoxDistrikt.resetIsDirty();
	m_wndCBoxInvType.resetIsDirty();
	m_wndCBoxSiSp.resetIsDirty();

	m_wndCBoxForvalt.resetIsDirty();
	m_wndCBoxDistrikt.resetIsDirty();	
	m_wndCBoxInvType.resetIsDirty();	
	m_wndCBoxSiSp.resetIsDirty();	

	int nTabPages = m_wndTabControl.getNumOfTabPages();

	BOOL bDirty=FALSE;
	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage8 *wndReport8;
				wndReport8 = DYNAMIC_DOWNCAST(CMyReportPage8,CWnd::FromHandle(pItem->GetHandle()));
				wndReport8->resetIsDirty();
				break;
			case 1:
				CMyReportPage9 *wndReport9;
				wndReport9 = DYNAMIC_DOWNCAST(CMyReportPage9,CWnd::FromHandle(pItem->GetHandle()));
				wndReport9->resetIsDirty();
				break;
			}			
		}
	}
}

BOOL CMDIScaGBTrakt2FormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;
	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
				 rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
				 TRACE0( "Warning: couldn't create client tab for view.\n" );
				 // pWnd will be cleaned up by PostNcDestroy
				 return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	return TRUE;
}



BOOL CMDIScaGBTrakt2FormView::setupTraktTabs(void)
{
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,CRect(0,0,0,0),this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	//m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS,NULL,0,CSize(16, 16),xtpImageNormal);
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Create tabpages
			AddView(RUNTIME_CLASS(CMyReportPage8),xml->str(IDS_STRING7582),2);	// F�renklad kvarvarande
			AddView(RUNTIME_CLASS(CMyReportPage9),xml->str(IDS_STRING7519),2);	// F�renklad stickv�g
		}
		delete xml;
	}
	return TRUE;
}

void CMDIScaGBTrakt2FormView::OnCbnEditchangeCombo63()
{
	// TODO: Add your control notification handler code here
	CString csTest=_T("");
	int nIdx = m_wndCBoxDistrikt.GetCurSel(),value=-1;

	if (nIdx != CB_ERR)
	{
		return;
	}	
	else
	{
		m_wndCBoxDistrikt.GetWindowText(csTest);
		value=_tstoi(csTest.GetBuffer(0));
		if(!isInt(csTest.GetBuffer(0)))
		{
			AfxMessageBox(_T("Distrikt bara numeriska tecken!"));
			csTest=_T("");
			m_wndCBoxDistrikt.SetWindowText(csTest);
		}
		else
		{		
			if(value>0 && value<99)
				return;
			else
			{
				AfxMessageBox(_T("Distrikt bara 2 tecken!"));
				csTest.Delete(2,csTest.GetLength()-2);
				m_wndCBoxDistrikt.SetWindowText(csTest);
			}
		}
		return;
	}
}

void CMDIScaGBTrakt2FormView::OnCbnEditchangeCombo61()
{
	// TODO: Add your control notification handler code here
	int nIdx = m_wndCBoxForvalt.GetCurSel(),value=-1;
	CString csTest=_T("");
	if (nIdx != CB_ERR)
	{
		return;
	}
	else
	{
		m_wndCBoxForvalt.GetWindowText(csTest);
		value=_tstoi(csTest.GetBuffer(0));
		if(!isInt(csTest.GetBuffer(0)))
		{
			AfxMessageBox(_T("F�rvaltning bara numeriska tecken!"));
			csTest=_T("");
			m_wndCBoxForvalt.SetWindowText(csTest);
		}
		else
		{		
			if(value>0 && value<999)
				return;
			else
			{
				AfxMessageBox(_T("F�rvaltning bara 3 tecken!"));
				csTest.Delete(3,csTest.GetLength()-3);
				m_wndCBoxForvalt.SetWindowText(csTest);
			}
		}
	}
}
