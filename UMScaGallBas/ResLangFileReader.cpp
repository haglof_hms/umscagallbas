#include "StdAfx.h"
#include "ResLangFileReader.h"

inline void CHECK( HRESULT _hr ) 
{ 
	if FAILED(_hr) throw(_hr); 
}


RLFReader::RLFReader(void)
{
	CHECK(CoInitialize(NULL));
	m_sModulePath = _T("");
	m_pResources = new CXTPPropExchangeXMLNode(TRUE, 0, _T("resource"));
}

RLFReader::~RLFReader()
{
	CoUninitialize();
	delete m_pResources;
	m_sModulePath = _T("");
}

BOOL RLFReader::Load(LPCTSTR xml_fn)
{
	return m_pResources->LoadFromFile(xml_fn);
}

CString RLFReader::str(DWORD resid)
{
	CXTPPropExchangeEnumeratorPtr ptrEnumeratorStrings(m_pResources->GetEnumerator(_T("string")));
	POSITION pos = ptrEnumeratorStrings->GetPosition();

	while (pos)
	{
		CXTPPropExchangeXMLNode* ptrSectionString = (CXTPPropExchangeXMLNode*)ptrEnumeratorStrings->GetNext(pos);
		CString strValue;
		DWORD dwId = 0;

		PX_DWord(ptrSectionString, _T("id"), dwId);
		PX_String(ptrSectionString, _T("value"), strValue);
		CXTPPropExchangeXMLNode::PreformatString(strValue, FALSE);
		if (dwId == resid)
		{
			return strValue;
		}
	}
	return _T(" ");
}
