// MDIScaGBTraktFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIScaGBTraktFormView.h"

#include "ResLangFileReader.h"


// CMDIScaGBTraktFormView dialog

IMPLEMENT_DYNAMIC(CMDIScaGBTraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIScaGBTraktFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIScaGBTraktFormView::CMDIScaGBTraktFormView()
	: CXTResizeFormView(CMDIScaGBTraktFormView::IDD)
{
}

CMDIScaGBTraktFormView::~CMDIScaGBTraktFormView()
{
}

BOOL CMDIScaGBTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIScaGBTraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_STATIC1, m_wndLbl1);
	DDX_Control(pDX, IDC_STATIC2, m_wndLbl2);
	DDX_Control(pDX, IDC_STATIC3, m_wndLbl3);
	DDX_Control(pDX, IDC_STATIC4, m_wndLbl4);
	DDX_Control(pDX, IDC_STATIC5, m_wndLbl5);
	DDX_Control(pDX, IDC_STATIC6, m_wndLbl6);
	DDX_Control(pDX, IDC_STATIC7, m_wndLbl7);
	DDX_Control(pDX, IDC_STATIC8, m_wndLbl8);
	DDX_Control(pDX, IDC_STATIC9, m_wndLbl9);
	DDX_Control(pDX, IDC_STATIC10, m_wndLbl10);
	DDX_Control(pDX, IDC_STATIC11, m_wndLbl11);
	DDX_Control(pDX, IDC_STATIC12, m_wndLbl12);
	DDX_Control(pDX, IDC_STATIC13, m_wndLbl13);
	DDX_Control(pDX, IDC_STATIC14, m_wndLbl14);
	DDX_Control(pDX, IDC_STATIC15, m_wndLbl15);
	DDX_Control(pDX, IDC_STATIC16, m_wndLbl16);
	DDX_Control(pDX, IDC_STATIC17, m_wndLbl17);
	DDX_Control(pDX, IDC_STATIC18, m_wndLbl18);
	DDX_Control(pDX, IDC_STATIC19, m_wndLbl19);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);	
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit11);
	DDX_Control(pDX, IDC_EDIT12, m_wndEdit12);
	DDX_Control(pDX, IDC_EDIT13, m_wndEdit13);
	DDX_Control(pDX, IDC_EDIT14, m_wndEdit14);
	DDX_Control(pDX, IDC_EDIT15, m_wndEdit15);
	DDX_Control(pDX, IDC_EDIT16, m_wndEdit16);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit17);
	

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);
}

#ifdef _DEBUG
void CMDIGallBasScaGBTraktFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasScaGBTraktFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CMDIScaGBTraktFormView message handlers

void CMDIScaGBTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndCBox1.SetEnabledColor(BLACK,WHITE );
	m_wndCBox1.SetDisabledColor(BLACK,INFOBK );
	
	m_wndCBox2.SetEnabledColor(BLACK,WHITE );
	m_wndCBox2.SetDisabledColor(BLACK,INFOBK );

	setAllReadOnly(TRUE,TRUE);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getForvaltsFromDB();
	addForvaltsToCBox();

	getDistrictFromDB();
	addDistrictToCBox();


	// Setup language filename; 051214 p�d
	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	setLanguage();

	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	m_bIsDirty = FALSE;
}

BOOL CMDIScaGBTraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIScaGBTraktFormView::OnSetFocus(CWnd*)
{
	if (!m_vecTraktData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}

void CMDIScaGBTraktFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Trakt information
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING7501));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING7503));
			
		}
	}
}

void CMDIScaGBTraktFormView::populateData(UINT idx)
{
	DISTRICT_DATA data;
	if (!m_vecTraktData.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktData.size())
	{
		m_recActiveTrakt = m_vecTraktData[idx];

		m_wndCBox1.SetWindowText(getForvaltSelected(m_recActiveTrakt.m_nTrakt_forvalt_num));
		m_wndCBox2.SetWindowText(getDistrictSelected(m_recActiveTrakt.m_nTrakt_distrikt_num));		

	m_wndEdit1.setInt(m_recActiveTrakt.m_nTrakt_id_typ);
	m_wndEdit2.setInt(m_recActiveTrakt.m_nTrakt_id);
	m_wndEdit3.setInt(m_recActiveTrakt.m_nTrakt_del_id);
	m_wndEdit4.SetWindowText(m_recActiveTrakt.m_sTrakt_name);
	m_wndEdit5.setInt(m_recActiveTrakt.m_nTrakt_ursprung);
	m_wndEdit6.SetWindowText(m_recActiveTrakt.m_sTrakt_inv_typ);
	m_wndEdit7.setInt(m_recActiveTrakt.m_nTrakt_prod_ledare);
	m_wndEdit8.SetWindowText(m_recActiveTrakt.m_sTrakt_alt_id);
	m_wndEdit9.SetWindowText(m_recActiveTrakt.m_sTrakt_date);
	m_wndEdit10.setInt(m_recActiveTrakt.m_nTrakt_maskinlag);
	m_wndEdit11.setFloat(m_recActiveTrakt.m_fTrakt_areal,1);
	m_wndEdit12.setInt(m_recActiveTrakt.m_nTrakt_ant_ytor);
	m_wndEdit13.setFloat(m_recActiveTrakt.m_fTrakt_medel_ovre_hojd,1);
	m_wndEdit14.setInt(m_recActiveTrakt.m_nTrakt_si);
	m_wndEdit15.setInt(m_recActiveTrakt.m_nTrakt_alder);
	m_wndEdit16.setFloat(m_recActiveTrakt.m_fTrakt_gy_dir_fore,1);
	m_wndEdit17.setFloat(m_recActiveTrakt.m_fTrakt_gy_dir_efter,1);
	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setAllReadOnly( TRUE, TRUE );
		clearAll();
	}

}



// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIScaGBTraktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			m_enumAction = MANUALLY;
			addTraktManually();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			m_enumAction = FROM_FILE;
			addTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteTrakt();
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{

				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecTraktData.size() - 1))
					m_nDBIndex = (UINT)m_vecTraktData.size() - 1;
					
				if (m_nDBIndex == (UINT)m_vecTraktData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex = (UINT)m_vecTraktData.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

void CMDIScaGBTraktFormView::getTraktsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTraktFormView::getForvaltsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getForvalts(m_vecForvaltData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTraktFormView::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}


void CMDIScaGBTraktFormView::addTraktManually(void)
{
	setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d

	clearAll();
	m_wndListMachineBtn.EnableWindow( TRUE );
	m_bIsDirty = TRUE;
}

void CMDIScaGBTraktFormView::addTrakt(void)
{
/*	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);*/
}

void CMDIScaGBTraktFormView::deleteTrakt(void)
{
	TRAKT_DATA dataTrakt;
	CString sMsg;
	CString sText1;	//	Ta bort trakt
	CString sText2;	// OBS! Om trakt tas bort, kommer all underliggande information att tas bort
	CString sText3;	// Dvs. att alla Ytor raderas
	CString sText4;	// Skall denna trakt tas bort ... ?

	CString sT1,sT2,sT3,sT4,sT5,sT6,sT7,sT8,sT9,sT10,sT11,sT12,sT13,sT14;
	
	CString sCaption;	// Meddelande
	CString sOKBtn;	// Ta bort
	CString sCancelBtn;// Avbryt

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING7570);
			sText2 = xml->str(IDS_STRING7571);
			sText3 = xml->str(IDS_STRING7572);
			sText4 = xml->str(IDS_STRING7573);

			sT1	= xml->str(IDS_STRING7550);
			sT2	= xml->str(IDS_STRING7551);
			sT3	= xml->str(IDS_STRING7552);
			sT4	= xml->str(IDS_STRING7553);
			sT5	= xml->str(IDS_STRING7554);
			sT6	= xml->str(IDS_STRING7555);
			sT7	= xml->str(IDS_STRING7556);
			sT8	= xml->str(IDS_STRING7557);
			sT9	= xml->str(IDS_STRING7558);
			sT10	= xml->str(IDS_STRING7559);
			sT11	= xml->str(IDS_STRING7560);
			sT12	= xml->str(IDS_STRING7561);
			sT13	= xml->str(IDS_STRING7562);
			sT14	= xml->str(IDS_STRING7563);
			
			sCaption = xml->str(IDS_STRING7574);
			sOKBtn = xml->str(IDS_STRING7575);
			sCancelBtn = xml->str(IDS_STRING7576);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					// Get Regions in database; 061002 p�d	
					dataTrakt = m_vecTraktData[m_nDBIndex];
					// Setup a message for user upon deleting machine; 061010 p�d
					sMsg.Format("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b><br>%s : <b>%d</b><b>%d</b><br>%s : <b>%d</b><br>%s : <b>%s</b><br>%s : <b>%d</b><br>%s : <b>%s</b><br>%s : <b>%d</b><br>%s : <b>%d</b><br>%s : <b>%s</b><br>%s : <b>%s</b><br>%s : <b>%d</b><br>%s : <b>%10.3f</b><br>%s : <b>%d</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>",
						sText1,
						sT1,dataTrakt.m_nTrakt_forvalt_num,
						sT2,dataTrakt.m_nTrakt_id_typ,
						sT3,dataTrakt.m_nTrakt_id,
						sT4,dataTrakt.m_nTrakt_del_id,
						sT5,dataTrakt.m_sTrakt_name,
						sT6,dataTrakt.m_nTrakt_ursprung,
						sT7,dataTrakt.m_sTrakt_inv_typ,
						sT8,dataTrakt.m_nTrakt_distrikt_num,
						sT9,dataTrakt.m_nTrakt_prod_ledare,
						sT10,dataTrakt.m_sTrakt_alt_id,
						sT11,dataTrakt.m_sTrakt_date,
						sT12,dataTrakt.m_nTrakt_maskinlag,
						sT13,dataTrakt.m_fTrakt_areal,
						sT14,dataTrakt.m_nTrakt_ant_ytor,
						sText2,
						sText3,
						sText4);

					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delTrakt(dataTrakt);
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	resetTrakt(RESET_TO_LAST_SET_NB);
}

// PROTECTED
void CMDIScaGBTraktFormView::setDefaultTypeAndOrigin(void)
{
	/*
	CString sTmp;
	for (int i = 0;i < sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]);i++)
	{
		m_wndCBox1.AddString(INVENTORY_TYPE_ARRAY[i].sInvTypeName);
	}

	for (int i = 0;i < sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0]);i++)
	{
		m_wndCBox2.AddString(ORIGIN_ARRAY[i].sOrigin);
	}*/
}


// Get manually entered data; 061011 p�d
void CMDIScaGBTraktFormView::getEnteredData(void)
{
	
	// Identification information
	m_recNewTrakt.m_nTrakt_forvalt_num	= getForvaltID();
	m_recNewTrakt.m_nTrakt_distrikt_num	= getDistrictID();
	
	m_recNewTrakt.m_nTrakt_id_typ				= m_wndEdit1.getInt();
	m_recNewTrakt.m_nTrakt_id					= m_wndEdit2.getInt();
	m_recNewTrakt.m_nTrakt_del_id				= m_wndEdit3.getInt();
	m_recNewTrakt.m_sTrakt_name				= m_wndEdit4.getText();
	m_recNewTrakt.m_nTrakt_ursprung			= m_wndEdit5.getInt();
	m_recNewTrakt.m_sTrakt_inv_typ			= m_wndEdit6.getText();
	m_recNewTrakt.m_nTrakt_prod_ledare		= m_wndEdit7.getInt();
	m_recNewTrakt.m_sTrakt_alt_id				= m_wndEdit8.getText();
	m_recNewTrakt.m_sTrakt_date				= m_wndEdit9.getText();
	m_recNewTrakt.m_nTrakt_maskinlag			= m_wndEdit10.getInt();
	m_recNewTrakt.m_fTrakt_areal				= m_wndEdit11.getFloat();
	m_recNewTrakt.m_nTrakt_ant_ytor			= m_wndEdit12.getInt();
	m_recNewTrakt.m_fTrakt_medel_ovre_hojd	= m_wndEdit13.getFloat();
	m_recNewTrakt.m_nTrakt_si					= m_wndEdit14.getInt();
	m_recNewTrakt.m_nTrakt_alder				= m_wndEdit15.getInt();
	m_recNewTrakt.m_fTrakt_gy_dir_fore		= m_wndEdit16.getFloat();
	m_recNewTrakt.m_fTrakt_gy_dir_efter		= m_wndEdit17.getFloat();
	

	// Trakt information
	//CString sDate;
	//m_wndDatePicker.GetWindowText(sDate);
	//m_recNewTrakt.m_sTraktDate		= sDate;
	// Trakt data
	/*
	m_recNewTrakt.m_recData = CVariables(m_wndEdit7.getFloat(),
																				0.0,	// Road dist 2 not used
																				m_wndEdit8.getFloat(),
																				0.0,	// Road width 2 not used
																				m_wndEdit9.getFloat(),
																				m_wndEdit10.getFloat(),
																				m_wndEdit11.getFloat(),
																				m_wndEdit12.getFloat(),
																				m_wndEdit13.getFloat(),
																				m_wndEdit23.getFloat(),
																				m_wndEdit24.getFloat(),
																				0.0,
																				m_wndEdit14.getFloat(),
																				m_wndEdit15.getFloat(),
																				m_wndEdit16.getFloat(),
																				m_wndEdit17.getFloat(),
																				m_wndEdit18.getFloat(),
																				m_wndEdit19.getFloat(),
																				m_wndEdit20.getFloat(),
																				m_wndEdit21.getFloat(),
																				m_wndEdit22.getFloat(),
																				m_wndEdit26.getFloat(),
																				m_wndEdit27.getFloat(),
																				m_wndEdit28.getFloat(),
																				m_wndEdit29.getFloat(),
																				m_wndEdit30.getFloat());*/


	// Check RegionID and DistrictID. If they eq. -1 then
	// Set to m_recActiveTrakt values; 061011 p�d
	/*
	if (m_recNewMachine.m_nRegionID == -1 && m_recNewMachine.m_nDistrictID == -1)
	{
		m_recNewMachine.m_nRegionID = m_recActiveTrakt.m_nRegionID;
		m_recNewMachine.m_nDistrictID = m_recActiveTrakt.m_nDistrictID;
	}*/

	// Setup who data's entered, Maually or by file; 061201 p�d
	if (m_enumAction == MANUALLY)
		m_recNewTrakt.m_nTrakt_enter_type = 1;
		
}

// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/District is entered
// using a Dialog; 061011 p�d
void CMDIScaGBTraktFormView::clearAll(void)
{
	m_wndEdit1.SetWindowText("");
	m_wndEdit2.SetWindowText("");
	m_wndEdit3.SetWindowText("");
	m_wndEdit4.SetWindowText("");
	m_wndEdit5.SetWindowText("");
	m_wndEdit6.SetWindowText("");
	m_wndEdit7.SetWindowText("");
	m_wndEdit8.SetWindowText("");
	m_wndEdit9.SetWindowText("");
	m_wndEdit10.SetWindowText("");
	m_wndEdit11.SetWindowText("");
	m_wndEdit12.SetWindowText("");
	m_wndEdit13.SetWindowText("");
	m_wndEdit14.SetWindowText("");
	m_wndEdit15.SetWindowText("");
	m_wndEdit16.SetWindowText("");
	m_wndEdit17.SetWindowText("");
	
	m_wndCBox1.SetCurSel(-1);
	m_wndCBox2.SetCurSel(-1);
	
	//m_wndDatePicker.SetWindowText("");

	// Set m_recNewTrakt key-values to -1
	SetKeyValuesToNegative(m_recNewTrakt);
}

void CMDIScaGBTraktFormView::SetKeyValuesToNegative(TRAKT_DATA rec)
{
	rec.m_nTrakt_id				= -1;
	rec.m_nTrakt_del_id			= -1;
	rec.m_nTrakt_forvalt_num	= -1;
	rec.m_sTrakt_inv_typ			= "";	
}

// Set all Editboxes to read or read only, depending on
// if data comes from database and holds Compartmant(s) and Plot(s); 061010 p�d
void CMDIScaGBTraktFormView::setAllReadOnly(BOOL ro1,BOOL ro2)
{
	m_wndListMachineBtn.EnableWindow( FALSE );

	m_wndEdit1.SetReadOnly( ro1 );
	m_wndEdit2.SetReadOnly( ro1 );
	m_wndEdit3.SetReadOnly( ro1 );
	m_wndEdit4.SetReadOnly( ro1 );
	m_wndEdit5.SetReadOnly( ro1 );
	m_wndEdit6.SetReadOnly( ro1 );
	m_wndEdit7.SetReadOnly( ro1 );
	m_wndEdit8.SetReadOnly( ro1 );
	m_wndEdit9.SetReadOnly( ro1 );
	m_wndEdit10.SetReadOnly( ro1 );
	m_wndEdit11.SetReadOnly( ro1 );
	m_wndEdit12.SetReadOnly( ro1 );
	m_wndEdit13.SetReadOnly( ro1 );
	m_wndEdit14.SetReadOnly( ro1 );
	m_wndEdit15.SetReadOnly( ro1 );
	m_wndEdit16.SetReadOnly( ro1 );
	m_wndEdit17.SetReadOnly( ro1 );
	
//	Commented out 2007-04-19 on request from Holmen
//	this field'll always be editable; 070419 p�d
//	m_wndEdit32.SetReadOnly( ro1 );

	m_wndCBox1.SetReadOnly( ro2 );
	m_wndCBox2.SetReadOnly( ro2 );
}


// Add regions in registry, into ComboBox3; 061204 p�d
void CMDIScaGBTraktFormView::addForvaltsToCBox(void)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		m_wndCBox1.ResetContent();
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			sText.Format("%d - %s",data.m_nForvaltID,data.m_sForvaltName);
			m_wndCBox1.AddString(sText);
			m_wndCBox1.SetItemData(i, data.m_nForvaltID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

// Try to find region, based on index; 061204 p�d
CString CMDIScaGBTraktFormView::getForvaltSelected(int forvalt_num)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			if (data.m_nForvaltID == forvalt_num)
			{
				sText.Format("%d - %s",data.m_nForvaltID,data.m_sForvaltName);
				return sText;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	return "";
}

// Try to find index of forvaltnumber in m_vecForvaltData;
int CMDIScaGBTraktFormView::getForvaltID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Forvaltn is used on New item; 061205 p�d
	if (m_vecTraktData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktData.size() && m_enumAction == NOTHING)
	{
		return m_vecTraktData[m_nDBIndex].m_nTrakt_forvalt_num;
	}
	else
	{
		int nIdx = m_wndCBox1.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBox1.GetItemData(nIdx);
		}
	}
	return -1;
}

// Add Districts into Combobox4, based on selected region; 061204 p�d
void CMDIScaGBTraktFormView::addDistrictToCBox(void)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		m_wndCBox2.ResetContent();
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			sText.Format("%d - %s",data.m_nDistrictID,data.m_sDistrictName);
			m_wndCBox2.AddString(sText);
			m_wndCBox2.SetItemData(i, data.m_nDistrictID);
		}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	}	// if (m_vecDistrictData.size() > 0)
}

// Try to find district, based on index; 061204 p�d
CString CMDIScaGBTraktFormView::getDistrictSelected(int district_num)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nDistrictID == district_num)
			{
				sText.Format("%d - %s",data.m_nDistrictID,data.m_sDistrictName);
				return sText;
			}	// if (data.m_nDistrictID == district_num)
		}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	
		return "";
}

// Try to find index of regionnumber in m_vecDistrictData; 061204 p�d
int CMDIScaGBTraktFormView::getDistrictID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected District is used on New item; 061205 p�d
	if (m_vecTraktData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktData.size() && m_enumAction == NOTHING)
	{
		return m_vecTraktData[m_nDBIndex].m_nTrakt_distrikt_num;
	}
	else
	{
		int nIdx = m_wndCBox2.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBox2.GetItemData(nIdx);
		}
	}
	return -1;
}


// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIScaGBTraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}


// PUBLIC
void CMDIScaGBTraktFormView::saveTrakt(void)
{
	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			getEnteredData();
			// Setup index information; 061207 p�d
			
			m_structTraktIdentifer.nTrakt_del_id		= m_recNewTrakt.m_nTrakt_del_id;
			m_structTraktIdentifer.nTrakt_forvalt_num	= m_recNewTrakt.m_nTrakt_forvalt_num;
			m_structTraktIdentifer.nTrakt_id				= m_recNewTrakt.m_nTrakt_id;
			m_structTraktIdentifer.sTrakt_inv_typ		= m_recNewTrakt.m_sTrakt_inv_typ;			

			if (!pDB->addTrakt( m_recNewTrakt ))
			{
				if ((::MessageBox(0,m_sUpdateTraktMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
				{
					pDB->updTrakt( m_recNewTrakt );
				}
			}
			delete pDB;
		}	// if (pDB != NULL)

		m_bIsDirty = FALSE;
		resetIsDirty();
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
		m_enumAction = NOTHING;

	}	// if (isOKToSave())
}

// Check that information needed to be abel to save Trakt, is enterd
// by user; 061113 p�d
BOOL CMDIScaGBTraktFormView::isOKToSave(void)
{
	/*
	if (strcmp(m_wndCBox3.getText().Trim(),"") == 0 ||
		  strcmp(m_wndCBox4.getText().Trim(),"") == 0 ||
		  m_wndEdit5.getInt() <= 0 ||
		  strcmp(m_wndEdit31.getText().Trim(),"") == 0 ||
			getType_setByUser().Trim() == "" ||
		  getOrigin_setByUser() == -1) 
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}
	*/
	return TRUE;
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CMDIScaGBTraktFormView::isDataChanged(void)
{
	if (getIsDirty())
	{
		if (::MessageBox(0,m_sSaveData,m_sErrCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		{
			saveTrakt();
		}
		resetIsDirty();
		return TRUE;
	}
	return FALSE;
}

void CMDIScaGBTraktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
											 m_nDBIndex < (m_vecTraktData.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIScaGBTraktFormView::resetTrakt(enumRESET reset)
{
	// Get updated data from Database
	getTraktsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{

		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktData.size();i++)
			{
				TRAKT_DATA data = m_vecTraktData[i];
				if (data.m_nTrakt_id == m_structTraktIdentifer.nTrakt_id &&
					data.m_nTrakt_del_id == m_structTraktIdentifer.nTrakt_del_id &&
					data.m_sTrakt_inv_typ == m_structTraktIdentifer.sTrakt_inv_typ &&
					data.m_nTrakt_forvalt_num == m_structTraktIdentifer.nTrakt_forvalt_num)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons(m_nDBIndex > 0,
					   								m_nDBIndex < (m_vecTraktData.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}


BOOL CMDIScaGBTraktFormView::getIsDirty(void)
{
	if (m_bIsDirty)
		return m_bIsDirty;

	if (m_wndEdit1.isDirty() || m_wndEdit2.isDirty() || m_wndEdit3.isDirty() ||
		 m_wndEdit4.isDirty() || m_wndEdit5.isDirty() || m_wndEdit6.isDirty() ||
		 m_wndEdit7.isDirty() || m_wndEdit8.isDirty() || m_wndEdit9.isDirty() ||
		 m_wndEdit10.isDirty() || m_wndEdit11.isDirty() || m_wndEdit12.isDirty() ||
		 m_wndEdit13.isDirty() || m_wndEdit14.isDirty() || m_wndEdit15.isDirty() ||
		 m_wndEdit16.isDirty() || m_wndEdit17.isDirty() || m_wndCBox1.isDirty() ||
		 m_wndCBox2.isDirty()
		) 
	{
		return TRUE;
	}

	return FALSE;
}

void CMDIScaGBTraktFormView::resetIsDirty(void)
{
	m_wndEdit1.resetIsDirty();
	m_wndEdit2.resetIsDirty();
	m_wndEdit3.resetIsDirty();
	m_wndEdit4.resetIsDirty();
	m_wndEdit5.resetIsDirty();
	m_wndEdit6.resetIsDirty();
	m_wndEdit7.resetIsDirty();
	m_wndEdit8.resetIsDirty();
	m_wndEdit9.resetIsDirty();
	m_wndEdit10.resetIsDirty();
	m_wndEdit11.resetIsDirty();
	m_wndEdit12.resetIsDirty();
	m_wndEdit13.resetIsDirty();
	m_wndEdit14.resetIsDirty();
	m_wndEdit15.resetIsDirty();
	m_wndEdit16.resetIsDirty();
	m_wndEdit17.resetIsDirty();
	
	m_wndCBox1.resetIsDirty();
	m_wndCBox2.resetIsDirty();	
}