#pragma once

#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"

// CMDIScaGBTraktFormView dialog

class CMDIScaGBTraktFormView : public CXTResizeFormView
{
	DECLARE_DYNAMIC(CMDIScaGBTraktFormView)

	CXTPTabControl m_wndTabControl;

protected:		
	CMDIScaGBTraktFormView();   // standard constructor
	virtual ~CMDIScaGBTraktFormView();

	CMyExtStatic		m_wndLbl1;
	CMyExtStatic		m_wndLbl2;
	CMyExtStatic		m_wndLbl3;
	CMyExtStatic		m_wndLbl4;
	CMyExtStatic		m_wndLbl5;
	CMyExtStatic		m_wndLbl6;
	CMyExtStatic		m_wndLbl7;
	CMyExtStatic		m_wndLbl8;
	CMyExtStatic		m_wndLbl9;
	CMyExtStatic		m_wndLbl10;
	CMyExtStatic		m_wndLbl11;
	CMyExtStatic		m_wndLbl12;
	CMyExtStatic		m_wndLbl13;
	CMyExtStatic		m_wndLbl14;
	CMyExtStatic		m_wndLbl15;
	CMyExtStatic		m_wndLbl16;
	CMyExtStatic		m_wndLbl17;
	CMyExtStatic		m_wndLbl18;
	CMyExtStatic		m_wndLbl19;
	
	CMyExtEdit			m_wndEdit1;
	CMyExtEdit			m_wndEdit2;
	CMyExtEdit			m_wndEdit3;
	CMyExtEdit			m_wndEdit4;
	CMyExtEdit			m_wndEdit5;
	CMyExtEdit			m_wndEdit6;
	CMyExtEdit			m_wndEdit7;
	CMyExtEdit			m_wndEdit8;
	CMyExtEdit			m_wndEdit9;
	CMyExtEdit			m_wndEdit10;
	CMyExtEdit			m_wndEdit11;
	CMyExtEdit			m_wndEdit12;
	CMyExtEdit			m_wndEdit13;
	CMyExtEdit			m_wndEdit14;
	CMyExtEdit			m_wndEdit15;
	CMyExtEdit			m_wndEdit16;
	CMyExtEdit			m_wndEdit17;
	
	CMyComboBox			m_wndCBox1;
	CMyComboBox			m_wndCBox2;

	//CDatePickerCombo	m_wndDatePicker;

	CXTButton			m_wndListMachineBtn;

	vecTraktData		m_vecTraktData;
	vecForvaltData		m_vecForvaltData;
	vecDistrictData	m_vecDistrictData;
	
	TRAKT_DATA			m_recNewTrakt;
	TRAKT_DATA			m_recActiveTrakt;
	TRAKT_IDENTIFER	m_structTraktIdentifer;
	
	enumACTION			m_enumAction;

	UINT					m_nDBIndex;

	

	CString				m_sLangFN;
	BOOL					m_bIsDirty;
	CString				m_sErrCap;
	CString				m_sErrMsg;
	CString				m_sUpdateTraktMsg;
	CString				m_sNotOkToSave;
	CString				m_sSaveData;
	BOOL					m_bConnected;
	DB_CONNECTION_DATA	m_dbConnectionData;

	void		SetKeyValuesToNegative(TRAKT_DATA rec);

	void		setLanguage(void);
	void		getForvaltsFromDB(void);
	void		getDistrictFromDB(void);
	void		getTraktsFromDB(void);
	void		populateData(UINT);
	void		setNavigationButtons(BOOL,BOOL);
	void		addTraktManually(void);
	void		deleteTrakt(void);
	void		addTrakt(void);
	void		setDefaultTypeAndOrigin(void);
	void		setAllReadOnly(BOOL ro1,BOOL ro2);
	void		clearAll(void);
	void		getEnteredData(void);
	CString	getType_setByUser(void);
	CString	getTypeName_setByUser(void);
	int		getOrigin_setByUser(void);
	CString	getOriginName_setByUser(void);
	void		addForvaltsToCBox(void);
	CString	getForvaltSelected(int region_num);
	int		getForvaltID(void);
	void		addDistrictToCBox(void);
	CString	getDistrictSelected(int district_num);
	int		getDistrictID(void);

public:
// Dialog Data
	enum { IDD = IDD_FORMVIEW2 };

public:
	virtual void OnInitialUpdate();

	BOOL getIsDirty(void);
	void resetIsDirty(void);	// Set ALL edit boxes etc. to NOT DIRTY; 061113 p�d
	// Needs to be Public, to be visible in CMDIGallBasTraktFrame; 061017 p�d
	void saveTrakt(void);
	// Check if it's ok to save trakt; 061113 p�d
	BOOL isOKToSave(void);
	// Check if active data's chnged by user.
	// I.e. manually enterd data; 061113 p�d
	BOOL isDataChanged(void);

	void doPopulate(UINT);
	void resetTrakt(enumRESET reset);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	DECLARE_MESSAGE_MAP()
};
