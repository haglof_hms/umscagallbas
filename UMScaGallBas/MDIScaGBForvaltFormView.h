#pragma once
#include "SQLAPI.h" // main SQLAPI++ header

#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"


// CMDIScaGBForvaltFormView form view

class CMDIScaGBForvaltFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIScaGBForvaltFormView)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CMDIScaGBForvaltFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIScaGBForvaltFormView();

	CMyExtEdit2 m_wndEdit1;
	CMyExtEdit2 m_wndEdit2;

	CMyExtStatic2 m_wndLbl1;
	CMyExtStatic2 m_wndLbl2;

	CXTResizeGroupBox m_wndGroup;

	vecForvaltData m_vecForvaltData;
	FORVALT_DATA m_structForvaltIdentifer;
	UINT m_nDBIndex;

	void setLanguage(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);
	void addForvalt();
	void saveForvalt();
	void deleteForvalt();

	void getForvaltsFromDB();

	CString m_sSaveData;
	CString m_sErrCap;
	CString m_sNotOkToSave;
	CString m_sUpdateForvaltMsg;
	BOOL isDataChanged(void);
	BOOL isOKToSave(void);
	void getEnteredData(FORVALT_DATA&);
	void resetForvalt(enumRESET reset);

	BOOL m_bIsDirty;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void doPopulate(UINT);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIScaGBForvaltFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
