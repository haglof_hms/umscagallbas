#pragma once

#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"
#include "ScaGBTraktPages.h"

#include "DatePickerCombo.h"

// CMDIScaGBTraktFormView dialog

class CMDIScaGBTrakt2FormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIScaGBTrakt2FormView)

	

protected:		
	CMDIScaGBTrakt2FormView();   // standard constructor
	virtual ~CMDIScaGBTrakt2FormView();

	CMyTabControl2		m_wndTabControl;

	CMyExtStatic2 m_wndLbl_Forvalt;
	CMyExtStatic2 m_wndLbl_DrivEnh;
	CMyExtStatic2 m_wndLbl_AvvId;
	CMyExtStatic2 m_wndLbl_InvTyp;
	CMyExtStatic2 m_wndLbl_Avlagg;
	CMyExtStatic2 m_wndLbl_Distrikt;
	CMyExtStatic2 m_wndLbl_Maskinlag;
	CMyExtStatic2 m_wndLbl_Ursrpung;
	CMyExtStatic2 m_wndLbl_ProdLedare;
	CMyExtStatic2 m_wndLbl_Namn;
	CMyExtStatic2 m_wndLbl_Datum;
	CMyExtStatic2 m_wndLbl_Areal;
	CMyExtStatic2 m_wndLbl_AntYtor;
	CMyExtStatic2 m_wndLbl_SiAlder;
	CMyExtStatic2 m_wndLbl_MedOhjd;
	CMyExtStatic2 m_wndLbl_SiSp;
	CMyExtStatic2 m_wndLbl_GyDirFore;
	CMyExtStatic2 m_wndLbl_GyDirEfter;
	
	
	CMyComboBox2 m_wndCBoxForvalt;
	CMyComboBox2 m_wndCBoxInvType;
	CMyComboBox2 m_wndCBoxDistrikt;
	CMyComboBox2 m_wndCBoxSiSp;

	CMyExtEdit2 m_wndEditDrivEnh;
	CMyExtEdit2 m_wndEditAvvId;
	CMyExtEdit2 m_wndEditAvlagg;
	CMyExtEdit2 m_wndEditMaskinlag;
	CMyExtEdit2 m_wndEditUrsprung;
	CMyExtEdit2 m_wndEditProdLedare;
	CMyExtEdit2 m_wndEditNamn;
	//CMyExtEdit2 m_wndEditDatum;
	CMyExtEdit2 m_wndEditAreal;
	CMyExtEdit2 m_wndEditAntYtor;
	CMyExtEdit2 m_wndEditMedOhjd;
	CMyExtEdit2 m_wndEditSiTot;
	CMyExtEdit2 m_wndEditSiAlder;
	CMyExtEdit2 m_wndEditGyDirFore;
	CMyExtEdit2 m_wndEditGyDirEfter;
	CMyExtEdit2 m_wndEditNotes;

	CDatePickerCombo m_wndDatePicker;

	CXTResizeGroupBox m_wndGroup;
	CXTResizeGroupBox m_wndGroup2;
	CXTResizeGroupBox m_wndGroup3;

	vecForvaltData		m_vecForvaltData;
	vecDistriktData	m_vecDistriktData;
	vecInvTypeData		m_vecInvTypeData;
	vecInvTypeData		m_vecSiSpData;

	vecTraktIndex		m_vecTraktIndex;
	
	TRAKT_DATA			m_recNewTrakt;
	TRAKT_DATA			m_recActiveTrakt;
	TRAKT_IDENTIFER	m_structTraktIdentifer;
	
	enumACTION			m_enumAction;

	int					m_nDBIndex;

	CString				m_sLangFN;
	BOOL					m_bIsDirty;
	CString				m_sErrCap;
	CString				m_sErrMsg;
	CString				m_sUpdateTraktMsg;
	CString				m_sNotOkToSave;
	CString				m_sSaveData;

	BOOL					m_bConnected;
	DB_CONNECTION_DATA	m_dbConnectionData;

	void		SetKeyValuesToNegative(TRAKT_DATA rec);

	void		setLanguage(void);
	void		getForvaltsFromDB(void);
	void		getDistriktFromDB(void);
	void		getInvTypesFromFile(void);
	void		getSiSpFromFile(void);
	void		getTraktsFromDB(void);

	void		showData();
	void		showTabData(TRAKT_DATA data);
	void		populateData(UINT);
	void		setNavigationButtons(BOOL,BOOL);
	void		setNavigationButtonSave(BOOL on);
	void		setNavigationButtons2(void);
	
	void		addTraktManually(void);
	void		deleteTrakt(void);
	void		addTrakt(void);

	void		getTraktIndexFromDB(void);
	void		LoadActiveTrakt(int idx);
	
	void		setAllReadOnly(BOOL ro1,BOOL ro2);
	void		setKeysReadOnly();
	void		clearAll(void);
	
	void		getEnteredData(void);
	CString	getType_setByUser(void);
	CString	getTypeName_setByUser(void);
	int		getOrigin_setByUser(void);
	CString	getOriginName_setByUser(void);

	void		addInvTypesToCBox(void);
	CString	getInvTypeSelected(int inv_type);
	int		getInvTypeID(void);
	int		getInvTypeIndex(int inv_type);

	void		addSiSpToCBox(void);
	CString	getSiSpSelected(int inv_type);
	int		getSiSpID(void);
	int		getSiSpIndex(int inv_type);

	void		addForvaltsToCBox(void);
	CString	getForvaltSelected(int forvalt);
	int		getForvaltID(void);
	int		getForvaltIndex(int forvalt);

	void		addDistriktToCBox(void);
	CString	getDistriktSelected(int distrikt_num);
	int		getDistriktID(void);
	int		getDistriktIndex(int distrikt_num);

public:
	void printdirt(int nr);
// Dialog Data
	enum { IDD = IDD_FORMVIEW6 };

public:
	virtual void OnInitialUpdate();

	BOOL getIsDirty(void);
	BOOL getIsHeaderDirty(void);
	void resetIsDirty(void);	// Set ALL edit boxes etc. to NOT DIRTY; 061113 p�d
	// Needs to be Public, to be visible in CMDIGallBasTraktFrame; 061017 p�d
	void saveTrakt(void);
	// Check if it's ok to save trakt; 061113 p�d
	BOOL isOKToSave(void);
	// Check if active data's chnged by user.
	// I.e. manually enterd data; 061113 p�d
	BOOL isDataChanged(void);

	void doPopulate(int idx);
	void resetTrakt(enumRESET reset);
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	BOOL setupTraktTabs(void);
	void populateDataReport(UINT idx);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnClose();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnEditchangeCombo63();
	afx_msg void OnCbnEditchangeCombo61();
};
