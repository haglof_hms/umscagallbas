#if !defined(AFX_UMSCAGALLBASDB_H)
#define AFX_UMSCAGALLBASDB_H

#include "StdAfx.h"
#include "DBBaseClass_SQLApi.h"

#define MY_NULL											-1
//#include "AddDataToDataBase.h"



void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

//////////////////////////////////////////////////////////////////////////////////
// CGallBasDBDB; Handle ALL transactions for GallBas

// F�RVALTNING DATA STRUCTURES
typedef struct _forvalt_data
{
	int m_nForvaltID;
	CString m_sForvaltName;

	_forvalt_data(void)
	{
		m_nForvaltID = -1;
		m_sForvaltName = _T("");
	}

	_forvalt_data(int id,CString name)
	{
		m_nForvaltID = id;
		m_sForvaltName = name;
	}
} FORVALT_DATA;
typedef std::vector<FORVALT_DATA> vecForvaltData;

// F�RVALTNING DATA STRUCTURES
typedef struct _district_data
{
	int m_nDistriktID;
	CString m_sDistriktName;

	_district_data(void)
	{
		m_nDistriktID = -1;
		m_sDistriktName = _T("");
	}

	_district_data(int id,CString name)
	{
		m_nDistriktID = id;
		m_sDistriktName = name;
	}
} DISTRIKT_DATA;
typedef std::vector<DISTRIKT_DATA> vecDistriktData;

// Inv Type DATA STRUCTURES
typedef struct _inv_type_data
{
	int  m_nInvTypeID;
	CString m_sInvTypeName;

	_inv_type_data(void)
	{
		m_nInvTypeID = -1;
		m_sInvTypeName = _T("");
	}

	_inv_type_data(int id,CString name)
	{
		m_nInvTypeID = id;
		m_sInvTypeName = name;
	}
} INV_TYPE_DATA;
typedef std::vector<INV_TYPE_DATA> vecInvTypeData;

#define NUM_OF_KVARV					10
#define NUM_OF_FORE					10
#define NUM_OF_UTTAG					10

#define NUM_OF_KVARV_TO_SHOW		8
#define NUM_OF_FORE_TO_SHOW		8
#define NUM_OF_SP						7

#define NUM_OF_UTTAG_TO_SHOW		7
#define NUM_OF_FORENKLAD_TO_SHOW	3



#define PAGE_NR_FORE					0
#define PAGE_NR_KVARV				1
#define PAGE_NR_UTMSTV				2
#define PAGE_NR_UTISTV				3
#define PAGE_NR_UTTOT				4
#define PAGE_NR_FORENKLAD			10


#define UTTAG_VARINDEX_M3SK		0
#define UTTAG_VARINDEX_M3FUB		1
#define UTTAG_VARINDEX_M3STAMSK	2
#define UTTAG_VARINDEX_M3STAMFUB	3
#define UTTAG_VARINDEX_GY			4
#define UTTAG_VARINDEX_GYANDEL	5
#define UTTAG_VARINDEX_ANTAL		6
#define UTTAG_VARINDEX_ANTALANDEL 7
#define UTTAG_VARINDEX_DG			8
#define UTTAG_VARINDEX_DGV			9


#define KVARV_VARINDEX_M3SK		0
#define KVARV_VARINDEX_M3FUB		1
#define KVARV_VARINDEX_M3STAMSK	2
#define KVARV_VARINDEX_M3STAMFUB	3
#define KVARV_VARINDEX_GY			4
#define KVARV_VARINDEX_ANTAL		5
#define KVARV_VARINDEX_DG			6
#define KVARV_VARINDEX_HGV			7
#define KVARV_VARINDEX_H25			8
#define KVARV_VARINDEX_DGV			9

#define FORE_VARINDEX_M3SK		   0
#define FORE_VARINDEX_M3FUB		1
#define FORE_VARINDEX_M3STAMSK	2
#define FORE_VARINDEX_M3STAMFUB	3
#define FORE_VARINDEX_GY			4
#define FORE_VARINDEX_ANTAL		5
#define FORE_VARINDEX_DG			6
#define FORE_VARINDEX_HGV			7
#define FORE_VARINDEX_H25			8
#define FORE_VARINDEX_DGV			9				


#define NUM_OF_YTVAR_TOSHOW		30
#define NUM_OF_SKAD_COLS			6
#define NUM_OF_STICKVAG_COLS		2

#define NUM_OF_TREE_VAR				7

typedef struct _trakt_variables_struct
{
	double m_recFore[NUM_OF_FORE][NUM_OF_SP+1];
	double m_recKvarvarande[NUM_OF_KVARV][NUM_OF_SP+1];
	double m_recUttagMstv[NUM_OF_UTTAG][NUM_OF_SP+1];
	double m_recUttagIstv[NUM_OF_UTTAG][NUM_OF_SP+1];
	double m_recUttagTot[NUM_OF_UTTAG][NUM_OF_SP+1];
	double m_recGaKvot[NUM_OF_SP+1];

	double m_Skadade_Trad,m_Skadade_Stam,m_Skadade_Rot,m_Rota;
	double m_Zon1_Stam,m_Zon2_Stam,m_Zon1_Rot,m_Zon2_Rot,m_Zon1_Rot_Stam,m_Zon2_Rot_Stam;
	double m_Sparstracka,m_Spardjup,m_Spardjup_Andel;
	double m_Stv_Avst,m_Stv_Bredd,m_Stv_Areal;
	double m_Spar_over_20mha;
} TRAKT_VARIABLES;


class CTraktVariables
{
	int var,spec;
public:
	TRAKT_VARIABLES m_recVars;
	CTraktVariables(void)
	{
		for(spec=0;spec<NUM_OF_SP+1;spec++)
		{
			for(var=0;var<NUM_OF_KVARV;var++)
				m_recVars.m_recKvarvarande[var][spec]=0.0;		
			for(var=0;var<NUM_OF_FORE;var++)
				m_recVars.m_recFore[var][spec]=0.0;		
			for(var=0;var<NUM_OF_UTTAG;var++)
			{
				m_recVars.m_recUttagMstv[var][spec]=0.0;		
				m_recVars.m_recUttagIstv[var][spec]=0.0;		
				m_recVars.m_recUttagTot[var][spec]=0.0;		
			}
			m_recVars.m_recGaKvot[spec]=0.0;
		}		
	m_recVars.m_Skadade_Trad	=0.0;
	m_recVars.m_Skadade_Stam	=0.0;
	m_recVars.m_Skadade_Rot		=0.0;
	m_recVars.m_Rota				=0.0;

	m_recVars.m_Zon1_Stam		=0.0;
	m_recVars.m_Zon2_Stam		=0.0;

	m_recVars.m_Zon1_Rot			=0.0;
	m_recVars.m_Zon2_Rot			=0.0;

	m_recVars.m_Zon1_Rot_Stam	=0.0;
	m_recVars.m_Zon2_Rot_Stam	=0.0;

	m_recVars.m_Sparstracka		=0.0;
	m_recVars.m_Spardjup			=0.0;
	m_recVars.m_Spardjup_Andel	=0.0;

	m_recVars.m_Stv_Avst=0.0;
	m_recVars.m_Stv_Bredd=0.0;
	m_recVars.m_Stv_Areal=0.0;
	m_recVars.m_Spar_over_20mha=0.0;
	}
	
	CTraktVariables(TRAKT_VARIABLES recVars)
	{
	m_recVars=recVars;
	}

	CTraktVariables(const CTraktVariables &c)
	{
		*this = c;
	}

};

typedef struct _plot_data
{
 	int m_nPlot_id;
	double m_fPlot_stam_gy;
	double m_fPlot_stam_antal;
	double m_fPlot_stubb_gy;
	double m_fPlot_stubb_antal;

	double m_fPlot_skad_stam;
	double m_fPlot_skad_rot;
	double m_fPlot_skad_stam_rot;
	
	double m_fPlot_skadandel_tot;
	double m_fPlot_skadandel_stam;
	double m_fPlot_skadandel_rot;
	double m_fPlot_skadandel_rota;
	int	m_nPlot_X;
	int	m_nPlot_Y;
	//double m_fPlot_lat;
	//double m_fPlot_lon;			
	int m_nPlot_trakt_forvalt;
	CString m_sPlot_trakt_avv_id;
	int m_nPlot_trakt_inv_typ;
	int m_nPlot_trakt_forenklad;

	int m_nPlot_antal;
	int m_nPlot_areal;
	int m_nPlot_brosthojdsalder;
	int m_nPlot_si;
	int m_nPlot_si_tradslag;
	int m_nPlot_skad_rota;
	int m_nPlot_spar_langd1;
	int m_nPlot_spar_langd2;
	int m_nPlot_spar_matstracka;
	int m_nPlot_stv_avst1;
	int m_nPlot_stv_avst2;
	int m_nPlot_stv_bredd1;
	int m_nPlot_stv_bredd2;
	int m_nPlot_ohjd;

	int m_nPlot_skad_stam1;
	int m_nPlot_skad_stam2;
	int m_nPlot_skad_rot1;
	int m_nPlot_skad_rot2;
	int m_nPlot_skad_stam_rot1;
	int m_nPlot_skad_stam_rot2;

	_plot_data(void)
	{
			
	m_nPlot_id=MY_NULL;
	m_fPlot_stam_gy=0.0;
	m_fPlot_stam_antal=0.0;
	m_fPlot_stubb_gy=0.0;
	m_fPlot_stubb_antal=0.0;
	
	m_fPlot_skad_stam=0.0;
	m_fPlot_skad_rot=0.0;
	m_fPlot_skad_stam_rot=0.0;

	m_fPlot_skadandel_tot=0.0;
	m_fPlot_skadandel_stam=0.0;
	m_fPlot_skadandel_rot=0.0;
	m_fPlot_skadandel_rota=0.0;
	m_nPlot_X=0;
	m_nPlot_Y=0;			
	//m_fPlot_lat=MY_NULL;
	//m_fPlot_lon=MY_NULL;			
	m_nPlot_trakt_forvalt=MY_NULL;
	m_sPlot_trakt_avv_id=_T("");
	m_nPlot_trakt_inv_typ=MY_NULL;
	m_nPlot_trakt_forenklad=MY_NULL;

	m_nPlot_antal=0;
	m_nPlot_areal=0;
	m_nPlot_brosthojdsalder=0;
	m_nPlot_si=0;
	m_nPlot_si_tradslag=0;
	m_nPlot_skad_rota=0;
	m_nPlot_spar_langd1=0;
	m_nPlot_spar_langd2=0;
	m_nPlot_spar_matstracka=0;
	m_nPlot_stv_avst1=0;
	m_nPlot_stv_avst2=0;
	m_nPlot_stv_bredd1=0;
	m_nPlot_stv_bredd2=0;
	m_nPlot_ohjd=0;

	m_nPlot_skad_stam1=0;
   m_nPlot_skad_stam2=0;
	m_nPlot_skad_rot1=0;
	m_nPlot_skad_rot2=0;
	m_nPlot_skad_stam_rot1=0;
	m_nPlot_skad_stam_rot2=0;

	}

	_plot_data(
	int plot_id,
	double plot_stam_gy,
	double plot_stam_antal,
	double plot_stubb_gy,
	double plot_stubb_antal,
	double plot_skad_stam,
	double plot_skad_rot,
	double plot_skad_stam_rot,
	double plot_skadandel_tot,
	double plot_skadandel_stam,
	double plot_skadandel_rot,
	double plot_skadandel_rota,
	int plot_x,
	int plot_y,
	int plot_trakt_forvalt,
	CString plot_trakt_avv_id,
	int plot_trakt_inv_typ,
	int plot_trakt_forenklad,

	int plot_antal,
	int plot_areal,
	int plot_brosthojdsalder,
	int plot_si,
	int plot_si_tradslag,
	int plot_skad_rota,
	int plot_spar_langd1,
	int plot_spar_langd2,
	int plot_spar_matstracka,
	int plot_stv_avst1,
	int plot_stv_avst2,
	int plot_stv_bredd1,
	int plot_stv_bredd2,
	int plot_ohjd,
	int plot_skad_stam1,
	int plot_skad_stam2,
	int plot_skad_rot1,
	int plot_skad_rot2,
	int plot_skad_stam_rot1,
	int plot_skad_stam_rot2	
	)
	{
	m_nPlot_id=plot_id;
	m_fPlot_stam_gy=plot_stam_gy;
	m_fPlot_stam_antal=plot_stam_antal;
	m_fPlot_stubb_gy=plot_stubb_gy;
	m_fPlot_stubb_antal=plot_stubb_antal;
	m_fPlot_skad_stam=plot_skad_stam;
	m_fPlot_skad_rot=plot_skad_rot;
	m_fPlot_skad_stam_rot=plot_skad_stam_rot;
	m_fPlot_skadandel_tot=plot_skadandel_tot;
	m_fPlot_skadandel_stam=plot_skadandel_stam;
	m_fPlot_skadandel_rot=plot_skadandel_rot;
	m_fPlot_skadandel_rota=plot_skadandel_rota;
	m_nPlot_X=plot_x;
	m_nPlot_Y=plot_y;			
	//m_fPlot_lat=plot_lat;
	//m_fPlot_lon=plot_lon;			
	m_nPlot_trakt_forvalt=plot_trakt_forvalt;
	m_sPlot_trakt_avv_id=plot_trakt_avv_id;
	m_nPlot_trakt_inv_typ=plot_trakt_inv_typ;
	m_nPlot_trakt_forenklad=plot_trakt_forenklad;


	m_nPlot_antal=plot_antal;
	m_nPlot_areal=plot_areal;
	m_nPlot_brosthojdsalder=plot_brosthojdsalder;
	m_nPlot_si=plot_si;
	m_nPlot_si_tradslag=plot_si_tradslag;
	m_nPlot_skad_rota=plot_skad_rota;
	m_nPlot_spar_langd1=plot_spar_langd1;
	m_nPlot_spar_langd2=plot_spar_langd2;
	m_nPlot_spar_matstracka=plot_spar_matstracka;
	m_nPlot_stv_avst1=plot_stv_avst1;
	m_nPlot_stv_avst2=plot_stv_avst2;
	m_nPlot_stv_bredd1=plot_stv_bredd1;
	m_nPlot_stv_bredd2=plot_stv_bredd2;
	m_nPlot_ohjd=plot_ohjd;

	m_nPlot_skad_stam1=plot_skad_stam1;
	m_nPlot_skad_stam2=plot_skad_stam2;
	m_nPlot_skad_rot1=plot_skad_rot1;
	m_nPlot_skad_rot2=plot_skad_rot2;
	m_nPlot_skad_stam_rot1=plot_skad_stam_rot1;
	m_nPlot_skad_stam_rot2=plot_skad_stam_rot2;

	}
} PLOT_DATA;
typedef std::vector<PLOT_DATA> vecPlotData;


typedef struct _tree_data
{
	int m_nTree_Trakt_forvalt;
	CString m_sTree_Trakt_avv_id;
	int m_nTree_Trakt_inv_typ;
	int m_nTree_Trakt_forenklad;

	int m_nTree_PlotId;
	int m_nTree_TreeId;
	int m_nTree_SpecId;
	int m_nTree_Dia;
	int m_nTree_Anth;
	int m_nTree_Hgt;
	int m_nTree_Hgt2;
	int m_nTree_Typ;
		
	_tree_data(void)
	{
	m_nTree_Trakt_forvalt=MY_NULL;
	m_sTree_Trakt_avv_id=_T("");
	m_nTree_Trakt_inv_typ=MY_NULL;
	m_nTree_Trakt_forenklad=MY_NULL;

	m_nTree_PlotId=MY_NULL;
	m_nTree_TreeId=MY_NULL;

	m_nTree_SpecId=MY_NULL;
	m_nTree_Dia=MY_NULL;
	m_nTree_Anth=MY_NULL;
	m_nTree_Hgt=MY_NULL;
	m_nTree_Hgt2=MY_NULL;
	m_nTree_Typ=MY_NULL;
	}

	_tree_data(
	int Tree_Trakt_forvalt,
	CString Tree_Trakt_avv_id,
	int Tree_Trakt_inv_typ,
	int Tree_Trakt_forenklad,

	int Tree_PlotId,
	int Tree_TreeId,
	int Tree_SpecId,
	int Tree_Dia,
	int Tree_Anth,
	int Tree_Hgt,
	int Tree_Hgt2,
	int Tree_Typ
	)
	{
	m_nTree_Trakt_forvalt=Tree_Trakt_forvalt;
	m_sTree_Trakt_avv_id=Tree_Trakt_avv_id;
	m_nTree_Trakt_inv_typ=Tree_Trakt_inv_typ;
	m_nTree_Trakt_forenklad=Tree_Trakt_forenklad;
	
	m_nTree_PlotId=Tree_PlotId;
	m_nTree_TreeId=Tree_TreeId;
	m_nTree_SpecId=Tree_SpecId;
	m_nTree_Dia=Tree_Dia;
	m_nTree_Anth=Tree_Anth;
	m_nTree_Hgt=Tree_Hgt;
	m_nTree_Hgt2=Tree_Hgt2;
	m_nTree_Typ=Tree_Typ;
	}
} TREE_DATA;
typedef std::vector<TREE_DATA> vecTreeData;

typedef struct _plot_correction
{
	int n_Id;
	double f_Gy;
	_plot_correction(int _id,double _gy)
	{
	n_Id=_id;
	f_Gy=_gy;
	}
	_plot_correction(void)
	{
	n_Id=0;
	f_Gy=0;
	}
}PLOT_CORRECTION;


typedef struct _trakt_index
{
	int			m_nTrakt_forvalt;
	int			m_nTrakt_driv_enh;
	CString		m_sTrakt_avv_id;
	int			m_nTrakt_inv_typ;
	int			m_nTrakt_forenklad;

	_trakt_index(
	int Trakt_forvalt,
	int Trakt_driv_enh,
	CString Trakt_avv_id,
	int Trakt_inv_typ,
	int Trakt_forenklad)
	{
	m_nTrakt_forvalt=Trakt_forvalt;
	m_nTrakt_driv_enh=Trakt_driv_enh;
	m_sTrakt_avv_id=Trakt_avv_id;
	m_nTrakt_inv_typ=Trakt_inv_typ;
	m_nTrakt_forenklad=Trakt_forenklad;
	}

}TRAKT_INDEX;
typedef std::vector<TRAKT_INDEX> vecTraktIndex;

// TRAKT DATA STRUCTURES
typedef struct _trakt_data
{
int		m_nTrakt_forvalt;
int		m_nTrakt_driv_enh;
CString	m_sTrakt_avv_id;
int		m_nTrakt_inv_typ;
int		m_nTrakt_avlagg;
int		m_nTrakt_distrikt;
int		m_nTrakt_maskinlag;
int		m_nTrakt_ursprung;
int		m_nTrakt_prod_led;
CString	m_sTrakt_namn;
CString	m_sTrakt_date;
double	m_fTrakt_areal;
double	m_fTrakt_ant_ytor;
double	m_fTrakt_medel_ovre_hojd;
int		m_nTrakt_si_sp;
double	m_fTrakt_si_tot;
double	m_fTrakt_alder;
CString	m_sTrakt_progver;
double	m_fTrakt_gy_dir_fore;
double	m_fTrakt_gy_dir_mal;
int		m_nTrakt_enter_type;
int		m_nTrakt_forenklad;
CString  m_sTrakt_date2;
int		m_nTrakt_huggnform;
int		m_nTrakt_terrf;
int		m_nTrakt_partof;
int		m_nTrakt_coastdist;
int		m_nDefaultH25[NUM_OF_SP];
int		m_nShownTreeData;

CString	m_sTrakt_notes;
CString	m_sTrakt_coord;

  vecPlotData	m_vPlotData;
  vecTreeData	m_vTreeData;
  CTraktVariables m_recData;
    
  _trakt_data(void){
	  m_nTrakt_forvalt=MY_NULL;
	  m_nTrakt_driv_enh=MY_NULL;
	  m_sTrakt_avv_id=_T("");
	  m_nTrakt_inv_typ=MY_NULL;
	  m_nTrakt_avlagg=MY_NULL;
	  m_nTrakt_distrikt=MY_NULL;
	  m_nTrakt_maskinlag=MY_NULL;
	  m_nTrakt_ursprung=MY_NULL;
	  m_nTrakt_prod_led=MY_NULL;
	  m_sTrakt_namn=_T("");
	  m_sTrakt_date=_T("");
	  m_fTrakt_areal=MY_NULL;
	  m_fTrakt_ant_ytor=MY_NULL;
	  m_fTrakt_medel_ovre_hojd=MY_NULL;
	  m_nTrakt_si_sp=MY_NULL;
	  m_fTrakt_si_tot=MY_NULL;
	  m_fTrakt_alder=MY_NULL;
	  m_sTrakt_progver=_T("");
	  m_fTrakt_gy_dir_fore=MY_NULL;
	  m_fTrakt_gy_dir_mal=MY_NULL;
	  m_nTrakt_enter_type=MY_NULL;

	  m_sTrakt_date2=_T("");
	  m_nTrakt_huggnform=MY_NULL;
	  m_nTrakt_terrf=MY_NULL;
	  m_nTrakt_partof=MY_NULL;
	  m_nTrakt_coastdist=MY_NULL;


	  m_sTrakt_notes=_T("");
	  m_sTrakt_coord=_T("");
	  m_nTrakt_forenklad=0;

	  m_recData = CTraktVariables();
	  m_vPlotData.clear();
	  m_vTreeData.clear();
	  for(int i=0;i<NUM_OF_SP;i++)
		m_nDefaultH25[i]=0;
	  m_nShownTreeData=0;

  }

  
	_trakt_data(
		int		trakt_forvalt,
		int		trakt_driv_enh,
		CString	trakt_avv_id,
		int		trakt_inv_typ,
		int		trakt_avlagg,
		int		trakt_distrikt,
		int		trakt_maskinlag,
		int		trakt_ursprung,
		int		trakt_prod_led,
		CString	trakt_namn,
		CString	trakt_date,
		double	trakt_areal,
		double	trakt_ant_ytor,
		double	trakt_medel_ovre_hojd,
		int		trakt_si_sp,
		double	trakt_si_tot,
		double	trakt_alder,
		CString	trakt_progver,
		double	trakt_gy_dir_fore,
		double	trakt_gy_dir_mal,
		int		trakt_enter_type,
		int		trakt_forenklad,
		CString	trakt_notes,
		CString  trakt_date2,
		int		trakt_huggnform,
		int		trakt_terrf,
		int		trakt_partof,
		int		nDefaultH25_1,
		int		nDefaultH25_2,
		int		nDefaultH25_3,
		int		nDefaultH25_4,
		int		nDefaultH25_5,
		int		nDefaultH25_6,
		int		nDefaultH25_7,
		CString  trakt_coord,
		CTraktVariables rec
	){
	  m_nTrakt_forvalt=trakt_forvalt;
	  m_nTrakt_driv_enh=trakt_driv_enh;
	  m_sTrakt_avv_id=trakt_avv_id;
	  m_nTrakt_inv_typ=trakt_inv_typ;
	  m_nTrakt_avlagg=trakt_avlagg;
	  m_nTrakt_distrikt=trakt_distrikt;
	  m_nTrakt_maskinlag=trakt_maskinlag;
	  m_nTrakt_ursprung=trakt_ursprung;
	  m_nTrakt_prod_led=trakt_prod_led;
	  m_sTrakt_namn=trakt_namn;
	  m_sTrakt_date=trakt_date;

	  m_fTrakt_areal=trakt_areal;
	  m_fTrakt_ant_ytor=trakt_ant_ytor;
	  m_fTrakt_medel_ovre_hojd=trakt_medel_ovre_hojd;
	  m_nTrakt_si_sp=trakt_si_sp;
	  m_fTrakt_si_tot=trakt_si_tot;
	  m_fTrakt_alder=trakt_alder;
	  m_sTrakt_progver=trakt_progver;
	  m_fTrakt_gy_dir_fore=trakt_gy_dir_fore;
	  m_fTrakt_gy_dir_mal=trakt_gy_dir_mal;
	  m_nTrakt_enter_type=trakt_enter_type;
	  m_nTrakt_forenklad=trakt_forenklad;
	  m_sTrakt_notes=trakt_notes;

	  m_sTrakt_date2=trakt_date2;
	  m_nTrakt_huggnform=trakt_huggnform;
	  m_nTrakt_terrf=trakt_terrf;
	  m_nTrakt_partof=trakt_partof;
	  m_nDefaultH25[0]=nDefaultH25_1;
	  m_nDefaultH25[1]=nDefaultH25_2;
	  m_nDefaultH25[2]=nDefaultH25_3;
	  m_nDefaultH25[3]=nDefaultH25_4;
	  m_nDefaultH25[4]=nDefaultH25_5;
	  m_nDefaultH25[5]=nDefaultH25_6;
	  m_nDefaultH25[6]=nDefaultH25_7;
	  m_sTrakt_coord=trakt_coord;
	  m_recData =rec;
	}
} TRAKT_DATA;

typedef std::vector<TRAKT_DATA> vecTraktData;


typedef struct _trakt_identifer_struct
{
int		m_nTrakt_forvalt;
int		m_nTrakt_driv_enh;
CString		m_sTrakt_avv_id;
int		m_nTrakt_inv_typ;
int		m_nTrakt_forenklad;

} TRAKT_IDENTIFER;


class CScaGallBasDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
	
public:
	CScaGallBasDB();
	CScaGallBasDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CScaGallBasDB(DB_CONNECTION_DATA &db_connection);

	BOOL doTableExists(LPCTSTR table_name);
	BOOL createTable(LPCTSTR script_string);

	BOOL getForvalts(vecForvaltData&);
	BOOL addForvalt(FORVALT_DATA&);
	BOOL updForvalt(FORVALT_DATA&);
	BOOL delForvalt(FORVALT_DATA&);
	BOOL getDistrikts(vecDistriktData&);
	BOOL addDistrikt(DISTRIKT_DATA&);
	BOOL updDistrikt(DISTRIKT_DATA&);
	BOOL delDistrikt(DISTRIKT_DATA&);
	BOOL getTrakts(vecTraktData&,int forenklad);
	//BOOL getPlots(vecPlotData&);
	BOOL getCorrectPlotNumbers(TRAKT_DATA trkt,vector<PLOT_CORRECTION> &vct);
	BOOL getPlots(TRAKT_DATA &trkt);

	BOOL getTrees(TRAKT_DATA &trkt);


	void Load_Trakt_Variables(vecTraktData&);
	void Load_Trakt_Variables(TRAKT_DATA &);
	
	BOOL addPlot(TRAKT_DATA &trkt,PLOT_DATA &);
	BOOL updPlot(TRAKT_DATA &trkt,PLOT_DATA &);
	BOOL delPlot(TRAKT_DATA rec);

	BOOL addTree(TRAKT_DATA &trkt, TREE_DATA &rec);
	BOOL updTree(TRAKT_DATA &trkt,TREE_DATA &rec);
	
	BOOL delTrakt(TRAKT_DATA tr);
	BOOL addTrakt(TRAKT_DATA tr);
	BOOL updTrakt(TRAKT_DATA tr);
	
	
	BOOL getTraktIndex(vecTraktIndex &vec,int forenklad);
	BOOL getTrakt(TRAKT_INDEX&,TRAKT_DATA &);

	void GetKeyDBQuestion(CString *,TRAKT_DATA rec);
	void GetKeyPlotDBQuestion(CString *str,PLOT_DATA rec);
	void GetKeyTreeDBQuestion(CString *str,TREE_DATA rec);

	void SetSaCommandParam(TRAKT_DATA tr,int update);
	void SetSaCommandPlotParam(TRAKT_DATA &trkt,PLOT_DATA &rec,int update);
	void SetSaCommandTreeParam(TRAKT_DATA &trkt,TREE_DATA &rec,int update);
protected:
	void getSQLTraktQuestion(LPTSTR sql);
	void getSQLTraktQuestionIndex(LPTSTR sql);
	void getSQLPlotQuestion(LPTSTR sql);
	void getSQLTreeQuestion(LPTSTR sql);
};


//---------------------------------------------------------------------------
// CForvaltReportDataRec
//---------------------------------------------------------------------------
class CForvaltReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CForvaltReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CForvaltReportDataRec(UINT index,int id,LPCTSTR name)
	{
		m_nIndex = index;
		AddItem(new CIntItem(id));
		AddItem(new CTextItem(name));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

//---------------------------------------------------------------------------
// CTraktReportDataRec
//---------------------------------------------------------------------------
class CTraktReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CString tst;
	CString m_sLangFN;
	CString dat;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CDoubleItem : public CXTPReportRecordItemNumber
	{
		double m_nValue;
	public:
		CDoubleItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0.0;
		}

		CDoubleItem(double nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstof(szText);
				SetValue(m_nValue);
		}

		double getDoubleItem(void)	{ return m_nValue; }
	};

public:
	CTraktReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTraktReportDataRec(UINT index,TRAKT_DATA t,CString csSi)
	{
		m_nIndex = index;
		//1
		if(t.m_nTrakt_forvalt==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_forvalt));
		//2
		if(t.m_nTrakt_driv_enh==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_driv_enh));
		//3
		AddItem(new CTextItem(t.m_sTrakt_avv_id));

		//4
		if(t.m_nTrakt_inv_typ==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_inv_typ));
		//5
		if(t.m_nTrakt_avlagg==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_avlagg));
		//6
		if(t.m_nTrakt_distrikt==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_distrikt));
		//7
		if(t.m_nTrakt_maskinlag==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_maskinlag));
		//8
		if(t.m_nTrakt_ursprung==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_ursprung));
		//9
		if(t.m_nTrakt_prod_led==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_prod_led));
		//10
		AddItem(new CTextItem(t.m_sTrakt_namn));
		//11
		
		dat.Format(_T("%8.8s"),t.m_sTrakt_date);
		AddItem(new CTextItem(dat));
		//12
		if(t.m_fTrakt_areal==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_areal));
		//13
		if(t.m_fTrakt_ant_ytor==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem((int)t.m_fTrakt_ant_ytor));		 


		//14
		dat.Format(_T("%8.8s"),t.m_sTrakt_date2);
		AddItem(new CTextItem(dat));

		//15
		if(t.m_nTrakt_huggnform==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem((int)t.m_nTrakt_huggnform));		
		
		//16
		if(t.m_nTrakt_terrf==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem((int)t.m_nTrakt_terrf));		

		//17
		AddItem(new CTextItem(t.m_sTrakt_progver));

		//14
		/*if(t.m_fTrakt_medel_ovre_hojd==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_medel_ovre_hojd));*/
		//15
		/*AddItem(new CTextItem(csSi));*/
		//16
		/*if(t.m_fTrakt_alder==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_alder));*/
		//18
		/*if(t.m_fTrakt_gy_dir_fore==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_gy_dir_fore));
		//19
		if(t.m_fTrakt_gy_dir_efter==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_gy_dir_efter));*/
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	double getColumnDouble(int item)	
	{ 
		return ((CDoubleItem*)GetItem(item))->getDoubleItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

//---------------------------------------------------------------------------
// CTraktViewData
//---------------------------------------------------------------------------
class CTraktViewData : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	int idx,tmpIdx;
	CString tst;
protected:
	
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* , LPCTSTR szText)
		{
			if(_tcslen(szText)==0)
				m_nValue=MY_NULL;
			else
				m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	
		{ 

				return m_sText; 
		
		}
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CDoubleItem : public CXTPReportRecordItemNumber
	{
		double m_nValue;
	public:
		CDoubleItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0.0;
		}

		CDoubleItem(double nValue) : CXTPReportRecordItemNumber(nValue,_T("%.1f"))	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			if(_tcslen(szText)==0)
				m_nValue=MY_NULL;
			else
				m_nValue = _tstof(szText);
				SetValue(m_nValue);
		}

		double getDoubleItem(void)	{ return m_nValue; }
		void setDoubleItem(double nValue)	
		{ 
			m_nValue = nValue; 
			SetValue(m_nValue);
		}
	};

public:
	CTraktViewData(int page)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));		
		AddItem(new CTextItem(_T("")));		
		if(page==0)
			AddItem(new CTextItem(_T("")));
		if(page==3)
		{
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		}
	}

	CTraktViewData(UINT spec,int page,TRAKT_DATA rec,CString specie)
	{
		m_nIndex = spec;
		AddItem(new CTextItem(specie));
		
		switch(page)
		{
		case PAGE_NR_FORE:			
			for(idx=0;idx<NUM_OF_FORE_TO_SHOW;idx++)
			{
				tst=_T("");
				switch(idx)
				{
				case 0:	tmpIdx=FORE_VARINDEX_M3SK;	break;
				case 1:	tmpIdx=FORE_VARINDEX_M3FUB;	break;
				case 2:	tmpIdx=FORE_VARINDEX_M3STAMSK;	break;
				case 3:	tmpIdx=FORE_VARINDEX_GY;		break;
				case 4:	tmpIdx=FORE_VARINDEX_ANTAL;	break;
				case 5:	tmpIdx=FORE_VARINDEX_DG;		break;
				case 6:	tmpIdx=FORE_VARINDEX_H25;		break;	
				case 7:	tmpIdx=FORE_VARINDEX_DGV;		break;	
				}
				if(rec.m_recData.m_recVars.m_recFore[tmpIdx][spec]==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_recFore[tmpIdx][spec]);
					AddItem(new CTextItem(tst));	
				}
			}
			break;
		case PAGE_NR_KVARV:			
			for(idx=0;idx<NUM_OF_KVARV_TO_SHOW;idx++)
			{
				tst=_T("");
				switch(idx)
				{
				case 0:	tmpIdx=KVARV_VARINDEX_M3SK;	break;
				case 1:	tmpIdx=KVARV_VARINDEX_M3FUB;	break;
				case 2:	tmpIdx=KVARV_VARINDEX_M3STAMSK;	break;
				case 3:	tmpIdx=KVARV_VARINDEX_GY;		break;
				case 4:	tmpIdx=KVARV_VARINDEX_ANTAL;	break;
				case 5:	tmpIdx=KVARV_VARINDEX_DG;		break;
				case 6:	tmpIdx=KVARV_VARINDEX_H25;		break;	
				case 7:	tmpIdx=KVARV_VARINDEX_DGV;		break;					
				}
				if(rec.m_recData.m_recVars.m_recKvarvarande[tmpIdx][spec]==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_recKvarvarande[tmpIdx][spec]);
					AddItem(new CTextItem(tst));	
				}
					//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_recKvarvarande[tmpIdx][spec]));
			}
			break;
		case PAGE_NR_UTMSTV:
			for(idx=0;idx<NUM_OF_UTTAG_TO_SHOW;idx++)
			{
				switch(idx)
				{
				case 0:	tmpIdx=UTTAG_VARINDEX_M3SK;		break;
				case 1:	tmpIdx=UTTAG_VARINDEX_M3FUB;		break;
				case 2:	tmpIdx=UTTAG_VARINDEX_M3STAMSK;	break;
				case 3:	tmpIdx=UTTAG_VARINDEX_GY;			break;
				case 4:	tmpIdx=UTTAG_VARINDEX_ANTAL;		break;
				case 5:	tmpIdx=UTTAG_VARINDEX_DG;			break;
				case 6:	tmpIdx=UTTAG_VARINDEX_DGV;			break;
				}
				if(rec.m_recData.m_recVars.m_recUttagMstv[tmpIdx][spec]==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_recUttagMstv[tmpIdx][spec]);
					AddItem(new CTextItem(tst));	
				}
					//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_recUttagMstv[tmpIdx][spec]));
			}
			break;
		case PAGE_NR_UTISTV:
			for(idx=0;idx<NUM_OF_UTTAG_TO_SHOW;idx++)
			{
				switch(idx)
				{
				case 0:	tmpIdx=UTTAG_VARINDEX_M3SK;		break;
				case 1:	tmpIdx=UTTAG_VARINDEX_M3FUB;		break;
				case 2:	tmpIdx=UTTAG_VARINDEX_M3STAMSK;	break;
				case 3:	tmpIdx=UTTAG_VARINDEX_GY;			break;
				case 4:	tmpIdx=UTTAG_VARINDEX_ANTAL;		break;
				case 5:	tmpIdx=UTTAG_VARINDEX_DG;			break;
				case 6:	tmpIdx=UTTAG_VARINDEX_DGV;			break;
				}
				if(rec.m_recData.m_recVars.m_recUttagIstv[tmpIdx][spec]==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_recUttagIstv[tmpIdx][spec]);
					AddItem(new CTextItem(tst));	
				}

					//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_recUttagIstv[tmpIdx][spec]));
			}
			break;
		case PAGE_NR_UTTOT:
			//Lagt till en extra kolumn(NUM_OF_UTTAG_TO_SHOW+1) ga.kvot, h�mtar dock ingen data fr�n den s� d�rav ingen �kning av NUM_OF_UTTAG_TO_SHOW
			//aktivt i version 1.0.13
			for(idx=0;idx<NUM_OF_UTTAG_TO_SHOW+1;idx++)
			{
				switch(idx)
				{
				case 0:	tmpIdx=UTTAG_VARINDEX_M3SK;		break;
				case 1:	tmpIdx=UTTAG_VARINDEX_M3FUB;		break;
				case 2:	tmpIdx=UTTAG_VARINDEX_M3STAMSK;	break;
				case 3:	tmpIdx=UTTAG_VARINDEX_GY;			break;
				case 4:	tmpIdx=UTTAG_VARINDEX_ANTAL;		break;
				case 5:	tmpIdx=UTTAG_VARINDEX_DG;			break;
				case 6:	tmpIdx=UTTAG_VARINDEX_DGV;			break;
				case 7:	tmpIdx=-1;			break;
				}

				//Visa gallringskvot dg uttag/ dg kvarv
				if(tmpIdx==-1)
				{
					if(rec.m_recData.m_recVars.m_recGaKvot[spec]==MY_NULL)
						AddItem(new CTextItem(_T("")));
					else
					{
						tst.Format(_T("%10.2f"),rec.m_recData.m_recVars.m_recGaKvot[spec]);
						AddItem(new CTextItem(tst));	
					}
				}
				else
				{
					if(rec.m_recData.m_recVars.m_recUttagTot[tmpIdx][spec]==MY_NULL)
						AddItem(new CTextItem(_T("")));
					else
					{
						tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_recUttagTot[tmpIdx][spec]);
						AddItem(new CTextItem(tst));	
					}
				}
					//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_recUttagTot[tmpIdx][spec]));
			}
			break;		
		case PAGE_NR_FORENKLAD:			
			for(idx=0;idx<NUM_OF_FORENKLAD_TO_SHOW;idx++)
			{
				tst=_T("");
				switch(idx)
				{
				case 0:	tmpIdx=KVARV_VARINDEX_GY;		break;
				case 1:	tmpIdx=KVARV_VARINDEX_DGV;		break;
				case 2:	tmpIdx=KVARV_VARINDEX_HGV;		break;	
				}
				if(rec.m_recData.m_recVars.m_recKvarvarande[tmpIdx][spec]==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_recKvarvarande[tmpIdx][spec]);
					AddItem(new CTextItem(tst));	
				}
			}
			break;
		}
	}

	CTraktViewData(PLOT_DATA rec,int *YtVarShow,CString specie)
	{
		if(YtVarShow[0])
			if(rec.m_nPlot_id==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_id));
		if(YtVarShow[1])
			if(rec.m_fPlot_stam_gy==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_stam_gy));
		if(YtVarShow[2])
			if(rec.m_fPlot_stam_antal==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_stam_antal));


		if(YtVarShow[3])
			if(rec.m_fPlot_stam_antal==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_stam_antal*rec.m_nPlot_areal/10000.0));


		if(YtVarShow[4])
			if(rec.m_fPlot_stubb_gy==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_stubb_gy));
		if(YtVarShow[5])
			if(rec.m_fPlot_stubb_antal==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_stubb_antal));

		if(YtVarShow[6])
			if(rec.m_fPlot_stam_antal==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_stubb_antal*rec.m_nPlot_areal/10000.0));


		if(YtVarShow[7])
			if(rec.m_fPlot_skad_stam==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skad_stam));
		if(YtVarShow[8])
			if(rec.m_fPlot_skad_rot==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skad_rot));
		if(YtVarShow[9])
			if(rec.m_fPlot_skad_stam_rot==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skad_stam_rot));
		if(YtVarShow[10])
			if(rec.m_fPlot_skadandel_tot==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skadandel_tot));
		if(YtVarShow[11])
			if(rec.m_fPlot_skadandel_stam==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skadandel_stam));
		if(YtVarShow[12])
			if(rec.m_fPlot_skadandel_rot==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skadandel_rot));
		if(YtVarShow[13])
			if(rec.m_fPlot_skadandel_rota==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CDoubleItem(rec.m_fPlot_skadandel_rota));
		if(YtVarShow[14])
			if(rec.m_nPlot_X==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_X));
		if(YtVarShow[15])
			if(rec.m_nPlot_Y==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_Y));	

		if(YtVarShow[16])
			if(rec.m_nPlot_antal==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_antal));	
		if(YtVarShow[17])
			if(rec.m_nPlot_areal==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_areal));	
		if(YtVarShow[18])
			if(rec.m_nPlot_brosthojdsalder==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_brosthojdsalder));	
		if(YtVarShow[19])
			if(rec.m_nPlot_si==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_si));	
		if(YtVarShow[20])
			if(rec.m_nPlot_si_tradslag==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CTextItem(specie));	
		if(YtVarShow[21])
			if(rec.m_nPlot_skad_rota==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_skad_rota));	
		if(YtVarShow[22])
			if(rec.m_nPlot_spar_langd1==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_spar_langd1));	
		if(YtVarShow[23])
			if(rec.m_nPlot_spar_langd2==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_spar_langd2));	
		if(YtVarShow[24])
			if(rec.m_nPlot_spar_matstracka==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_spar_matstracka));	
		if(YtVarShow[25])
			if(rec.m_nPlot_stv_avst1==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_stv_avst1));	
		if(YtVarShow[26])
			if(rec.m_nPlot_stv_avst2==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_stv_avst2));	
		if(YtVarShow[27])
			if(rec.m_nPlot_stv_bredd1==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_stv_bredd1));	
		if(YtVarShow[28])
			if(rec.m_nPlot_stv_bredd2==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_stv_bredd2));	
		if(YtVarShow[29])
			if(rec.m_nPlot_ohjd==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
				AddItem(new CIntItem(rec.m_nPlot_ohjd));	
	}

	CTraktViewData(TREE_DATA rec,CString specie,CString type)
	{
		AddItem(new CIntItem(rec.m_nTree_TreeId));
		AddItem(new CIntItem(rec.m_nTree_PlotId));
		AddItem(new CTextItem(specie));		
		AddItem(new CIntItem(rec.m_nTree_Dia));
		AddItem(new CIntItem(rec.m_nTree_Hgt));
		AddItem(new CIntItem(rec.m_nTree_Hgt2));
		AddItem(new CTextItem(type));
	}


	CTraktViewData(UINT row,TRAKT_DATA rec,CString csStr1,CString csStr2)
	{
		m_nIndex = row;
		switch(row)
		{
		case 0:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Skadade_Trad==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Skadade_Trad);
					AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Skadade_Trad));
				AddItem(new CTextItem(_T("")));
				AddItem(new CTextItem(csStr2));
				if(rec.m_recData.m_recVars.m_Zon1_Stam==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{

					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Zon1_Stam);
					AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Zon1_Stam));
				if(rec.m_recData.m_recVars.m_Zon2_Stam==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Zon2_Stam);
					AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Zon2_Stam));
			break;
		case 1:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Skadade_Stam==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
					tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Skadade_Stam);
					AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Skadade_Stam));
				AddItem(new CTextItem(_T("")));
				AddItem(new CTextItem(csStr2));
				if(rec.m_recData.m_recVars.m_Zon1_Rot==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Zon1_Rot);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Zon1_Rot));
				if(rec.m_recData.m_recVars.m_Zon2_Rot==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Zon2_Rot);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Zon2_Rot));
			break;
		case 2:
			AddItem(new CTextItem(csStr1));
			if(rec.m_recData.m_recVars.m_Skadade_Rot==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
			{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Skadade_Rot);
				AddItem(new CTextItem(tst));
			}
			//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Skadade_Rot));
			AddItem(new CTextItem(_T("")));
			AddItem(new CTextItem(csStr2));
			if(rec.m_recData.m_recVars.m_Zon1_Rot_Stam==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
			{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Zon1_Rot_Stam);
				AddItem(new CTextItem(tst));
			}
			//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Zon1_Rot_Stam));
			if(rec.m_recData.m_recVars.m_Zon2_Rot_Stam==MY_NULL)
				AddItem(new CTextItem(_T("")));
			else
			{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Zon2_Rot_Stam);
				AddItem(new CTextItem(tst));
			}
			//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Zon2_Rot_Stam));
			break;
		case 3:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Rota==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Rota);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Rota));				
			break;
		case 4:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Sparstracka==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Sparstracka);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Sparstracka));				
			break;
		case 5:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Spardjup==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Spardjup);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Spardjup));				
			break;
		case 6:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Spardjup_Andel==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Spardjup_Andel);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Spardjup_Andel));				
			break;
		case 7:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Stv_Avst==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Stv_Avst);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Stv_Avst));				
			break;
		case 8:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Stv_Bredd==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Stv_Bredd);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Stv_Bredd));				
			break;
		case 9:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Stv_Areal==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Stv_Areal);
				AddItem(new CTextItem(tst));
				}
				//AddItem(new CDoubleItem(rec.m_recData.m_recVars.m_Stv_Areal));				
			break;
		case 10:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Skadade_Stam==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Skadade_Stam);
				AddItem(new CTextItem(tst));				
				}
			break;			
		case 11:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Spardjup==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Spardjup);
				AddItem(new CTextItem(tst));				
				}
			break;			
		case 12:
				AddItem(new CTextItem(csStr1));
				if(rec.m_recData.m_recVars.m_Spar_over_20mha==MY_NULL)
					AddItem(new CTextItem(_T("")));
				else
				{
				tst.Format(_T("%10.1f"),rec.m_recData.m_recVars.m_Spar_over_20mha);
				AddItem(new CTextItem(tst));				
				}
			break;			
		}
	}


	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item,CString text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	double getColumnDouble(int item)	
	{ 
		return ((CDoubleItem*)GetItem(item))->getDoubleItem();
	}

	void setColumnDouble(int item,double data)	
	{ 
		((CDoubleItem*)GetItem(item))->setDoubleItem(data);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


typedef struct _sql_trakt_question
{
int		trakt_forvalt;
int		trakt_driv_enh;
int		trakt_del_id;
int		trakt_inv_typ;
  
  TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_TRAKT_QUESTION;

#define NUM_OF_TRAKT_VARIABLES_TOT	460
extern TCHAR *DBVarName[NUM_OF_TRAKT_VARIABLES_TOT];

#endif
