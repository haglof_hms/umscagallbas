#pragma once
#include "StdAfx.h"

#define MAXDIA                      1000
#define DIACLASS                    20
#define NUM_OF_TYPES						4
#define MINDIA                      60
						//Stubbe i skog Kvarv   Stubbe i v�g
enum CALCMETHODS  {AVV_IN_FOREST,REMAINED,AVK_IN_ROAD,AVK_ALL};
					

typedef struct _stand_table
{
	int tp,sp,cl;
	unsigned short int StandTable[NUM_OF_TYPES][NUM_OF_SP+1][MAXDIA/DIACLASS+1];

	_stand_table()
	{

		for(tp=0;tp<NUM_OF_TYPES;tp++)
			for(sp=0;sp<NUM_OF_SP+1;sp++)
				for(cl=0;cl<MAXDIA/DIACLASS+1;cl++)
					StandTable[tp][sp][cl]=0;
	}
}STAND_TABLE;

typedef struct CALCVolType
{
  int sp;
  
  float h25[NUM_OF_SP+1];
  float mstamm3fub[NUM_OF_SP+1];
  float mstamm3sk[NUM_OF_SP+1];
  float volumem3sk[NUM_OF_SP+1];
  float volumefub[NUM_OF_SP+1];
  float stems[NUM_OF_SP+1];
  float gr[NUM_OF_SP+1];
  float da[NUM_OF_SP+1];
  float dg[NUM_OF_SP+1];
  float dgv[NUM_OF_SP+1];
  float hgv[NUM_OF_SP+1];

  float factor;
  float hurttrees; //DAMAGE TREES
  float hurttree; //DAMAGESTEM
  float hurtrots;//DAMAGEROTS
  float hurtrot;//DAMAGEDECAY
  float skidrowdist;
  float skidrowdistn;
  float skidrowwith;
  float skidrowwithn;
  float overheight;
  short int overheightn;
  float SItot;//TGL final siteindex
  short int SIspec;//TGL final sitespec
  float SI[3];//TGL siteindex dm
  float SIn[3];//TGL num
  float skidrowarea;
  //Lagt till 20061117 �lder
  float SI_Agen;
  float SI_Age;
  float skad_stam_tot;
  float skad_stam_zon1;
  float skad_stam_zon2;
  float skad_rot_tot;
  float skad_rot_zon1;
  float skad_rot_zon2;
  float skad_stamrot_tot;
  float skad_stamrot_zon1;
  float skad_stamrot_zon2;

  float spardjuplen;
  float spardjuplenn;
  float spardjup;
  float spardjupn;
  float spardjup_proc;

  CALCVolType()
  {
	  for(sp=0;sp<NUM_OF_SP+1;sp++)
	  {
		  h25[sp]=0.0;
		  mstamm3fub[sp]=0.0;
		  mstamm3sk[sp]=0.0;
		  volumem3sk[sp]=0.0;
		  volumefub[sp]=0.0;
		  stems[sp]=0.0;
		  gr[sp]=0.0;
		  da[sp]=0.0;
		  dg[sp]=0.0;
		  dgv[sp]=0.0;
		  hgv[sp]=0.0;
	  }
	  SI[0]=0.0;//TGL siteindex dm
	  SI[1]=0.0;//TGL siteindex dm
	  SI[2]=0.0;//TGL siteindex dm
	  SIn[0]=0.0;//TGL num
	  SIn[1]=0.0;//TGL num
	  SIn[2]=0.0;//TGL num
	  factor=0.0;
	  skidrowarea=0.0;
	  hurttrees=0.0; //DAMAGE TREES
	  hurttree=0.0; //DAMAGESTEM
	  hurtrots=0.0;//DAMAGEROTS
	  hurtrot=0.0;//DAMAGEDECAY
	  skidrowdist=0.0;
	  skidrowdistn=0.0;
	  skidrowwith=0.0;
	  skidrowwithn=0.0;
	  overheight=0.0;
	  overheightn=0;
	  SItot=0.0;//TGL final siteindex
	  SIspec=0;//TGL final sitespec
	  //Lagt till 20061117 �lder
	  SI_Agen=0.0;
	  SI_Age=0.0;
	  skad_stam_tot=0.0;
	  skad_stam_zon1=0.0;
	  skad_stam_zon2=0.0;
	  skad_rot_tot=0.0;
	  skad_rot_zon1=0.0;
	  skad_rot_zon2=0.0;
	  skad_stamrot_tot=0.0;
	  skad_stamrot_zon1=0.0;
	  skad_stamrot_zon2=0.0;
	  spardjuplen=0.0;
	  spardjuplenn=0.0;
	  spardjup=0.0;
	  spardjupn=0.0;
	  spardjup_proc=0.0;
  }


}CALCVolType;

typedef struct _CALCSampleType
{
  unsigned char spc;
  short int dia;
  short int hgt1;
  short int hgt2;
  _CALCSampleType()
  {
  spc=0;
  dia=0;
  hgt1=0;
  hgt2=0;
  }
}CALCSAMPLETYPE;

typedef std::vector<CALCSAMPLETYPE> vecSampleType;

#define IDI_SPEC1							1
#define IDI_SPEC2							2
#define IDI_SPEC3							3
#define IDI_SPEC4							4
#define IDI_SPEC5							5
#define IDI_SPEC6							6
#define IDI_SPEC7							7
#define IDI_SPEC9							100


typedef struct _defined_species
{
	int SpecNr[NUM_OF_SP+1];
_defined_species()
	{	
	SpecNr[0]=IDI_SPEC1;
	SpecNr[1]=IDI_SPEC2;
	SpecNr[2]=IDI_SPEC3;
	SpecNr[3]=IDI_SPEC4;
	SpecNr[4]=IDI_SPEC5;
	SpecNr[5]=IDI_SPEC6;
	SpecNr[6]=IDI_SPEC7;
	//SpecNr[7]=IDI_SPEC8;
	SpecNr[7]=IDI_SPEC9;
	}
} DEFINED_SPEC;



int make_thinning_result(TRAKT_DATA *Trakt);
int CountStandtable(TRAKT_DATA *Trakt);
int CountPlot(TRAKT_DATA *Trakt,CALCVolType *V);
void Count_Plot_Thinning(TRAKT_DATA *Trakt,PLOT_DATA Plot,float *stemnr,float *stubbnr,float *stemgy,float *stubbgy);
void CountVol(TRAKT_DATA *Trakt,int type,CALCVolType *V);
int Vol(TRAKT_DATA *Trakt,CALCVolType *V,int type);
void CalH25(CALCVolType *V,TRAKT_DATA *Trakt);
float GetArealFactor(TRAKT_DATA *Trakt);
float Brandel(float dia,float height,float a,float b,float c,float d,float e);
int GetMax(float *f,int n);
int Get_Defined_SpeciePlace(int _SpecNr);
