#include "stdafx.h"
#include "UMHmsMisc.h"
#include "HXLHandling.h"
#include "Calculations.h"


int HXL_Open(DB_CONNECTION_DATA con,LPCTSTR lang_fn,CWnd *wnd,TRAKT_DATA *trakt_data)
{
	CFileDialog fdlg(true,NULL,NULL,OFN_ENABLESIZING | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,HXL_FILE_FILTER,wnd ,0);
	CString sFileName=_T("");

	if( fdlg.DoModal ()==IDOK )
	{		
		
		//POSITION pos = fdlg.GetStartPosition();
		//while (pos)
		//{
			sFileName=fdlg.GetPathName();
			//sFileName = fdlg.GetNextPathName(pos);


			
			if(HXL_FileOpen(sFileName.GetBuffer())==0)
			{
				return HXL_MakeData(trakt_data);				
			}
			else
			{
				AfxMessageBox(_T("Lyckades ej l�sa fil!"));
				return -1;
			}
		//}		
	}	
	return -1;
}

int HXL_FileOpen(TCHAR *filename)
{
	CString Err;
	int ret;
	HXL_PROC1 procFileOpen;
	HINSTANCE hDllInst;

	hDllInst=AfxLoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{//HXL_DLL_PATH
		Err.Format(_T("%s %s"),_T("Can't Load Dll "),getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -1;
	}
	procFileOpen = (HXL_PROC1)GetProcAddress(hDllInst, HXL_FUNC_OPEN);

	
	if(procFileOpen==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_OPEN);
		AfxMessageBox(Err,MB_OK);
		return -2;
	}
	CStringA csFilename;
	csFilename.Format("%S", filename);
	ret = procFileOpen(csFilename.GetBuffer());

	
	return ret;
}


/***************************************************************
/ Retur	H�ndelse																/		
/  0		Data finns										   				/
/ -1		Inga resultat finns												/
/ -2		Tr�dslaget finns ej												/
/ -3		Typen (f�re kvarvarande uttag) finns  ej					/
/ -4		Variabeln finns  ej												/
/ -5		Inga resultat f�r det best�ndsnumret						/
/ -6		Kan inte ladda dll												/
/ -7		Kan inte ladda funktion i dll									/
***************************************************************/
/*int HXL_GetData(int _standnr, int _specie, int _type, int _var, float *ret)
{
	CString Err;
	int nRet;
	HXL_PROC2 procGetData;
	HINSTANCE hDllInst;

	hDllInst=LoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll ",getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	#ifdef UNICODE
		USES_CONVERSION;
		procGetData=(HXL_PROC2)GetProcAddress(hDllInst,W2A(HXL_FUNC_GETDATA));
	#else
		USES_CONVERSION;
		procGetData=(HXL_PROC2)GetProcAddress(hDllInst,HXL_FUNC_GETDATA);
	#endif

	
	if(procGetData==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_GETDATA);
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	nRet=procGetData(_standnr,_specie,_type,_var,ret);
	return nRet;
}*/

/***************************************************************
/ Retur	H�ndelse																/		
/  0		Data finns										   				/
/ -1		Inga resultat finns												/
/ -4		Variabeln finns  ej												/
/ -5		Inga resultat f�r det best�ndsnumret						/
/  Variabler																	/
/ 0  Traktnamn
/ 1  Inventeringstyp
/ 2  F�rvaltning
/ 3  DelId/Delbest�nd
/ 4  Id
/ 5  Datum
/ 6  Metod
/ 7  Areal
/ 8  Antal ytor
/ 9  �vre h�jd
/ 10 SI tr�dslag
/ 11 SI �lder
***************************************************************/
int HXL_GetHeadData(int _standnr,int _var, TCHAR *_ret)	
{
	CString Err;
	int nRet;
	HXL_PROC3 procGetHeadData;
	HINSTANCE hDllInst;
	char cret[248];

	hDllInst=LoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll ",getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	procGetHeadData = (HXL_PROC3)GetProcAddress(hDllInst, HXL_FUNC_GETHEADDATA);
	
	if(procGetHeadData==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_GETHEADDATA);
		AfxMessageBox(Err,MB_OK);
		return -6;
	}

	nRet=procGetHeadData(_standnr,_var,cret);	
	_stprintf(_ret,_T("%S"),cret);

	return nRet;
}


int HXL_GetCoordData(int _standnr,CStringA &_ret)	
{
	CString Err;
	//int nRet;
	HXL_PROC7 procGetCoordData;
	HINSTANCE hDllInst;

	hDllInst=AfxLoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll ",getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	procGetCoordData = (HXL_PROC7)GetProcAddress(hDllInst, HXL_FUNC_GETCOORDDATA);
	
	if(procGetCoordData==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_GETCOORDDATA);
		AfxMessageBox(Err,MB_OK);
		return -6;
	}

	procGetCoordData(_standnr,_ret);	
	//_stprintf(_ret,_T("%S"),cret);

	AfxFreeLibrary(hDllInst);
	//return nRet;
	return 0;
}


int HXL_GetPlotData(int _standnr,int _pi,int _var,int *_ret)	
{
	CString Err;
	int nRet;
	HXL_PROC4 procGetPlotData;
	HINSTANCE hDllInst;

	hDllInst=LoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll ",getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	procGetPlotData = (HXL_PROC4)GetProcAddress(hDllInst, HXL_FUNC_GETPLOTDATA);
	
	if(procGetPlotData==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_GETPLOTDATA);
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	nRet=procGetPlotData(_standnr,_pi,_var,_ret);
	return nRet;
}

int HXL_GetTreeData(int _standnr,int _nTreeIndex,int _var,int *_ret)
{
	CString Err;
	int nRet;
	HXL_PROC6 procGetTreeData;
	HINSTANCE hDllInst;

	hDllInst=LoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll ",getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	procGetTreeData = (HXL_PROC6)GetProcAddress(hDllInst, HXL_FUNC_GETTREEDATA);
	
	if(procGetTreeData==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_GETTREEDATA);
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	nRet=procGetTreeData(_standnr,_nTreeIndex,_var,_ret);
	return nRet;
}

/*int HXL_GetHurtData(int _standnr,int _var,float *_ret)	
{
	CString Err;
	int nRet;
	HXL_PROC5 procGetHurtData;
	HINSTANCE hDllInst;

	hDllInst=LoadLibrary(getHxlDllDir());
	if(hDllInst==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll ",getHxlDllDir());
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	#ifdef UNICODE
		USES_CONVERSION;
		procGetHurtData=(HXL_PROC5)GetProcAddress(hDllInst,W2A(HXL_FUNC_GETHURTDATA));
	#else
		procGetHurtData=(HXL_PROC5)GetProcAddress(hDllInst,HXL_FUNC_GETHURTDATA);
	#endif
	
	if(procGetHurtData==NULL)
	{
		Err.Format(_T("%s %s"),"Can't Load Dll func ",HXL_FUNC_GETHURTDATA);
		AfxMessageBox(Err,MB_OK);
		return -6;
	}
	nRet=procGetHurtData(_standnr,_var,_ret);
	return nRet;
}*/


CString FixLocale(CString csSource)
{
	CString csBuf;
	csBuf = csSource;
	lconv* lc = localeconv();


	if(*lc->decimal_point=='.')
	{
		csBuf.Replace(_T(","),_T("."));
	}
	else if(*lc->decimal_point==',')
	{
		csBuf.Replace(_T("."),_T(","));
	}
	return csBuf;
}



int HXL_MakeData(TRAKT_DATA *trakt_data)
{
	CTraktVariables data = CTraktVariables();
	int stand=0,specnr=1,type=1,varidx=0;
	float fret=0.0;
	int species[NUM_OF_SP+1]={1,2,3,4,5,6,7,100};
	int spec=0,var=0,row=0,col=0;
	int max_var[MAX_NUM_OF_TYPES]={NUM_OF_FORE,NUM_OF_KVARV,NUM_OF_UTTAG,NUM_OF_UTTAG,NUM_OF_UTTAG,1};	
	int NUM_OF_STAND_VAR_FROM_HXL=30;
	TCHAR cret[248];
	CString tst=_T(""),tst2=_T(""),errMsg=_T(""),csTemp;
	CStringA csRet;

	/***************************************************************
	/ Retur	H�ndelse																/		
	/  0		Data finns										   				/
	/ -1		Inga resultat finns												/
	/ -4		Variabeln finns  ej												/
	/ -5		Inga resultat f�r det best�ndsnumret						/
	/ 0   IDS_STAND_PROGVER					"PROGRAMVERSION"
	/ 1   IDS_STAND_DATE						"DATE"
	/ 2   IDS_STAND_AREAL					"AREAL"
	/ 3   IDS_STAND_NUMPLOT					"NUMPLOT"
	/ 4   IDS_STAND_INVTYP					"INVTYP"
	/ 5   IDS_STAND_FORVALT					"FORVALT"
	/ 6   IDS_STAND_DRIVNENH				"DRIVNENH"
	/ 7   IDS_STAND_DELID					"DELID"
	/ 8   IDS_STAND_AVLAGG					"AVLAGG"
	/ 9  IDS_STAND_DISTRIKT					"DISTRIKT"
	/ 10  IDS_STAND_MASKINLAG				"MASKINLAG"
	/ 11  IDS_STAND_URSPRUNG				"URSPRUNG"
	/ 12  IDS_STAND_PRODLEDARE				"PRODLEDARE"
	/ 13  IDS_STAND_NAMN						"NAMN"
	/ 14  IDS_STAND_GYDIRFORE				"GYDIRFORE"
	/ 15  IDS_STAND_GYDIRMAL				"GYDIRMAL"
	/ 16  IDS_STAND_DATE2					"DATE"
	/ 17  IDS_STAND_HUGGNF					"HUGGNFORM"
	/ 18  IDS_STAND_TERRF					"TERRF"
	/ 19 IDS_STAND_PARTOF					"PARTOF"
	/ 20 IDS_STAND_COASTDIST				"COASTDIST"
	/ 21-27 H25
	/ 28	KOORDINATER
	/ 29  AvvID
	***************************************************************/

	int ch=0;
	
	for(var=0;var<NUM_OF_STAND_VAR_FROM_HXL;var++)
	{
		_tcscpy(cret,_T(""));
		//Koordinater
		if(var==28)
			ch=HXL_GetCoordData(0,csRet);
		else
			ch=HXL_GetHeadData(0,var,cret);
		if(ch==0)
		{
			switch(var)
			{
			case 0:	 if(_tcslen(cret)>0)
							 trakt_data->m_sTrakt_progver=cret;					break;
			case 1:	 if(_tcslen(cret)>0)
							 trakt_data->m_sTrakt_date=cret;						break;
			case 2:	 if(_tcslen(cret)>0)
							 trakt_data->m_fTrakt_areal=_tstof(cret)/10000.0;	break;
			case 3:	 if(_tcslen(cret)>0)
							 trakt_data->m_fTrakt_ant_ytor=_tstof(cret);		break;
			case 4:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_inv_typ=_tstoi(cret);		break;
			case 5:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_forvalt=_tstoi(cret);		break;
			case 6:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_driv_enh=_tstoi(cret);		break;
			case 7:	 if(_tcslen(cret)>0)
							 /*trakt_data->m_nTrakt_del_id=_tstoi(cret);*/		break;
			case 8:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_avlagg=_tstoi(cret);		break;
			case 9:	 if(_tcslen(cret)>0)
				 		 trakt_data->m_nTrakt_distrikt=_tstoi(cret);			break;
			case 10:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_maskinlag=_tstoi(cret);	break;
			case 11:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_ursprung=_tstoi(cret);		break;
			case 12:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_prod_led=_tstoi(cret);		break;
			case 13:	 if(_tcslen(cret)>0)
							 trakt_data->m_sTrakt_namn=cret;						break;
			case 14:	 if(_tcslen(cret)>0)
						 {
							 //Check user settings if comma or period is used ac decimal separator
							 csTemp=cret;
							 csTemp=FixLocale(csTemp);
							 trakt_data->m_fTrakt_gy_dir_fore=_tstof(csTemp.GetBuffer(0));	
						 }
							 break;
			case 15:	 if(_tcslen(cret)>0)
						 {
							 //Check user settings if comma or period is used ac decimal separator
							 csTemp=cret;
							 csTemp=FixLocale(csTemp);

							 trakt_data->m_fTrakt_gy_dir_mal=_tstof(csTemp.GetBuffer(0));	
						 }
						 break;
			case 16:	 if(_tcslen(cret)>7)
							 trakt_data->m_sTrakt_date2=cret;					break;
			case 17:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_huggnform=_tstoi(cret);	break;
			case 18:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_terrf=_tstoi(cret);			break;
			case 19:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_partof=_tstoi(cret);		break;
			case 20:	 if(_tcslen(cret)>0)
							 trakt_data->m_nTrakt_coastdist=_tstoi(cret);	break;

			case 21:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[0]=_tstoi(cret);	break;
			case 22:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[1]=_tstoi(cret);	break;
			case 23:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[2]=_tstoi(cret);	break;
			case 24:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[3]=_tstoi(cret);	break;
			case 25:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[4]=_tstoi(cret);	break;
			case 26:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[5]=_tstoi(cret);	break;
			case 27:	 if(_tcslen(cret)>0)
							 trakt_data->m_nDefaultH25[6]=_tstoi(cret);	break;
			case 28:	 trakt_data->m_sTrakt_coord=csRet; break;
			case 29:
				trakt_data->m_sTrakt_avv_id = cret;
				break;
			}
		}
	}

	
	
	/*
	type=0;
	do
	{
		var=0;
		do
		{
			spec=0;
			do
			{
				specnr=species[spec];
				varidx=var;
				switch(HXL_GetData(stand,specnr,type,varidx,&fret))
				{
				case 0:
					switch(type)
					{
						case 0: // F�re
							data.m_recVars.m_recFore[var][spec]=fret;					break;
						case 1: //Kvarvarande
							data.m_recVars.m_recKvarvarande[var][spec]=fret;					break;
						case 2: // Uttag tot
							//tst.Format("%8.3f\n",fret);
							//tst2+=tst;							
							data.m_recVars.m_recUttagTot[var][spec]=fret;						break;
						case 3: // Uttag mellan stv
							data.m_recVars.m_recUttagMstv[var][spec]=fret;						break;
						case 4: // Uttag i stv
							data.m_recVars.m_recUttagIstv[var][spec]=fret;						break;
						case 5: // Gallringskvot
							data.m_recVars.m_recGaKvot[spec]=fret;									break;
					}					
					break;
				case -1:		AfxMessageBox(_T("Inga resultat finns"));								break;												
				case -2:		errMsg.Format(_T("%s %d"),"Tr�dslaget finns ej",specnr);
								AfxMessageBox(errMsg);								break;
				case -3:		AfxMessageBox(_T("Typen (f�re kvarvarande uttag) finns  ej"));	break;
				case -4:		AfxMessageBox(_T("Variabeln finns  ej"));								break;
				case -5:		AfxMessageBox(_T("Inga resultat f�r det best�ndsnumret"));		break;
				case -6:		AfxMessageBox(_T("Kan inte ladda dll"));								break;
				case -7:		AfxMessageBox(_T("Kan inte ladda funktion i dll"));					break;
				}
				spec++;
			}while(spec<NUM_OF_SP+1);
			var++;			
		}while(var<max_var[type]);		
		type++;
	}while(type<MAX_NUM_OF_TYPES);
	//AfxMessageBox(tst2);
	trakt_data->m_recData=data;

	float value=0.0;
	for(var=0;var<NUM_OF_SKAD_DATA;var++)
	{
	   ch=HXL_GetHurtData(0,var,&value);
		if(ch==0)
		{
		   switch(var)
			{
			case 0: trakt_data->m_recData.m_recVars.m_Skadade_Trad=value;	break;
			case 1: trakt_data->m_recData.m_recVars.m_Skadade_Stam=value;	break;
			case 2: trakt_data->m_recData.m_recVars.m_Skadade_Rot=value;	break;
			case 3: trakt_data->m_recData.m_recVars.m_Rota=value;				break;
			case 4: trakt_data->m_recData.m_recVars.m_Stv_Avst=value;		break;
			case 5: trakt_data->m_recData.m_recVars.m_Stv_Bredd=value;		break;
			case 6: trakt_data->m_recData.m_recVars.m_Stv_Areal=value;		break;
			case 7: trakt_data->m_recData.m_recVars.m_Sparstracka=value;	break;
			case 8: trakt_data->m_recData.m_recVars.m_Spardjup=value;		break;
			case 9: trakt_data->m_recData.m_recVars.m_Spardjup_Andel=value;break;		

			case 10: trakt_data->m_recData.m_recVars.m_Zon1_Stam=value;		break;		
			case 11: trakt_data->m_recData.m_recVars.m_Zon2_Stam=value;		break;		
			case 12: trakt_data->m_recData.m_recVars.m_Zon1_Rot=value;		break;		
			case 13: trakt_data->m_recData.m_recVars.m_Zon2_Rot=value;		break;		
			case 14: trakt_data->m_recData.m_recVars.m_Zon1_Rot_Stam=value;break;		
			case 15: trakt_data->m_recData.m_recVars.m_Zon2_Rot_Stam=value;break;		
			}
		}
	}
	*/

	//H�mta plotdata
	PLOT_DATA pData;
	int value=0,nTreeIndex=0;
	for(int pi=0;pi<trakt_data->m_fTrakt_ant_ytor;pi++)
	{
	   pData=PLOT_DATA();
		pData.m_nPlot_trakt_forvalt	= trakt_data->m_nTrakt_forvalt;
		pData.m_sPlot_trakt_avv_id		= trakt_data->m_sTrakt_avv_id;
		pData.m_nPlot_trakt_inv_typ	= trakt_data->m_nTrakt_inv_typ;
		
		for(var=0;var<NUM_OF_YTVAR_TOSHOW;var++)
		{
			ch=HXL_GetPlotData(0,pi,var,&value);
			if(ch==0)
			{
				switch(var)
				{
				case 0: pData.m_nPlot_id=value;					break;
				case 1: pData.m_nPlot_areal=value;				break;
				case 2: pData.m_nPlot_ohjd=value;				break;
				case 3: pData.m_nPlot_brosthojdsalder=value;	break;
				case 4: //Bonitet
					break;
				case 5: pData.m_nPlot_si_tradslag=value;		break;
				case 6: pData.m_nPlot_si=value;					break;
				case 7: pData.m_nPlot_antal=value;				break;
				case 8: //Gy
					break;
				case 9://Gfactor
					break;
				case 10: pData.m_nPlot_skad_stam1=value;			break;
				case 11: pData.m_nPlot_skad_stam2=value;			break;
				case 12: pData.m_nPlot_skad_rot1=value;			break;
				case 13: pData.m_nPlot_skad_rot2=value;			break;
				case 14: pData.m_nPlot_skad_stam_rot1=value;	break;
				case 15: pData.m_nPlot_skad_stam_rot2=value;	break;
				case 16: pData.m_nPlot_skad_rota=value;			break;
				case 17: pData.m_nPlot_stv_bredd1=value;		break;
				case 18: pData.m_nPlot_stv_bredd2=value;		break;
				case 19: pData.m_nPlot_stv_avst1=value;			break;
				case 20: pData.m_nPlot_stv_avst2=value;			break;
				case 21: pData.m_nPlot_spar_matstracka=value;	break;
				case 22: pData.m_nPlot_spar_langd1=value;		break;
				case 23: pData.m_nPlot_spar_langd2=value;		break;
				case 24: pData.m_nPlot_X=value;		break;
				case 25: pData.m_nPlot_Y=value;		break;

				}
			}
		}
		trakt_data->m_vPlotData.push_back(pData);		
	}

	// H�mta tr�ddata
	TREE_DATA tData;
	tData.m_nTree_Trakt_forvalt	= trakt_data->m_nTrakt_forvalt;
	tData.m_sTree_Trakt_avv_id		= trakt_data->m_sTrakt_avv_id;
	tData.m_nTree_Trakt_inv_typ	= trakt_data->m_nTrakt_inv_typ;
	tData.m_nTree_PlotId				= pData.m_nPlot_id;
	nTreeIndex=0;
	do
	{
		tData=TREE_DATA();
		for(var=0;var<NUM_OF_TREE_VAR;var++)
		{
			value=0;
			ch=HXL_GetTreeData(0,nTreeIndex,var,&value);
			if(ch==0)
			{
				switch(var)
				{
				case 0: tData.m_nTree_Anth=value;				break;
				case 1: tData.m_nTree_Dia=value;					break;
				case 2: tData.m_nTree_Hgt=value/10;				break;
				case 3: tData.m_nTree_Hgt2=value;				break;
				case 4: tData.m_nTree_PlotId=value;				break;
				case 5: tData.m_nTree_SpecId=value;				break;
				case 6: tData.m_nTree_Typ=value;					break;
				}
			}
		}
		if(ch==0)
		{
			tData.m_nTree_TreeId=nTreeIndex+1;
			trakt_data->m_vTreeData.push_back(tData);
			nTreeIndex++;
		}
	}while(ch==0);

	make_thinning_result(trakt_data);
	return 1;

}