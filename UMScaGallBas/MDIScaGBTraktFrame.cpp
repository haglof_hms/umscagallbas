// MDIScaGBTraktFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
//#include "UMScaGallBas.h"
#include "MDIScaGBTraktFrame.h"
#include "MDIScaGBTraktFormView.h"
#include "ResLangFileReader.h"
#include "ScaGBTraktSelectList.h"
#include "UMScaGallBas.h"

// CMDIScaGBTraktFrame dialog

IMPLEMENT_DYNCREATE(CMDIScaGBTraktFrame, CMDIChildWnd)

CMDIScaGBTraktFrame::CMDIScaGBTraktFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
	WriteToLog(_T("CMDIScaGBTraktFrame()"),false);
}

CMDIScaGBTraktFrame::~CMDIScaGBTraktFrame()
{
}

BEGIN_MESSAGE_MAP(CMDIScaGBTraktFrame, CMDIChildWnd)
		ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


void CMDIScaGBTraktFrame::OnDestroy(void)
{
	// save window position
	WriteToLog(_T("CMDIScaGBTraktFrame::OnDestroy "),false);
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_TRAKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}


void CMDIScaGBTraktFrame::OnClose(void)
{

	CMDIScaGBTraktFormView *pView = (CMDIScaGBTraktFormView *)getFormViewByID(IDD_FORMVIEW2);
	WriteToLog(_T("CMDIScaGBTraktFrame::OnClose 1"),false);
	if (pView)
	{
		WriteToLog(_T("CMDIScaGBTraktFrame::OnClose 2"),false);
		if (pView->getIsDirty())
		{
			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
			{
				pView->saveTrakt();
			}
		}
	}
	else
		WriteToLog(_T("CMDIScaGBTraktFrame::OnClose 3"),true);
	

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);


	CMDIChildWnd::OnClose();
}

int CMDIScaGBTraktFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
 WriteToLog(_T("CMDIScaGBTraktFrame::OnCreate 1"),false);
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
	{
		WriteToLog(_T("CMDIScaGBTraktFrame::OnCreate 2"),true);
		return -1;
	}

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	//if (!isDBConnection(m_sLangFN))
	//	return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}


	if (fileExists(m_sLangFN))
	{
		WriteToLog(_T("CMDIScaGBTraktFrame::OnCreate 3"),false);
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			WriteToLog(_T("CMDIScaGBTraktFrame::OnCreate 4"),false);
			m_sMsgCap = xml->str(IDS_STRING7624);
			m_sMsg1 = xml->str(IDS_STRING7629);
		}	// if (xml->Load(m_sLangFN))
		else
			WriteToLog(_T("CMDIScaGBTraktFrame::OnCreate 5"),true);
	}	// if (fileExists(m_sLangFN))
	else
		WriteToLog(_T("CMDIScaGBTraktFrame::OnCreate 6"),true);

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDIScaGBTraktFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);
	nForenklad_global=0;
  WriteToLog(_T("CMDIScaGBTraktFrame::OnShowWindow 1"),false);
  if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
	   WriteToLog(_T("CMDIScaGBTraktFrame::OnShowWindow 2"),false);
		m_bFirstOpen = FALSE;
		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_TRAKT_KEY);
		LoadPlacement(this, csBuf);
		WriteToLog(_T("CMDIScaGBTraktFrame::OnShowWindow 3"),false);
  }
  else
	  WriteToLog(_T("CMDIScaGBTraktFrame::OnShowWindow 4"),false);
}

void CMDIScaGBTraktFrame::OnSetFocus(CWnd*)
{
	WriteToLog(_T("CMDIScaGBTraktFrame::OnSetFocus 1"),false);
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
/*	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,TRUE);*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}

BOOL CMDIScaGBTraktFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	WriteToLog(_T("CMDIScaGBTraktFrame::PreCreateWindow 1"),false);
	if( !CMDIChildWnd::PreCreateWindow(cs) )
	{
		WriteToLog(_T("CMDIScaGBTraktFrame::PreCreateWindow 2"),true);
		return FALSE;
	}

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIScaGBTraktFrame diagnostics

#ifdef _DEBUG
void CMDIScaGBTraktFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIScaGBTraktFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIScaGBTraktFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	WriteToLog(_T("CMDIScaGBTraktFrame::OnGetMinMaxInfo 1"),false);
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCAGB_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCAGB_TRAKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIScaGBTraktFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
   WriteToLog(_T("CMDIScaGBTraktFrame::OnMDIActivate 1"),false);
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIScaGBTraktFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIScaGBTraktFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	WriteToLog(_T("CMDIScaGBTraktFrame::OnMessageFromShell 1"),false);
	if (wParam == ID_DBNAVIG_LIST)
	{
		WriteToLog(_T("CMDIScaGBTraktFrame::OnMessageFromShell 2"),false);
		nForenklad_global=0;
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		showFormView(IDD_FORMVIEW3,m_sLangFN);
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
		//CScaGBTraktSelectList *pView = (CScaGBTraktSelectList *)getFormViewByID(IDD_FORMVIEW3);
		//pView->forenklad(0);
	}
	else
	{
		WriteToLog(_T("CMDIScaGBTraktFrame::OnMessageFromShell 3"),false);
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			WriteToLog(_T("CMDIScaGBTraktFrame::OnMessageFromShell 4"),false);
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
		else
			WriteToLog(_T("CMDIScaGBTraktFrame::OnMessageFromShell 5"),true);
	}
	return 0L;
}

/****************************************************/
/////////////////////////////////////////////////////////////////////////////
// CScaGBTraktSelectListFrame

IMPLEMENT_DYNCREATE(CScaGBTraktSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CScaGBTraktSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CScaGBTraktSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CScaGBTraktSelectListFrame::CScaGBTraktSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CScaGBTraktSelectListFrame::~CScaGBTraktSelectListFrame()
{
}

void CScaGBTraktSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_LIST_TRAKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CScaGBTraktSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CScaGBTraktSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCAGB_LIST_TRAKT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CScaGBTraktSelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CScaGBTraktSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CScaGBTraktSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCAGB_LIST_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCAGB_LIST_TRAKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CScaGBTraktSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CScaGBTraktSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CScaGBTraktSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

