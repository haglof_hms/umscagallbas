// GallBasTraktSelectList.cpp : implementation file
#include "stdafx.h"
#include "UMScaGallBasDB.h"
#include "ScaGBTraktSelectList.h"
#include "ResLangFileReader.h"
#include "MDIScaGBTraktFormView.h"
#include "MDIScaGBTrakt2FormView.h"
#include "MDIScaGBTraktFrame.h"

int nForenklad_global;

IMPLEMENT_DYNCREATE(CScaGBTraktSelectList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CScaGBTraktSelectList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_TRAKT_REPORT, OnReportItemDblClick)
//	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CScaGBTraktSelectList::CScaGBTraktSelectList()
	: CXTResizeFormView(CScaGBTraktSelectList::IDD)
{
}

CScaGBTraktSelectList::~CScaGBTraktSelectList()
{
}

void CScaGBTraktSelectList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
//	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
}


/*void CScaGBTraktSelectList::forenklad(int forenklad)
{
	getTraktsFromDB(forenklad);
	m_nForenklad=forenklad;
	setupReport();
}*/

void CScaGBTraktSelectList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	SetScaleToFitSize(CSize(90, 1));
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);
	getTraktsFromDB(nForenklad_global);
	setupReport();
}

BOOL CScaGBTraktSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

BOOL CScaGBTraktSelectList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	return TRUE;
}



// CScaGBTraktSelectList message handlers
void CScaGBTraktSelectList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);
	if (m_wndTraktReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTraktReport,1,40,rect.right - 2,rect.bottom - 42);
	}
}

void CScaGBTraktSelectList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CScaGBTraktSelectList::setupReport(void)
{
	int nNumOfTabs = 0,col=0;
	CXTPReportColumn *pCol = NULL;
	int arr[NUM_OF_LIST_COLS]={	IDS_STRING7550,IDS_STRING7551,IDS_STRING7552,IDS_STRING7553,IDS_STRING7554,
						IDS_STRING7555,IDS_STRING7556,IDS_STRING7557,IDS_STRING7558,IDS_STRING7559,
						IDS_STRING7560,IDS_STRING7561,IDS_STRING7562,
						IDS_STRING7663};

	if (m_wndTraktReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndTraktReport.Create(this, IDC_TRAKT_REPORT ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				if (m_wndTraktReport.GetSafeHwnd() != NULL)
				{
					CScaGBTraktSelectListFrame *pView = (CScaGBTraktSelectListFrame *)getFormViewByID(IDD_FORMVIEW3)->GetParent();
					if (pView)
					{
						CDocument *pDoc = pView->GetActiveDocument();
						if (pDoc != NULL)
						{
							if(nForenklad_global)
								pDoc->SetTitle(xml->str(IDS_STRING757));		
							else
								pDoc->SetTitle(xml->str(IDS_STRING753));		

						}
					}
					m_wndTraktReport.ShowWindow( SW_NORMAL );
					for(col=0;col<NUM_OF_LIST_COLS;col++)
					{
						pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(col, xml->str(arr[col]), 30));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );
					}
					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					//setResize(&m_wndTraktReport,1,40,rect.right - 2,rect.bottom - 42);
					setResize(&m_wndTraktReport,1,rect.top,rect.right - 2,rect.bottom - 42);
					populateReport();
				}	// if (m_wndTraktReport.GetSafeHwnd() != NULL)
				// Also set language for Buttons etc; 061009 p�d
				//m_wndBtn1.SetWindowText(_T(xml->str(IDS_STRING401)));
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	}
	else
	{
		m_wndTraktReport.ClearReport();
		RECT rect;
		GetClientRect(&rect);
		// resize window = display window in tab; 061002 p�d
		//setResize(&m_wndTraktReport,1,40,rect.right - 2,rect.bottom - 42);
		setResize(&m_wndTraktReport,1,rect.top,rect.right - 2,rect.bottom - 42);
		populateReport();
	}
	return TRUE;
}

void CScaGBTraktSelectList::populateReport(void)
{
	CString sInvType=_T(""),csSi=_T("");
	m_wndTraktReport.ClearReport();
	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		TRAKT_DATA data = m_vecTraktData[i];
		csSi=_T("");
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				switch(data.m_nTrakt_si_sp)
				{
				case 1:
					csSi.Format(_T("%s %4.1f"),xml->str(IDS_STRING7600),data.m_fTrakt_si_tot);
					break;
				case 2:
					csSi.Format(_T("%s %4.1f"),xml->str(IDS_STRING7601),data.m_fTrakt_si_tot);
					break;
				case 3:
					csSi.Format(_T("%s %4.1f"),xml->str(IDS_STRING7609),data.m_fTrakt_si_tot);
					break;
				}
			}
		}
		m_wndTraktReport.AddRecord(new CTraktReportDataRec(i,data,csSi));
	}
	m_wndTraktReport.Populate();
	m_wndTraktReport.UpdateWindow();
}

void CScaGBTraktSelectList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pRow)
	{
		CTraktReportDataRec *pRec = (CTraktReportDataRec*)pItemNotify->pItem->GetRecord();
		if(nForenklad_global)
		{
			CMDIScaGBTrakt2FormView *pView2 = (CMDIScaGBTrakt2FormView *)getFormViewByID(IDD_FORMVIEW6);
			if (pView2)
				pView2->doPopulate(pRec->getIndex());
		}
		else
		{
			CMDIScaGBTraktFormView *pView = (CMDIScaGBTraktFormView *)getFormViewByID(IDD_FORMVIEW2);
			if (pView)
				pView->doPopulate(pRec->getIndex());
		}		
	}
}

// CScaGBTraktSelectList message handlers
/*
void CScaGBTraktSelectList::OnBnClickedButton1()
{
	BOOL bRet;
	CSQLTraktQuestionDlg *pDlg = new CSQLTraktQuestionDlg();

	if (pDlg)
	{
		bRet = (pDlg->DoModal() == IDOK);	
		delete pDlg;
		if (bRet)
		{
			getTraktsFromDB();
			populateReport();
			CMDIScaGBTraktFormView *pView = (CMDIScaGBTraktFormView *)getFormViewByID(IDD_FORMVIEW3);
			if (pView)
			{
				pView->resetTrakt(RESET_TO_LAST_NO_NB);
			}
		}
	}
}*/

void CScaGBTraktSelectList::getTraktsFromDB(int forenklad)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			//CString t=_T("");
			//t.Format(_T("%d"),forenklad);
			//AfxMessageBox(t);
			pDB->getTrakts(m_vecTraktData,forenklad);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}