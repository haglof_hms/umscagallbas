// MDIScaGBDistriktFormView.cpp : implementation file
//
#include "stdafx.h"
#include "MDIScaGBDistriktFormView.h"
#include "MDIScaGBDistriktFrame.h"
#include "ResLangFileReader.h"
#include "UMHmsMisc.h"
// CMDIScaGBDistriktFormView

IMPLEMENT_DYNCREATE(CMDIScaGBDistriktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIScaGBDistriktFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIScaGBDistriktFormView::CMDIScaGBDistriktFormView()
	: CXTResizeFormView(CMDIScaGBDistriktFormView::IDD)
{
	m_bConnected = FALSE;
}

CMDIScaGBDistriktFormView::~CMDIScaGBDistriktFormView()
{
}

void CMDIScaGBDistriktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL3_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL3_2, m_wndLbl2);

	DDX_Control(pDX, IDC_EDIT3_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT3_2, m_wndEdit2);

	DDX_Control(pDX, IDC_GROUP3_1, m_wndGroup);
	//}}AFX_DATA_MAP
}


BOOL CMDIScaGBDistriktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIScaGBDistriktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly( TRUE );
	m_wndEdit1.SetLimitText(DISTRIKT_MAX_LEN);

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
   m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly( TRUE );
	

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	getDistriktsFromDB();
	m_nDBIndex = 0;
	populateData(m_nDBIndex);
	setLanguage();
	m_bIsDirty=FALSE;
	
}

void CMDIScaGBDistriktFormView::getDistriktsFromDB()
{
	if (m_bConnected)
	{
		// Get Distrikt information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistrikts(m_vecDistriktData);
			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

void CMDIScaGBDistriktFormView::OnSetFocus(CWnd*)
{
	if(m_vecDistriktData.size()==0)
		setNavigationButtons(FALSE,FALSE);
	else
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));
}

BOOL CMDIScaGBDistriktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CMDIScaGBDistriktFormView diagnostics

#ifdef _DEBUG
void CMDIScaGBDistriktFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIScaGBDistriktFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIScaGBDistriktFormView message handlers

void CMDIScaGBDistriktFormView::setLanguage(void)
{
	CString csBuf=_T("");
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			//m_wndLbl1.SetWindowText(xml->str(IDS_STRING7503)+" :")));
			csBuf.Format(_T("%s%s"),xml->str(IDS_STRING7503),_T(" :"));
			m_wndLbl1.SetWindowText(csBuf);
			//m_wndLbl2.SetWindowText(xml->str(IDS_STRING7504)+" :")));
			csBuf.Format(_T("%s%s"),xml->str(IDS_STRING7504),_T(" :"));
			m_wndLbl2.SetWindowText(csBuf);
			
			m_sErrCap = xml->str(IDS_STRING7574);
			m_sSaveData.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7655),
									xml->str(IDS_STRING7656));
			m_sNotOkToSave.Format(_T("%s\n\n%s\n\n%s"),
									xml->str(IDS_STRING7657),
									xml->str(IDS_STRING7647),
									xml->str(IDS_STRING7648));
			m_sUpdateDistriktMsg.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING7658),
									xml->str(IDS_STRING7659));

		}
		delete xml;
	}

}

void CMDIScaGBDistriktFormView::populateData(UINT idx)
{
	DISTRIKT_DATA data;
	if (!m_vecDistriktData.empty() && 
		  idx >= 0 && 
			idx < m_vecDistriktData.size())
	{
		data = m_vecDistriktData[idx];
		m_structDistriktIdentifer.m_nDistriktID=data.m_nDistriktID;
		m_structDistriktIdentifer.m_sDistriktName=data.m_sDistriktName;
		m_wndEdit1.setInt(data.m_nDistriktID);
		m_wndEdit2.SetWindowText(data.m_sDistriktName);

		CString tst=_T(""),tst2=_T(""),tst3=_T("");
		RLFReader *xml = new RLFReader;
		CMDIScaGBDistriktFrame *pView = (CMDIScaGBDistriktFrame *)getFormViewByID(IDD_FORMVIEW4)->GetParent();
		if (xml->Load(m_sLangFN))
			tst3=xml->str(IDS_STRING7502);

		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),idx+1,m_vecDistriktData.size());
				tst.Format(_T("%s%s"),tst3,tst2);
				pDoc->SetTitle(tst);		
			}
		}
	}
	else
	{
		m_wndEdit1.SetWindowText(_T(""));
		m_wndEdit2.SetWindowText(_T(""));

		CString tst=_T(""),tst2=_T(""),tst3=_T("");
		RLFReader *xml = new RLFReader;
		CMDIScaGBDistriktFrame *pView = (CMDIScaGBDistriktFrame *)getFormViewByID(IDD_FORMVIEW4)->GetParent();
		if (xml->Load(m_sLangFN))
			tst3=xml->str(IDS_STRING7502);

		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),0,0);
				tst.Format(_T("%s%s"),tst3,tst2);
				pDoc->SetTitle(tst);		
			}
		}
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDIScaGBDistriktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			addDistrikt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveDistrikt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteDistrikt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (!isDataChanged())
			{
				if(m_vecDistriktData.size()==0)
				{
					m_nDBIndex=0;
					setNavigationButtons(FALSE,FALSE);
					populateData(-1);
				}
				else
				{
					m_nDBIndex = 0;
					setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));
					//setNavigationButtons(FALSE,TRUE);
					populateData(m_nDBIndex);
				}
			}
				break;
		}
		case ID_DBNAVIG_PREV :
		{
			if (!isDataChanged())
			{

				if(m_vecDistriktData.size()==0)
				{
					m_nDBIndex=0;
					setNavigationButtons(FALSE,FALSE);
					populateData(-1);
				}
				else
				{
					m_nDBIndex--;
					if (m_nDBIndex < 0)
						m_nDBIndex = 0;

					setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));
					/*if (m_nDBIndex == 0)
					{
					setNavigationButtons(FALSE,TRUE);
					}
					else
					{
					setNavigationButtons(TRUE,TRUE);
					}*/

					populateData(m_nDBIndex);
				}
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			if (!isDataChanged())
			{
				if(m_vecDistriktData.size()==0)
				{
					m_nDBIndex=0;
					setNavigationButtons(FALSE,FALSE);
					populateData(-1);
				}
				else
				{
					m_nDBIndex++;
					if (m_nDBIndex > ((UINT)m_vecDistriktData.size() - 1))
						m_nDBIndex = (UINT)m_vecDistriktData.size() - 1;

					setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));

					/*if (m_nDBIndex == (UINT)m_vecDistriktData.size() - 1)
					{
					setNavigationButtons(TRUE,FALSE);
					}
					else
					{
					setNavigationButtons(TRUE,TRUE);
					}*/


					populateData(m_nDBIndex);
				}
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			if (!isDataChanged())
			{
				if(m_vecDistriktData.size()==0)
				{
					m_nDBIndex=0;
					setNavigationButtons(FALSE,FALSE);
					populateData(-1);
				}
				else
				{
				m_nDBIndex = (UINT)m_vecDistriktData.size()-1;
				setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));
				//setNavigationButtons(TRUE,FALSE);
				populateData(m_nDBIndex);
				}
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

BOOL CMDIScaGBDistriktFormView::isDataChanged(void)
{
	if (m_bIsDirty)
	{
		if (::MessageBox(0,m_sSaveData,m_sErrCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		{
			saveDistrikt();
			m_bIsDirty=FALSE;
		   return TRUE;
		}
		else
			m_bIsDirty=FALSE;
	}
	return FALSE;
}

void CMDIScaGBDistriktFormView::addDistrikt()
{
	
	m_wndEdit1.SetReadOnly( FALSE );
	m_wndEdit2.SetReadOnly( FALSE );
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_bIsDirty=TRUE;

}

void CMDIScaGBDistriktFormView::getEnteredData(DISTRIKT_DATA &fData)
{
	fData.m_sDistriktName=m_wndEdit2.getText();
	fData.m_nDistriktID=m_wndEdit1.getInt();
	/*CString tst;
	tst.Format("Num: %d Namn: %s",m_wndEdit1.getInt(),m_wndEdit2.getText());
	AfxMessageBox(tst);*/
}
void CMDIScaGBDistriktFormView::saveDistrikt()
{
	DISTRIKT_DATA fData=DISTRIKT_DATA();
	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			getEnteredData(fData);
			// Setup index information; 061207 p�d
			

			if (!pDB->addDistrikt( fData ))
			{
				if ((::MessageBox(0,m_sUpdateDistriktMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
				{
						pDB->updDistrikt( fData );
						m_structDistriktIdentifer.m_nDistriktID=fData.m_nDistriktID;
						m_structDistriktIdentifer.m_sDistriktName=fData.m_sDistriktName;
				}
			}
			else
			{
			   m_structDistriktIdentifer.m_nDistriktID=fData.m_nDistriktID;
				m_structDistriktIdentifer.m_sDistriktName=fData.m_sDistriktName;
			}
			delete pDB;
		}	// if (pDB != NULL)		
		m_bIsDirty=FALSE;		

	}	// if (isOKToSave())
	resetDistrikt(RESET_TO_JUST_ENTERED_SET_NB);

}

void CMDIScaGBDistriktFormView::resetDistrikt(enumRESET reset)
{
	// Get updated data from Database
	getDistriktsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecDistriktData.size() > 0)
	{
		/*
		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}*/
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecDistriktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{
			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecDistriktData.size();i++)
			{
				DISTRIKT_DATA data = m_vecDistriktData[i];
				if (data.m_nDistriktID == m_structDistriktIdentifer.m_nDistriktID &&
					 data.m_sDistriktName == m_structDistriktIdentifer.m_sDistriktName)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistriktData.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
	m_wndEdit1.SetReadOnly( TRUE );
	m_wndEdit2.SetReadOnly( TRUE );
}

BOOL CMDIScaGBDistriktFormView::isOKToSave(void)
{
	CString tst=m_wndEdit2.getText();
	if (	tst==_T("") || m_wndEdit1.getInt() <= 0 )
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}
	return TRUE;
}

void CMDIScaGBDistriktFormView::deleteDistrikt()
{
	DISTRIKT_DATA dataDistrikt;
	CString sMsg=_T("");
	CString sText1;	//	Ta bort trakt
	CString sText2;	// Skall denna trakt tas bort ... ?
	CString sNr,sName,sT1,sT2;

	CString sCaption;	// Meddelande
	CString sOKBtn;	// Ta bort
	CString sCancelBtn;// Avbryt
	BOOL bDidDel=FALSE;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING7660);
			sText2 = xml->str(IDS_STRING7661);
			sCaption = xml->str(IDS_STRING7574);
			sOKBtn = xml->str(IDS_STRING7575);
			sCancelBtn = xml->str(IDS_STRING7576);
			sT1 = xml->str(IDS_STRING7503);
			sT2 = xml->str(IDS_STRING7504);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecDistriktData.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					dataDistrikt = m_vecDistriktData[m_nDBIndex];
					// Setup a message for user upon deleting 
					sName=dataDistrikt.m_sDistriktName;
					sNr.Format(_T("%d"),dataDistrikt.m_nDistriktID);
															
					sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%s</b><br>%s : <b>%s</b><br><hr><font size=\"+4\" color=\"#ff0000\"><br><br><center>%s</center></font>"),
						sText1,
						sT1,sNr,
						sT2,sName,
						sText2);


					/*
					sMsg.Format("%s","<font size=\"+4\"><center><b>");
					tmp.Format("%s</b></center></font><br><hr><br>",sText1);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT1,dataTrakt.m_nTrakt_forvalt_num);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT2,dataTrakt.m_nTrakt_id_typ);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT3,dataTrakt.m_nTrakt_id);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT4,dataTrakt.m_nTrakt_del_id);sMsg+=tmp;
					tmp.Format("%s : <b>%s</b><br>",sT5,dataTrakt.m_sTrakt_name);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT6,dataTrakt.m_nTrakt_ursprung);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT7,dataTrakt.m_nTrakt_inv_typ);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT8,dataTrakt.m_nTrakt_distrikt_num);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT9,dataTrakt.m_nTrakt_prod_ledare);sMsg+=tmp;
					tmp.Format("%s : <b>%s</b><br>",sT10,dataTrakt.m_sTrakt_alt_id);sMsg+=tmp;
					tmp.Format("%s : <b>%s</b><br>",sT11,dataTrakt.m_sTrakt_date);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br>",sT12,dataTrakt.m_nTrakt_maskinlag);sMsg+=tmp;
					tmp.Format("%s : <b>%10.3f</b><br>",sT13,dataTrakt.m_fTrakt_areal);sMsg+=tmp;
					tmp.Format("%s : <b>%d</b><br><hr><br>",sT14,dataTrakt.m_nTrakt_ant_ytor);sMsg+=tmp;
					tmp.Format("%s<br>",sText2);sMsg+=tmp;
					tmp.Format("%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>",sText3);sMsg+=tmp;
					tmp.Format("%s</center></font>",sText4);sMsg+=tmp;*/
					
					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delDistrikt(dataDistrikt);
						bDidDel=TRUE;
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	if(bDidDel==TRUE)
		resetDistrikt(RESET_TO_LAST_SET_NB);
	else
		resetDistrikt(RESET_TO_JUST_ENTERED_SET_NB);
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIScaGBDistriktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC
void CMDIScaGBDistriktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0, m_nDBIndex < (m_vecDistriktData.size()-1));
}
