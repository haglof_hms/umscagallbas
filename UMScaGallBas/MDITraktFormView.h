#pragma once


// CMDITraktFormView dialog

class CMDITraktFormView : public CXTResizeFormView
{
	DECLARE_DYNAMIC(CMDITraktFormView)

	CXTPTabControl m_wndTabControl;

protected:
	CMDITraktFormView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMDITraktFormView();

	CMyExtStatic			m_wndLbl1;
	CMyExtStatic			m_wndLbl2;

	CMyComboBox				m_wndCBox1;
	CMyComboBox				m_wndCBox2;

	vecForvaltData			m_vecRegionData;
	vecDistrictData		m_vecDistrictData;

	TRAKT_DATA				m_recActiveTrakt;

	enumACTION				m_enumAction;

	UINT						m_nDBIndex;

	BOOL						m_bConnected;
	DB_CONNECTION_DATA	m_dbConnectionData;

	void		setLanguage(void);

	void		getRegionsFromDB(void);
	void		getDistrictFromDB(void);

	void		populateData(UINT);

	void		addRegionsToCBox(void);
	CString	getRegionSelected(int region_num);
	int		getRegionID(void);
	void		addDistrictsToCBox(void);
	CString	getDistrictSelected(int region_num,int district_num);
	int		getDistrictID(void);

public:
// Dialog Data
	enum { IDD = IDD_FORMVIEW2 };

	public:
	virtual void OnInitialUpdate();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	DECLARE_MESSAGE_MAP()
};
