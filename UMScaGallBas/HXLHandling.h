#pragma once
#include "StdAfx.h"
#include <locale.h>


#define MAX_NUM_OF_TYPES	6
#define NUM_OF_SKAD_DATA	16

const LPCTSTR HXL_FILE_FILTER			=_T("Hagl�f Filer (*.hxl)|*.hxl|Alla Filer (*.*)|*.*||");
//const LPCTSTR HXL_DLL_PATH			= _T("C:\\Documents and Settings\\jonorn-6.HAGLOF\\Mina dokument\\Visual Studio 2005\\Projects\\XMLScaReader\\release\\XMLScaReader.dll");

const LPCSTR HXL_FUNC_OPEN			= "HXL_FileOpen";
typedef int (*HXL_PROC1)(char *); 

/*const LPCTSTR HXL_FUNC_GETDATA		=_T("HXL_GetData");
typedef int (*HXL_PROC2)(int _standnr, int _specie, int _type, int _var, float *ret); */

const LPCSTR HXL_FUNC_GETHEADDATA		= "HXL_GetHeadData";
typedef int (*HXL_PROC3)(int _standnr, int _var,char *ret); 

const LPCSTR HXL_FUNC_GETPLOTDATA		= "HXL_GetPlotData";
typedef int (*HXL_PROC4)(int _standnr, int _pi,int _var,int *_ret); 

/*const LPCTSTR HXL_FUNC_GETHURTDATA		=_T("HXL_GetHurtData");
typedef int (*HXL_PROC5)(int _standnr,int _var,float *_ret); */

const LPCSTR HXL_FUNC_GETTREEDATA		= "HXL_GetTreeData";
typedef int (*HXL_PROC6)(int _standnr,int _nTreeIndex,int _var,int *_ret); 

const LPCSTR HXL_FUNC_GETCOORDDATA		= "HXL_GetCoordData";
typedef CRuntimeClass * (*HXL_PROC7)(int _standnr,CStringA &ret); 


int HXL_Open(DB_CONNECTION_DATA con,LPCTSTR lang_fn,CWnd *wnd,TRAKT_DATA *t);
int HXL_FileOpen(TCHAR *filename);
//int HXL_GetData(int _standnr, int _specie, int _type, int _var, float *ret);
int HXL_GetHeadData(int _standnr,int _var, TCHAR *_ret);
int HXL_GetCoordData(int _standnr,CString &_ret);
int HXL_GetPlotData(int _standnr,int _pi,int _var,int *_ret);
int HXL_GetTreeData(int _standnr,int _nTreeIndex,int _var,int *_ret);
//int HXL_GetHurtData(int _standnr,int _var,float *ret);
int HXL_MakeData(TRAKT_DATA *t);
CString FixLocale(CString csSource);