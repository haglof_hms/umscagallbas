#pragma once
#include "StdAfx.h"
#include "License.h"
#include "ResLangFileReader.h"
#include "pad_hms_miscfunc.h"
#include "UMHmsMisc.h"
#include "UMScaGallBas.h"



BOOL License(void)
{
	// Extract filename of this module
	TCHAR szBuf[MAX_PATH], szModule[MAX_PATH];
	CString tst;
	GetModuleFileName(g_hInstance, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szModule, NULL);

	WriteToLog(_T("Kollar licens"),false);
	// Check if we have a valid license
	_user_msg msg(820, LICENSE_CHECK, LICENSE_FILE_NAME, UMSCAGALLBAS_LIC_ID, PROGRAM_NAME, (bShowLicenseDialog ? _T("1") : _T("0")), szModule);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	if (bShowLicenseDialog)
	{
		bShowLicenseDialog = FALSE;
	}

	// Check if License dll is missing
	if(_tcscmp(msg.getFunc(), _T("CheckLicense")) == 0)
	{
		CString sHmsShellLangFN=_T("");
		sHmsShellLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),"HMSShell",getLangSet(),LANGUAGE_FN_EXT);
		if (fileExists(sHmsShellLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(sHmsShellLangFN))
			{
				// Trakt information
				AfxMessageBox(xml->str(14011));
			}
			delete xml;
		}
		else
		{

			AfxMessageBox(_T("License.dll saknas!"));
		}
		WriteToLog(_T("Licensen funkar inte!"),true);
		return FALSE;
	}
	else
	{
		tst=msg.getFunc();
		WriteToLog(_T("Licensretur "+tst),false);
		return (_tcscmp(tst, _T("1")) == 0);
	}
}

