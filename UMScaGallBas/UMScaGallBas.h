#if !defined(AFX_UMSCAGALLBAS_H)
#define AFX_UMSCAGALLBAS_H

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

extern HINSTANCE g_hInstance;

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" AFX_EXT_API void DoAlterTables(LPCTSTR dbname);

BOOL check_if_tables_exists();

void WriteToLog(CString message, bool error/*=false*/);

//#define _DEBUG

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

#endif